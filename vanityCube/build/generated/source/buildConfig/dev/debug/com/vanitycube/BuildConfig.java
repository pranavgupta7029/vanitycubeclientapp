/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.vanitycube;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.vanitycube";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "dev";
  public static final int VERSION_CODE = 50;
  public static final String VERSION_NAME = "1.926";
}
