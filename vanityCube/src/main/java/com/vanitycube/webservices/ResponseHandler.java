package com.vanitycube.webservices;


import com.vanitycube.model.CommonResponse;

/**
 * Created on 25-Mar-17.
 */

public class ResponseHandler {

    public static void handleResponse(Object responseObj, ResponseCode responseCode) {
        CommonResponse topicResponse = (CommonResponse) responseObj;
        if (topicResponse.isSuccess() == 1) {
            responseCode.preformCode();
        } else {
            ToastHelper.toast("Something went wrong");
        }
    }

    public abstract static class ResponseCode {
        public abstract void preformCode();
    }

}
