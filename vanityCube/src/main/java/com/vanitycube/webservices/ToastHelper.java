package com.vanitycube.webservices;

import android.widget.Toast;

import com.vanitycube.constants.VcApplicationContext;


/**
 * Created on 24-Mar-17.
 */

public class ToastHelper {
    public static void toast(String message) {
        Toast.makeText(VcApplicationContext.getInstance(), message, Toast.LENGTH_LONG).show();
    }
}
