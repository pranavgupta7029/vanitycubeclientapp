package com.vanitycube.webservices;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by milin on 11-Nov-15.
 */
public class ApiManager {

    private SQLiteDatabase sqldb;
    private Context context;
    private ServerResponseListener serverResponseListener;
    private Boolean jsonParsing;
    private Type classTypeForJson;
    private HashMap<String, String> headerParams;


    public ApiManager(Context context, ServerResponseListener serverResponseListener) {
        this.context = context;
        this.serverResponseListener = serverResponseListener;
    }

    private void performJsonParsing(String TAG, String response, Type type) {
        jsonParsing = false;
        classTypeForJson = null;
        if (response != null && response.equalsIgnoreCase("null")) {
            response = null;
        }

        if (response != null) {
            Gson gson = new GsonBuilder().create();

            try {
                Object responseObject = gson.fromJson(response, type);
                serverResponseListener.positiveResponse(TAG, responseObject);
            } catch (JsonSyntaxException jse) {
                jse.printStackTrace();
                FirebaseCrash.report(new Exception(jse.getMessage()));

                String errorString;
                errorString = "error For Developer: Some Issue in Json Syntax. Unable to parse it";
                Log.d("----err jsonString:", response);
                Log.d("----err json:", jse.getMessage());

                serverResponseListener.negativeResponse(TAG, errorString);
            } catch (Exception e) {
                e.printStackTrace();

                //FirebaseCrash.report(new Exception(e.getMessage()));

                String errorString;
                errorString = "error For Developer: Some general exception in creating objects from returned json";
                Log.d("----err jsonString:", response);
                Log.d("----err json:", e.getMessage());

                serverResponseListener.negativeResponse(TAG, errorString);
            }
        } else {
            String errorString;
            errorString = "error CommonResponse = null";
            serverResponseListener.negativeResponse(TAG, errorString);
        }
    }

    public void getStringPostResponse(String TAG, String Url, HashMap<String, String> arr) {
        final Post post = new Post(TAG, Url, arr);

        if (!isNetworkConnected()) {
            Toast.makeText(context, "No Internet Access", Toast.LENGTH_LONG).show();
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                post.execute();
            }
        });
        thread.start();
    }

    public void setSqldb(SQLiteDatabase sqldb) {
        this.sqldb = sqldb;
    }


    private class Post extends AsyncTask<Void, Void, String> {

        private String TAG;
        String Url;
        int errorFlag = 0;

        HashMap<String, String> params;

        Post(String TAG, String Url) {
            this.TAG = TAG;
            this.Url = Url;
        }

        Post(String TAG, String Url, HashMap<String, String> arr) {
            this.Url = Url;
            this.params = arr;
            this.TAG = TAG;
        }

        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);

            if (errorFlag == 0) {
                if (jsonParsing != null && jsonParsing)
                    performJsonParsing(TAG, result, classTypeForJson);
                else
                    serverResponseListener.positiveResponse(TAG, result);
            } else {
                serverResponseListener.negativeResponse(TAG, result);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            StringBuilder responseStr = new StringBuilder();
            HashMap<String, String> nameValuePair = new HashMap<>();
            nameValuePair.put("", "");

            URL url;
            try {
                url = new URL(Url);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (headerParams != null) {
                    applyHeaders(urlConnection);
                }
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                OutputStream sendingStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(sendingStream, "UTF-8"));

                if (this.params == null) {
                    writer.write(getQuery(nameValuePair));
                } else {
                    writer.write(getQuery(this.params));
                }

                writer.flush();
                writer.close();
                sendingStream.close();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

                    while ((line = br.readLine()) != null) {
                        responseStr.append(line);
                    }
                } else {
                    responseStr = new StringBuilder("Reponse Code : " + responseCode + "(" + urlConnection.getResponseMessage() + ")");
                    errorFlag = 1;
                }

            } catch (IOException e) {
                e.printStackTrace();
                FirebaseCrash.report(new Exception(e.getMessage()));
                errorFlag = 1;
                responseStr = new StringBuilder(e.getMessage());
            }

            headerParams = null;
            return filter(responseStr.toString());
        }
    }


    private void initiate(String tableName) {
        String Query = "PRAGMA table_info(" + tableName + ")";
        Cursor my_cursor = sqldb.rawQuery(Query, null);

        int pos = 0;

        StringBuilder Q = new StringBuilder("insert into " + tableName + " values (");

        for (int i = 0; i < my_cursor.getCount(); i++) {
            my_cursor.moveToPosition(i);

            if (i != 0) {
                Q.append(",");

            }

            String type = my_cursor.getString(my_cursor.getColumnIndex("type"));

            if (type.contains("char")) {
                Q.append("''");

            } else if (type.contains("date")) {
                Q.append("'0-0-0'");

            } else
                Q.append("0");

        }

        Q.append(");");

        sqldb.execSQL(Q.toString());

    }

    private class PostFile extends AsyncTask<Void, Void, String> {

        private HashMap<String, File> fileParams;
        String Url;
        String TAG;
        private int errorFlag = 0;
        HashMap<String, String> params;

        PostFile(String TAG, String url, HashMap<String, File> fileParams, HashMap<String, String> params) {
            this.Url = url;
            this.TAG = TAG;
            this.params = params;
            this.fileParams = fileParams;

            if (fileParams == null) {
                Toast.makeText(context, "NO files to send", Toast.LENGTH_LONG).show();
                this.cancel(true);
            }
        }

        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("----post result", result);

            if (errorFlag == 0) {
                Log.d("----Call : ", "positive");

                if (jsonParsing != null && jsonParsing) {
                    performJsonParsing(TAG, result, classTypeForJson);
                } else {
                    serverResponseListener.positiveResponse(TAG, result);
                }
            } else {
                Log.d("----Call : ", "negative");
                serverResponseListener.negativeResponse(TAG, result);
            }

        }

        @Override
        protected String doInBackground(Void... params) {
            String boundary = "===" + System.currentTimeMillis() + "===";
            String lineEnd = "\r\n";
            String twoHyphens = "--";

            HashMap<String, String> nameValuePair = new HashMap<>(2);
            nameValuePair.put("", "");

            String contentDisposition = "Content-Disposition: form-data;";
            String contentTypeFile = "Content-Type: application/octet-stream";
            StringBuilder responseStr = new StringBuilder();

            URL url;
            try {
                url = new URL(Url);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                if (headerParams != null) applyHeaders(urlConnection);

                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                urlConnection.setRequestProperty("uploaded_file", "file");

                if (fileParams.size() == 0) {
                    errorFlag = 1;
                    return "";
                }

                DataOutputStream writer = new DataOutputStream(urlConnection.getOutputStream());
                writer.flush();

                for (Map.Entry<String, File> fileParam : fileParams.entrySet()) {
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(twoHyphens + boundary + lineEnd);
                    writer.writeBytes(contentDisposition);
                    writer.writeBytes(String.format("name=%s;", fileParam.getKey()));
                    writer.writeBytes(String.format("filename='%s';", fileParam.getValue().getName()));
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(contentTypeFile);
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(lineEnd);
                    writer.write(fullyReadFileToBytes(fileParam.getValue()));
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(twoHyphens + boundary);
                }

                for (Map.Entry<String, String> param : this.params.entrySet()) {
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(contentDisposition);
                    writer.writeBytes(String.format("name=%s", param.getKey()));
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(param.getValue());
                    writer.writeBytes(lineEnd);
                    writer.writeBytes(twoHyphens + boundary);
                }
                writer.writeBytes(twoHyphens + lineEnd);
                writer.close();

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        responseStr.append(line);
                    }
                } else {
                    responseStr = new StringBuilder("Reponse Code : " + responseCode + "(" + urlConnection.getResponseMessage() + ")");
                    errorFlag = 1;
                }

            } catch (Exception e) {
                e.printStackTrace();
                FirebaseCrash.report(new Exception(e.getMessage()));
                errorFlag = 1;
                responseStr = new StringBuilder(e.getMessage());
            }

            headerParams = null;
            return filter(responseStr.toString());

        }
    }

    private class GetFile extends AsyncTask<Void, Void, String> {

        String Url;
        String TAG;
        File saveFile;
        String saveFilePath;
        String saveFileName;
        HashMap<String, String> params;
        private int errorFlag = 0;

        public GetFile(String TAG, String Url, HashMap<String, String> params, String saveFilePath, String saveFileName) {
            this.Url = Url;
            this.TAG = TAG;
            this.params = params;
            this.saveFilePath = saveFilePath;
            this.saveFileName = saveFileName;

            if (this.params == null)
                Log.d("----", "empty params");


            if (saveFilePath == null || saveFilePath.length() == 0 || saveFileName == null || saveFileName.length() == 0)
                Toast.makeText(context, "Field can't left be null", Toast.LENGTH_LONG).show();
            else
                this.saveFile = new File(this.saveFilePath, saveFileName);

        }

        @Override
        public void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if (errorFlag == 0) {
                Log.d("----Call : ", "positive");
                if (jsonParsing != null && jsonParsing)
                    performJsonParsing(TAG, result, classTypeForJson);
                else
                    serverResponseListener.positiveResponse(TAG, result);
            } else {
                Log.d("----Call : ", "negative");
                serverResponseListener.negativeResponse(TAG, result);
            }

        }

        @Override
        protected String doInBackground(Void... params) {
            String responseStr = "";

            HashMap<String, String> nameValuePair = new HashMap<>();
            nameValuePair.put("", "");

            URL url;
            try {
                url = new URL(Url);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                if (headerParams != null) {
                    applyHeaders(urlConnection);
                }
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);


                OutputStream sendingStream = urlConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(sendingStream, "UTF-8"));

                if (this.params == null)
                    writer.write(getQuery(nameValuePair));
                else
                    writer.write(getQuery(this.params));

                writer.flush();
                writer.close();
                sendingStream.close();

                if (urlConnection.getHeaderField("Content-Type").toString().trim().equals("text/html")) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        responseStr += line;
                    }
                    return responseStr;
                }
                int responseCode = urlConnection.getResponseCode();
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    InputStream in = urlConnection.getInputStream();
                    Log.d("----1", saveFilePath);
                    Log.d("----2", String.valueOf(new File(saveFilePath).mkdir()));
                    Log.d("----3", String.valueOf(saveFile.createNewFile()));
                    OutputStream fileOutput = new FileOutputStream(saveFile);
                    BufferedOutputStream bufferedFileOutput = new BufferedOutputStream(fileOutput);
                    byte[] buf = new byte[1024];
                    int len;
                    int progressLen = 0;
                    long totalLen = urlConnection.getContentLength();
                    while ((len = in.read(buf)) > 0) {
                        progressLen += len;
                        Log.d("----download progress", String.valueOf(progressLen * 100 / totalLen) + "%");

                        bufferedFileOutput.write(buf, 0, len);
                    }
                    bufferedFileOutput.flush();
                    bufferedFileOutput.close();
                    in.close();
                    responseStr = "Download Completed";
                } else {
                    responseStr = "CommonResponse Code : " + responseCode + "(" + urlConnection.getResponseMessage() + ")";
                    errorFlag = 1;
                }

            } catch (IOException e) {
                e.printStackTrace();
                errorFlag = 1;
                FirebaseCrash.report(new Exception(e.getMessage()));
                responseStr = e.getMessage();
            }
            headerParams = null;
            return filter(responseStr);
        }
    }

    public void getStringGetResponse(String TAG, String Url) {
        final Get get = new Get(TAG, Url);

        if (!isNetworkConnected()) {
            Toast.makeText(context, "No Internet Access", Toast.LENGTH_LONG).show();
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                get.execute();
            }
        });
        thread.start();
    }

    public void getStringGetResponse(String TAG, String Url, HashMap<String, String> params) {
        final Get get = new Get(TAG, Url, params);

        if (!isNetworkConnected()) {
            Toast.makeText(context, "No Internet Access", Toast.LENGTH_LONG).show();
            return;
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                get.execute();
            }
        });
        thread.start();
    }

    private class Get extends AsyncTask<Void, Void, String> {

        private String TAG;
        String Url;
        int errorFlag = 0;

        HashMap<String, String> params;

        Get(String TAG, String Url) {
            this.TAG = TAG;
            this.Url = Url;
            params = null;
        }

        Get(String TAG, String Url, HashMap<String, String> params) {
            this.Url = Url;
            this.params = params;
            this.TAG = TAG;
        }

        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("----get result", result);

            if (errorFlag == 0) {
                Log.d("----Call : ", "positive");
                if (jsonParsing != null && jsonParsing)
                    performJsonParsing(TAG, result, classTypeForJson);
                else
                    serverResponseListener.positiveResponse(TAG, result);
            } else {
                Log.d("----Call : ", "negative");
                serverResponseListener.negativeResponse(TAG, result);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            StringBuilder responseStr = new StringBuilder();
            URL url;

            try {
                if (this.params != null) {
                    Url += "?" + urlEncodeUTF8(this.params);
                }

                url = new URL(Url);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                if (headerParams != null) applyHeaders(urlConnection);

                urlConnection.setRequestMethod("GET");

                int responseCode = urlConnection.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        responseStr.append(line);
                    }
                } else {
                    responseStr = new StringBuilder("Reponse Code : " + responseCode + "(" + urlConnection.getResponseMessage() + ")");
                    errorFlag = 1;
                }

            } catch (IOException e) {
                e.printStackTrace();
                FirebaseCrash.report(new Exception(e.getMessage()));
                errorFlag = 1;
                responseStr = new StringBuilder(e.getMessage());
            }

            Log.d("----get response", responseStr.toString());
            headerParams = null;
            return filter(responseStr.toString());
        }
    }


    private String filter(String response) {
        response = response.split("<!-- Hosting24 Analytics Code -->")[0];
        response = response.replace("<html>", "");
        response = response.replace("</html>", "");
        response = response.trim();

        return response;
    }

    private String getQuery(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> pair : params.entrySet()) {
            if (first) first = false;
            else result.append("&");

            result.append(URLEncoder.encode(pair.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    private static String urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));
        }

        return sb.toString();
    }

    private byte[] fullyReadFileToBytes(File f) throws IOException {
        int size = (int) f.length();
        byte bytes[] = new byte[size];
        byte tmpBuff[] = new byte[size];

        try (FileInputStream fis = new FileInputStream(f)) {

            int read = fis.read(bytes, 0, size);
            if (read < size) {
                int remain = size - read;
                while (remain > 0) {
                    read = fis.read(tmpBuff, 0, remain);
                    System.arraycopy(tmpBuff, 0, bytes, size - remain, read);
                    remain -= read;
                }
            }
        }

        return bytes;
    }

    private void applyHeaders(HttpURLConnection urlConnection) {
        for (Map.Entry<String, String> param : headerParams.entrySet()) {
            urlConnection.setRequestProperty(param.getKey(), param.getValue());
        }
    }

    public void doJsonParsing() {
        this.jsonParsing = true;
    }

    public void doJsonParsing(boolean flag) {
        this.jsonParsing = flag;
    }

    public void setClassTypeForJson(Type classTypeForJson) {
        this.classTypeForJson = classTypeForJson;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
