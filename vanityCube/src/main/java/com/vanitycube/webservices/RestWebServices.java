package com.vanitycube.webservices;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.database.VcTableList;
import com.vanitycube.dbmodel.AreaModel;
import com.vanitycube.dbmodel.CityModel;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.AddNewUserResponseModel;
import com.vanitycube.model.Banner;
import com.vanitycube.model.BookingListModel;
import com.vanitycube.model.BookingSummaryDetailModel;
import com.vanitycube.model.CancelBookingResponseModel;
import com.vanitycube.model.CheckOTPResponseModel;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.ConfirmOrderModel;
import com.vanitycube.model.CouponModel;
import com.vanitycube.model.ExpectedDateTimeModel;
import com.vanitycube.model.GetUserStateResponseModel;
import com.vanitycube.model.LoginResponseModel;
import com.vanitycube.model.MultipleBookingResponseModel;
import com.vanitycube.model.Offer;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.StandardResponseModelBooking;
import com.vanitycube.model.TimeSlotModel;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.settings.ApplicationSettings;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class RestWebServices {

    private static final String TAG = "RestWebServices";
    private final int SUCCESS_RESPONSE = 200;
    private static final String URL = ApplicationSettings.APPLICATION_SERVICE_URL;
    private AlertDialog mAlertDialog;

    private static final String METHOD_GET_CONFIGURATION = "getConfiguration&appType=android";
    private static final String METHOD_LOGIN = "isLogin";
    private static final String METHOD_LOGIN_GOOGLE = "isLoginGoogle";
    private static final String METHOD_GET_USER_STATE = "getUserState";
    private static final String METHOD_ADD_GUEST_USER = "addGuestUser";
    private static final String METHOD_SIGNUP = "addNewUser";
    private static final String METHOD_SIGNUP_GOOGLE = "addNewUserGoogle";
    private static final String METHOD_GET_ALL_CITIES = "getAllCities";
    private static final String METHOD_ADD_ADDRESS = "addAddress";
    private static final String METHOD_GET_ALL_ACTIVE_ADDRESSES = "getAllActiveAddress";
    private static final String METHOD_GET_DISCOUNT_DETAILS = "getUserDiscountDetails";
    private static final String METHOD_GET_EXPECTED_DATE_TIME_SLOTS = ApplicationSettings.METHOD_GET_EXPECTED_DATE_TIME_SLOTS;
    private static final String METHOD_GET_TIME_SLOTS_BY_DATE = ApplicationSettings.METHOD_GET_TIME_SLOTS_BY_DATE;
    private static final String METHOD_GET_ALL_SERVICE_WITH_SERVICE_TYPES = "getAllServicesWithServiceTypes";
    private static final String METHOD_CHANGE_PASSWORD = "changePassword";
    private static final String METHOD_FORGOT_PASSWORD = "forgotPassword";
    private static final String METHOD_UPDATE_USER = "updateUser";
    private static final String METHOD_ADD_MULTIPLE_BOOKING = "addMultipleCategoryBookings";
    private static final String METHOD_CANCEL_BOOKING = "cancelBooking";
    private static final String METHOD_GET_ACTIVE_BOOKING = "getActiveBookingsByUserId";
    private static final String METHOD_GET_HISTORY_BOOKING = "getHistoryBookingsByUserId";
    private static final String METHOD_CHECK_OTP = "checkOtp";
    private static final String METHOD_RESEND_OTP = "resendOtp";
    private static final String METHOD_CHECK_EXISTING_EMAIL = "checkExistingEmail";
    private static final String METHOD_UPDATE_ADDRESS = "updateAddress";
    private static final String METHOD_DISABLE_ADDRESS = "disableAddress";
    private static final String METHOD_GET_ALL_AREAS = "getAllAreas";
    private static final String METHOD_IS_FEEDBACK = "isFeedBackGivenByUserId";
    private static final String METHOD_SUBMIT_FEEDBACK = "postBookingRating";
    private static final String METHOD_GET_OFFERS = "getOfferBanners";
    private static final String METHOD_GET_BANNERS = "getBanners";
    private static final String METHOD_CHECK_COUPONS = "checkCoupon";
    //private static final String METHOD_CONFIRM_BOOKING = "confirmOrder";
    private static final String METHOD_CONFIRM_BOOKING = "confirmBooking";
    public static final String METHOD_GET_AVAILABLE_POINTS = "getEasyRewardzAvailablePointsAction";
    public static final String METHOD_GET_AVAILABLE_REEDEMEBALE_POINTS = "getEasyRewardzPointsDetails";
    public static final String METHOD_BLOCK_LOYALTY_POINTS = "easyRewardzCheckRedemptionPointsAction";
    public static final String METHOD_RESEND_LOYALTY_POINTS = "easyRewardzResendOtpAction";
    public static final String METHOD_RELEASE_LOYALTY_POINTS = "easyRewardzReleaseRedemptionPointsAction";
    public static final String METHOD_SUBMITOTP_LOYALTY_POINTS = "easyRewardzConfirmOtpAction";
    public static final String METHOD_GET_LOYALTY_TRANSACTIONS = "easyRewardzGetTransactionsAction";
    private static final String METHOD_GET_LAST_ADDRESS = "getLastAddressesByUserId";
    public static final String METHOD_GET_SERVICES = "services";
    private static final String METHOD_GET_IS_MEMBER = "isMember";
    public static final String METHOD_SENDOTP_TO_USER = "otpRegister";
    public static final String METHOD_USER_LOGIN = "logInwithOtpOrPassword";
    public static final String METHOD_RESENDOTP_TO_USER = "resendOtp";
    public static final String METHOD_SIGNUP_NEW_USER = "addUserNew";
    public static final String METHOD_OTP_VERFICATION_NEW_USER = "otpVerificationUser";



    private static final String FILE_UPLOAD_URL = ApplicationSettings.BASE_URL + "upload.php";

    private SharedPref pref;
    private VcDatabaseQuery mDBQuery;
    private HttpResponse response;
    private HttpClient client;

    private String requestURL;
    private String statusCode;

    private JSONObject jsonObject, responseDataJson;
    private UserModel userModel;
    private String[] successMessageArray = {"0", "Unknown reason!"};
    private String[] successMessageArrayNew = {"0", "Unknown reason!", ""};

    private Gson gson;
    private String json;
    private String userId;

    private boolean isNormalUser, isGuestUser;
    private int billAmount;

    public RestWebServices() {
        pref = new SharedPref(VcApplicationContext.getInstance());
        mDBQuery = new VcDatabaseQuery();
        gson = new Gson();

        isGuestUser = false;
        isNormalUser = false;

        billAmount = 0;

        getUser();
    }

    public RestWebServices(Context pContext) {
        pref = new SharedPref(VcApplicationContext.getInstance());
        mDBQuery = new VcDatabaseQuery();
    }


    private void getUser() {
        json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            userId = userModel.getID();

            if (isValidString(userId)) {
                if (userModel.getGuest()) {
                    isGuestUser = true;
                } else {
                    isNormalUser = true;
                }
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    /*  -------------------------------------
        |
        |   HTTP HELPER FUNCTIONS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private HttpClient sslClient(HttpClient client) {
        try {
            X509TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    Log.e(TAG, "Running checkClientTrusted");
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                    Log.e(TAG, "Running checkServerTrusted");
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[]{tm}, null);
            ClientConnectionManager ccm = client.getConnectionManager();

            return new DefaultHttpClient(ccm, client.getParams());
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        mAlertDialog = new AlertDialog.Builder(context).create();
        // Setting Dialog Title
        mAlertDialog.setTitle(title);
        // Setting Dialog Message
        mAlertDialog.setMessage(message);
        if (status != null)
            // Setting alert dialog icon
            // TODO find the right icon for this
            mAlertDialog.setIcon((status) ? R.drawable.ic_launcher : R.drawable.ic_launcher);
        // Setting OK Button
        mAlertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });

        // Showing Alert Message
        mAlertDialog.show();
    }


    @SuppressWarnings("deprecation")
    private String getHttpResponseStructure(HttpResponse pHttpResponseObj) {
        String lResponseString = null;

        if (pHttpResponseObj.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
            Reader inputStreamReaderObj = null;

            try {
                inputStreamReaderObj = new BufferedReader(new InputStreamReader(pHttpResponseObj.getEntity().getContent(), "UTF-8"));
            } catch (IllegalStateException | IOException e) {
                e.printStackTrace();
            }

            StringBuilder httpResponseDataBuilder = new StringBuilder();
            char[] buf = new char[1000];
            int l = 0;

            while (l >= 0) {
                httpResponseDataBuilder.append(buf, 0, l);

                try {
                    assert inputStreamReaderObj != null;
                    l = inputStreamReaderObj.read(buf);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            lResponseString = httpResponseDataBuilder.toString();
        }

        return lResponseString;
    }

    private HttpResponse getHttpResponse(String requestURL) throws IOException {
        client = new DefaultHttpClient();
        client = sslClient(client);
        HttpGet httpGet = new HttpGet(requestURL);
        assert client != null;
        return client.execute(httpGet);
    }

    private JSONObject getJSONObjectFromResponse(HttpResponse response) throws JSONException {
        JSONObject jObj = null;
        try {
            String serverResponse = getHttpResponseStructure(response);
            JSONTokener tokener = new JSONTokener(serverResponse);
            jObj = new JSONObject(tokener);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return jObj;
    }

    private HttpResponse getHttpPostResponse(String requestURL, List<NameValuePair> list) throws IOException {
        client = new DefaultHttpClient();
        client = sslClient(client);
        HttpPost httpPost = new HttpPost(requestURL);
        httpPost.setEntity(new UrlEncodedFormEntity(list));
        assert client != null;
        return client.execute(httpPost);
    }

    private JSONObject getJSONObjectFromHttpPostResponse(HttpResponse response) throws JSONException {
        HttpEntity httpEntity = response.getEntity();
        String entityString;
        JSONObject jo = new JSONObject();

        if (httpEntity != null) {
            try {
                entityString = EntityUtils.toString(httpEntity);
                JSONTokener tokener = new JSONTokener(entityString);
                jo = new JSONObject(tokener);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return jo;
    }

    /*  -------------------------------------
        |
        |   LOGIN
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private LoginResponseModel parseLoginResponse(HttpResponse pResponse) {
        LoginResponseModel loginResponseModel = new LoginResponseModel();
        UserModel userModel = new UserModel();

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            loginResponseModel = new LoginResponseModel(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginResponseModel;
    }

    @SuppressWarnings("deprecation")
    public LoginResponseModel login(String pEmail, String pPassword, boolean isFacebook) {
        LoginResponseModel loginResponseModel = new LoginResponseModel();
        String facebook;

        if (isFacebook) {
            facebook = "1";
        } else {
            facebook = "0";
        }

        try {
            requestURL = URL + "?method=" + METHOD_LOGIN + "&contact=" + pEmail + "&password=" + pPassword + "&isFacebook=" + facebook + "&udeviceid="
                    + pref.getGCMToken() + "&udevicetype=" + "android";

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                loginResponseModel = parseLoginResponse(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginResponseModel;
    }

    /*  -------------------------------------
        |
        |   LOGIN USING GOOGLE
        |
        -------------------------------------
    */

    public LoginResponseModel loginGoogle(String pEmail, String pPassword) {
        LoginResponseModel loginResponseModel = new LoginResponseModel();

        requestURL = URL + "?method=" + METHOD_LOGIN_GOOGLE + "&contact=" + pEmail + "&password=" + pPassword + "&udeviceid=" +
                pref.getGCMToken() + "&udevicetype=" + "android";

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                loginResponseModel = parseLoginResponse(response);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return loginResponseModel;
    }

    /*  -------------------------------------
        |
        |   CREATE JSON ENVELOPE FOR SIGNUP API
        |
        -------------------------------------
    */

    private String createSignUpString(String pName, String pEmail, String pPassword, String pMobile, String pRefName,
                                      boolean isFacebook, String pGender) throws JSONException {

        jsonObject = new JSONObject();
        jsonObject.put("uname", pName);
        jsonObject.put("useremail", pEmail);
        jsonObject.put("password", pPassword);
        jsonObject.put("contact", pMobile);
        jsonObject.put("refname", pRefName);
        jsonObject.put("gender", pGender);
        jsonObject.put("udeviceid", pref.getGCMToken());
        jsonObject.put("udevicetype", "android");

        if (isFacebook) jsonObject.put("isFacebook", "1");
        else jsonObject.put("isFacebook", "0");

        return jsonObject.toString();
    }

    private String createSignUpStringGoogle(String pName, String pEmail, String pMobile, String pRefName, String pGender) throws JSONException {

        jsonObject = new JSONObject();
        jsonObject.put("uname", pName);
        jsonObject.put("useremail", pEmail);
        jsonObject.put("password", "");
        jsonObject.put("contact", pMobile);
        jsonObject.put("refname", pRefName);
        jsonObject.put("gender", pGender);
        jsonObject.put("udeviceid", pref.getGCMToken());
        jsonObject.put("udevicetype", "android");

        return jsonObject.toString();
    }

    /*  -------------------------------------
        |
        |   ADD NEW USER
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public AddNewUserResponseModel addNewUser(String pName, String pEmail, String pPassword, String pMobile, String pRefName, boolean isFacebook, String pGender)
            throws JSONException {

        AddNewUserResponseModel addNewUserResponseModel = new AddNewUserResponseModel();
        String lData = createSignUpString(pName, pEmail, pPassword, pMobile, pRefName, isFacebook, pGender);

        try {
            requestURL = URL + "?method=" + METHOD_SIGNUP + "&data=" + URLEncoder.encode(lData, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                addNewUserResponseModel = new AddNewUserResponseModel(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return addNewUserResponseModel;
    }

    /*  -------------------------------------
        |
        |   ADD NEW USER USING GOOGLE
        |
        -------------------------------------
    */

    public String[] addNewUserGoogle(String pName, String pEmail, String pMobile, String pRefName, String pGender) throws JSONException {

        String lData = createSignUpStringGoogle(pName, pEmail, pMobile, pRefName, pGender);
        requestURL = "";

        try {
            requestURL = URL + "?method=" + METHOD_SIGNUP_GOOGLE + "&data=" + URLEncoder.encode(lData, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {

                try {
                    jsonObject = getJSONObjectFromResponse(response);
                    responseDataJson = jsonObject.getJSONObject("responsedata");
                    statusCode = responseDataJson.getString("success");
                    successMessageArray[0] = responseDataJson.getString("success");

                    if (("1").equalsIgnoreCase(statusCode)) {
                        successMessageArray[1] = responseDataJson.optString("userid");

                        userModel = new UserModel();
                        userModel.setEmail(pEmail);
                        userModel.setID(responseDataJson.getString("userid"));
                        userModel.setName(pName);
                        userModel.setNumber(pMobile);
                        userModel.setProfileImagePath("");
                        userModel.setReferralName(pRefName);

                        gson = new Gson();
                        pref.putUserModel(gson.toJson(userModel));
                        pref.putLoginId(responseDataJson.getString("userid"));
                    } else {
                        successMessageArray[1] = responseDataJson.optString("msg");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return successMessageArray;
    }

    /*  -------------------------------------
        |
        |   CREATING JSON ENVELOPES FOR ADDRESS APIs
        |
        -------------------------------------
    */

    private String createAddressData(String state, String city, String area, String landmark, String address) throws JSONException {
        jsonObject = new JSONObject();

        if (isNormalUser) {
            jsonObject.put("isGuestUser", 0);
        } else if (isGuestUser) {
            jsonObject.put("isGuestUser", 1);
        }

        jsonObject.put("user_id", userId);
        jsonObject.put("state", state);
        jsonObject.put("city", city);
        jsonObject.put("area", area);
        jsonObject.put("landmark", landmark);
        jsonObject.put("address", address);
        jsonObject.put("isGuestUser", isGuestUser);

        return jsonObject.toString();
    }

    private String createUpdateAddressData(String landmark, String address, String userAddressId, String userId) throws JSONException {
        jsonObject = new JSONObject();
        jsonObject.put("user_id", userId);
        jsonObject.put("user_address_id", userAddressId);
        jsonObject.put("landmark", landmark);
        jsonObject.put("address", address);

        return jsonObject.toString();
    }

    /*  -------------------------------------
        |
        |   ADD ADDRESS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String[] addAddress(String stateId, String cityId, String areaId, String landmark, String address) throws JSONException {

        //Create JSON from data
        String lData = createAddressData(stateId, cityId, areaId, landmark, address);

        try {
            requestURL = URL + "?method=" + METHOD_ADD_ADDRESS + "&data=" + URLEncoder.encode(lData, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                try {
                    jsonObject = getJSONObjectFromResponse(response);
                    responseDataJson = jsonObject.getJSONObject("responsedata");
                    statusCode = responseDataJson.getString("success");
                    successMessageArrayNew[0] = statusCode;
                    successMessageArrayNew[1] = responseDataJson.getString("result");
                    successMessageArrayNew[2] = responseDataJson.getString("data");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return successMessageArrayNew;
    }

    /*  -------------------------------------
        |
        |   PARSE ALL ADDRESSES
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private ArrayList<UserAddressModel> parseAllAddresses(HttpResponse pResponse) {
        ArrayList<UserAddressModel> mUserAddress = new ArrayList<>();

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);

            responseDataJson = jsonObject.getJSONObject("responsedata");
            statusCode = responseDataJson.getString("success");
            JSONArray jsonArrayResponse = responseDataJson.getJSONArray("result");

            if (("1").equalsIgnoreCase(statusCode)) {
                for (int i = 0; i < jsonArrayResponse.length(); i++) {
                    UserAddressModel addressValue = new UserAddressModel(jsonArrayResponse.getJSONObject(i));
                    mUserAddress.add(addressValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mUserAddress;
    }

    /*  -------------------------------------
        |
        |   GET ALL ADDRESSES
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public ArrayList<UserAddressModel> getAllAddresses() throws JSONException {
        ArrayList<UserAddressModel> mUserAddress = new ArrayList<>();
        if (isStringInt(userId)) {
            requestURL = URL + "?method=" + METHOD_GET_ALL_ACTIVE_ADDRESSES + "&userid=" + Integer.parseInt(userId);

            try {
                response = getHttpResponse(requestURL);

                if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                    mUserAddress = parseAllAddresses(response);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return mUserAddress;
    }

    public boolean isStringInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }


    /*  -------------------------------------
        |
        |   CHECK COUPON
        |
        -------------------------------------
    */

    private CouponModel parseCouponResponse(HttpResponse pResponse, String coupon) {
        CouponModel couponModel = new CouponModel();

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            String success = responseDataJson.getString("success");
            String couponAmount = responseDataJson.getString("msg");

            if (success.equals("1")) {
                String couponId = responseDataJson.getString("coupon_id");
                couponModel = new CouponModel(couponId, coupon, couponAmount);
            } else {
                couponModel = new CouponModel("", coupon, couponAmount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return couponModel;
    }

    private JSONArray getServiceTypeData(ArrayList<ServiceTypeModel> mFinalSelectedServiceTypesList) {
        JSONArray jsonArray = new JSONArray();

        int totalAmount = 0;

        for (ServiceTypeModel stm : mFinalSelectedServiceTypesList) {

            try {
                JSONObject jo = new JSONObject();
                jo.put("st_id", stm.getServiceTypeID());
                jo.put("st_people", stm.getNumOfPeople());
                jsonArray.put(jo);
                totalAmount += (Integer.parseInt(stm.getPrice()) * Integer.parseInt(stm.getNumOfPeople()));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        billAmount = totalAmount;

        return jsonArray;
    }

    public CouponModel checkCoupon(ArrayList<ServiceTypeModel> mFinalSelectedServiceTypesList, String coupon) {
        CouponModel couponModel = new CouponModel();

        try {
            JSONObject dataObject = new JSONObject();
            JSONArray jsonArray = getServiceTypeData(mFinalSelectedServiceTypesList);

            dataObject.put("services", jsonArray);
            dataObject.put("bill_amount", billAmount);
            dataObject.put("coupon_code", coupon);
            dataObject.put("user_id", userId);

            requestURL = URL + "?method=" + METHOD_CHECK_COUPONS + "&data=" + URLEncoder.encode(dataObject.toString(), "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                couponModel = parseCouponResponse(response, coupon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return couponModel;
    }




    /*  -------------------------------------
        |
        |   GET USER ACCOUNT DETAILS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String[] getUserDiscountDetails() {
        String[] discounts = {"", ""};
        boolean isResponse = false;
        HttpGet httpGet = null;
        HttpResponse response = null;

        String requestURL = URL + "?method=" + METHOD_GET_DISCOUNT_DETAILS + "&userid=" + userId;
        Log.i(TAG, " Request URL:: " + requestURL);
        HttpClient client = new DefaultHttpClient();
        client = sslClient(client);
        try {
            httpGet = new HttpGet(requestURL);
            response = client.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                discounts = parseDiscounts(response);
                isResponse = true;
            } else {
                isResponse = false;
            }
        } catch (IllegalArgumentException ie) {
            Log.e(TAG, ie.getMessage());
            isResponse = false;
        } catch (Exception ie) {
            Log.e(TAG, ie.getMessage());
            isResponse = false;
        }
        return discounts;

    }

    @SuppressWarnings("deprecation")
    public String[] parseDiscounts(HttpResponse pResponse) {
        String[] discounts = {"", ""};
        String serverResponse;
        String statusCode;
        boolean isResponse = false;
        serverResponse = getHttpResponseStructure(pResponse);
        JSONTokener tokener = new JSONTokener(serverResponse);
        try {
            JSONObject jsonObject = new JSONObject(tokener);
            JSONObject responseDataJson = jsonObject.getJSONObject("responsedata");

            statusCode = responseDataJson.getString("success");
            Log.i(TAG, " Status Code :: " + statusCode);
            JSONObject jsonObjectResponse = responseDataJson.getJSONObject("result");
            if (("1").equalsIgnoreCase(statusCode)) {
                discounts[0] = jsonObjectResponse.getString("first_booking");
                discounts[1] = jsonObjectResponse.getString("referral");
                isResponse = true;
            } else {
                isResponse = false;
            }
        } catch (JSONException ex) {
            Log.e(TAG, ex.getMessage());
            isResponse = false;
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
            isResponse = false;
        }
        return discounts;
    }



    /*  -------------------------------------
        |
        |   SERVICE WITH SERVICE TYPES PARSER
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private ArrayList<ServiceModel> serviceWithServiceTypesParser(HttpResponse pResponse) {
        ArrayList<ServiceModel> lServiceModelList = new ArrayList<>();

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            statusCode = responseDataJson.getString("success");

            if (("1").equalsIgnoreCase(statusCode) && !responseDataJson.getString("result").equalsIgnoreCase("null")) {
                JSONArray jsonArrayResponse = responseDataJson.getJSONArray("result");

                for (int i = 0; i < jsonArrayResponse.length(); i++) {
                    ServiceModel lServiceModel = new ServiceModel(jsonArrayResponse.getJSONObject(i));
                    lServiceModelList.add(lServiceModel);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lServiceModelList;
    }

    /*  -------------------------------------
        |
        |   GET ALL SERVICE TYPES
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public ArrayList<ServiceModel> getAllServicesWithServiceTypes(String hubId) {
        ArrayList<ServiceModel> lServiceModelList = new ArrayList<>();
        requestURL = URL + "?method=" + METHOD_GET_ALL_SERVICE_WITH_SERVICE_TYPES + "&hubId=" + hubId;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                lServiceModelList = serviceWithServiceTypesParser(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lServiceModelList;
    }

    /*  -------------------------------------
        |
        |   CREATE A JSON ENVELOPE FOR CHANGE PASSWORD API
        |
        -------------------------------------
    */

    private String createChangePassArray(String userid, String pOld, String pNew) {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("userid", userid);
            jsonObject.put("old", pOld);
            jsonObject.put("new", pNew);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    /*  -------------------------------------
        |
        |   VERIFY CHANGE PASSWORD
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private boolean verifyChangePassword(HttpResponse pResponse) {
        boolean isChanged;

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            statusCode = responseDataJson.getString("success");
            boolean result = responseDataJson.getBoolean("result");
            isChanged = ("1").equalsIgnoreCase(statusCode) && result;
        } catch (Exception e) {
            e.printStackTrace();
            isChanged = false;
        }

        return isChanged;
    }

    /*  -------------------------------------
        |
        |   CHANGE PASSWORD
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public boolean changePassword(String userid, String pOldPass, String pNewPass) {
        boolean isResponse;
        String data = createChangePassArray(userid, pOldPass, pNewPass);

        try {
            requestURL = URL + "?method=" + METHOD_CHANGE_PASSWORD + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);
            isResponse = response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE && verifyChangePassword(response);
        } catch (Exception e) {
            e.printStackTrace();
            isResponse = false;
        }

        return isResponse;
    }

    /*  -------------------------------------
        |
        |   VERIFY FORGOT PASSWORD
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private String verifyForgotPassword(HttpResponse pResponse) {
        String userID = "";

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            statusCode = responseDataJson.getString("success");
            if (("1").equalsIgnoreCase(statusCode)) {
                userID = responseDataJson.getString("result");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userID;
    }

    /*  -------------------------------------
        |
        |   FORGOT PASSWORD
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String forgotPassword(String pContact) {
        String userID = "";

        try {
            requestURL = URL + "?method=" + METHOD_FORGOT_PASSWORD + "&contact=" + pContact;
            response = getHttpResponse(requestURL);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                userID = verifyForgotPassword(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userID;
    }

    @SuppressWarnings("deprecation")
    public String sendOtpToUser(String pContact) {
        String userID = "";

        try {
            requestURL = URL + "?method=" + METHOD_SENDOTP_TO_USER + "&otp=" + pContact;
            response = getHttpResponse(requestURL);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                userID = verifyForgotPassword(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userID;
    }

    /*  -------------------------------------
        |
        |   CREATE A JSON ENVELOPE FOR UPDATE USER API
        |
        -------------------------------------
    */

    private String createUpdateUserArray(String pName, String pDOB, String pImageURL, String pEmail) {
        jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", userId);
            jsonObject.put("uname", pName);
            jsonObject.put("dob", pDOB);
            jsonObject.put("image", pImageURL);
            jsonObject.put("useremail", pEmail);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    /*  -------------------------------------
        |
        |   CONFIRM UPDATE USER
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private boolean confirmUpdateUser(HttpResponse pResponse) {
        boolean isChanged;

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            statusCode = responseDataJson.getString("success");
            isChanged = ("1").equalsIgnoreCase(statusCode);
        } catch (Exception e) {
            e.printStackTrace();
            isChanged = false;
        }

        return isChanged;
    }

    /*  -------------------------------------
        |
        |   UPDATE USER
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public boolean updateUser(String pName, String pDOB, String pImageURL, String pEmail) {
        boolean isResponse = false;
        String data = createUpdateUserArray(pName, pDOB, pImageURL, pEmail);

        try {
            requestURL = URL + "?method=" + METHOD_UPDATE_USER + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE && confirmUpdateUser(response)) {
                isResponse = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isResponse;
    }

        /*  -------------------------------------
        |
        |   UPDATE PROFILE PHOTO
        |
        -------------------------------------
    */

    public String[] updateProfilePic(File file) throws IOException {
        String[] resultArray = {"0", ""};
        String serverResponse;
        String statusCode;

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost();
            FileBody bin1 = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity();
            reqEntity.addPart("file", bin1);
            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            HttpEntity httpEntity = response.getEntity();
            final String responseString = EntityUtils.toString(httpEntity);
            JSONTokener tokener = new JSONTokener(responseString);
            try {
                final JSONObject jsonObject = new JSONObject(responseString);
                JSONObject responseDataJson = jsonObject.getJSONObject("responseData");
                statusCode = responseDataJson.getString("success");
                if (("1").equalsIgnoreCase(statusCode)) {
                    resultArray[0] = responseDataJson.getString("success");
                    resultArray[1] = responseDataJson.getString("data");
                    return resultArray;
                }
            } catch (JSONException ex) {
                Log.e(TAG, ex.getMessage());
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage());
            }
        } catch (Exception ex) {
            Log.e("Debug", "error: " + ex.getMessage(), ex);
        }

        return resultArray;
    }


    /*  -------------------------------------
        |
        |  CONFIRM BOOKING
        |
        -------------------------------------
    */

    private String createConfirmBooingJSON(ConfirmOrderModel confirmOrderModel) {
        jsonObject = new JSONObject();

        try {
            jsonObject.put("orderId", confirmOrderModel.getOrderId());
            jsonObject.put("booking_id", confirmOrderModel.getBookingIds());
            jsonObject.put("user_id", confirmOrderModel.getUserId());
            jsonObject.put("payment_mode", "online");
            jsonObject.put("payment_id", confirmOrderModel.getPaymentId());
            jsonObject.put("success", confirmOrderModel.getSuccess());
            jsonObject.put("payment_status", confirmOrderModel.getSuccess());
            jsonObject.put("failure_code", confirmOrderModel.getFailureCode());
            jsonObject.put("device", "Android");
            jsonObject.put("statusCode", confirmOrderModel.getStatusCode());
            jsonObject.put("statusMessage", confirmOrderModel.getStatusMessage());
            jsonObject.put("responseMessage", confirmOrderModel.getResponseMessage());
            jsonObject.put("paymentType", confirmOrderModel.getPaymentType());


            /*{"user_id":"16674","payment_mode":"online","payment_id":"20180516111212800110168567625585313",
                    "success":"1","payment_status":"1","booking_id":"[47793]","failure_code":"","failure_response":"",
                    "orderId":"3995","device":"Android"}*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @SuppressWarnings("deprecation")
    public StandardResponseModelBooking confirmBooking(ConfirmOrderModel confirmOrderModel) {
        StandardResponseModelBooking standardResponseModel = new StandardResponseModelBooking();

        try {
            String data = createConfirmBooingJSON(confirmOrderModel);
            requestURL = URL + "?method=" + METHOD_CONFIRM_BOOKING + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                standardResponseModel = new StandardResponseModelBooking(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return standardResponseModel;
    }

    /*  -------------------------------------
        |
        |  ADD MULTIPLE BOOKING
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private MultipleBookingResponseModel parseBookingAddedResponse(HttpResponse pResponse) {
        String success, responseMessage;
        MultipleBookingResponseModel multipleBookingResponseModel = new MultipleBookingResponseModel();

        try {
            jsonObject = getJSONObjectFromResponse(pResponse);
            responseDataJson = jsonObject.getJSONObject("responsedata");
            success = responseDataJson.getString("success");

            if (success.equals("1")) {
                JSONObject result = responseDataJson.getJSONObject("result");

                if (result != null) {
                    multipleBookingResponseModel = new MultipleBookingResponseModel(result);
                }
            } else {
                responseMessage = responseDataJson.getString("message");
                multipleBookingResponseModel = new MultipleBookingResponseModel(responseMessage);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return multipleBookingResponseModel;
    }

    @SuppressWarnings("deprecation")
    public MultipleBookingResponseModel addBooking(ArrayList<BookingSummaryDetailModel> bookingSummaryDetailList) {
        MultipleBookingResponseModel multipleBookingResponseModel = new MultipleBookingResponseModel();

        try {
            Gson gson = new GsonBuilder().create();
            JsonArray jsonArray = gson.toJsonTree(bookingSummaryDetailList).getAsJsonArray();
            String data = jsonArray.toString();

            requestURL = URL + "?method=" + METHOD_ADD_MULTIPLE_BOOKING + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                multipleBookingResponseModel = parseBookingAddedResponse(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return multipleBookingResponseModel;
    }

    /*  -------------------------------------
        |
        |  GET AVAILABLE TIME SLOTS NEAREST
        |
        -------------------------------------
    */

    private ExpectedDateTimeModel expectedTimeSlotsResponseParser(HttpResponse response) {
        ArrayList<TimeSlotModel> timeSlots = new ArrayList<>();
        ExpectedDateTimeModel expectedDateTimeModel = new ExpectedDateTimeModel();

        try {
            jsonObject = getJSONObjectFromResponse(response);
            responseDataJson = jsonObject.getJSONObject("responsedata");

            if (responseDataJson.getString("success").equals("1")) {
                JSONArray jsonArray1 = responseDataJson.getJSONArray("result");
                JSONObject jsonObject1 = jsonArray1.getJSONObject(0);
                String date = jsonObject1.getString("date");

                if (isValidString(date)) {
                    JSONArray jsonArray2 = jsonObject1.getJSONArray("timeSlots");

                    for (int j = 0; j < jsonArray2.length(); j++) {
                        JSONObject jo = jsonArray2.getJSONObject(j);
                        TimeSlotModel timeSlotModel = new TimeSlotModel(jo);
                        timeSlots.add(timeSlotModel);
                    }

                    expectedDateTimeModel = new ExpectedDateTimeModel(date, timeSlots);
                } else {
                    String message = jsonObject1.getString("message");
                    expectedDateTimeModel = new ExpectedDateTimeModel(date, timeSlots, message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return expectedDateTimeModel;
    }

    public ExpectedDateTimeModel getExpectedTimeSlots(String data) {
        ExpectedDateTimeModel expectedDateTimeModel = new ExpectedDateTimeModel();

        try {
            requestURL = URL + "?method=" + METHOD_GET_EXPECTED_DATE_TIME_SLOTS + "&data=" + URLEncoder.encode(data, "utf-8");

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                expectedDateTimeModel = expectedTimeSlotsResponseParser(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return expectedDateTimeModel;
    }

    /*  -------------------------------------
        |
        |  GET AVAILABLE TIME SLOTS BY DATE
        |
        -------------------------------------
    */

    private ArrayList<TimeSlotModel> timeSlotsResponseParser(HttpResponse response) {
        ArrayList<TimeSlotModel> timeSlots = new ArrayList<>();

        try {
            jsonObject = getJSONObjectFromResponse(response);
            responseDataJson = jsonObject.getJSONObject("responsedata");

            if (responseDataJson.getString("success").equals("1")) {
                JSONArray jsonArray1 = responseDataJson.getJSONArray("result");

                for (int i = 0; i < jsonArray1.length(); i++) {
                    JSONArray jsonArray2 = jsonArray1.getJSONArray(i);

                    for (int j = 0; j < jsonArray2.length(); j++) {
                        JSONObject jo = jsonArray2.getJSONObject(j);
                        TimeSlotModel lTimeSlotModel = new TimeSlotModel(jo);
                        timeSlots.add(lTimeSlotModel);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeSlots;
    }

    public ArrayList<TimeSlotModel> getAvailableTimeSlotsByDate(String data) {
        ArrayList<TimeSlotModel> timeSlots = new ArrayList<>();

        try {
            requestURL = URL + "?method=" + METHOD_GET_TIME_SLOTS_BY_DATE + "&data=" + URLEncoder.encode(data, "utf-8");

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                timeSlots = timeSlotsResponseParser(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return timeSlots;
    }

    /*  -------------------------------------
        |
        |  CANCEL BOOKING
        |
        -------------------------------------
    */

    private String createCancelBookingJSON(String bookingID) {
        jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingid", bookingID);
            jsonObject.put("reason", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }

    @SuppressWarnings("deprecation")
    public CancelBookingResponseModel cancelBooking(String bookingID) {
        CancelBookingResponseModel cancelBookingResponseModel = new CancelBookingResponseModel();
        String data = createCancelBookingJSON(bookingID);

        try {
            requestURL = URL + "?method=" + METHOD_CANCEL_BOOKING + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                cancelBookingResponseModel = new CancelBookingResponseModel(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cancelBookingResponseModel;
    }

    /*  -------------------------------------
        |
        |  GET ACTIVE BOOKING
        |
        -------------------------------------
    */

    @SuppressWarnings({"unused", "deprecation"})
    public ArrayList<BookingListModel> getActiveBooking() {
        ArrayList<BookingListModel> bookingList = new ArrayList<>();
        requestURL = URL + "?method=" + METHOD_GET_ACTIVE_BOOKING + "&userid=" + userId;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);

                responseDataJson = jsonObject.getJSONObject("responsedata");
                String statusCode = responseDataJson.getString("success");
                JSONArray lJsonResponseArray = responseDataJson.getJSONArray("bookingDetails");

                if (statusCode.equalsIgnoreCase("1")) {
                    for (int i = 0; i < lJsonResponseArray.length(); i++) {
                        BookingListModel bookingHistoryModel = new BookingListModel();
                        JSONObject jsonObject = lJsonResponseArray.optJSONObject(i);

                        if (jsonObject != null) {
                            BookingListModel bookingListModel = new BookingListModel(jsonObject);
                            bookingList.add(bookingListModel);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookingList;
    }

    /*  -------------------------------------
        |
        |  GET HISTORY BOOKING
        |
        -------------------------------------
    */

    @SuppressWarnings({"unused", "deprecation"})
    public ArrayList<BookingListModel> getHistoryBooking() {
        ArrayList<BookingListModel> bookingList = new ArrayList<>();

        String requestURL = URL + "?method=" + METHOD_GET_HISTORY_BOOKING + "&userid=" + userId;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                String statusCode = responseDataJson.getString("success");
                JSONArray lJsonResponseArray = responseDataJson.getJSONArray("bookingDetails");

                if (statusCode.equalsIgnoreCase("1")) {
                    for (int i = 0; i < lJsonResponseArray.length(); i++) {
                        BookingListModel bookingHistoryModel = new BookingListModel();
                        JSONObject jsonObject = lJsonResponseArray.optJSONObject(i);

                        if (jsonObject != null) {
                            BookingListModel bookingListModel = new BookingListModel(jsonObject);
                            bookingList.add(bookingListModel);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookingList;
    }

    /*  -------------------------------------
        |
        |  CHECK OTP
        |
        -------------------------------------
    */

    private String createCheckOTPString(String otp, String user_id) throws JSONException {
        jsonObject = new JSONObject();
        jsonObject.put("otp", otp);
        jsonObject.put("user_id", user_id);

        return jsonObject.toString();
    }

    @SuppressWarnings("deprecation")
    public CheckOTPResponseModel checkOTP(String otp, String userId) throws JSONException {
        CheckOTPResponseModel checkOTPResponseModel = new CheckOTPResponseModel();
        String data = createCheckOTPString(otp, userId);

        try {
            requestURL = URL + "?method=" + METHOD_CHECK_OTP + "&data=" + URLEncoder.encode(data, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                checkOTPResponseModel = new CheckOTPResponseModel(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return checkOTPResponseModel;
    }

    /*  -------------------------------------
        |
        |  RESEND OTP
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public boolean resendOTP(String userId) throws JSONException {
        boolean isResponse;

        try {
            requestURL = URL + "?method=" + METHOD_RESEND_OTP + "&userid=" + userId;
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                statusCode = responseDataJson.getString("success");
                boolean result = responseDataJson.getBoolean("result");
                isResponse = ("1").equalsIgnoreCase(statusCode) && result;
            } else {
                isResponse = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            isResponse = false;
        }

        return isResponse;
    }


    @SuppressWarnings("deprecation")
    public Boolean checkEmailExistsBool(String email) {
        boolean emailExists = false;

        try {
            requestURL = URL + "?method=" + METHOD_CHECK_EXISTING_EMAIL + "&data=" + URLEncoder.encode(email, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                statusCode = responseDataJson.getString("success");

                String result = responseDataJson.getString("result");

                if (statusCode.equals("1") && result.equals("true")) {
                    emailExists = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emailExists;
    }

    @SuppressWarnings("deprecation")
    public void showAlertDialog(Context context, String message) {
        mAlertDialog = new AlertDialog.Builder(context).create();
        mAlertDialog.setTitle("Change Failed..");
        mAlertDialog.setMessage(message);

        mAlertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                mAlertDialog.dismiss();
            }
        });

        mAlertDialog.show();
    }


    public String updateProfilePicOld(File file) throws IOException {
        String filePath = "";
        try {
            client = new DefaultHttpClient();

            HttpPost post = new HttpPost(FILE_UPLOAD_URL);
            FileBody bin1 = new FileBody(file);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("file", bin1);
            post.setEntity(reqEntity);

            HttpResponse response = client.execute(post);
            HttpEntity httpEntity = response.getEntity();

            final String responseString = EntityUtils.toString(httpEntity);

            final JSONObject jsonObject = new JSONObject(responseString);
            JSONObject responseDataJson = jsonObject.getJSONObject("responseData");
            statusCode = responseDataJson.getString("success");

            if (("1").equalsIgnoreCase(statusCode)) {
                filePath = responseDataJson.getString("data");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return filePath;
    }

    /*  -------------------------------------
        |
        |  CHECK IF EMAIL EXISTS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String[] checkEmailExists(String email) {
        HttpGet httpGet = null;
        HttpResponse response = null;
        String lData = email;
        String requestURL = null;
        String[] responseArray = {"0", "false"};

        try {
            requestURL = URL + "?method=" + METHOD_CHECK_EXISTING_EMAIL + "&data=" + URLEncoder.encode(lData, "utf-8");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        HttpClient client = new DefaultHttpClient();
        client = sslClient(client);

        try {
            httpGet = new HttpGet(requestURL);
            response = client.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                String serverResponse = getHttpResponseStructure(response);
                JSONTokener tokener = new JSONTokener(serverResponse);
                try {
                    JSONObject jsonObject = new JSONObject(tokener);
                    JSONObject responseDataJson = jsonObject.getJSONObject("responsedata");
                    String statusCode = responseDataJson.getString("success").toString();
                    String data = responseDataJson.getString("result");
                    Log.i(TAG, " Status Code :: " + statusCode);
                    if ("1".equalsIgnoreCase(statusCode)) {
                        responseArray[0] = "1";
                        responseArray[1] = data;
                    }
                } catch (JSONException ex) {

                } catch (Exception ex) {

                }
            }
        } catch (IllegalArgumentException ie) {

        } catch (Exception ie) {

        }

        return responseArray;
    }
    /*  -------------------------------------
        |
        |  UPDATE ADDRESS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String updateAddress(String landmark, String address, String userAddressId) throws JSONException {
        String isUpdated = "";
        String lData = createUpdateAddressData(landmark, address, userAddressId, userId);

        try {
            requestURL = URL + "?method=" + METHOD_UPDATE_ADDRESS + "&data=" + URLEncoder.encode(lData, "utf-8");
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                statusCode = responseDataJson.getString("success");

                if (("1").equalsIgnoreCase(statusCode)) {
                    isUpdated = responseDataJson.getString("result");
                } else {
                    isUpdated = responseDataJson.getString("Error Message");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isUpdated;
    }

    /*  -------------------------------------
        |
        |  REMOVE ADDRESS
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public boolean removeAddress(String addressId) throws JSONException {
        Boolean isRemoved = false;
        requestURL = URL + "?method=" + METHOD_DISABLE_ADDRESS + "&userId=" + userId + "&addressId=" + addressId;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                statusCode = responseDataJson.getString("success");

                if (("1").equalsIgnoreCase(statusCode)) {
                    if (responseDataJson.getString("result").equalsIgnoreCase("true")) {
                        isRemoved = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isRemoved;
    }

    /*  -------------------------------------
        |
        |  GET AREA DATA FROM SERVER
        |
        -------------------------------------
    */

    public void getAreaDataFromServer() {
        String requestURL = URL + "?method=" + METHOD_GET_ALL_AREAS;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getString("success").equalsIgnoreCase("1")) {
                    //mDBQuery.truncateAreaTable("DELETE from " + VcTableList.TABLE_AREA);
                    JSONArray jsonArray = responseDataJson.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        String pQuery = "INSERT INTO " + VcTableList.TABLE_AREA + " VALUES('"
                                + jsonObject2.getString(VcTableList.AREA_ID) + "', '"
                                + jsonObject2.getString(VcTableList.AREA_NAME) + "', '"
                                + jsonObject2.getString(VcTableList.CITY_ID) + "', '"
                                + jsonObject2.getString(VcTableList.HUB_ID) + "', '"
                                + jsonObject2.getString(VcTableList.AREA_MIN_AMOUNT) + "')";
                        // if(!mDBQuery.isExistingArea(jsonObject2.getString(VcTableList.AREA_ID)))
                        mDBQuery.insertIntoAreaTable(pQuery);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*  -------------------------------------
        |
        |  GET CITY LIST VIEW
        |
        -------------------------------------
    */

    public Bundle getCityListNew() {
        Bundle returnBundle = new Bundle();
        returnBundle.putBoolean("success", false);
        ArrayList<CityModel> lObjCityModelVector = new ArrayList<>();

        try {
            String requestURL = URL + "?method=" + METHOD_GET_ALL_CITIES;
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getString("success").equalsIgnoreCase("1")) {
                    returnBundle.putBoolean("success", true);
                    JSONArray jsonArray = responseDataJson.getJSONArray("result");
                    mDBQuery.clearTable(VcTableList.TABLE_CITY);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        CityModel lCityModel = new CityModel(jsonArray.getJSONObject(i));
                        lObjCityModelVector.add(lCityModel);
                    }
                    returnBundle.putParcelableArrayList("cities", lObjCityModelVector);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnBundle;
    }

    /*  -------------------------------------
        |
        |  GET CONFIGURATION
        |
        -------------------------------------
    */

    public Bundle getConfiguration() {
        Bundle returnBundle = new Bundle();
        returnBundle.putBoolean("success", false);
        String requestURL = URL + "?method=" + METHOD_GET_CONFIGURATION;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);

                if (jsonObject.getString("success").equalsIgnoreCase("1")) {
                    JSONObject responseDataJson = jsonObject.getJSONObject("result");

                    returnBundle.putBoolean("success", true);
                    returnBundle.putString("min_supported_version", responseDataJson.getString("min_supported_version"));
                    returnBundle.putString("current_version", responseDataJson.getString("current_version"));
                    returnBundle.putString("update_message", responseDataJson.getString("update_message"));
                    returnBundle.putString("referral_limit", responseDataJson.getString("referral_limit"));
                    returnBundle.putString("phone_number", responseDataJson.getString("phone_number"));
                    returnBundle.putString("invite_message", responseDataJson.getString("invite_message"));
                    returnBundle.putString("terms_and_conditions", responseDataJson.getString("terms_and_conditions"));
                    returnBundle.putString("privacy_policy", responseDataJson.getString("privacy_policy"));

                    ConfigurationModel configurationModel = new ConfigurationModel(returnBundle);
                    json = gson.toJson(configurationModel);
                    pref.putConfiguration(json);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnBundle;
    }

    /*  -------------------------------------
        |
        |  FEEDBACK
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public String[] isFeedback() throws JSONException {
        String[] bookingIdTimeDate = {"", "", ""};
        requestURL = URL + "?method=" + METHOD_IS_FEEDBACK + "&userid=" + userId;

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");
                String statusCode = responseDataJson.getString("success");

                if (("1").equalsIgnoreCase(statusCode)) {
                    JSONObject resultObj = responseDataJson.optJSONObject("result");

                    if (resultObj != null) {
                        bookingIdTimeDate[0] = resultObj.getString("booking_id");
                        bookingIdTimeDate[1] = resultObj.getString("booking_time");
                        bookingIdTimeDate[2] = resultObj.getString("booking_date");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookingIdTimeDate;
    }

    /*  -------------------------------------
        |
        |  CREATE FEEDBACK STRING
        |
        -------------------------------------
    */

    private String createFeedbackString(String pBookingID, String pRating, String pDesription) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user_id", userId);
        jsonObject.put("booking_id", pBookingID);
        jsonObject.put("rating", pRating);
        jsonObject.put("description", pDesription);
        return jsonObject.toString();
    }

    /*  -------------------------------------
        |
        |  SUBMIT FEEDBACK
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    public boolean submitFeedback(String pBookingID, String pRating, String pDesription) throws JSONException {
        boolean isResponse = false;
        String data = createFeedbackString(pBookingID, pRating, pDesription);

        try {
            requestURL = URL + "?method=" + METHOD_SUBMIT_FEEDBACK + "&data=" + URLEncoder.encode(data, "utf-8");

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                String statusCode = responseDataJson.getString("success");
                Log.i(TAG, " Status Code :: " + statusCode);

                if (("1").equalsIgnoreCase(statusCode)) {
                    isResponse = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isResponse = false;
        }

        return isResponse;
    }


    public boolean getIsUserMember(String userId) throws JSONException {
        boolean isResponse = false;
        //String data = createFeedbackString(pBookingID, pRating, pDesription);

        try {
            requestURL = URL + "?method=" + METHOD_GET_IS_MEMBER + "&userid=" + userId;

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                int statusCode = responseDataJson.getInt("success");
                Log.i(TAG, " Status Code :: " + statusCode);

                if (statusCode == 1) {
                    int isMember = responseDataJson.getInt("is_member");
                    if (isMember == 1)
                        isResponse = true;
                    else
                        isResponse = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            isResponse = false;
        }

        return isResponse;
    }

    /*  -------------------------------------
        |
        |   GET OFFERS
        |
        -------------------------------------
    */

    public ArrayList<Offer> getOffers() {
        ArrayList<Offer> offersList = new ArrayList<>();
        String requestURL = URL + "?method=" + METHOD_GET_OFFERS + "&target=1";

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getString("success").equalsIgnoreCase("1")) {
                    JSONArray jsonArray = responseDataJson.getJSONArray("offerBanners");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject offerObject = jsonArray.optJSONObject(i);

                        if (offerObject != null && !offerObject.optString("OfferImage").equalsIgnoreCase("")) {
                            Offer offer = new Offer(offerObject);
                            offersList.add(offer);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return offersList;
    }

    /*  -------------------------------------
        |
        |  GET BANNERS
        |
        -------------------------------------
    */

    public ArrayList<Banner> getBanners() {
        ArrayList<Banner> bannersList = new ArrayList<>();
        String requestURL, bannerImage, actionUrl;
        int bannerID, bannerOrder, bannerStatus, bannerTarget;

        try {
            requestURL = URL + "?method=" + METHOD_GET_BANNERS + "&target=2";

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getString("success").equalsIgnoreCase("1")) {
                    JSONArray jsonArray = responseDataJson.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject bannerObject = jsonArray.optJSONObject(i);

                        if (bannerObject != null && !bannerObject.optString("img_url").equalsIgnoreCase("")) {
                            bannerImage = bannerObject.optString("img_url");
                            bannerID = Integer.parseInt(bannerObject.optString("id"));
                            bannerOrder = Integer.parseInt(bannerObject.optString("order"));
                            bannerStatus = Integer.parseInt(bannerObject.optString("status"));
                            bannerTarget = Integer.parseInt(bannerObject.optString("target"));
                            actionUrl = bannerObject.optString("action_url");
                            Banner banner = new Banner(bannerID, bannerOrder, bannerStatus, bannerTarget, bannerImage, actionUrl);
                            bannersList.add(banner);
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bannersList;
    }

    public Map<String, String> getLocation(Double lat, Double longi) {
        Map<String, String> mapOfResponse = new HashMap<>();
        String requestURL, bannerImage;
        int bannerID, bannerOrder, bannerStatus, bannerTarget;

        try {
            requestURL = URL + "?method=" + ApplicationSettings.METHOD_GET_LOCATION + "&latitude=" + lat + "&longitude=" + longi;

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getInt("success") == 1) {


                    JSONObject jObj = responseDataJson.getJSONObject("result");
                    mapOfResponse.put("areaId", jObj.getString("areaId"));
                    mapOfResponse.put("areaName", jObj.getString("areaName"));
                    mapOfResponse.put("hub_id", jObj.getString("hub_id"));
                    mapOfResponse.put("cityId", jObj.getString("cityId"));
                    mapOfResponse.put("cityName", jObj.getString("cityName"));
                    mapOfResponse.put("latitude", jObj.getString("latitude"));
                    mapOfResponse.put("longitude", jObj.getString("longitude"));
                    mapOfResponse.put("distance", jObj.getString("distance"));
                    mapOfResponse.put("message", responseDataJson.getString("message"));


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapOfResponse;
    }

    /*  -------------------------------------
        |
        |   GUEST USER LOGIN
        |
        -------------------------------------
    */

    public GetUserStateResponseModel getUserState(String name, String mobileNumber) {
        GetUserStateResponseModel getUserStateResponseModel = new GetUserStateResponseModel();

        try {
            String requestURL = URL + "?method=" + METHOD_GET_USER_STATE + "&name=" + name + "&contact=" + mobileNumber;

            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                getUserStateResponseModel = new GetUserStateResponseModel(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return getUserStateResponseModel;
    }

    /*  -------------------------------------
        |
        |   GET ALL AREAS
        |
        -------------------------------------
    */

    public Bundle getAllAreasNew() {
        Bundle returnBundle = new Bundle();
        returnBundle.putBoolean("success", false);
        requestURL = URL + "?method=" + METHOD_GET_ALL_AREAS;
        ArrayList<AreaModel> lDataArrayList = new ArrayList<>();

        try {
            response = getHttpResponse(requestURL);

            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                jsonObject = getJSONObjectFromResponse(response);
                responseDataJson = jsonObject.getJSONObject("responsedata");

                if (responseDataJson.getString("success").equalsIgnoreCase("1")) {
                    returnBundle.putBoolean("success", true);
                    JSONArray jsonArray = responseDataJson.getJSONArray("result");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        AreaModel lModel = new AreaModel(jsonObject2);
                        lDataArrayList.add(lModel);
                    }
                    returnBundle.putParcelableArrayList("areas", lDataArrayList);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnBundle;
    }

    @SuppressWarnings("deprecation")
    public UserAddressModel getLastAddress()
            throws JSONException {
        boolean isResponse = false;
        HttpGet httpGet = null;
        HttpResponse response = null;
        UserAddressModel lLastAddress = new UserAddressModel();
        String requestURL = null;
        requestURL = URL + "?method=" + METHOD_GET_LAST_ADDRESS + "&userid=" + pref.getLoginId();
        Log.i(TAG, " Request URL:: " + requestURL);
        HttpClient client = new DefaultHttpClient();
        client = sslClient(client);
        try {
            httpGet = new HttpGet(requestURL);
            response = client.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                lLastAddress = parseLastAddress(response);
                isResponse = true;
            } else {
                isResponse = false;
            }
        } catch (IllegalArgumentException ie) {
            // SafeStartMacros.logError(mLoggerFlag, TAG, ie.getMessage());
            isResponse = false;
        } catch (Exception ie) {
            // SafeStartMacros.logError(mLoggerFlag, TAG, ie.getMessage());
            isResponse = false;
        }
        return lLastAddress;
    }

    @SuppressWarnings("deprecation")
    private UserAddressModel parseLastAddress(HttpResponse pResponse) {
        String serverResponse;
        String statusCode;
        boolean isValid = false;
        UserAddressModel addressValue = new UserAddressModel();
        ArrayList<UserAddressModel> mUserAddress = new ArrayList<UserAddressModel>();
        serverResponse = getHttpResponseStructure(pResponse);
        JSONTokener tokener = new JSONTokener(serverResponse);
        try {
            JSONObject jsonObject = new JSONObject(tokener);
            JSONObject responseDataJson = jsonObject.getJSONObject("responsedata");

            statusCode = responseDataJson.getString("success");
            Log.i(TAG, " Status Code :: " + statusCode);
            JSONObject jsonObjectResponse = responseDataJson.getJSONObject("result");
            Log.i(TAG, " CommonResponse get Address:: " + jsonObjectResponse.toString());
            if (("1").equalsIgnoreCase(statusCode)) {
                addressValue = new UserAddressModel(jsonObjectResponse);
                isValid = true;
                // SafeStartMacros.logInfo(mLoggerFlag, TAG, "Logged in successfully.");
            } else {
                isValid = false;
                // SafeStartMacros.logInfo(mLoggerFlag, TAG, "Log in failed.");
            }
        } catch (JSONException ex) {
            // SafeStartMacros.logError(mLoggerFlag, TAG, "JSONException :: " + ex.getMessage());
        } catch (Exception ex) {
            // SafeStartMacros.logError(mLoggerFlag, TAG, "Exception :: " + ex.getMessage());
        }
        return addressValue;
    }


}