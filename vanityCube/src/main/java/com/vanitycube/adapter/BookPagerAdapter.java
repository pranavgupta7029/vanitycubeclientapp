package com.vanitycube.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.vanitycube.fragments.BookFragment;
import com.vanitycube.model.CategoryModel;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by getit on 11/2/2015.
 */
public class BookPagerAdapter extends FragmentStatePagerAdapter {

    private Context mContext;
    private String[] titles = new String[]{"HOME", "SALES", "PENDING"};
    private ArrayList<String> titlesList = new ArrayList<>();
    private ViewPager viewPager;
    private HashMap<String, CategoryModel> mapCategoryModelByName = new HashMap<>();
    private ArrayList<Integer> addressAreaIdsList;
    private ArrayList<Integer> serviceTimesList;


    public BookPagerAdapter(FragmentManager fm, Context context, ArrayList<String> titleList, ViewPager viewPager, HashMap<String, CategoryModel> mapCategoryModelByName, ArrayList<Integer> addressAreaIdsList, ArrayList<Integer> serviceTimesList) {
        super(fm);
        mContext = context;
        this.titlesList = titleList;
        this.viewPager = viewPager;
        this.mapCategoryModelByName = mapCategoryModelByName;
        this.addressAreaIdsList = addressAreaIdsList;
        this.serviceTimesList = serviceTimesList;
    }

    /**
     * Create pager adapter
     */
    public BookPagerAdapter(FragmentManager fm) {

        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment result = null;
        switch (position) {
                /*case 0:
                    result = new BookFragment(0,mapCategoryModelByName.get(titlesList.get(position)),addressAreaIdsList.get(position),serviceTimesList.get(position));
                    break;
                case 1:
                    // First Fragment of Second Tab
                    result = new BookFragment(1);
                    break;
                case 2:
                    // First Fragment of Second Tab
                    result = new BookFragment(2);
                    break;*/
            default:
                result = new BookFragment(position, mapCategoryModelByName.get(titlesList.get(position)), addressAreaIdsList.get(0), serviceTimesList.get(position));
                break;
        }

        return result;
    }

    @Override
    public int getCount() {
        return titlesList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titlesList.get(position);
    }

}
