package com.vanitycube.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.fonts.VCTextView;
import com.vanitycube.model.BookingListModel;
import com.vanitycube.settings.ApplicationSettings;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BookingListAdapter extends BaseAdapter {
    Activity activity;
    private ArrayList<BookingListModel> allBookingsList;
    private BookingListAdapterInterface bookingListAdapterInterface;
    private Boolean isActive;

    public BookingListAdapter(Activity activity, ArrayList<BookingListModel> allBookingsList, Boolean isActive) {
        this.activity = activity;
        this.allBookingsList = allBookingsList;
        this.isActive = isActive;
    }

    //Listener when list gets updated
    public void setOnCancelBookingClickedListener(BookingListAdapterInterface bookingListAdapterInterface) {
        this.bookingListAdapterInterface = bookingListAdapterInterface;
    }

    public interface BookingListAdapterInterface {
        void onCancelBookingClicked(String bookingId);

        void onItemClicked(BookingListModel bookingListModel);
    }

    @Override
    public int getCount() {
        return allBookingsList.size();
    }

    @Override
    public Object getItem(int position) {
        return allBookingsList.get(position);
    }

    static class ViewHolder {
        CardView mainCardView;
        TextView bookingIdTextView, bookingAmountTextView, bookingDateTextView, bookingTimeTextView;
        VCTextView cancelBookingTextView,payNow;
        View dividerLine1;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        view = convertView;

        if (view == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            view = inflator.inflate(R.layout.booking_history_adapter_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.mainCardView = view.findViewById(R.id.mainCardView);
            viewHolder.bookingIdTextView = view.findViewById(R.id.bookingIdTextView);
            viewHolder.bookingAmountTextView = view.findViewById(R.id.bookingAmountTextView);
            viewHolder.bookingDateTextView = view.findViewById(R.id.bookingDateTextView);
            viewHolder.bookingTimeTextView = view.findViewById(R.id.bookingTimeTextView);
            viewHolder.cancelBookingTextView = view.findViewById(R.id.cancelBookingTextView);
            viewHolder.dividerLine1 = view.findViewById(R.id.dividerLine1);
            viewHolder.payNow = view.findViewById(R.id.payNow);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        try {
            final String bookingId = allBookingsList.get(position).getBookingID();
            final String bookingAmount = allBookingsList.get(position).getNetAmount();
            final String bookingDate = allBookingsList.get(position).getDate();
            final String bookingTime = allBookingsList.get(position).getBooktime();

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            float netBookingAmount = Float.parseFloat(bookingAmount);

            viewHolder.bookingIdTextView.setText(ApplicationSettings.BOOKING_ID_PREFIX.concat(bookingId));
            viewHolder.bookingAmountTextView.setText(String.valueOf(formatter.format(netBookingAmount)));
            viewHolder.bookingDateTextView.setText(getFormattedBookingDate(bookingDate));
            viewHolder.bookingTimeTextView.setText(bookingTime);

            if (isActive) {
                viewHolder.cancelBookingTextView.setVisibility(View.VISIBLE);
                viewHolder.cancelBookingTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (bookingListAdapterInterface != null) {
                            bookingListAdapterInterface.onCancelBookingClicked(bookingId);
                        }
                    }
                });
            }

            if (allBookingsList.get(position).getBookingStatus().equalsIgnoreCase("cancelled")) {
                viewHolder.dividerLine1.setBackgroundColor(ContextCompat.getColor(activity, R.color.red));
            } else if (allBookingsList.get(position).getBookingStatus().equalsIgnoreCase("done")) {
                viewHolder.dividerLine1.setBackgroundColor(ContextCompat.getColor(activity, R.color.SeaGreen));
            } else {
                viewHolder.dividerLine1.setBackgroundColor(ContextCompat.getColor(activity, R.color.vcOrange));
            }

            viewHolder.mainCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookingListAdapterInterface.onItemClicked(allBookingsList.get(position));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }


    private String getFormattedBookingDate(String inputBookingDate) {
        String outputBookingDate = "";

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat givenFormat = new SimpleDateFormat("yyyy-MM-dd");
            @SuppressLint("SimpleDateFormat") SimpleDateFormat requiredDateFormat = new SimpleDateFormat("dd MMM, yyyy");
            Date date = givenFormat.parse(inputBookingDate);
            if (date != null) {
                outputBookingDate = requiredDateFormat.format(date);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputBookingDate;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
