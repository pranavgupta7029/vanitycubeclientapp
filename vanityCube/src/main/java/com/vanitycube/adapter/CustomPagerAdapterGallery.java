package com.vanitycube.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.vanitycube.R;

// Created by prade on 12/26/2017.

public class CustomPagerAdapterGallery extends PagerAdapter {
    private LayoutInflater mLayoutInflater;
    private int[] sliderImagesList;

    public CustomPagerAdapterGallery(Context context, int[] sliderImagesList) {
        this.sliderImagesList = sliderImagesList;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return sliderImagesList.length;
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.gallery_image_page, container, false);
        ImageView imageView = itemView.findViewById(R.id.galleryImage);
        imageView.setImageResource(sliderImagesList[position]);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        try {
            container.removeView((RelativeLayout) object);
        } catch (Exception ex) {

        }
    }
}