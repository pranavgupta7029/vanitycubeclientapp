package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.SearchAreaModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchAreaAdapter extends BaseAdapter implements OnClickListener, Filterable {
    Activity context;
    private List<SearchAreaModel> addactivities;
    private List<SearchAreaModel> filteredActivities;

    private SearchFilter mFilter = new SearchFilter();

    public SearchAreaAdapter(Activity context, List<SearchAreaModel> activities) {
        this.context = context;
        addactivities = activities;
        filteredActivities = activities;
    }

    @Override
    public int getCount() {
        return filteredActivities.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredActivities.get(position);
    }

    static class ViewHolder {
        TextView textViewName;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        view = convertView;
        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.search_area_adapter_row, null);

            viewHolder = new ViewHolder();
            viewHolder.textViewName = (TextView) view
                    .findViewById(R.id.search_area);
            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.textViewName.setText(filteredActivities.get(position).getName());

        return view;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private class SearchFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String searchString = constraint.toString().toLowerCase(Locale.getDefault());

            FilterResults results = new FilterResults();
            final ArrayList<SearchAreaModel> contactList = (ArrayList<SearchAreaModel>) addactivities;
            int count = contactList.size();
            final ArrayList<SearchAreaModel> resultVector = new ArrayList<SearchAreaModel>(count);
            SearchAreaModel filterableContact;

            for (int x = 0; x < contactList.size(); x++) {
                filterableContact = contactList.get(x);
                if ((filterableContact.getName() != null && filterableContact
                        .getName().toLowerCase(Locale.getDefault())
                        .startsWith(searchString))) {

                    resultVector.add(filterableContact);
                }
            }

            results.values = resultVector;
            results.count = resultVector.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredActivities = (ArrayList<SearchAreaModel>) results.values;
            notifyDataSetChanged();
        }
    }
}