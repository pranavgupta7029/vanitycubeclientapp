package com.vanitycube.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vanitycube.R;
import com.vanitycube.viewHelpers.PackageViewHolder;

import java.util.ArrayList;

/**
 * Created by Nitin on 25/10/17.
 */

public class PackageAdapter extends RecyclerView.Adapter<PackageViewHolder> {
    private ArrayList<String> list = new ArrayList<>();
    private Context context;
    private OnItemClickListener onItemClickListener;
    public static String serviceName = "";

    public PackageAdapter(Context context, ArrayList<String> Data, PackageAdapter.OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = Data;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(String name, Integer position, View mView);
    }

    @Override
    public PackageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_recycle_item, parent, false);
        return new PackageViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(final PackageViewHolder holder, int position) {
        try {
            //String url = list.get(position).getImgURL();
            holder.bind(list.get(position), position, onItemClickListener);
            // Picasso.with(context).load(url).into(holder.coverImageView);
            holder.coverImageView.setText(list.get(position));
            if (holder.coverImageView.getText().toString().equalsIgnoreCase(serviceName)) {
                holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_orange_20dp));
                holder.coverImageView.setTextColor(ContextCompat.getColor(context, R.color.white));
                //ContextCompat.getColor(getContext(),R.color.white)
            } else {
                //holder.coverImageView.setBackground(ContextCompat.getDrawable(context,R.drawable.rounded_text_view_20dp));
                holder.coverImageView.setTextColor(ContextCompat.getColor(context, R.color.white));

                if (position == 0)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_0));
                else if (position == 1)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_1));
                else if (position == 2)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_2));
                else if (position == 3)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_3));
                else if (position == 4)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_4));
                else if (position == 5)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_5));
                else if (position == 6)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_6));
                else if (position == 7)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_0));
                else if (position == 8)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_1));
                else if (position == 9)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_2));
                else if (position == 10)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_3));
                else if (position == 11)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_4));
                else if (position == 12)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_5));
                else if (position == 13)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_6));
                else if (position == 14)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_0));
                else if (position == 15)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_1));
                else if (position == 16)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_2));
                else if (position == 17)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_3));
                else if (position == 18)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_4));
                else if (position == 19)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_5));
                else if (position == 20)
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp_6));
                else
                    holder.coverImageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_text_view_20dp));
            }
            holder.coverImageView.setTag(list.get(position));
            holder.fLayout.setTag(list.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
