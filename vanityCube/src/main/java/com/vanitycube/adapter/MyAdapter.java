package com.vanitycube.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vanitycube.R;
import com.vanitycube.model.Offer;
import com.vanitycube.viewHelpers.MyViewHolder;

import java.util.ArrayList;

/**
 * Created by Nitin on 25/10/17.
 */

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {
    private ArrayList<Offer> list = new ArrayList<>();
    private Context context;
    private OnItemClickListener onItemClickListener;

    public MyAdapter(Context context, ArrayList<Offer> Data, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.list = Data;
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(Offer offerItem);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            String url = list.get(position).getImgURL();
            holder.bind(list.get(position), onItemClickListener);
            //Picasso.with(context).load(url).into(holder.coverImageView);
            holder.coverImageView.setText("Services");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
