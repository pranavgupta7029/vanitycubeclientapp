package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.activities.MultipleAddressActivity;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.UserAddressModel;

import java.util.ArrayList;

public class UserAddressListAdapter extends BaseAdapter implements OnClickListener {

    Activity context;
    private ArrayList<UserAddressModel> addactivities;
    private userAddressInterface mObjuserAddressInterface;
    private boolean isGuestUser;
    private SharedPref pref;
    private MultipleAddressActivity activity;

    public UserAddressListAdapter(Activity context, ArrayList<UserAddressModel> activities, userAddressInterface pObjuserAddressInterface, boolean isGuestUser, MultipleAddressActivity activity) {
        this.context = context;
        addactivities = activities;
        this.mObjuserAddressInterface = pObjuserAddressInterface;
        this.isGuestUser = isGuestUser;
        pref = new SharedPref(VcApplicationContext.getInstance());
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return addactivities.size();
    }

    @Override
    public Object getItem(int position) {
        return addactivities.get(position);
    }

    static class ViewHolder {
        TextView userNameTextView;
        TextView fullAddressTextView;
        ImageView updateAddressImageView;
        ImageView deleteAddressImageView;
        RadioButton radio;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;
        view = convertView;

        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.add_new_address_adapter_row, null);
            viewHolder = new ViewHolder();
            viewHolder.userNameTextView = view.findViewById(R.id.name_address_textview);
            viewHolder.fullAddressTextView = view.findViewById(R.id.full_address_textview);
            viewHolder.updateAddressImageView = view.findViewById(R.id.update_address_imageview);
            viewHolder.deleteAddressImageView = view.findViewById(R.id.delete_address_imageview);
            viewHolder.radio = view.findViewById(R.id.radio);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        //Hide in case of Guest User
        if (isGuestUser) viewHolder.updateAddressImageView.setVisibility(View.GONE);

        viewHolder.updateAddressImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UserAddressModel lObjUserAddressModel = addactivities.get(position);
                String areaId = lObjUserAddressModel.getAreaId();
                String area = lObjUserAddressModel.getAreaName();
                String landmark = lObjUserAddressModel.getLandmark();
                String address = lObjUserAddressModel.getAddress();
                String city = lObjUserAddressModel.getCityName();
                String state = lObjUserAddressModel.getStateName();
                String addressId = lObjUserAddressModel.getAddressId();
                mObjuserAddressInterface.updateAddress(state, city, area, areaId, landmark, address, addressId);
            }
        });

        viewHolder.deleteAddressImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                UserAddressModel lObjUserAddressModel = addactivities.get(position);
                String addressId = lObjUserAddressModel.getAddressId();
                mObjuserAddressInterface.removeAddress(addressId);
            }
        });

        UserAddressModel lObjUserAddressModel = addactivities.get(position);
        String username = MultipleAddressActivity.mUserName;
        String lFullAddress = lObjUserAddressModel.toString();
        viewHolder.userNameTextView.setText(username);
        viewHolder.fullAddressTextView.setText(lFullAddress);

        viewHolder.radio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (viewHolder.radio.isChecked()) {
                    activity.getChild(position);
                    UserAddressModel lObjUserAddressModel = addactivities.get(position);
                    String selectedCityId = lObjUserAddressModel.getCityId();
                    String currentSetCityId = pref.getCityId();

                    if (currentSetCityId.equals(selectedCityId)) {
                        activity.setUserAddressParams(addactivities, position);
                        activity.addressSelectedFlag = true;
                        viewHolder.radio.setChecked(true);
                    /*setUserAddressParams(mUserAddresses, position);
                    gotoBook();*/

                    } else {
                        Toast.makeText(context, "Please select an address with same city", Toast.LENGTH_SHORT).show();
                        viewHolder.radio.setChecked(false);
                    }
                }
            }
        });

        return view;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public void onClick(View v) {
    }

    public interface userAddressInterface {
        void updateAddress(String state, String city, String area, String areaId, String landmark, String address, String addressId);

        void removeAddress(String mAddressId);
    }

}