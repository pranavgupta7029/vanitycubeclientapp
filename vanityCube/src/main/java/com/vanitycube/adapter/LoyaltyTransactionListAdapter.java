
package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.loyalty.MemberTransactionResponseListDTO;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LoyaltyTransactionListAdapter extends BaseAdapter {
    Activity context;
    private List<MemberTransactionResponseListDTO> memberTransactionResponseListDTOList;
    private SimpleDateFormat slotDateFormat;
    private String mDateString = "";

    public LoyaltyTransactionListAdapter(Activity context, List<MemberTransactionResponseListDTO> memberTransactionResponseListDTOList) {
        this.context = context;
        this.memberTransactionResponseListDTOList = memberTransactionResponseListDTOList;

    }

    public List<MemberTransactionResponseListDTO> getBookingModelList() {
        return memberTransactionResponseListDTOList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return memberTransactionResponseListDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return memberTransactionResponseListDTOList.get(position);
    }

    // @Override
    // public long getItemId(int position) {
    // // TODO Auto-generated method stub
    // return allNotifications.get(position);
    // }

    static class ViewHolder {
        protected TextView srNo, bookingId, billDate, amount, redeemedPoints, accruedPoints;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view;
        ViewHolder viewHolder;
        view = convertView;
        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.loyalty_transaction_adapter, null);
            viewHolder = new ViewHolder();
            viewHolder.srNo = (TextView) view.findViewById(R.id.srNo);
            viewHolder.bookingId = (TextView) view.findViewById(R.id.bookingId);
            viewHolder.billDate = (TextView) view.findViewById(R.id.billDate);
            viewHolder.amount = (TextView) view.findViewById(R.id.amount);
            viewHolder.redeemedPoints = (TextView) view.findViewById(R.id.redeemedPoints);
            viewHolder.accruedPoints = (TextView) view.findViewById(R.id.accruedPoints);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        MemberTransactionResponseListDTO memberTransactionResponseListDTO = memberTransactionResponseListDTOList.get(position);
        Integer srNumber = position + 1;
        viewHolder.srNo.setText("" + srNumber);
        String bId = memberTransactionResponseListDTO.getFormattedBookingId();
        if (bId != null && !bId.equals(""))
            viewHolder.bookingId.setText(bId);
        else
            viewHolder.bookingId.setText("");
        slotDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat givenFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date lDate = null;
        try {
            if (memberTransactionResponseListDTO.getBillDate() != null)
                lDate = givenFormat.parse(memberTransactionResponseListDTO.getBillDate());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (lDate != null) {
            mDateString = slotDateFormat.format(lDate);
        }
        viewHolder.billDate.setText(mDateString);
        //viewHolder.amount.setText(memberTransactionResponseListDTO.getTotalBilledAmount());
        DecimalFormat formatter = new DecimalFormat("#,###,###");

        Double amount = 0.0;
        if (memberTransactionResponseListDTO.getTotalBilledAmount() != null)
            amount = Double.valueOf(memberTransactionResponseListDTO.getTotalBilledAmount());
        viewHolder.amount.setText(formatter.format(amount));
        viewHolder.redeemedPoints.setText(memberTransactionResponseListDTO.getTotalRedeemPoints());
        viewHolder.accruedPoints.setText(memberTransactionResponseListDTO.getTotalAccruedPoints());
        return view;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
