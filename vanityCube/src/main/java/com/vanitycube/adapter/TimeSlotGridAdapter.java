package com.vanitycube.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.fragments.BookFragment;
import com.vanitycube.model.TimeSlotModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TimeSlotGridAdapter extends BaseAdapter {
    private ArrayList<TimeSlotModel> timeSlots;
    private LayoutInflater mInflater;
    private Context context;
    private Map<String, String> mapOfTimes = new HashMap<>();
    private BookFragment bookFragment;

    private void createMap() {
        mapOfTimes.put("09:00 am", "09:00 - 09:30 am");
        mapOfTimes.put("09:30 am", "09:30 - 10:00 am");
        mapOfTimes.put("10:00 am", "10:00 - 10:30 am");
        mapOfTimes.put("10:30 am", "10:30 - 11:00 am");
        mapOfTimes.put("11:00 am", "11:00 - 11:30 am");
        mapOfTimes.put("11:30 am", "11:30 - 12:00 pm");
        mapOfTimes.put("12:00 pm", "12:00 - 12:30 pm");
        mapOfTimes.put("12:30 pm", "12:30 - 01:00 pm");
        mapOfTimes.put("01:00 pm", "01:00 - 01:30 pm");
        mapOfTimes.put("01:30 pm", "01:30 - 02:00 pm");
        mapOfTimes.put("02:00 pm", "02:00 - 02:30 pm");
        mapOfTimes.put("02:30 pm", "02:30 - 03:00 pm");
        mapOfTimes.put("03:00 pm", "03:00 - 03:30 pm");
        mapOfTimes.put("03:30 pm", "03:30 - 04:00 pm");
        mapOfTimes.put("04:00 pm", "04:00 - 04:30 pm");
        mapOfTimes.put("04:30 pm", "04:30 - 05:00 pm");
        mapOfTimes.put("05:00 pm", "05:00 - 05:30 pm");
        mapOfTimes.put("05:30 pm", "05:30 - 06:00 pm");
        mapOfTimes.put("06:00 pm", "06:00 - 06:30 pm");
        mapOfTimes.put("06:30 pm", "06:30 - 07:00 pm");
        mapOfTimes.put("07:00 pm", "07:00 - 07:30 pm");
        mapOfTimes.put("07:30 pm", "07:30 - 08:00 pm");
        mapOfTimes.put("08:00 pm", "08:30 - 08:30 pm");
        mapOfTimes.put("08:30 pm", "08:30 - 09:00 pm");
    }

    public TimeSlotGridAdapter(Context context, ArrayList<TimeSlotModel> timeSlots, BookFragment bookFragment) {
        this.context = context;
        this.timeSlots = timeSlots;
        this.bookFragment = bookFragment;
        createMap();
        mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return timeSlots.size();
    }

    @Override
    public Object getItem(int position) {
        return timeSlots.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.time_slot_grid, null);
        }

        TextView text = convertView.findViewById(R.id.dashText);
        String time = timeSlots.get(position).getSlotName();

        try {
            time = mapOfTimes.get(timeSlots.get(position).getSlotName()) != null ? mapOfTimes.get(timeSlots.get(position).getSlotName()) : mapOfTimes.get(timeSlots.get(position).getSlotName().toLowerCase());
        } catch (Exception ex) {
            time = timeSlots.get(position).getSlotName();
        }
        text.setText(time);
        text.setTag(time);

        //Pre selecting the first grid capsule
        if (position == 0) {
            try {
                text.setBackground(context.getDrawable(R.drawable.rounded_text_view_orange_rounded));
                text.setTextColor(ContextCompat.getColor(context, R.color.white));
                /*if(bookFragment.flagOfCalendarClick) {
                    bookFragment.setTimeText(text.getText().toString());
                    bookFragment.flagOfCalendarClick=false;
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return convertView;
    }

}