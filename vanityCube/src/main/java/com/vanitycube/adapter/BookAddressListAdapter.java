
package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.UserAddressModel;

import java.util.List;

public class BookAddressListAdapter extends BaseAdapter implements OnClickListener {
    Activity context;
    List<UserAddressModel> addactivities;
    //boolean[] isChecked;

    public BookAddressListAdapter(Activity context, List<UserAddressModel> activities) {
        this.context = context;
        addactivities = activities;
        //isChecked = new boolean[addactivities.size()];
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return addactivities.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return addactivities.get(position);
    }

    static class ViewHolder {
        protected TextView addressTextView;
        //protected RadioButton radioButton;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view;
        final ViewHolder viewHolder;
        view = convertView;
        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.book_address_adapter_row, null);

            viewHolder = new ViewHolder();
            viewHolder.addressTextView = (TextView) view
                    .findViewById(R.id.bookAddressTextView);
            //viewHolder.radioButton = (RadioButton) view.findViewById(R.id.bookAddressListRadioButton);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.addressTextView.setText(addactivities.get(position).getAddress());

//        if (isChecked[position]) {
//            viewHolder.radioButton.setChecked(true);
//        } else {
//            viewHolder.radioButton.setChecked(true);
//        }
//
//        viewHolder.radioButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                if (viewHolder.radioButton.isChecked()) {
//                    isChecked = new boolean[addactivities.size()];
//                    isChecked[position] = true;
//                }
//                else {
//                    isChecked[position] = false;
//                }
//                notifyDataSetChanged();
//            }
//        });

        return view;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    //    public boolean[] getIsCheckedArray() {
//        return isChecked;
//    }
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {

            default:
                break;
        }
    }

}
