package com.vanitycube.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.fonts.VCTextView;
import com.vanitycube.model.CategoryModel;
import com.vanitycube.model.TimeSlotModel;

import java.util.ArrayList;
import java.util.Calendar;

// Created by prade on 11/30/2017.

public class BookAdapter extends BaseAdapter {
    private Activity context;
    private String categoryDate;
    private String categoryTime;
    private String mTimeId;
    private ArrayList<CategoryModel> categoriesList;
    private ArrayList<TimeSlotModel> timeSlots;
    private ArrayList<Boolean> timeSlotsSelected;
    private BookAdapterInterface bookAdapterInterface;
    private int lastSpinnerItemSelected;


    public BookAdapter(Activity context, ArrayList<CategoryModel> categoriesList, BookAdapterInterface bookAdapterInterface) {
        this.context = context;
        this.categoriesList = categoriesList;
        this.bookAdapterInterface = bookAdapterInterface;
        this.timeSlotsSelected = new ArrayList<>();
    }

    public void setCategoriesList(ArrayList<CategoryModel> categoriesList) {
        this.categoriesList = categoriesList;
        timeSlotsSelected = new ArrayList<>();
    }

    static class ViewHolder {
        RelativeLayout dateTimeLayout;
        RelativeLayout errorLayout;
        VCTextView categoryName, errorTextView;
        EditText datePicker;
        Spinner timePicker;
    }

    public interface BookAdapterInterface {
        void onListUpdated(String date, int index);

        void onAllFieldsSet(String time, String timeId, int index);
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }


    /*  -------------------------------------
        |
        |   GET VIEW
        |
        -------------------------------------
    */

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        final int rowPos = position;

        View view = convertView;

        String categoryName;
        String categoryMessage;

        timeSlots = new ArrayList<>();
        ArrayList<String> timeSlotStringsList = new ArrayList<>();

        timeSlotStringsList.add("Select Time");

        lastSpinnerItemSelected = 0;

        if (view == null) {
            viewHolder = new ViewHolder();

            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.book_adapter_row, parent, false);
            viewHolder.dateTimeLayout = view.findViewById(R.id.dateTimeLayout);
            viewHolder.errorLayout = view.findViewById(R.id.errorLayout);
            viewHolder.errorTextView = viewHolder.errorLayout.findViewById(R.id.errorTextView);
            viewHolder.categoryName = view.findViewById(R.id.categoryName);
            viewHolder.datePicker = view.findViewById(R.id.categoryDate);
            viewHolder.timePicker = view.findViewById(R.id.categoryTime);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        categoryName = categoriesList.get(rowPos).getCategoryName();
        categoryMessage = categoriesList.get(rowPos).getMessage();
        categoryDate = categoriesList.get(rowPos).getCategoryDate();
        categoryTime = categoriesList.get(rowPos).getCategoryTime();
        timeSlots = categoriesList.get(rowPos).getTimeSlots();

        viewHolder.categoryName.setText(categoryName);
        viewHolder.datePicker.setText(categoryDate);

        for (TimeSlotModel tsm : timeSlots) {
            timeSlotStringsList.add(tsm.getSlotName());
        }

        /*  -------------------------------------
            |   SET THE DATE PICKER LISTENER HERE
            ------------------------------------- */

        viewHolder.datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                long currentTimeInMillis = System.currentTimeMillis();
                long maxDateValue = currentTimeInMillis + 10368000000L;
                int mDay = calendar.get(Calendar.DAY_OF_MONTH);
                int mMonth = calendar.get(Calendar.MONTH);
                int mYear = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String monthYr = monthOfYear + 1 + "";
                                String dayOfMon = dayOfMonth + "";
                                if (monthOfYear <= 9) {
                                    monthYr = "0" + monthYr;
                                }

                                if (dayOfMonth <= 9) {
                                    dayOfMon = "0" + dayOfMon;
                                }
                                categoryDate = String.valueOf(year + "").concat("-").concat(monthYr).concat("-").concat(dayOfMon);
                                viewHolder.datePicker.setText(categoryDate);
                                bookAdapterInterface.onListUpdated(categoryDate, rowPos);
                            }
                        }, mYear, mMonth, mDay);

                datePickerDialog.getDatePicker().setMinDate(currentTimeInMillis);
                datePickerDialog.getDatePicker().setMaxDate(maxDateValue);
                datePickerDialog.show();
            }
        });

        /*  -------------------------------------
            |   SET THE SPINNER HERE
            ------------------------------------- */
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, timeSlotStringsList) {

            @NonNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(ContextCompat.getColor(context, R.color.vcDarkGrey));
                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                Typeface tf = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.otf");
                ((TextView) v).setTypeface(tf);
                return v;
            }

            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) v;
                tv.setTextColor(ContextCompat.getColor(context, R.color.vcDarkGrey));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                return v;
            }
        };

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewHolder.timePicker.setAdapter(dataAdapter);

        //Pre select timeslot if already there
        if (categoryTime != null && !categoryTime.equals("")) {
            int index = 0;
            timeSlotsSelected.add(true);

            for (String s : timeSlotStringsList) {
                if (s.equals(categoryTime)) {
                    viewHolder.timePicker.setSelection(index);
                    break;
                }
                index++;
            }
        } else {
            timeSlotsSelected.add(false);
        }

        viewHolder.timePicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (timeSlotsSelected != null && timeSlotsSelected.size() >= rowPos && timeSlotsSelected.get(rowPos)) {
                        timeSlotsSelected.set(rowPos, false);
                    } else {
                        if (lastSpinnerItemSelected == position) {
                            bookAdapterInterface.onAllFieldsSet("selectTime", "0", rowPos);
                            return;
                        } else {
                            lastSpinnerItemSelected = position;

                            if (position > 0) {
                                timeSlots = categoriesList.get(rowPos).getTimeSlots();
                                categoryTime = timeSlots.get(position - 1).getSlotName();
                                mTimeId = timeSlots.get(position - 1).getSlotId();
                                bookAdapterInterface.onAllFieldsSet(categoryTime, mTimeId, rowPos);
                            }
                        }
                    }

                    // Set text style
                    TextView tv = (TextView) parent.getChildAt(0);
                    tv.setTextColor(ContextCompat.getColor(context, R.color.vcDarkGrey));
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                } catch (Exception ex) {
                    Log.e("BOOKADAPTER", ex.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (isValidString(categoryMessage)) {
            viewHolder.datePicker.setVisibility(View.GONE);
            viewHolder.timePicker.setVisibility(View.GONE);
            viewHolder.errorLayout.setVisibility(View.VISIBLE);
            viewHolder.errorTextView.setText(categoryMessage);
        }

        return view;
    }

    @Override
    public int getCount() {
        return categoriesList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoriesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
