package com.vanitycube.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vanitycube.R;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.settings.ApplicationSettings;

import java.util.ArrayList;

/**
 * Created by Nitin on 25/10/17.
 */

public class MainSplashAdapter extends BaseAdapter {
    private Context mContext;
    //private final List<Item> mItems = new ArrayList<Item>();
    private final LayoutInflater mInflater;
    private ArrayList<ServiceModel> allServicesWithServiceTypesList;

    public MainSplashAdapter(Context context, ArrayList<ServiceModel> allServicesWithServiceTypesList) {
        mInflater = LayoutInflater.from(context);
        this.allServicesWithServiceTypesList = allServicesWithServiceTypesList;
        mContext = context;

        /*mItems.add(new Item("BLEACH",       R.drawable.bleach));
        mItems.add(new Item("BRIDAL",   R.drawable.bridal));
        mItems.add(new Item("FACIAL", R.drawable.facial));
        mItems.add(new Item("Hair & Chemical",      R.drawable.hair_and_chamical));
        mItems.add(new Item("HAIR",     R.drawable.hair));
        mItems.add(new Item("MAKEUP",      R.drawable.makeup));
        mItems.add(new Item("MASSAGE",      R.drawable.massage));
        mItems.add(new Item("MENS",      R.drawable.mens));
        mItems.add(new Item("NAILS",      R.drawable.nails));
        mItems.add(new Item("PRODUCTS",      R.drawable.products));
        mItems.add(new Item("THREADING",      R.drawable.threading));
        mItems.add(new Item("WAX",      R.drawable.wax));*/
    }

    @Override
    public int getCount() {
        return allServicesWithServiceTypesList.size();
    }

    @Override
    public ServiceModel getItem(int i) {
        return allServicesWithServiceTypesList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture, newBox;
        TextView name;

        if (v == null) {
            v = mInflater.inflate(R.layout.media_adapter, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.textName, v.findViewById(R.id.textName));
            v.setTag(R.id.newBox, v.findViewById(R.id.newBox));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.textName);
        newBox = (ImageView) v.getTag(R.id.newBox);

        ServiceModel item = getItem(i);
        String url = ApplicationSettings.BASE_URL_PAY + item.getServiceImage();

        //picture.setImageResource(item.drawableId);
        Picasso.with(mContext).load(url).into(picture);
        name.setText(item.getServiceName());
        if (item.getServiceName().contains("Weight Loss") || item.getServiceName().contains("weight loss")) {
            newBox.setVisibility(View.VISIBLE);
        } else {
            newBox.setVisibility(View.GONE);
        }

        return v;
    }

    private static class Item {
        public final String name;
        public final int drawableId;

        Item(String name, int drawableId) {
            this.name = name;
            this.drawableId = drawableId;
        }
    }


}
