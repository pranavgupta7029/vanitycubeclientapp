package com.vanitycube.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.model.ServiceTypeModel;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class BookingSummaryAdapter extends BaseAdapter {
    Activity context;
    private ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes;
    private OnListUpdatedListener mOnListUpdatedListener;
    private SharedPref pref;
    private final DecimalFormat formatter = new DecimalFormat("#,###,###");

    //Listener when list gets updated
    public void setOnListUpdatedListener(OnListUpdatedListener onListUpdatedListener) {
        mOnListUpdatedListener = onListUpdatedListener;
    }

    public interface OnListUpdatedListener {
        void onListUpdated();
    }

    public BookingSummaryAdapter(Activity context, ArrayList<ServiceTypeModel> finalSelectedServiceTypes) {
        this.context = context;
        pref = new SharedPref(context);
        mFinalSelectedServiceTypes = finalSelectedServiceTypes;
    }

    public void setServicesList(ArrayList<ServiceTypeModel> finalSelectedServiceTypes) {
        mFinalSelectedServiceTypes = finalSelectedServiceTypes;
    }

    static class ViewHolder {
        TextView serviceName;
        TextView serviceAmount, bookListViewTotalAmount, bookListViewTime, rupeeSymbol;
        ImageView remove, plus, minus;
        protected TextView number;
    }

    /*  -------------------------------------
        |
        |   UPDATING LIST DATA
        |
        -------------------------------------
    */

    private int inList(ArrayList<ServiceTypeModel> list, ServiceTypeModel object) {
        int returnIndex = -1;
        int iterateIndex = 0;

        for (ServiceTypeModel serviceTypeObj : list) {
            if (serviceTypeObj.getServiceTypeID().equalsIgnoreCase(object.getServiceTypeID())) {
                returnIndex = iterateIndex;
                break;
            }

            iterateIndex++;
        }

        return returnIndex;
    }

    private void setFinalSelectedServiceTypesList() {
        pref.putCartData(mFinalSelectedServiceTypes);

        if (mOnListUpdatedListener != null) {
            mOnListUpdatedListener.onListUpdated();
        }
    }

    private void removeFromList(int position) {
        mFinalSelectedServiceTypes.remove(position);
    }

    private void updateFinalSelectedServiceTypesList(String mode, ServiceTypeModel serviceTypeModel) {
        int numOfPeople;
        int index = inList(mFinalSelectedServiceTypes, serviceTypeModel);

        if (mode.equalsIgnoreCase("increase")) {

            if (index == -1) {
                serviceTypeModel.setNumOfPeople("1");
                mFinalSelectedServiceTypes.add(serviceTypeModel);
            } else {
                numOfPeople = Integer.parseInt(mFinalSelectedServiceTypes.get(index).getNumOfPeople());
                numOfPeople += 1;
                mFinalSelectedServiceTypes.get(index).setNumOfPeople(String.valueOf(numOfPeople));
            }

            Toast.makeText(context, "Service Added", Toast.LENGTH_LONG).show();

        } else if (mode.equalsIgnoreCase("decrease")) {

            if (index != -1) {
                numOfPeople = Integer.parseInt(mFinalSelectedServiceTypes.get(index).getNumOfPeople());

                if (numOfPeople >= 1) {
                    numOfPeople -= 1;

                    if (numOfPeople == 0) {
                        removeFromList(index);
                    } else {
                        mFinalSelectedServiceTypes.get(index).setNumOfPeople(String.valueOf(numOfPeople));
                    }
                } else {
                    mFinalSelectedServiceTypes.remove(index);
                }
            }

        }

        setFinalSelectedServiceTypesList();
    }

    /*  -------------------------------------
        |
        |   GET VIEW
        |
        -------------------------------------
    */

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        view = convertView;
        final ViewHolder viewHolder;

        //Get service type model
        final ServiceTypeModel serviceType = mFinalSelectedServiceTypes.get(position);
        final int numOfPeople = Integer.parseInt(mFinalSelectedServiceTypes.get(position).getNumOfPeople());
        int serviceAmount = 0;
        if (mFinalSelectedServiceTypes.get(position).getFinalAmount() != null)
            serviceAmount = Integer.parseInt(mFinalSelectedServiceTypes.get(position).getFinalAmount());
        final String serviceTypeName = mFinalSelectedServiceTypes.get(position).getServiceTypeName();
        final String serviceTypeDescription = mFinalSelectedServiceTypes.get(position).getDescription();

        int discount = 0;
        if (serviceType != null && serviceType.getDiscount() != null) {
            discount = Integer.valueOf(serviceType.getDiscount());
        }
        int initialAmount = 0;
        if (serviceType != null && serviceType.getPrice() != null)
            initialAmount = Integer.valueOf(serviceType.getPrice());
        int finalAmount = 0;
        if (serviceType != null && serviceType.getFinalAmount() != null)
            finalAmount = Integer.valueOf(serviceType.getFinalAmount());

        int membersPrice = 0;
        int percentage = 0;
        if (serviceType.getPercentageForMembers() != null)
            percentage = Integer.parseInt(serviceType.getPercentageForMembers());

        if (percentage == 0) {
            percentage = 5;
            serviceType.setPercentageForMembers(5 + "");
        }

        if (discount > 0) {

            if (serviceType.getFinalAmount() != null) {
                finalAmount = Integer.valueOf(serviceType.getFinalAmount());
            } else {
                finalAmount = initialAmount - discount;
            }
            membersPrice = finalAmount - (finalAmount * percentage / 100);

        } else {
            if (serviceType.getFinalAmount() != null) {
                finalAmount = Integer.valueOf(serviceType.getFinalAmount());
            } else {
                finalAmount = initialAmount;
            }
            membersPrice = finalAmount - (finalAmount * percentage / 100);
        }

        if (pref.getIsUserMember()) {
            finalAmount = membersPrice;
            if (serviceType.getMembersPrice() != null && !serviceType.getMembersPrice().equals("")) {

            } else {
                serviceType.setMembersPrice(membersPrice + "");
            }
        }

        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.booking_summary_adapter_row, parent, false);

            viewHolder = new ViewHolder();

            viewHolder.serviceName = view.findViewById(R.id.serviceNameTextView);
            viewHolder.serviceAmount = view.findViewById(R.id.serviceFinalAmountTextView);
            viewHolder.remove = view.findViewById(R.id.serviceDeleteButton);
            viewHolder.minus = view.findViewById(R.id.minusButton);
            viewHolder.number = view.findViewById(R.id.serviceQuantity);
            viewHolder.plus = view.findViewById(R.id.plusButton);
            viewHolder.bookListViewTime = view.findViewById(R.id.bookListViewTime);
            viewHolder.bookListViewTotalAmount = view.findViewById(R.id.bookListViewTotalAmount);
            viewHolder.rupeeSymbol = view.findViewById(R.id.rupeeSymbol);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if (mFinalSelectedServiceTypes != null && mFinalSelectedServiceTypes.get(position) != null && mFinalSelectedServiceTypes.get(position).getCategoryId() != null) {
            if (mFinalSelectedServiceTypes.get(position).getCategoryId().equalsIgnoreCase("6")) {
                viewHolder.serviceName.setText(serviceTypeDescription);
            } else {
                if(viewHolder.serviceName!=null)
                    viewHolder.serviceName.setText(serviceTypeName);
            }
        } else {
            if(viewHolder.serviceName!=null)
                viewHolder.serviceName.setText(serviceTypeName);
        }

        viewHolder.serviceAmount.setText(String.valueOf(formatter.format(serviceAmount)));

        viewHolder.number.setText(String.valueOf(numOfPeople));

        viewHolder.remove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFromList(position);
                setFinalSelectedServiceTypesList();
            }
        });

        //Plus button
        viewHolder.plus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFinalSelectedServiceTypesList("increase", serviceType);
            }
        });

        //Minus button
        viewHolder.minus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                updateFinalSelectedServiceTypesList("decrease", serviceType);
            }
        });


        if (discount > 0) {
            viewHolder.bookListViewTotalAmount.setVisibility(View.VISIBLE);
            viewHolder.bookListViewTotalAmount.setText(formatter.format(initialAmount));
            viewHolder.bookListViewTotalAmount.setPaintFlags(viewHolder.bookListViewTotalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.bookListViewTotalAmount.setTextColor(ContextCompat.getColor(context, R.color.red));
            viewHolder.serviceAmount.setText(formatter.format(finalAmount));
            viewHolder.serviceAmount.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
            viewHolder.rupeeSymbol.setTextColor(ContextCompat.getColor(context, R.color.red));

        } else {
            viewHolder.bookListViewTotalAmount.setVisibility(View.GONE);
            viewHolder.serviceAmount.setText(formatter.format(finalAmount));
            viewHolder.serviceAmount.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
            viewHolder.rupeeSymbol.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
        }

        //Set time
        viewHolder.bookListViewTime.setText(serviceType.getTime());

        return view;
    }

    @Override
    public int getCount() {
        return mFinalSelectedServiceTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return mFinalSelectedServiceTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
