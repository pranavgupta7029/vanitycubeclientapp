
package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vanitycube.R;
import com.vanitycube.model.NotificationModel;

import java.util.List;

public class NotificationAdapter extends BaseAdapter {
    Activity context;
    private List<NotificationModel> allNotifications;

    public NotificationAdapter(Activity context, List<NotificationModel> notifications) {
        this.context = context;
        allNotifications = notifications;

    }

    public List<NotificationModel> getBookingModelList() {
        return allNotifications;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return allNotifications.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return allNotifications.get(position);
    }

    // @Override
    // public long getItemId(int position) {
    // // TODO Auto-generated method stub
    // return allNotifications.get(position);
    // }

    static class ViewHolder {
        protected TextView notificationTitle;
        protected TextView notificationMessage;
        protected ImageView imgView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view;
        ViewHolder viewHolder;
        view = convertView;
        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.notification_adapter_row, null);
            viewHolder = new ViewHolder();
            viewHolder.notificationTitle = (TextView) view.findViewById(R.id.notificationTitle);
            viewHolder.notificationMessage = (TextView) view.findViewById(R.id.notificationMessage);
            viewHolder.imgView = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.notificationTitle.setText(allNotifications.get(position).getTitle());
        viewHolder.notificationMessage.setText(allNotifications.get(position).getMessage());
        String url = allNotifications.get(position).getPicUrl();
        if(url!=null && !url.equalsIgnoreCase("")) {
            viewHolder.imgView.setVisibility(View.VISIBLE);
            Picasso.with(context).load(url).into(viewHolder.imgView);
        }
        else{
            if(viewHolder.imgView!=null)
                viewHolder.imgView.setVisibility(View.GONE);
        }


        return view;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
