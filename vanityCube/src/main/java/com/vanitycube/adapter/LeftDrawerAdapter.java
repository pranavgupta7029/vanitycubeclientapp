package com.vanitycube.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.activities.InviteFriendsActivity;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.NavigationDrawerItem;

import java.util.ArrayList;

public class LeftDrawerAdapter extends BaseAdapter {
    private Activity mContext;
    private LayoutInflater mInflater;
    private ArrayList<NavigationDrawerItem> mItems;
    private int[] visibility;

    public LeftDrawerAdapter(Activity context, ArrayList<NavigationDrawerItem> items) {
        mContext = context;
        mItems = items;
        visibility = new int[items.size()];
        mInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int x = 0; x < visibility.length; x++) {
            visibility[x] = View.GONE;
        }
    }

    public void setVisible(boolean isLoggedIn) {
        if (!isLoggedIn) visibility[3] = View.VISIBLE;
        else visibility[5] = View.VISIBLE;
    }

    public void setInVisible(boolean isLoggedIn) {
        if (!isLoggedIn) visibility[3] = View.GONE;
        else visibility[5] = View.GONE;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.left_drawer_element, null);
        }

        ImageView icon = convertView.findViewById(R.id.drawerlistimage);
        final TextView text = convertView.findViewById(R.id.draweroption);

        ImageView whatsApp = convertView.findViewById(R.id.whatsappImage);
        ImageView message = convertView.findViewById(R.id.messageImage);

        icon.setColorFilter(ContextCompat.getColor(mContext, R.color.vcDarkGrey), PorterDuff.Mode.SRC_ATOP);

        //Whatsapp action
        whatsApp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteWhatsApp();
            }
        });

        //Message action
        message.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent messageShareActivity = new Intent(mContext, InviteFriendsActivity.class);
                mContext.startActivity(messageShareActivity);
            }
        });

        LinearLayout inviteLayout = convertView.findViewById(R.id.inviteLayout);
        inviteLayout.setVisibility(visibility[position]);

        text.setTag(position);
        icon.setBackgroundResource(mItems.get(position).getIcon());
        text.setText(("   ").concat(mItems.get(position).getName()));

        return convertView;
    }

    private void inviteWhatsApp() {
        Intent waIntent = new Intent(Intent.ACTION_SEND);
        waIntent.setType("text/plain");
        waIntent.setPackage("com.whatsapp");

        String urlstr = "https://goo.gl/d8BPY3";

        String message = "<html>Download VanityCube On demand professional home salon app and get Rs 400 off " +
                "on your first service. \nWe wish you all beautiful things. \n <a href="
                + urlstr + ">\n Download VanityCube:  </a>" + urlstr + " </html>";
        message = getConfiguration();


        waIntent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(message).toString());
        mContext.startActivity(Intent.createChooser(waIntent, "Share with"));
    }

    private String getConfiguration() {
        String inviteMessage = "";
        SharedPref pref = new SharedPref(VcApplicationContext.getInstance());
        Gson gson = new Gson();
        String json = pref.getConfiguration();
        ConfigurationModel configurationModel = gson.fromJson(json, new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            inviteMessage = configurationModel.getInviteMessage();
        }
        return inviteMessage;
    }

}
