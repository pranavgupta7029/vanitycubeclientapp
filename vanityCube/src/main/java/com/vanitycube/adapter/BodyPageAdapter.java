package com.vanitycube.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.vanitycube.R;
import com.vanitycube.activities.DashboardActivity;
import com.vanitycube.activities.SignInPageActivity;
import com.vanitycube.activities.WebPageActivity;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.fragments.DashboardFragment;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.utilities.FireBaseHelper;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BodyPageAdapter extends BaseAdapter implements Filterable {
    private Activity context;
    private ArrayList<ServiceTypeModel> serviceTypesList;
    private ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes;
    private OnListUpdatedListener mOnListUpdatedListener;
    private ArrayList<ServiceTypeModel> mDisplayedValues;    // Values to be displayed
    public SharedPref pref;
    private ArrayList<ServiceTypeModel> mAllServices;

    //For BodyPageModel type of services
    public BodyPageAdapter(Activity context, ArrayList<ServiceTypeModel> serviceTypesList, ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes, ArrayList<ServiceTypeModel> mAllServices) {
        pref = new SharedPref(VcApplicationContext.getInstance());
        this.context = context;
        this.serviceTypesList = serviceTypesList;
        this.mFinalSelectedServiceTypes = mFinalSelectedServiceTypes;
        mDisplayedValues = serviceTypesList;
        this.mAllServices = mAllServices;
    }

    //Listener when list gets updated
    public void setOnListUpdatedListener(OnListUpdatedListener onListUpdatedListener) {
        mOnListUpdatedListener = onListUpdatedListener;
    }

    public interface OnListUpdatedListener {
        void onListUpdated();
    }

    public void setFinalSelectedServiceTypesList(ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes) {
        this.mFinalSelectedServiceTypes = mFinalSelectedServiceTypes;
    }

    public void setServiceTypesList(ArrayList<ServiceTypeModel> serviceTypesList) {
        this.serviceTypesList = serviceTypesList;
    }

    public ArrayList<ServiceTypeModel> getServiceTypesList() {
        return this.serviceTypesList;
    }

    /*  -------------------------------------
        |
        |   UPDATING LIST DATA
        |
        -------------------------------------
    */

    private int inList(ArrayList<ServiceTypeModel> list, ServiceTypeModel object) {
        int returnIndex = -1;
        int iterateIndex = 0;

        for (ServiceTypeModel serviceTypeObj : list) {
            if (serviceTypeObj.getServiceTypeID().equalsIgnoreCase(object.getServiceTypeID())) {
                returnIndex = iterateIndex;
                break;
            }

            iterateIndex++;
        }

        return returnIndex;
    }

    private void updateCartData() {

        if (mFinalSelectedServiceTypes.size() >= 0)
            pref.putCartData(mFinalSelectedServiceTypes);

        if (mOnListUpdatedListener != null) {
            mOnListUpdatedListener.onListUpdated();
        }
    }

    private void removeFromList(int position) {
        mFinalSelectedServiceTypes.remove(position);
    }

    private void logCart(String mode, ServiceTypeModel serviceTypeModel) {
        AppEventsLogger logger;
        logger = AppEventsLogger.newLogger(context);
        Bundle parameters = new Bundle();
        parameters.putString("SERVICE_SELECTED", serviceTypeModel.getServiceTypeName());
        parameters.putString("SERVICE_ID", serviceTypeModel.getServiceID());
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        parameters.putString("Date", date);
        logger.logEvent("EVENT_ADD_TO_CART", parameters);
        parameters = new Bundle();

        parameters.putString(FirebaseAnalytics.Param.ITEM_NAME, serviceTypeModel.getServiceTypeName());
        parameters.putString(FirebaseAnalytics.Param.ITEM_ID, serviceTypeModel.getServiceID());
        logGoogleEvents(FirebaseAnalytics.Event.ADD_TO_CART, parameters);

    }

    public void logGoogleEvents(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(context);
            firebaseHelper.logEvents(eventName, bundle);
        } catch (Exception ex) {
            Log.e("PackageSelected", ex.getMessage());
        }
    }

    private void updateFinalSelectedServiceTypesList(String mode, ServiceTypeModel serviceTypeModel) {
        logCart(mode, serviceTypeModel);
        int numOfPeople;
        int index = inList(mFinalSelectedServiceTypes, serviceTypeModel);

        if (mode.equalsIgnoreCase("increase")) {

            if (index == -1) {
                serviceTypeModel.setNumOfPeople("1");
                mFinalSelectedServiceTypes.add(serviceTypeModel);
            } else {
                numOfPeople = Integer.parseInt(mFinalSelectedServiceTypes.get(index).getNumOfPeople());
                numOfPeople += 1;
                mFinalSelectedServiceTypes.get(index).setNumOfPeople(String.valueOf(numOfPeople));
            }

            Toast.makeText(context, "Your Services", Toast.LENGTH_LONG).show();

        } else if (mode.equalsIgnoreCase("decrease")) {

            if (index != -1) {
                numOfPeople = Integer.parseInt(mFinalSelectedServiceTypes.get(index).getNumOfPeople());

                if (numOfPeople >= 1) {
                    numOfPeople -= 1;

                    if (numOfPeople == 0) {
                        removeFromList(index);
                    } else {
                        mFinalSelectedServiceTypes.get(index).setNumOfPeople(String.valueOf(numOfPeople));
                    }
                } else {
                    mFinalSelectedServiceTypes.remove(index);
                }
            }

        }

        updateCartData();
    }

    /*  -------------------------------------
        |
        |   GET VIEW
        |
        -------------------------------------
    */

    static class ViewHolder {
        TextView textViewName, textQuantitiy, viewCart, headingtext;
        TextView textviewAmount;
        TextView textViewTotalAmount;
        TextView textviewTime, descriptionServices, rupeeSymbol, memberPrice, memberPriceName, rupees;
        ImageView plus, minus, carotDown;
        LinearLayout lenDescription;
        RelativeLayout relLayout, add, plusMinus, heading, memPricerel;
        CardView cardViewAdapter;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;
        view = convertView;

        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.body_page_adapter_row, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.textViewName = view.findViewById(R.id.bookListViewName);
            viewHolder.textviewAmount = view.findViewById(R.id.bookListViewAmount);
            viewHolder.textViewTotalAmount = view.findViewById(R.id.bookListViewTotalAmount);
            viewHolder.textviewTime = view.findViewById(R.id.bookListViewTime);
            viewHolder.plus = view.findViewById(R.id.plusSign);
            viewHolder.minus = view.findViewById(R.id.minusSign);
            viewHolder.descriptionServices = view.findViewById(R.id.descriptionServices);
            viewHolder.carotDown = view.findViewById(R.id.carotDown);
            viewHolder.lenDescription = view.findViewById(R.id.lenDescription);
            viewHolder.relLayout = view.findViewById(R.id.relLayout);
            viewHolder.viewCart = view.findViewById(R.id.viewCartButton);
            viewHolder.plusMinus = view.findViewById(R.id.plusMinus);
            viewHolder.textQuantitiy = view.findViewById(R.id.textQuantitiy);
            viewHolder.add = view.findViewById(R.id.add);
            viewHolder.cardViewAdapter = view.findViewById(R.id.cardView);
            viewHolder.heading = view.findViewById(R.id.heading);
            viewHolder.headingtext = view.findViewById(R.id.headingtext);
            viewHolder.rupeeSymbol = view.findViewById(R.id.rupeeSymbol);
            viewHolder.memberPrice = view.findViewById(R.id.membersPrice);
            viewHolder.memberPriceName = view.findViewById(R.id.memberPriceName);
            viewHolder.rupees = view.findViewById(R.id.rupees);
            viewHolder.memPricerel = view.findViewById(R.id.memPricerel);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        DecimalFormat formatter = new DecimalFormat("#,###,###");

        final ServiceTypeModel serviceType = serviceTypesList.get(position);
        final String serviceTypeName = serviceType.getServiceTypeName();
        int discount = 0;
        if (serviceType != null && serviceType.getDiscount() != null) {
            discount = Integer.valueOf(serviceType.getDiscount());
        }
        int initialAmount = 0;
        if (serviceType != null && serviceType.getPrice() != null)
            initialAmount = Integer.valueOf(serviceType.getPrice());
        int finalAmount = 0;
        if (serviceType != null && serviceType.getFinalAmount() != null)
            finalAmount = Integer.valueOf(serviceType.getFinalAmount());


        int membersPrice = 0;
        int percentage = Integer.parseInt(serviceType.getPercentageForMembers());

        if (discount > 0) {

            if (serviceType.getFinalAmount() != null) {
                finalAmount = Integer.valueOf(serviceType.getFinalAmount());
            } else {
                finalAmount = initialAmount - discount;
            }
            membersPrice = finalAmount - (finalAmount * percentage / 100);

        } else {
            //membersPrice = finalAmount-(finalAmount*percentage/100);
            if (serviceType.getFinalAmount() != null) {
                finalAmount = Integer.valueOf(serviceType.getFinalAmount());
            } else {
                finalAmount = initialAmount;
            }
            membersPrice = finalAmount - (finalAmount * percentage / 100);
        }

        if (!pref.getIsUserMember()) {

            viewHolder.memPricerel.setClickable(true);
            viewHolder.memPricerel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {

                        Intent i = new Intent(context, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        context.startActivity(i);

                    } else {
                        Intent intent = new Intent(context, SignInPageActivity.class);
                        intent.putExtra("membership_login", true);
                        context.startActivityForResult(intent, DashboardActivity.RC_MEMBER_LOGIN);
                    }
                }
            });

            viewHolder.memberPriceName.setClickable(true);
            viewHolder.memberPriceName.setMovementMethod(LinkMovementMethod.getInstance());
            viewHolder.memberPriceName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {

                        Intent i = new Intent(context, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        context.startActivity(i);

                    } else {
                        Intent intent = new Intent(context, SignInPageActivity.class);
                        intent.putExtra("membership_login", true);
                        context.startActivityForResult(intent, DashboardActivity.RC_MEMBER_LOGIN);
                    }
                }
            });


            viewHolder.rupees.setClickable(true);
            viewHolder.rupees.setMovementMethod(LinkMovementMethod.getInstance());
            viewHolder.rupees.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {

                        Intent i = new Intent(context, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        context.startActivity(i);

                    } else {
                        Intent intent = new Intent(context, SignInPageActivity.class);
                        intent.putExtra("membership_login", true);
                        context.startActivityForResult(intent, DashboardActivity.RC_MEMBER_LOGIN);
                    }
                }
            });


            viewHolder.memberPrice.setClickable(true);
            viewHolder.memberPrice.setMovementMethod(LinkMovementMethod.getInstance());
            viewHolder.memberPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {

                        Intent i = new Intent(context, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        context.startActivity(i);

                    } else {
                        Intent intent = new Intent(context, SignInPageActivity.class);
                        intent.putExtra("membership_login", true);
                        context.startActivityForResult(intent, DashboardActivity.RC_MEMBER_LOGIN);
                    }
                }
            });
        }

        serviceType.setFinalAmount(finalAmount + "");
        serviceType.setMembersPrice(membersPrice + "");

        String serviceId = "", serviceId1 = "";
        if (position != 0)
            serviceId = serviceTypesList.get(position - 1).getServiceID();
        serviceId1 = serviceTypesList.get(position).getServiceID();
        boolean flag = false;
        String num = "";
        if (mFinalSelectedServiceTypes != null && mFinalSelectedServiceTypes.size() > 0) {
            for (ServiceTypeModel s : mFinalSelectedServiceTypes) {
                if (s.getServiceTypeID().equalsIgnoreCase(serviceType.getServiceTypeID())) {
                    num = s.getNumOfPeople();
                    flag = true;
                    break;
                    /*viewHolder.plusMinus.setVisibility(View.VISIBLE);
                    viewHolder.add.setVisibility(View.GONE);
                    viewHolder.textQuantitiy.setText(s.getNumOfPeople());*/
                }
            }
        }

        if (flag) {
            viewHolder.plusMinus.setVisibility(View.VISIBLE);
            viewHolder.viewCart.setVisibility(View.GONE);
            if (viewHolder.textQuantitiy != null) {
                viewHolder.textQuantitiy.setText(num);
            }
            flag = false;
        } else {
            if (viewHolder.plusMinus != null)
                viewHolder.plusMinus.setVisibility(View.GONE);
            if (viewHolder.add != null)
                viewHolder.viewCart.setVisibility(View.VISIBLE);

            if (viewHolder.textQuantitiy != null) {
                viewHolder.textQuantitiy.setText("");
            }
            flag = false;
        }

        if (position == 0 || !serviceId.equalsIgnoreCase(serviceId1)) {
            if (viewHolder.heading != null)
                viewHolder.heading.setVisibility(View.VISIBLE);
            if (viewHolder.headingtext != null && serviceType.getServiceName() != null)
                viewHolder.headingtext.setText(serviceType.getServiceName());
            viewHolder.relLayout.setTag(serviceType.getServiceName());
            viewHolder.cardViewAdapter.setTag(serviceType.getServiceName());
            viewHolder.heading.setTag(serviceType.getServiceName());

        } else {
            viewHolder.heading.setVisibility(View.GONE);
        }

        DashboardFragment.mapOfPositionAdapter.put(serviceType.getCategoryName(), position);

        //Plus button
        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer val = Integer.parseInt(viewHolder.textQuantitiy.getText().toString());
                viewHolder.textQuantitiy.setText(val + 1 + "");
                updateFinalSelectedServiceTypesList("increase", serviceType);
            }
        });

        viewHolder.viewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.plusMinus.setVisibility(View.VISIBLE);
                viewHolder.viewCart.setVisibility(View.GONE);
                viewHolder.textQuantitiy.setText("1");
                updateFinalSelectedServiceTypesList("increase", serviceType);
            }
        });

        /*viewHolder.carotDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewHolder.descriptionServices.getVisibility()==View.VISIBLE) {
                    viewHolder.descriptionServices.setVisibility(View.GONE);
                    viewHolder.descriptionServices.setVisibility(View.GONE);
                    viewHolder.carotDown.setBackgroundResource(R.drawable.arrow_down);
                }else{
                    viewHolder.descriptionServices.setVisibility(View.VISIBLE);
                    viewHolder.descriptionServices.setVisibility(View.VISIBLE);
                    viewHolder.carotDown.setBackgroundResource(R.drawable.arrow_up);
                }
            }
        });*/

        //Minus button
        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer val = Integer.parseInt(viewHolder.textQuantitiy.getText().toString());
                if (val != 1)
                    viewHolder.textQuantitiy.setText(val - 1 + "");
                else {
                    viewHolder.plusMinus.setVisibility(View.GONE);
                    viewHolder.textQuantitiy.setText("");
                    /*viewHolder.plus.setVisibility(View.GONE);
                    viewHolder.minus.setVisibility(View.GONE);
                    viewHolder.textQuantitiy.setVisibility(View.GONE);*/
                    viewHolder.viewCart.setVisibility(View.VISIBLE);
                }
                updateFinalSelectedServiceTypesList("decrease", serviceType);
            }
        });
        viewHolder.descriptionServices.setText(serviceType.getDescription());


        //Set name
        viewHolder.textViewName.setText(serviceTypeName);

        //Set amount
        if (discount > 0) {
            viewHolder.textViewTotalAmount.setVisibility(View.VISIBLE);
            viewHolder.textViewTotalAmount.setText(formatter.format(initialAmount));
            viewHolder.textViewTotalAmount.setPaintFlags(viewHolder.textViewTotalAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.textViewTotalAmount.setTextColor(ContextCompat.getColor(context, R.color.red));
            viewHolder.textviewAmount.setText(formatter.format(finalAmount));
            viewHolder.textviewAmount.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
            viewHolder.rupeeSymbol.setTextColor(ContextCompat.getColor(context, R.color.red));

        } else {
            viewHolder.textViewTotalAmount.setVisibility(View.GONE);
            viewHolder.textviewAmount.setText(formatter.format(finalAmount));
            viewHolder.textviewAmount.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
            viewHolder.rupeeSymbol.setTextColor(ContextCompat.getColor(context, R.color.SeaGreen));
        }
        viewHolder.memberPrice.setText(membersPrice + "");

        //Set time
        viewHolder.textviewTime.setText(serviceType.getTime());
        return view;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        if (serviceTypesList != null)
            return serviceTypesList.size();
        else
            return 0;
    }

    @Override
    public Object getItem(int position) {
        return serviceTypesList.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                serviceTypesList = new ArrayList<>();
                if (constraint.length() == 0) {
                    serviceTypesList = mDisplayedValues;
                }
                serviceTypesList = (ArrayList<ServiceTypeModel>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                ArrayList<ServiceTypeModel> FilteredArrList = new ArrayList<ServiceTypeModel>();

                if (mAllServices == null) {
                    mAllServices = new ArrayList<ServiceTypeModel>(serviceTypesList); // saves the original data in mOriginalValues
                }

                /********
                 *
                 *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                 *  else does the Filtering and returns FilteredArrList(Filtered)
                 *
                 ********/
                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = mAllServices.size();
                    results.values = mAllServices;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mAllServices.size(); i++) {
                        String data = mAllServices.get(i).getServiceTypeName();
                        if (data.toLowerCase().contains(constraint.toString())) {
                            FilteredArrList.add(new ServiceTypeModel(mAllServices.get(i).getCategoryId(), mAllServices.get(i).getCategoryName(), mAllServices.get(i).getServiceTypeID(), mAllServices.get(i).getServiceTypeName(), mAllServices.get(i).getDescription(), mAllServices.get(i).getImage(), mAllServices.get(i).getPrice(), mAllServices.get(i).getDiscount(), mAllServices.get(i).getFinalAmount(), mAllServices.get(i).getTime(), mAllServices.get(i).getIsPackage(), mAllServices.get(i).getNumOfPeople(), mAllServices.get(i).getServiceID(), mAllServices.get(i).getServiceName(), mAllServices.get(i).getPercentageForMembers(), mAllServices.get(i).getMembersPrice()));
                        }
                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;
    }


}
