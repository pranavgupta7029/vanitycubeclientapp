package com.vanitycube.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.vanitycube.R;
import com.vanitycube.activities.WebPageActivityBanner;
import com.vanitycube.fragments.MainSplashFragment;
import com.vanitycube.model.Banner;

import java.util.ArrayList;

// Created by prade on 12/26/2017.

public class CustomPagerAdapterDashboard extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Banner> bannerItemsList;
    private MainSplashFragment mainSplashFragment;

    public CustomPagerAdapterDashboard(Context context, ArrayList<Banner> bannerItemsList, MainSplashFragment mainSplashFragment) {
        this.context = context;
        this.bannerItemsList = bannerItemsList;
        this.mainSplashFragment = mainSplashFragment;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return bannerItemsList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View itemView = layoutInflater.inflate(R.layout.dashboard_offer_pager, container, false);

        ImageView imageView = itemView.findViewById(R.id.dashOfferPagerImageView);

        //Load banner image here
        if (bannerItemsList.size() > 0) {
            String imgURL = bannerItemsList.get(position).getImgURL();
            if (imgURL != null && imageView != null)
                Picasso.with(context).load(imgURL).into(imageView);
        }
        if (imageView != null) {
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    Banner banner = bannerItemsList.get(position);
                    //Toast.makeText(context, banner.getActionUrl(), Toast.LENGTH_SHORT).show();
                    if (null != banner.getActionUrl() && !banner.getActionUrl().equals("") && !banner.getActionUrl().equalsIgnoreCase("null")) {
                        if (banner.getActionUrl().equalsIgnoreCase("https://www.vanitycube.in/v-club")) {
                            Intent i = new Intent(context, WebPageActivityBanner.class);
                            i.putExtra("url", banner.getActionUrl());
                            context.startActivity(i);
                        } else {
                            mainSplashFragment.bannerAction(banner.getActionUrl());
                        }
                    }
                    //this will log the page number that was click
                    //Log.i("TAG", "This page was clicked: " + pos);
                }
            });
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
