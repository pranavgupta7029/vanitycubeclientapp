
package com.vanitycube.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.ContactsModel;

import java.util.ArrayList;

public class MyFriendListAdapter extends ArrayAdapter<ContactsModel> implements Filterable {
    Activity context;
    public ArrayList<ContactsModel> friendlist;
    public ArrayList<ContactsModel> friendlistComplete;
    MyFriendListAdapter friendAdapter;
    public static boolean[] itemChecked;
    private SearchFilter mFilter;
    private Context mContext;

    public MyFriendListAdapter(Activity context, ArrayList<ContactsModel> friendlist) {
        super(context, R.layout.friend_list_row, friendlist);
        this.friendlist = friendlist;
        this.friendlistComplete = friendlist;
        this.context = context;
        this.friendAdapter = this;
    }

    static class ViewHolder {
        protected TextView rowtext;
        ImageView profileImage;
        protected TextView invite;

    }

    @Override
    public int getCount() {
        return friendlist.size();
    }

    @Override
    public ContactsModel getItem(int position) {
        if (friendlist != null)
            return friendlist.get(position);
        else
            return null;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;
        view = convertView;

        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.friend_list_row, null);
            viewHolder = new ViewHolder();
            viewHolder.rowtext = (TextView) view.findViewById(R.id.rowtext);
            viewHolder.invite = (TextView) view.findViewById(R.id.inviteTextView);
            viewHolder.profileImage = (ImageView) view.findViewById(R.id.profileImage);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        if (friendlist.get(position).getlName() != null && friendlist.get(position).getlName().length() > 0) {
            viewHolder.rowtext.setText(friendlist.get(position).getFname() + " " + friendlist.get(position).getlName());
        } else {
            viewHolder.rowtext.setText(friendlist.get(position).getFname());
        }

        String url = friendlist.get(position).getPhotoPath();

        if ((url != null && url.length() > 6)) {
            viewHolder.profileImage.setImageBitmap(getScaledBitmap(url, 200, 200));
        } else {
            Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.user);
            viewHolder.profileImage.setImageBitmap(bmp);
        }

        viewHolder.invite.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String phNumber = friendlist.get(position).getPhoneNumber();
                phNumber = phNumber.replaceAll("[\\s\\-()]", "");
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", phNumber);
                String urlstr = "https://goo.gl/FFHMsF";
                String message = "<html>Download VanityCube On demand professional home salon app and get Rs 400 off on your first service. \nWe wish you all beautiful things. \n <a href="
                        + urlstr + ">\n Download VanityCube:  </a>" + urlstr + " </html>";

                confirmDialogBox(smsIntent, message);
            }

        });

        return view;
    }

    public void confirmDialogBox(Intent intent, String message) {
        final String msg = message;
        final Intent lIntent = intent;
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Confirm Invite");
        alert.setMessage("Are you sure you want to invite this contact?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                lIntent.putExtra("sms_body", Html.fromHtml(msg).toString());
                mContext.startActivity(lIntent);
            }
        });

        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (dialog != null)
                    dialog.dismiss();
            }
        });
        alert.show();
    }

    private Bitmap getScaledBitmap(String picturePath, int width, int height) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, sizeOptions);
        int inSampleSize = calculateInSampleSize(sizeOptions, width, height);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;
        return BitmapFactory.decodeFile(picturePath, sizeOptions);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public class SearchFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (prefix == null || prefix.length() == 0) {
                ArrayList<ContactsModel> list = new ArrayList<ContactsModel>(friendlistComplete);
                results.values = list;
                results.count = list.size();

            } else {
                String prefixString = prefix.toString().toLowerCase();
                final ArrayList<ContactsModel> values = friendlistComplete;
                final int count = values.size();
                final ArrayList<ContactsModel> newValues = new ArrayList<>(count);

                for (int i = 0; i < count; i++) {
                    final ContactsModel value = values.get(i);
                    final String nameFirst = value.getFname().toLowerCase();
                    final String nameLast = value.getlName().toLowerCase();
                    if (nameFirst.startsWith(prefixString) || nameLast.startsWith(prefixString)) {
                        newValues.add(value);
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            friendlist = (ArrayList<ContactsModel>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) mFilter = new SearchFilter();
        return mFilter;
    }

}
