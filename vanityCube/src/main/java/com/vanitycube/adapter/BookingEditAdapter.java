
package com.vanitycube.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.BookingSummaryModel;

import java.util.List;

public class BookingEditAdapter extends BaseAdapter {
    Activity context;
    List<BookingSummaryModel> addactivities;

    public BookingEditAdapter(Activity context, List<BookingSummaryModel> activities) {
        this.context = context;
        addactivities = activities;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return addactivities.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return addactivities.get(position);
    }

    static class ViewHolder {
        protected TextView serviceName;
        protected TextView serviceTime;
        protected TextView serviceAmount;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view;
        ImageView addActivityDetails;
        view = convertView;
        ViewHolder viewHolder;
        if (view == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.booking_summary_adapter_row, null);
            viewHolder = new ViewHolder();
            viewHolder.serviceName = (TextView) view
                    .findViewById(R.id.bookingSummaryNameTextView);
            viewHolder.serviceTime = (TextView) view
                    .findViewById(R.id.bookingSummaryTimeTextView);
            viewHolder.serviceAmount = (TextView) view
                    .findViewById(R.id.bookingSummaryAmountTextView);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.serviceName.setText(addactivities.get(position).getServiceTypeName());
        viewHolder.serviceTime.setText(addactivities.get(position).getRequiredTime());
        int amountInt = Integer.valueOf(addactivities.get(position).getAmount()) * Integer.valueOf(addactivities.get(position).getNumOfPeople());
        viewHolder.serviceAmount.setText(String.valueOf(amountInt));

        return view;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

}
