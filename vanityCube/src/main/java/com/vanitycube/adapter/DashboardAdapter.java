package com.vanitycube.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vanitycube.R;

import java.util.ArrayList;

public class DashboardAdapter extends BaseAdapter {
    private ArrayList<String> serviceNamesList;
    private LayoutInflater mInflater;
    private Context context;

    public DashboardAdapter(Context context, ArrayList<String> serviceNamesList) {
        this.context = context;
        this.serviceNamesList = serviceNamesList;
        mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return serviceNamesList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceNamesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.dashboard_element, null);
        }

        TextView text = convertView.findViewById(R.id.dashText);
        text.setText(serviceNamesList.get(position));

        //Pre selecting the first grid capsule
        if (position == 0) {
            try {
                text.setBackground(context.getDrawable(R.drawable.rounded_text_view_orange));
                text.setTextColor(ContextCompat.getColor(context, R.color.white));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return convertView;
    }

}