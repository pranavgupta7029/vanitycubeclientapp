package com.vanitycube.utilities;

import java.util.ArrayList;

public class ColorCodes {
    public static ArrayList<String> listOfColors = new ArrayList<>();

    public static ArrayList<String> getListOfColors() {
        return listOfColors;
    }

    public static void setListOfColors(ArrayList<String> listOfColors) {
        ColorCodes.listOfColors = listOfColors;
    }
}
