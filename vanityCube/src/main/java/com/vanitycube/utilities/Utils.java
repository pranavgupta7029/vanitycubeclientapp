package com.vanitycube.utilities;

import java.io.InputStream;
import java.io.OutputStream;

public class Utils {

    public static final String PRODUCT_FRAGMENT = "product_fragment";
    public static final String FRIEND_FRAGMENT = "friend_fragment";
    public static final String PRODUCT_GRID_FRAGMENT = "grid_fragment";
    public static final String CURRENT_GRID_KEY = "CURRENT_GRID_FRAGMENT";
    public static final String TIMELINE = "timeline";
    public static final String HOME = "home";
    public static final String CATEGORY_NAME = "categoryName";
    public static final String CHAT_SERVER_URL = "54.149.189.223";
    public static final String CHAT_SERVER_IP_PORT = "2342";

    // method called in imageLoader class
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }
}
