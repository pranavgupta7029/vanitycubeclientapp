/*
 * Ashish : ImageSelector
 * Use: To open a Dialog to Select Image from camera or file system
 * How to Use:
 *     ImageSelector ObjImageSelector = new ImageSelector(context);
 *     ObjImageSelector.selectImage();
 *     Bitmap bitmapToPutInImage = ObjImageSelector.getBitmap();
 */

package com.vanitycube.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageSelector extends Activity {
    private int REQUEST_CAMERA = 0;
    private int SELECT_FILE = 1;
    private Context mContext;
    Bitmap mImageBitmap = null;

    public ImageSelector(Context context) {
        mContext = context;
    }

    public Bitmap getBitmap() {
        return mImageBitmap;
    }

    public void selectImage() {
        final CharSequence[] lItems = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(lItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (lItems[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (lItems[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (lItems[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        mImageBitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream lBytes = new ByteArrayOutputStream();
        mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, lBytes);

        File lDestination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream lFileOutputStream;
        try {
            lDestination.createNewFile();
            lFileOutputStream = new FileOutputStream(lDestination);
            lFileOutputStream.write(lBytes.toByteArray());
            lFileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] lProjection = {MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, lProjection, null, null,
                null);

        int lColumn_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();

        String lSelectedImagePath = cursor.getString(lColumn_index);

        BitmapFactory.Options lObjOptions = new BitmapFactory.Options();
        lObjOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(lSelectedImagePath, lObjOptions);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (lObjOptions.outWidth / scale / 2 >= REQUIRED_SIZE
                && lObjOptions.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        lObjOptions.inSampleSize = scale;
        lObjOptions.inJustDecodeBounds = false;
        mImageBitmap = BitmapFactory.decodeFile(lSelectedImagePath, lObjOptions);

    }
}
