package com.vanitycube.utilities;

/**
 * Created by prade on 11/13/2017.
 */

public class Strings {

    public static final String Empty = "";

    public static String nullSafeString(String s) {
        return s == null ? Strings.Empty : s;
    }
}
