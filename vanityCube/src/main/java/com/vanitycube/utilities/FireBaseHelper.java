package com.vanitycube.utilities;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class FireBaseHelper {

    private static final String PAGE_RUNTIME = "page_runtime";
    private static final String LOGIN = "login";
    private static final String USER_NAME = "user_name";
    private static final String USER_ID = "user_id";

    private final FirebaseAnalytics mFirebaseAnalytics;

    private SharedPref pref;
    private Gson gson;
    private String userId, userEmail;

    public FireBaseHelper(Activity activity) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        pref = new SharedPref(VcApplicationContext.getInstance());
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        gson = new Gson();
        getUser();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();

            if (isValidString(userId)) {
                userEmail = userModel.getEmail();

                if (!isValidString(userEmail)) {
                    userEmail = "";
                }
            }
        }
    }

    public void logPage(String page) {
        Bundle params = new Bundle();
        params.putString(USER_NAME, userEmail);
        params.putString(USER_ID, userId);
        mFirebaseAnalytics.logEvent(page, params);
    }

    public void logLogin(int hour) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.format("%d:00 HRS - %d:00 HRS", hour, hour + 1 % 24));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, String.valueOf(hour));
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, LOGIN);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public void logPageRuntime(Calendar startTime, Calendar endTime, String page) {
        for (int index = 0; index < minutesBetween(startTime, endTime); index++) {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, page);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, String.valueOf(index));
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, PAGE_RUNTIME);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        }
    }

    private static long minutesBetween(Calendar startDate, Calendar endDate) {
        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toMinutes(Math.abs(end - start));
    }

    public void logEvents(String eventName, Bundle bundle) {
        if (userEmail != null && userId != null) {
            bundle.putString(USER_NAME, userEmail);
            bundle.putString(USER_ID, userId);
        }
        mFirebaseAnalytics.logEvent(eventName, bundle);
    }
}
