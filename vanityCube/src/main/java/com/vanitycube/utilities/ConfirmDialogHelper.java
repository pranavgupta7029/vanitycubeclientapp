package com.vanitycube.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by Milind on 15-Jun-17.
 */

public class ConfirmDialogHelper {

    Activity activity;

    public ConfirmDialogHelper(Activity activity) {
        this.activity = activity;
    }

    public static ConfirmDialogHelper with(Activity activity) {
        return new ConfirmDialogHelper(activity);
    }

    public void confirm(String title, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_HOLO_LIGHT);
        AlertDialog dialog = builder
                .setTitle(title)
                .setMessage("Are you sure?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, null).create();

        dialog.show();
    }

    public void confirm(String title, DialogInterface.OnClickListener listener, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_HOLO_LIGHT);
        AlertDialog dialog = builder
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener)
                .setNegativeButton(android.R.string.no, null).create();

        dialog.show();
    }

    public void confirmNoCancel(String title, DialogInterface.OnClickListener listener, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, AlertDialog.THEME_HOLO_LIGHT);
        AlertDialog dialog = builder
                .setTitle(title)
                .setMessage(message)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, listener).create();
        if (!((Activity) activity).isFinishing()) {
            //show dialog
            dialog.show();
        }


    }
}
