
package com.vanitycube;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.vanitycube.activities.FeedbackActivity;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.model.NotificationModel;

public class GCMIntentService extends GcmListenerService {
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        String title = data.getString("title");
        String type = data.getString("type");

        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Title: " + title);
        Log.d(TAG, "Message: " + message);

        if (type != null && type.equalsIgnoreCase("feedback")) {
            String id = data.getString("booking_id");
            String time = data.getString("booking_time");
            String date = data.getString("booking_date");
            if (id != null && time != null && date != null)
                sendFeedbackNotification(title, id, time, date);
        } else {

            // Toast.makeText(VcApplicationContext.getInstance(), "Push : " + message, Toast.LENGTH_LONG).show();
            /**
             * Production applications would usually process the message here. Eg: - Syncing with server. - Store message in
             * local database. - Update UI.
             */

            /**
             * In some cases it may be useful to show a notification indicating to the user that a message was received.
             */
            sendNotification(title, message);
            VcDatabaseQuery mDbQuery = new VcDatabaseQuery();
            NotificationModel lObjNotification = new NotificationModel();
            lObjNotification.setTitle(title);
            lObjNotification.setMessage(message);
            mDbQuery.insertNotification(lObjNotification);
        }

    }

    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, MainSplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("notification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.huge)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(41 /* ID of notification */, notificationBuilder.build());
    }

    private void sendFeedbackNotification(String title, String booking_id, String booking_time, String booking_date) {
        Intent intent = new Intent(this, FeedbackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String[] bookingIdTImeDate = {booking_id, booking_time, booking_date};
        intent.putExtra("booking_id", bookingIdTImeDate);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.huge)
                .setContentTitle(title)
                .setContentText("We would love to hear your feedback.")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(41 /* ID of notification */, notificationBuilder.build());
    }

}
