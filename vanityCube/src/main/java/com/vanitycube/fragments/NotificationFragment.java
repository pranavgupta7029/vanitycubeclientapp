
package com.vanitycube.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.adapter.NotificationAdapter;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.database.VcTableList;
import com.vanitycube.model.NotificationModel;
import com.vanitycube.webservices.RestWebServices;

import java.util.ArrayList;
import java.util.Collections;

public class NotificationFragment extends Fragment implements OnItemClickListener, OnClickListener {
    private static final String TAG = "NotificationFragment";
    private RestWebServices mRestWebServices;
    private ListView activitiesList;
    NotificationAdapter notifyAdapter;
    private TextView empty;
    private ArrayList<NotificationModel> activities;
    private LinearLayout clearLayout;

    public NotificationFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRestWebServices = new RestWebServices();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_notification, null);
        activitiesList = (ListView) rootView.findViewById(R.id.listViewNotification);
        empty = (TextView) rootView.findViewById(R.id.empty);
        MainSplashActivity.changeHeader = true;

        TextView clearNotification = (TextView) rootView.findViewById(R.id.clearNotification);
        clearLayout = (LinearLayout) rootView.findViewById(R.id.clearLayout);
        clearNotification.setOnClickListener(this);

        populateNotifications();
        return rootView;
    }

    private void populateNotifications() {
        activities = new ArrayList<NotificationModel>();
//        for (int i = 0; i < mBookinglist.size(); i++) {
//            BookingListModel notify = mBookinglist.get(i);
//            // notify.setFacilityName("Hair");
//            // notify.setBookedDate("22June2015");
//            // notify.setBookedTime("02:55PM");
//            // notify.setBookingID("ABC21251");
//            // notify.setPrice("1000");
//            activities.add(notify);
//        }
        VcDatabaseQuery mDbQuery = new VcDatabaseQuery();
        activities = mDbQuery.retreiveNotifications();
        if (activities.size() > 0) {
            clearLayout.setVisibility(View.VISIBLE);
        } else {
            clearLayout.setVisibility(View.GONE);
        }
        Collections.reverse(activities);
        notifyAdapter = new NotificationAdapter(getActivity(), activities);
        activitiesList.setEmptyView(empty);
        activitiesList.setAdapter(notifyAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.clearNotification:
                VcDatabaseQuery mDbQuery = new VcDatabaseQuery();
                mDbQuery.clearTable(VcTableList.TABLE_NOTIFICATION);
                populateNotifications();
                break;
            default:
                break;
        }
    }

}