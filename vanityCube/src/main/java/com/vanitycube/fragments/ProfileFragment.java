
package com.vanitycube.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.activities.MultipleAddressActivity;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.model.loyalty.AbvailablePointsResponse;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.RoundedImageView;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileFragment extends Fragment implements OnClickListener, DatePickerDialog.OnDateSetListener, ServerResponseListener {
    private static final String IS_EDIT_ALLOWED = "is_edit_allowed";
    private static final String GET_ABVAILABLE_LOYALTY_POINTS = "easyRewardzGetAvailablePointsAction";
    private static final String GET_ABVAILABLE_CASHBACK_POINTS = "getAvailableCashbackPoints";
    String mFilePath;
    //private static final String UPLOAD_URL = ApplicationSettings.APPLICATION_SERVICE_URL + "?method=updateUserProfilePic";
    private static final String TAG = "ProfileFragment";
    private View rootView;
    private boolean mEdit = false;
    private EditText mName, mAge, mPhone, mEmail;
    private TextView mAddress, mReferral, mDOB, viewtransaction;
    private RoundedImageView mProfileImageView;
    private TextView mHeaderText, mFooterButton, loyaltyPointsTextview, cashBackPointsTextview;
    private RestWebServices mRestWebServices;
    private String mImageURL = "";
    private String filePath;
    private String mImageName = "";
    private VcDatabaseQuery mDBQuery;
    private UserModel mUserInfo;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1, CHOOSE_IMAGE_REQUEST = 2;
    private TextView mAddressText;
    private ImageView mEditProfilePlus;
    private TextView mAddAddressImageView;
    private SharedPref pref;
    private Bitmap mProfileBitmap;
    private DatePicker datePicker;
    private Calendar calendar;
    private int year, month, day;
    private String dob;
    private OnDateSetListener mDateSetListener;
    private boolean emailExists = false;
    private List<String> picOptionsList;
    private Uri capturedImageUri;
    ProgressDialog progressDialog;
    private ApiManager apiManager;
    private String json, userId, orderId;
    private Gson gson;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gson = new Gson();

        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebServices = new RestWebServices();
        mDBQuery = new VcDatabaseQuery();
        getUser();//mDBQuery.retreiveUser();
        calendar = Calendar.getInstance();
        progressDialog = new ProgressDialog(getActivity());

        //Populate the choice list
        picOptionsList = new ArrayList<String>();
        picOptionsList.add("Choose from Library");
        if (hasCamera()) picOptionsList.add("Take Photo");
        picOptionsList.add("Cancel");
        getAvailablePoints(mUserInfo.getID());
        //getAvailablePointsCashBack(mUserInfo.getID());
    }

    private void getUser() {
        json = pref.getUserModel();
        mUserInfo = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (mUserInfo == null) {
            mUserInfo = new UserModel();
        } else {
            userId = mUserInfo.getID();
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_profile, null);

        init();
        setFooter();
        MainSplashActivity.changeHeader = true;

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new getDiscountsAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection.", Toast.LENGTH_LONG).show();
        }

        return rootView;

    }

    /*  --------------------------------------------------------------------------
        |
        |   SELECT PROFILE PHOTO & SET A ROUNDED IMAGEVIEW
        |
        --------------------------------------------------------------------------
    */

    private void init() {

        mProfileImageView = (RoundedImageView) rootView.findViewById(R.id.profile_image);
        mProfileImageView.setOnClickListener(this);
        mProfileImageView.setClickable(false);
        mAddressText = (TextView) rootView.findViewById(R.id.profile_address1);
        mAddAddressImageView = (TextView) rootView.findViewById(R.id.add_address_image);
        mName = (EditText) rootView.findViewById(R.id.profile_name);
        mAge = (EditText) rootView.findViewById(R.id.profile_age);
        mAge.setVisibility(View.INVISIBLE);
        mAddress = (TextView) rootView.findViewById(R.id.profile_address);
        mReferral = (TextView) rootView.findViewById(R.id.referral_name);
        mPhone = (EditText) rootView.findViewById(R.id.profile_phone);
        mEmail = (EditText) rootView.findViewById(R.id.profile_email);
        mDOB = (TextView) rootView.findViewById(R.id.profile_dob);
        mFooterButton = (TextView) rootView.findViewById(R.id.footerButton);
        ((LinearLayout) rootView.findViewById(R.id.common_footer_layout)).setOnClickListener(this);
        mReferral.setText(mUserInfo.getReferralName());
        mReferral.setVisibility(View.GONE);
        mEditProfilePlus = (ImageView) rootView.findViewById(R.id.edit_plus);
        loyaltyPointsTextview = (TextView) rootView.findViewById(R.id.loyaltyPointsTextview);
        cashBackPointsTextview = (TextView) rootView.findViewById(R.id.cashBackPointsTextview);

        viewtransaction = (TextView) rootView.findViewById(R.id.viewtransaction);
        viewtransaction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                /*TextView tView = (TextView)rootView.findViewById(R.id.headerText) ;
                tView.setText("Loyalty Transaction");
                tView.setVisibility(View.VISIBLE);*/
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                LoyaltyTransactionFragment profileFrag = new LoyaltyTransactionFragment();
                transaction.replace(R.id.dash_content_frame, profileFrag);
                transaction.commit();
            }
        });

        preFill();

        String imageDownloadURL = ApplicationSettings.PROFILE_IMAGE_PATH + mUserInfo.getProfileImagePath();
        mImageName = mUserInfo.getProfileImagePath();

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new DownloadImageTask(mProfileImageView).execute(imageDownloadURL, null, null);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection.", Toast.LENGTH_LONG).show();
        }

        mAddAddressImageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                String lUserId = mUserInfo.getID();
                String lUserName = mUserInfo.getName();
                Intent lMultipleAddressIntent = new Intent(getActivity(), MultipleAddressActivity.class);
                lMultipleAddressIntent.putExtra("user_id", lUserId);
                lMultipleAddressIntent.putExtra("user_name", lUserName);
                lMultipleAddressIntent.putExtra("came_from_settings", true);
                startActivity(lMultipleAddressIntent);
            }

        });

    }

    /*  -------------------------------------
        |
        |   PREFILL EDIT TEXT
        |
        -------------------------------------
    */

    public void preFill() {
        mName.setText(mUserInfo.getName());
        mPhone.setText(mUserInfo.getNumber());
        mEmail.setText(mUserInfo.getEmail());

        if (mUserInfo!=null && mUserInfo.getDOB().split("-")[0].length() == 4) {
            String[] text = mUserInfo.getDOB().split("-");
            mDOB.setText(text[2] + "-" + text[1] + "-" + text[0]);
        } else
            mDOB.setText(mUserInfo.getDOB());

        if (mDOB.getText() != null && mDOB.getText().toString().contains("00-00-0000")) {
            mDOB.setText("N/A");
        }

        if (mDOB.getText() != null && mDOB.getText().toString().equals("")) {
            mDOB.setText("N/A");
        }
    }

    public void setDOB(String dob) {
        mDOB.setText(dob);
    }

    public boolean hasCamera() {
        if (getActivity().getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    private void selectImage() {

        //Convert list to CharSequence
        final CharSequence[] cs = picOptionsList.toArray(new CharSequence[picOptionsList.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");

        builder.setItems(cs, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (cs[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (cs[item].equals("Choose from Library")) {

                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

                } else if (cs[item].equals("Cancel")) {
                    dialog.dismiss();
                }

            }
        });

        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    onCaptureImageResult(data);
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == SELECT_FILE) {
            if (resultCode == Activity.RESULT_OK) {
                onSelectFromGalleryResult(data);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getActivity(), "User cancelled image selection", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Sorry! Failed to select image", Toast.LENGTH_SHORT).show();
            }
        }

    }

    /*  -------------------------------------
        |
        |   AFTER CAPTURING THE IMAGE
        |
        -------------------------------------
    */

    private void onCaptureImageResult(Intent data) {
        Bitmap lThumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream lBytes = new ByteArrayOutputStream();
        lThumbnail.compress(Bitmap.CompressFormat.JPEG, 90, lBytes);
        File lDestination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        mFilePath = lDestination.getAbsolutePath();
        Log.i("CameraImagePath", mFilePath);
        FileOutputStream lFileOutputStream;
        try {
            lDestination.createNewFile();
            lFileOutputStream = new FileOutputStream(lDestination);
            lFileOutputStream.write(lBytes.toByteArray());
            lFileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mProfileImageView.setImageBitmap(lThumbnail);
        mProfileBitmap = lThumbnail;
        uploadProfileImage();
    }

    /*  -------------------------------------
        |
        |   AFTER SELECTING FROM GALLERY
        |
        -------------------------------------
    */

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] lProjection = {MediaColumns.DATA};
        Cursor cursor = getActivity().managedQuery(selectedImageUri, lProjection, null, null,
                null);
        int lColumn_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();

        String lSelectedImagePath = cursor.getString(lColumn_index);
        mFilePath = lSelectedImagePath;
        if (mFilePath != null) {
            //Log.i("GalleryImagePath", lSelectedImagePath);
            Bitmap lObjBitMap;
            BitmapFactory.Options lObjOptions = new BitmapFactory.Options();
            lObjOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(lSelectedImagePath, lObjOptions);
            final int REQUIRED_SIZE = 200;
            int scale = 1;
            while (lObjOptions.outWidth / scale / 2 >= REQUIRED_SIZE
                    && lObjOptions.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            lObjOptions.inSampleSize = scale;
            lObjOptions.inJustDecodeBounds = false;
            lObjBitMap = BitmapFactory.decodeFile(lSelectedImagePath, lObjOptions);

            mProfileImageView.setImageBitmap(lObjBitMap);
            mProfileBitmap = lObjBitMap;
            uploadProfileImage();
        }
    }

    private void setupEditProfile() {
        mProfileImageView.setClickable(true);
        mFooterButton.setText("Save");
        mName.setEnabled(true);
        mEmail.setEnabled(true);
        mEmail.setBackgroundResource(R.drawable.line);
        mAddress.setEnabled(true);
        mAddress.setBackgroundResource(R.drawable.line);
        //mEditProfilePlus.setBackgroundResource(R.drawable.addd);
        mDOB.setEnabled(true);
        mDOB.setBackgroundResource(R.drawable.line);
        mDOB.setOnClickListener(this);
        mAge.setEnabled(true);
        mAge.setFocusableInTouchMode(true);
    }

    @SuppressWarnings("deprecation")
    private void setupProfile() {
        mProfileImageView.setClickable(false);
        mFooterButton.setText("Edit Profile");
        mName.setEnabled(false);
        mAddress.setEnabled(false);
        mAddress.setBackgroundDrawable(null);
        mPhone.setEnabled(false);
        mPhone.setBackgroundDrawable(null);
        mEmail.setEnabled(false);
        mEmail.setGravity(Gravity.START);
        mEmail.setBackgroundDrawable(null);
        mDOB.setEnabled(false);
        mEditProfilePlus.setBackgroundDrawable(null);
        mDOB.setBackgroundDrawable(null);
        mDOB.setFocusableInTouchMode(false);
        mDOB.setFocusable(false);
        mAge.setEnabled(false);
        mAge.setFocusableInTouchMode(false);
    }

    private boolean emailValidator(String pEmail) {
        // Checking the email for +++@+++.+++
        String regex = "^(.+)@(.+)\\.(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pEmail);
        return matcher.matches();
    }

    public void completeUserUpdate(boolean ifExists) {

    }

    /*  -------------------------------------
        |
        |   EXISTING EMAIL CHECK API CALL
        |
        -------------------------------------
    */

    private class EmailExistsAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;
        String name;
        String dob;
        String mImageName;
        String email;

        EmailExistsAsyncTask(String name, String dob, String mImageName, String email) {
            this.progressDialog = new ProgressDialog(getActivity());
            this.name = name;
            this.dob = dob;
            this.mImageName = mImageName;
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Updating user");
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(Void... url) {
            String[] successResultArray = {"0", "Unknown reasons!"};

            if (this.email != null)
                successResultArray = mRestWebServices.checkEmailExists(this.email);

            return successResultArray;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result[0].equalsIgnoreCase("1")) {
                    //If email exists
                    if (result[1].equalsIgnoreCase("true") && !this.email.equalsIgnoreCase(mUserInfo.getEmail())) {
                        Toast.makeText(getActivity(), "Email already belongs to an existing account! Please enter a different one.", Toast.LENGTH_LONG).show();
                    } else if ((result[1].equalsIgnoreCase("true") && this.email.equalsIgnoreCase(mUserInfo.getEmail())) || (result[1].equalsIgnoreCase("false"))) {
                        new UpdateUserAsyncTask(this.name, this.dob, this.mImageName, this.email).execute(null, null, null, null);
                        //Set new changes
                        setupProfile();
                    }
                } else if (result[0].equalsIgnoreCase("0")) {
                    Toast.makeText(getActivity(), "An error occurred while updating profile, please try again", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                Log.e(TAG, "<<Exception on BookingSummaryDetailActivity>>" + e.getMessage());
            }
        }
    }

    private void setFooter() {
        TextView FooterButton = (TextView) rootView.findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText("Edit Profile");
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.common_footer_layout:
            case R.id.footerButton:
                mEdit = !mEdit;
                if (mEdit == true) {
                    setupEditProfile();
                } else {

                    String name = mName.getText().toString();
                    String dob = mDOB.getText().toString();
                    String email = mEmail.getText().toString();

                    if (!emailValidator(email)) {
                        Toast.makeText(getActivity(), "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                        new EmailExistsAsyncTask(name, dob, mImageName, email).execute(null, null, null);
                    } else {
                        Toast.makeText(getActivity(), "Check your internet connection.", Toast.LENGTH_LONG).show();
                        break;
                    }

                }
                break;
            case R.id.profile_image:
                if (mEdit == true)
                    //selectImage();
                    break;
            case R.id.profile_dob:
                Calendar cal = Calendar.getInstance();
                DatePickerDialog dateDialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        setDOB(String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year));
                    }
                }, cal.get(Calendar.YEAR),
                        cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
                dateDialog.getDatePicker().setMaxDate(new Date().getTime());
                dateDialog.show();
                break;
            default:
                break;
        }
    }

    /*  -------------------------------------
        |
        |   UPDATE USER ASYNC TASK
        |
        -------------------------------------
    */

    class UpdateUserAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        boolean isChanged = false;
        String mName, mDOB, mImageURL, mEmail;

        public UpdateUserAsyncTask(String pName, String pDOB, String pImageURL, String pEmail) {
            this.mName = pName;
            this.mDOB = pDOB;
            this.mImageURL = pImageURL;
            this.mEmail = pEmail;
            progressDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Updating User Information");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            isChanged = mRestWebServices.updateUser(mName, mDOB, mImageURL, mEmail);
            return isChanged;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    Toast.makeText(VcApplicationContext.getInstance(), "Your profile has been updated", Toast.LENGTH_SHORT).show();

                    // Setting the updated user values here.
                    UserModel lUpdatedModel = new UserModel();
                    lUpdatedModel.setName(mName);
                    lUpdatedModel.setDOB(mDOB);
                    lUpdatedModel.setEmail(mEmail);
                    lUpdatedModel.setProfileImagePath(mImageURL);
                    VcDatabaseQuery lDBQuery = new VcDatabaseQuery();
                    lDBQuery.updateProfileDetailsTable(lUpdatedModel);

                    // Update drawer
                    ((MainSplashActivity) getActivity()).setNameInDrawer(mName);
                    ((MainSplashActivity) getActivity()).setImageInDrawer(mProfileBitmap);
                } else {
                    mRestWebServices.showAlertDialog(getActivity(),
                            "Change Failed..", "Failed to Update Profile",
                            false);
                }
            } catch (Exception e) {
                Log.e(TAG, "<<Exception on Update User>>" + e.getMessage());
            }

        }
    }

    /*  -------------------------------------
        |
        |   DOWNLOAD PROFILE IMAGE ASYNC TASK
        |
        -------------------------------------
    */

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        RoundedImageView bmImage;

        public DownloadImageTask(RoundedImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                //urldisplay is of this type -> http://192.168.0.16/vcube/public/img/users
                InputStream in = new java.net.URL(urldisplay).openStream();
                if (in != null)
                    mIcon11 = getScaledBitmap(in, 800, 800);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                bmImage.setImageBitmap(result);
                mProfileBitmap = result;
            }

        }
    }

    private Bitmap getScaledBitmap(InputStream in, int width, int height) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        int inSampleSize = calculateInSampleSize(sizeOptions, width, height);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(in, null, sizeOptions);
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }

        return inSampleSize;
    }

    /*  -------------------------------------
        |
        |   UPLOAD IMAGE ASYNC TASK
        |
        -------------------------------------
    */

    private class SingleUploadMediaObjectTask extends AsyncTask<Void, Void, String[]> {

        ProgressDialog progressDialog;
        protected static final String TAG = "SingleUploadMediaTask";
        String AsyncTag = "SingleUploadMediaObjectTask";

        @Override
        protected void onPreExecute() {
            this.progressDialog = new ProgressDialog(getActivity());
            super.onPreExecute();
            progressDialog.setMessage("Uploading profile photo");
            progressDialog.show();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected String[] doInBackground(Void... url) {

            String[] successResultArray = {"0", "Unknown reasons!"};
            String fileNameNPath = mFilePath;
            File file = new File(fileNameNPath);
            try {
                successResultArray = mRestWebServices.updateProfilePic(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return successResultArray;
        }

        @Override
        protected void onPostExecute(String[] result) {

            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                //If success
                if (result[0].equalsIgnoreCase("1")) {
                    mImageName = result[1];
                    Toast.makeText(getActivity(), "Upload successful, do not forget to Save your changes", Toast.LENGTH_LONG).show();
                }
                //If error
                else {
                    Toast.makeText(getActivity(), "Upload failed, please try again", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, "<<Exception on BookingSummaryDetailActivity>>" + e.getMessage());
            }

        }

    }

    private void uploadProfileImage() {
        new SingleUploadMediaObjectTask().execute(null, null, null);
    }

    class getLastAddress extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        String[] startEndTime = {"", ""};
        UserAddressModel lAddressModel = new UserAddressModel();

        public getLastAddress() {
            progressDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Getting Addresses");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... url) {
            String value = "";

            try {
                lAddressModel = mRestWebServices.getLastAddress();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (lAddressModel.getAddressId().length() > 0) {
                value = lAddressModel.toString();
            }

            return value;
        }

        @Override
        protected void onPostExecute(String value) {
            boolean result = value.length() > 0;
            mAddress.setText(value);
            {
                super.onPostExecute(value);
                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }

                    if (result) {
                        // Toast.makeText(Book.this, "Got Start and End Time", Toast.LENGTH_SHORT).show();
                    } else {
                        mAddress
                                .setText("No Default Address was found for the User, Please select an Address or Add a New one.");
                    }
                } catch (Exception e) {
                    Log.e(TAG, "<<Exception on getLastAddress>>" + e.getMessage());
                }

            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // TODO Auto-generated method stub
    }

    class getDiscountsAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;

        public getDiscountsAsyncTask() {
            progressDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading Profile Details");
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(Void... url) {
            String[] discounts = {};
            discounts = mRestWebServices.getUserDiscountDetails();
            return discounts;
        }

        @Override
        protected void onPostExecute(String[] result) {
            {
                super.onPostExecute(result);
                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }

                    if (result.length != 0) {

                        String firstBookingDiscount = result[0];
                        String referralDiscount = result[1];

                        DecimalFormat formatter = new DecimalFormat("#,###,###");

                        int totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount);
                        ((TextView) (getView().findViewById(R.id.profileWalletBalanceTextView)))
                                .setText(formatter.format(totalDiscount));
                    }
                } catch (Exception e) {
                    Log.e(TAG, "<<Exception on Fetching Discounts>>" + e.getMessage());
                }

            }
        }
    }

    private void getAvailablePoints(String userId) {
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        apiManager = new ApiManager(getActivity(), this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(AbvailablePointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_AVAILABLE_POINTS);
        params.put(ApplicationSettings.PARAM_API_DATA, "[\"" + userId + "\"]");
        params.put(ApplicationSettings.PARAM_REWARD_TYPE, "loyalty");

        ///apiManager.getStringPostResponse(GET_ABVAILABLE_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
        apiManager.getStringGetResponse(GET_ABVAILABLE_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private void getAvailablePointsCashBack(String userId) {
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        apiManager = new ApiManager(getActivity(), this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(AbvailablePointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_AVAILABLE_POINTS);
        params.put(ApplicationSettings.PARAM_API_DATA, "[\"" + userId + "\"]");
        params.put(ApplicationSettings.PARAM_REWARD_TYPE, "cashback");

        apiManager.getStringGetResponse(GET_ABVAILABLE_CASHBACK_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    @Override
    public void positiveResponse(String TAG, String response) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (TAG == GET_ABVAILABLE_LOYALTY_POINTS && responseObj instanceof AbvailablePointsResponse) {

            AbvailablePointsResponse abvailablePointsResponse = (AbvailablePointsResponse) responseObj;

            if (abvailablePointsResponse.getReturnMessage().equalsIgnoreCase("Success.")) {
                //useLoyaltyCb.setVisibility(View.VISIBLE);
                loyaltyPointsTextview.setText(abvailablePointsResponse.getAvailablePoints());
                mReferral.setText(abvailablePointsResponse.getReferralCode());

                if (!mUserInfo.getReferralName().equalsIgnoreCase(abvailablePointsResponse.getReferralCode())) {
                    UserModel lUpdatedModel = new UserModel();
                    lUpdatedModel.setName(mUserInfo.getName());
                    lUpdatedModel.setDOB(mUserInfo.getDOB());
                    lUpdatedModel.setEmail(mUserInfo.getEmail());
                    lUpdatedModel.setProfileImagePath(mUserInfo.getProfileImagePath());
                    lUpdatedModel.setReferralName(abvailablePointsResponse.getReferralCode());
                    VcDatabaseQuery lDBQuery = new VcDatabaseQuery();
                    lDBQuery.updateProfileDetailsTableWithRefferalCode(lUpdatedModel);
                }
                // useLoyaltyCb.setText("Use your " + abvailablePointsResponse.getAvailablePoints() + " VLCC Reward Club points");

            } else {
                //useLoyaltyCb.setVisibility(View.GONE);
                loyaltyPointsTextview.setText("Error!!!");
            }

            getAvailablePointsCashBack(mUserInfo.getID());
        }

        if (TAG == GET_ABVAILABLE_CASHBACK_POINTS && responseObj instanceof AbvailablePointsResponse) {

            AbvailablePointsResponse abvailablePointsResponse = (AbvailablePointsResponse) responseObj;

            if (abvailablePointsResponse.getReturnMessage().equalsIgnoreCase("Success.")) {
                //useLoyaltyCb.setVisibility(View.VISIBLE);
                cashBackPointsTextview.setText(abvailablePointsResponse.getAvailablePoints());
                //mReferral.setText(abvailablePointsResponse.getReferralCode());

                /*if(!mUserInfo.getReferralName().equalsIgnoreCase(abvailablePointsResponse.getReferralCode())){
                    UserModel lUpdatedModel = new UserModel();
                    lUpdatedModel.setName(mUserInfo.getName());
                    lUpdatedModel.setDOB(mUserInfo.getDOB());
                    lUpdatedModel.setEmail(mUserInfo.getEmail());
                    lUpdatedModel.setProfileImagePath(mUserInfo.getProfileImagePath());
                    lUpdatedModel.setReferralName(abvailablePointsResponse.getReferralCode());
                    VcDatabaseQuery lDBQuery = new VcDatabaseQuery();
                    lDBQuery.updateProfileDetailsTableWithRefferalCode(lUpdatedModel);
                }*/
                // useLoyaltyCb.setText("Use your " + abvailablePointsResponse.getAvailablePoints() + " VLCC Reward Club points");

            } else {
                //useLoyaltyCb.setVisibility(View.GONE);
                cashBackPointsTextview.setText("Error!!!");
            }


        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {
        System.out.print(errorResponse);
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
