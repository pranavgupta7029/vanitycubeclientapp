
package com.vanitycube.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.webservices.RestWebServices;

public class ChangePasswordFragment extends Fragment implements OnClickListener {
    private static final String TAG = "ChangePasswordFragment";
    private View rootView;
    private RestWebServices mRestWebServices;
    private SharedPref pref;
    private EditText mOldEditText, mNewEditText, mConfirmEditText;
    private Gson gson;
    private String json;
    private String userId;
    private UserModel userModel;

    public ChangePasswordFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebServices = new RestWebServices();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.change_password, null);

        userModel = new UserModel();

        getUser();

        init();

        setFooter();

        return rootView;
    }

    private void init() {
        mOldEditText = rootView.findViewById(R.id.changePwdoldText);
        mNewEditText = rootView.findViewById(R.id.changePwdnewText);
        mConfirmEditText = rootView.findViewById(R.id.changePwdconfirmText);
    }

    private void getUser() {
        json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            userId = userModel.getID();
        }
    }

    private void setFooter() {
        LinearLayout FooterButtonLayout = rootView.findViewById(R.id.common_footer_layout);
        FooterButtonLayout.setOnClickListener(this);
        FooterButtonLayout.setVisibility(View.VISIBLE);
        TextView FooterButton = rootView.findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText("Save");
        FooterButton.setOnClickListener(this);
    }

    /*  -------------------------------------
        |
        |   CHANGE PASSWORD ASYNC TASK
        |
        -------------------------------------
    */

    class ChangePasswordAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        boolean isChanged = false;
        String mOld, mNew;

        public ChangePasswordAsyncTask(String pOld, String pNew) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            this.mOld = pOld;
            this.mNew = pNew;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Changing Password");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            Log.i(TAG, " Calling Rest Service with " + mOld + " " + mNew);
            isChanged = mRestWebServices.changePassword(userModel.getID(), mOld, mNew);
            return isChanged;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    Toast.makeText(VcApplicationContext.getInstance(), "Your password has been changed", Toast.LENGTH_SHORT).show();
                } else {
                    mRestWebServices.showAlertDialog(getActivity(), "Current password incorrect, please try again"
                    );
                }

            } catch (Exception e) {
                Log.e(TAG, "<<Exception on ChangePassword>>" + e.getMessage());
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.common_footer_layout:
            case R.id.footerButton:
                String oldPass = mOldEditText.getText().toString();
                String newPass = mNewEditText.getText().toString();
                String confirmPass = mConfirmEditText.getText().toString();

                if (newPass.length() == 0 || confirmPass.length() == 0) {
                    Toast.makeText(getActivity(),
                            "Please enter a password!", Toast.LENGTH_SHORT)
                            .show();
                    break;
                }

                if (newPass.equalsIgnoreCase(confirmPass)) {
                    if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                        new ChangePasswordAsyncTask(oldPass, newPass).execute(null, null, null);
                    } else {
                        Toast.makeText(getActivity(),
                                "Check your internet connection", Toast.LENGTH_LONG)
                                .show();
                    }
                } else {
                    Toast.makeText(getActivity(),
                            "Passwords do not match!", Toast.LENGTH_SHORT)
                            .show();
                }

                break;
            default:
                break;

        }
    }
}