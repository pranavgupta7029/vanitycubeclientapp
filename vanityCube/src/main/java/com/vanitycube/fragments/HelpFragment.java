
package com.vanitycube.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vanitycube.R;

public class HelpFragment extends Fragment implements OnClickListener {

    private View rootView;

    public HelpFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.help, null);
        //   setFooter();
        // setHeader();
        return rootView;
    }

    private void setHeader() {

        TextView HeaderText = (TextView) rootView.findViewById(R.id.headerText);
        HeaderText.setText("Help & Support");

    }

    private void setFooter() {
        TextView FooterButton = (TextView) rootView.findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText("Save");

        FooterButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.footerButton:

                break;

            default:
                break;
        }
    }
}