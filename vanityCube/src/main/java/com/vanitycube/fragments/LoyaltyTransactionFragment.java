
package com.vanitycube.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.BookingListAdapter;
import com.vanitycube.adapter.LoyaltyTransactionListAdapter;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.loyalty.LoyaltyTransactionResponse;
import com.vanitycube.model.loyalty.MemberTransactionResponseListDTO;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class LoyaltyTransactionFragment extends Fragment implements OnItemClickListener, ServerResponseListener {
    private static final String TAG = "BookingHistoryFragment";
    private static final String LIST_TYPE = "list_type";
    private static final String GET_LOYALTY_TRANSACTIONS = "easyRewardzGetTransactionsAction";
    private String listType = "";
    private RestWebServices mRestWebservices;
    private ListView activitiesList;
    private boolean isActive = false;
    private ArrayList<MemberTransactionResponseListDTO> memberTransactionResponseList;
    BookingListAdapter notifyAdapter;
    private View emptyView;
    private TextView empty;
    ProgressDialog progressDialog;
    private ApiManager apiManager;
    private LoyaltyTransactionListAdapter loyaltyTransactionListAdapter;
    private VcDatabaseQuery mDBQuery;
    private UserModel mUserInfo;
    private String json, userId, orderId;
    private Gson gson;
    private SharedPref pref;

    public LoyaltyTransactionFragment() {
    }

    public static LoyaltyTransactionFragment newInstanceForBookingHistoryFragment(String listType) {
        Bundle args = new Bundle();
        args.putString(LIST_TYPE, listType);
        LoyaltyTransactionFragment loyaltyTransactionFragment = new LoyaltyTransactionFragment();
        loyaltyTransactionFragment.setArguments(args);
        return loyaltyTransactionFragment;
    }

    private void getUser() {
        json = pref.getUserModel();
        mUserInfo = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (mUserInfo == null) {
            mUserInfo = new UserModel();
        } else {
            userId = mUserInfo.getID();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRestWebservices = new RestWebServices();
        progressDialog = new ProgressDialog(getActivity());
        mDBQuery = new VcDatabaseQuery();
        gson = new Gson();

        pref = new SharedPref(VcApplicationContext.getInstance());
        //mUserInfo = mDBQuery.retreiveUser();
        getUser();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
//            new getActiveBookingAsyncTask(isActive).execute(null, null, null);
//        } else {
//            Toast.makeText(getActivity(),
//                    "Check your internet connection.", Toast.LENGTH_LONG)
//                    .show();
//        }
        // notifyAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_loyalty_transaction, null);
        activitiesList = (ListView) rootView.findViewById(R.id.listViewLoyaltyTransaction);
        empty = (TextView) rootView.findViewById(R.id.empty);
        //activitiesList.setEmptyView(empty);
        memberTransactionResponseList = new ArrayList<MemberTransactionResponseListDTO>();
        // ArrayList<BookingListModel> activities = new ArrayList<BookingListModel>();
        // for (int i = 0; i < 2; i++) {
        // BookingListModel notify = new BookingListModel();
        // notify.setFacilityName("Hair");
        // notify.setBookedDate("22June2015");
        // notify.setBookedTime("02:55PM");
        // notify.setBookingID("ABC21251");
        // notify.setPrice("1000");
        //
        // activities.add(notify);
        // }
        // BookingListAdapter notifyAdapter = new BookingListAdapter(getActivity(), activities);
        // activitiesList.setAdapter(notifyAdapter);
        setGetLoyaltyTransactions(mUserInfo.getID());
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

    }

    private void setGetLoyaltyTransactions(String userId) {
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        apiManager = new ApiManager(getActivity(), this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(LoyaltyTransactionResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_LOYALTY_TRANSACTIONS);
        String data = createTransactionRequestParamas(userId);
        params.put(ApplicationSettings.PARAM_API_DATA, data);

        apiManager.getStringPostResponse(GET_LOYALTY_TRANSACTIONS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private String createTransactionRequestParamas(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);


            //{"userId":"36788","smsCode":"455536","loyaltyTransactionCode":"dfa9166c-a55a-4093-963f-949495539982"}


        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return jsonObject.toString();
    }

    @Override
    public void positiveResponse(String TAG, String response) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (TAG == GET_LOYALTY_TRANSACTIONS && responseObj instanceof LoyaltyTransactionResponse) {

            LoyaltyTransactionResponse loyaltyTransactionResponse = (LoyaltyTransactionResponse) responseObj;
            //activitiesList.setEmptyView(empty);
            if (loyaltyTransactionResponse.getSuccess() == 1) {
                memberTransactionResponseList = loyaltyTransactionResponse.getMemberTransactionResponseListDTO();
                if (memberTransactionResponseList != null && memberTransactionResponseList.size() > 0) {
                    loyaltyTransactionListAdapter = new LoyaltyTransactionListAdapter(getActivity(), memberTransactionResponseList);
                    activitiesList.setAdapter(loyaltyTransactionListAdapter);
                    empty.setVisibility(View.GONE);
                    activitiesList.setVisibility(View.VISIBLE);
                    loyaltyTransactionListAdapter.notifyDataSetChanged();
                }

            } else {
                empty.setVisibility(View.VISIBLE);
                activitiesList.setVisibility(View.GONE);
            }


        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {
        System.out.print(errorResponse);
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


}
