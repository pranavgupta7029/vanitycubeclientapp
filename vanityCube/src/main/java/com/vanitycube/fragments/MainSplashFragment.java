package com.vanitycube.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.github.clans.fab.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.activities.DashboardActivity;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.adapter.CustomPagerAdapterDashboard;
import com.vanitycube.adapter.MainSplashAdapter;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.Banner;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.RestWebServices;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainSplashFragment extends Fragment {
    private GridView gridView;
    private ViewPager viewPager;
    CustomPagerAdapterDashboard customPagerAdapter;

    private int dotsCount;

    private ImageView[] dots;

    private Context context;
    private ArrayList<Banner> bannerItemsList;
    private RestWebServices restWebServices;
    private SharedPref pref;
    private FireBaseHelper firebaseHelper;
    private String TAG;
    private Gson gson;
    private ArrayList<ServiceModel> allServicesWithServiceTypesList;
    private static final String BANNER_ITEMS_LIST = "banner_items_list";
    private static final String SERVICES_LIST = "services_list";
    private static final String USER_ID = "user_id";
    private static final String FIRST_RUN = "first_run";
    private String userId;
    private boolean firstRunDashboardFragment;
    public int currentPage = 0;
    public int NUM_PAGES = 0;
    public MainSplashFragment mainSplashFragment;
    private String configNumber;
    private FloatingActionButton fab;


    public MainSplashFragment() {
    }

    //Parcelable is used to pass arraylist to fragments
    public static MainSplashFragment newInstanceForMainSplashFragment(ArrayList<Banner> bannerItemsList,
                                                                      ArrayList<ServiceModel> allServicesWithServiceTypesList,
                                                                      String userId) {

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BANNER_ITEMS_LIST, bannerItemsList);
        bundle.putParcelableArrayList(SERVICES_LIST, allServicesWithServiceTypesList);
        bundle.putString(USER_ID, userId);
        bundle.putBoolean(FIRST_RUN, true);
        MainSplashFragment dashboardFragment = new MainSplashFragment();
        dashboardFragment.setArguments(bundle);

        return dashboardFragment;
    }

    //Here we will receive the total service types list from Dashboard Activity
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = new SharedPref(VcApplicationContext.getInstance());

        bannerItemsList = new ArrayList<>();

        bannerItemsList = getArguments().getParcelableArrayList(BANNER_ITEMS_LIST);
        allServicesWithServiceTypesList = getArguments().getParcelableArrayList(SERVICES_LIST);
        userId = getArguments().getString(USER_ID);

        firstRunDashboardFragment = getArguments().getBoolean(FIRST_RUN);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        /*if(MainSplashActivity.scheme!=null && !MainSplashActivity.scheme.equals("")){
            DeepLink();
        }*/
        View rootView = inflater.inflate(R.layout.main_splash_fragment, parent, false);

        gson = new Gson();
        mainSplashFragment = this;

        gridView = (GridView) rootView.findViewById(R.id.gridview);
        gridView.setBackgroundColor(Color.WHITE);
        gridView.setVerticalSpacing(1);
        gridView.setHorizontalSpacing(1);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                AppEventsLogger logger;
                logger = AppEventsLogger.newLogger(context);
                Bundle parameters = new Bundle();
                parameters.putString("PACKAGE", allServicesWithServiceTypesList.get(position).getServiceName());
                //String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                //parameters.putString("Date",date);
                logger.logEvent("EVENT_PACKAGE_SELECTED", parameters);
                parameters = new Bundle();
                parameters.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, allServicesWithServiceTypesList.get(position).getServiceName());
                logGoogleEvents(ApplicationSettings.PAGE_CATEGORY, parameters);
                //int i1 = 0/0;

                Intent i = new Intent(getActivity(), DashboardActivity.class);
                i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
                i.putExtra("serviceName", allServicesWithServiceTypesList.get(position).getServiceName());
                startActivity(i);
            }
        });

        viewPager = rootView.findViewById(R.id.DashPager);
        NUM_PAGES = bannerItemsList.size();
        getConfiguration();
        fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateCallProcess();
            }
        });

        customPagerAdapter = new CustomPagerAdapterDashboard(getActivity(), bannerItemsList, mainSplashFragment);


        //Set number of dots based on image count
        LinearLayout dotsContainer = rootView.findViewById(R.id.dotsContainer);

        dotsCount = customPagerAdapter.getCount();

        dots = new ImageView[dotsCount];

        context = getContext();

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(context);
            dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.dot_light));

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(8, 0, 8, 0);
            dotsContainer.addView(dots[i], layoutParams);
        }

        if (dots.length > 0) {
            dots[0].setImageDrawable((ContextCompat.getDrawable(context, R.drawable.dot)));
        }

        viewPager.setAdapter(customPagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.dot_light));
                }

                dots[position].setImageDrawable((ContextCompat.getDrawable(context, R.drawable.dot)));
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        //bannerItemsList = new ArrayList<>();
        gridView.setAdapter(new MainSplashAdapter(getActivity(), allServicesWithServiceTypesList));
        //((MainSplashActivity) getActivity()).updateCart();


        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 1000, 3000);


        return rootView;
    }

    public void logGoogleEvents(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(getActivity());
            firebaseHelper.logEvents(eventName.toLowerCase(), bundle);
        } catch (Exception ex) {
            Log.e("PackageSelected", ex.getMessage());
        }
    }

    private void DeepLink() {
        if (MainSplashActivity.scheme.contains("_")) {
            MainSplashActivity.scheme = MainSplashActivity.scheme.replace("_", " ");
        }
        AppEventsLogger logger;
        logger = AppEventsLogger.newLogger(context);
        Bundle parameters = new Bundle();
        parameters.putString("PACKAGE", MainSplashActivity.scheme);
        //String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        //parameters.putString("Date",date);
        logger.logEvent("EVENT_PACKAGE_SELECTED", parameters);
        parameters = new Bundle();
        parameters.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, MainSplashActivity.scheme);
        logGoogleEvents(ApplicationSettings.PAGE_CATEGORY, parameters);
        //int i1 = 0/0;

        Intent i = new Intent(getActivity(), DashboardActivity.class);
        i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
        i.putExtra("serviceName", MainSplashActivity.scheme);
        startActivity(i);
    }

    public void bannerAction(String actionUrl) {
        String lastWord = getLastWord(actionUrl);
        if (lastWord.contains("_")) {
            lastWord = lastWord.replaceAll("_", " ");
        }
        Intent i = new Intent(getActivity(), DashboardActivity.class);
        i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
        i.putExtra("serviceName", lastWord);
        startActivity(i);
    }

    private String getLastWord(String word) {
        //String testString = "This is a sentence";
        String[] parts = word.split("/");
        String lastWord = parts[parts.length - 1];
        System.out.println(lastWord);
        if (lastWord.equalsIgnoreCase("Hair_and_Chemical"))
            lastWord = "Hair & Chemical";
        return lastWord;
    }

    private void initiateCallProcess() {
        if ((Build.VERSION.SDK_INT >= 23)) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, DashboardActivity.RC_CALL_PHONE);
            } else {
                fireCallIntent();
            }
        } else {
            fireCallIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case DashboardActivity.RC_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fireCallIntent();
                } else {
                    Toast.makeText(context, "Operation cancelled", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void fireCallIntent() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + configNumber));
        startActivity(intent);
    }

    private void getConfiguration() {
        ConfigurationModel configurationModel = gson.fromJson(pref.getConfiguration(), new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            configNumber = configurationModel.getPhoneNumber();
        } else {
            configNumber = "";
        }
    }


}