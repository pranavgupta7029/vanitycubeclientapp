package com.vanitycube.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.vanitycube.R;
import com.vanitycube.activities.Book;
import com.vanitycube.adapter.TimeSlotGridAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.CategoryModel;
import com.vanitycube.model.ExpectedDateTimeModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.TimeSlotModel;
import com.vanitycube.webservices.RestWebServices;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * Created by Nitin on 13/02/17.
 */

public class BookFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener {

    private ListView rechargeListView;
    private TextView noRecord, totalRecords, dayText, monthText, yearText, dateText, timeText;
    private ProgressBar progressBar;
    private FloatingActionButton filter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog;
    private Integer tabNumber = 0;
    public String date = null;
    public boolean isFilterOpen = false;
    public ArrayList<String> listModel = new ArrayList<>();
    public ArrayList<String> listVin = new ArrayList<>();
    private String filterOn = "";
    private String filterString = "";
    private ArrayList<TimeSlotModel> timeSlots;
    private CategoryModel categoryModel;
    private Integer categoryId;
    private Integer addressAreaId;
    private Integer serviceTime;
    private RestWebServices mRestWebServices;
    private ImageView calendar;
    Calendar timeout;
    private GridView mGridView;
    private Date dateSet;
    private BookFragment bookFragment;
    public Boolean flagOfCalendarClick = false;
    private Map<String, String> mapOfTimes = new HashMap<>();
    private RelativeLayout calendarLayout;
    private String selectedDate = "";


    public BookFragment() {

    }


    @SuppressLint("ValidFragment")
    public BookFragment(Integer tab, CategoryModel ctm, Integer addressId, Integer serviceTime) {
        tabNumber = tab;
        this.categoryModel = ctm;
        this.addressAreaId = addressId;
        this.serviceTime = serviceTime;
        bookFragment = this;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.date_picker_book, container, false);
        //DatePicker dp = rootView.findViewById(R.id.datePicker);

        dayText = rootView.findViewById(R.id.dayText);
        monthText = rootView.findViewById(R.id.monthText);
        yearText = rootView.findViewById(R.id.yearTxt);
        dateText = rootView.findViewById(R.id.dateText);
        calendar = rootView.findViewById(R.id.calendar);
        mGridView = rootView.findViewById(R.id.dash_grid);
        timeText = rootView.findViewById(R.id.timeText);
        calendarLayout = rootView.findViewById(R.id.calendarLayout);
        noRecord = rootView.findViewById(R.id.noRecord);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //OptionSelected(position);
                //setPosition(position);
                FlexboxLayout layout = (FlexboxLayout) parent.getChildAt(position);
                if(layout!=null) {
                    View v = layout.getChildAt(0);

                    int countOfViewsinGrid = parent.getChildCount();

                    for (int i = 0; i < countOfViewsinGrid; i++) {
                        if (i == position) {
                            TextView tv1 = (TextView) v;
                            tv1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_orange_rounded));
                            tv1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));

                            if (timeSlots != null && timeSlots.size() > 0) {
                                Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), true);
                                for (CategoryModel cm : Book.categoriesWithServiceTypesList) {
                                    if (cm.getCategoryName().equalsIgnoreCase(categoryModel.getCategoryName())) {
                                        cm.setCategoryTime(timeSlots.get(position).getSlotName());
                                        cm.setCategoryTimeId(timeSlots.get(position).getSlotId());
                                        cm.setCategoryDate(selectedDate);
                                    }
                                }
                            }
                            timeText.setText(tv1.getText().toString());
                        } else {
                            FlexboxLayout flexLayout = (FlexboxLayout) parent.getChildAt(i);
                            TextView textView = (TextView) flexLayout.getChildAt(0);
                            textView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view));
                            textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.vcDarkGrey));
                        }
                    }
                }
            }
        });


        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        long currentTimeInMillis = System.currentTimeMillis();
        final long maxDateValue = currentTimeInMillis + 10368000000L;
        mRestWebServices = new RestWebServices();
        timeout = Calendar.getInstance();
        timeout.setTimeInMillis(maxDateValue);

        // set current date into textview


        // set current date into datepicker
       /* dp.init(year, month, day, dateChangedListener);
        dp.setMinDate(currentTimeInMillis);
        dp.setMaxDate(maxDateValue);*/

        createMap();
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            flagOfCalendarClick = true;
            new getExpectedTimeSlotsAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }
        /*Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        getAvailableTimeSlotsSingleCategory(categoryModel.getCategoryId(),modifiedDate);*/

        calendarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd;
                if (dateSet != null && !dateSet.equals("")) {


                    now.setTime(dateSet);
                    dpd = DatePickerDialog.newInstance(
                            BookFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );

                } else {
                    dpd = DatePickerDialog.newInstance(
                            BookFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );
                }
                dpd.setThemeDark(false);
                dpd.vibrate(true);
                dpd.dismissOnPause(false);
                dpd.showYearPickerFirst(false);
                dpd.setMinDate(c);
                dpd.setMaxDate(timeout);
                dpd.setVersion(true ? DatePickerDialog.Version.VERSION_2 : DatePickerDialog.Version.VERSION_1);
                if (true) {
                    dpd.setAccentColor(Color.parseColor("#f36f21"));
                }
                if (true) {
                    dpd.setTitle("Select Date");
                }

                /*if (true) {
                    Calendar date1 = Calendar.getInstance();
                    Calendar date2 = Calendar.getInstance();
                    date2.add(Calendar.WEEK_OF_MONTH, -1);
                    Calendar date3 = Calendar.getInstance();
                    date3.add(Calendar.WEEK_OF_MONTH, 1);
                    Calendar[] days = {date1, date2, date3};
                    dpd.setHighlightedDays(days);
                }*/
        /*if (true) {
            Calendar[] days = new Calendar[13];
            for (int i = -6; i < 7; i++) {
                Calendar day = Calendar.getInstance();
                day.add(Calendar.DAY_OF_MONTH, i * 2);
                days[i + 6] = day;
            }
            dpd.setSelectableDays(days);
        }*/
                dpd.show(getActivity().getFragmentManager(), "BookFragment");
            }


        });


        //dp.setOnDateChangedListener(dateChangedListener);

        return rootView;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = year + "-" + (++monthOfYear) + "-" + dayOfMonth;
        //Toast.makeText(getActivity(), date, Toast.LENGTH_SHORT).show();
        Calendar cal = GregorianCalendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth); //Set Day of the Month, 1..31
        cal.set(Calendar.MONTH, monthOfYear - 1); //Set month, starts with JANUARY = 0
        cal.set(Calendar.YEAR, year); //Set year
        //System.out.println(cal.get(Calendar.DAY_OF_WEEK));
        //int dayo = cal.DAY_OF_WEEK;
        dateText.setText(dayOfMonth + "");
        yearText.setText(year + "");

        dateSet = cal.getTime();
        SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.US);
        String d = format.format(cal.getTime());
        //System.out.println(format.format(cal.getTime()));
        dayText.setText(d + ", ");
        SimpleDateFormat format1 = new SimpleDateFormat("MMM", Locale.US);
        String d1 = format1.format(cal.getTime());
        //System.out.println(d1);
        monthText.setText(d1 + "");
        timeText.setText("");


        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        String dateNew = format2.format(cal.getTime());
        flagOfCalendarClick = true;
        getAvailableTimeSlotsSingleCategory(categoryModel.getCategoryId(), dateNew);


        //selectedDate.setText(date);
        //getDietPlanList(date);
    }


    @Override
    public void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getActivity().getFragmentManager().findFragmentByTag("BookFragment");
        if (dpd != null) dpd.setOnDateSetListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();

    }


    public void applyFilter(String filterOn, String filterString) {
        this.filterOn = filterOn;
        this.filterString = filterString;
    }


    private void getAvailableTimeSlotsSingleCategory(String categoryId, String dateString) {
        JSONObject data = new JSONObject();
        CategoryModel requestedCategory = new CategoryModel();

        /*for (CategoryModel ctm : categoriesWithServiceTypesList) {
            if (ctm.getCategoryId().equals(categoryId)) {
                requestedCategory = ctm;
                break;
            }
        }*/
        requestedCategory = categoryModel;

        ArrayList<Integer> category = new ArrayList<>();
        category.add(Integer.parseInt(categoryId));

        ArrayList<Integer> time = new ArrayList<>();
        time.add(requestedCategory.getTotalServiceTime());

        ArrayList<String> date = new ArrayList<>();
        date.add(dateString);
        selectedDate = dateString;

        ArrayList<String> addressAreaIdsList = new ArrayList<>();
        addressAreaIdsList.add(addressAreaId + "");


        ArrayList<String> dateFormated = new ArrayList<>();
        /*for(String d:date){
            dateFormated.add(formatDate(d));
        }*/

        try {
            data.put("categoryIds", new JSONArray(category));
            data.put("areaIds", new JSONArray(addressAreaIdsList));
            data.put("servicesTime", new JSONArray(time));
            data.put("dates", new JSONArray(date));
        } catch (JSONException e) {
            Log.e("BookFragment", e.getMessage());
        }

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new getAvailableTimeSlotsByDateAsyncTask(data.toString()).execute(null, null, null);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    class getAvailableTimeSlotsByDateAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        String data = "";
        boolean result = false;

        getAvailableTimeSlotsByDateAsyncTask(String data) {
            progressDialog = new ProgressDialog(getActivity());
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Getting available time slots");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            timeSlots = new ArrayList<>();
            timeSlots = mRestWebServices.getAvailableTimeSlotsByDate(data);


            if (timeSlots.size() != 0) result = true;
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (timeSlots != null && timeSlots.size() > 0) {
                    String time = mapOfTimes.get(timeSlots.get(0).getSlotName()) != null ? mapOfTimes.get(timeSlots.get(0).getSlotName()) : mapOfTimes.get(timeSlots.get(0).getSlotName().toLowerCase());
                    timeText.setText(time);
                    Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), true);
                    for (CategoryModel cm : Book.categoriesWithServiceTypesList) {
                        if (cm.getCategoryName().equalsIgnoreCase(categoryModel.getCategoryName())) {
                            cm.setCategoryTime(timeSlots.get(0).getSlotName());
                            cm.setCategoryTimeId(timeSlots.get(0).getSlotId());
                            cm.setCategoryDate(selectedDate);

                        }
                    }
                } else {

                }


                if (result) {
                    System.out.print(timeSlots);
                    if (timeSlots != null && timeSlots.size() > 0) {
                        mGridView.setVisibility(View.VISIBLE);
                        TimeSlotGridAdapter dashboardAdapter = new TimeSlotGridAdapter(getActivity(), timeSlots, bookFragment);

                        mGridView.setAdapter(dashboardAdapter);

                        dashboardAdapter.notifyDataSetChanged();
                    } else {
                        mGridView.setVisibility(View.GONE);
                        String message = "We are going busy on " + selectedDate + ", please select another date or change service " + getServiceTypeNamesFromCategory(categoryModel);
                        categoryModel.setMessage(message);
                        // Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        noRecord.setVisibility(View.VISIBLE);
                        noRecord.setText(message);
                        timeText.setText("");
                        Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), false);
                    }
                } else {
                    mGridView.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), "We are going busy, please select another date or change service", Toast.LENGTH_LONG).show();
                    String message = "We are going busy on " + selectedDate + ", please select another date or change service " + getServiceTypeNamesFromCategory(categoryModel);
                    categoryModel.setMessage(message);
                    // Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    noRecord.setVisibility(View.VISIBLE);
                    noRecord.setText(message);
                    timeText.setText("");
                    Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), false);
                }
            } catch (Exception e) {
                Log.e("Bookfragment", e.getMessage());
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }


    public class getExpectedTimeSlotsAsyncTask extends AsyncTask<Void, Void, ExpectedDateTimeModel> {
        ProgressDialog progressDialog;
        JSONObject data = new JSONObject();
        ExpectedDateTimeModel expectedDateTimeModel = new ExpectedDateTimeModel();

        getExpectedTimeSlotsAsyncTask() {
            progressDialog = new ProgressDialog(getActivity());

            try {
                ArrayList<Integer> category = new ArrayList<>();
                if (categoryModel != null && categoryModel.getCategoryId() != null)
                    category.add(Integer.parseInt(categoryModel.getCategoryId()));

                ArrayList<Integer> time = new ArrayList<>();
                if(categoryModel!=null)
                    time.add(categoryModel.getTotalServiceTime());

                ArrayList<Integer> addressAreaIdsList = new ArrayList<>();
                addressAreaIdsList.add(addressAreaId);

                data.put("categoryIds", new JSONArray(category));
                data.put("areaIds", new JSONArray(addressAreaIdsList));
                data.put("servicesTime", new JSONArray(time));
            } catch (JSONException e) {
                Log.e("getExpectedTimeSlots", e.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Getting available time slots");
            progressDialog.show();
        }

        @Override
        protected ExpectedDateTimeModel doInBackground(Void... url) {
            expectedDateTimeModel = mRestWebServices.getExpectedTimeSlots(data.toString());
            return expectedDateTimeModel;
        }

        @Override
        protected void onPostExecute(ExpectedDateTimeModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                CategoryModel categoryModel1 = categoryModel;
                String date = expectedDateTimeModel.getDate();
                selectedDate = date;
                setDate(date);
                timeSlots = expectedDateTimeModel.getTimeSlots();
                String message = expectedDateTimeModel.getMessage();

                if (isValidString(date) && timeSlots != null && timeSlots.size() != 0) {
                    categoryModel1.setCategoryDate(date);
                    categoryModel1.setCategoryTime(timeSlots.get(0).getSlotName());
                    categoryModel1.setCategoryTimeId(timeSlots.get(0).getSlotId());
                    categoryModel1.setTimeSlots(timeSlots);

                    String time = mapOfTimes.get(timeSlots.get(0).getSlotName()) != null ? mapOfTimes.get(timeSlots.get(0).getSlotName()) : mapOfTimes.get(timeSlots.get(0).getSlotName().toLowerCase());
                    timeText.setText(time);
                    Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), true);
                    for (CategoryModel cm : Book.categoriesWithServiceTypesList) {
                        if (cm.getCategoryName().equalsIgnoreCase(categoryModel.getCategoryName())) {
                            cm.setCategoryTime(timeSlots.get(0).getSlotName());
                            cm.setCategoryTimeId(timeSlots.get(0).getSlotId());
                            cm.setCategoryDate(date);
                        }
                    }
                    mGridView.setVisibility(View.VISIBLE);
                    TimeSlotGridAdapter dashboardAdapter = new TimeSlotGridAdapter(getActivity(), timeSlots, bookFragment);

                    mGridView.setAdapter(dashboardAdapter);

                    dashboardAdapter.notifyDataSetChanged();

                } else {
                    if (isValidString(message)) {
                        String serviceTypeNames = getServiceTypeNamesFromCategory(categoryModel);
                        message = message.replace("{{categoryName}}", serviceTypeNames);
                        if (message != null) {
                            categoryModel.setMessage(message);
                            mGridView.setVisibility(View.GONE);
                            noRecord.setVisibility(View.VISIBLE);
                            noRecord.setText(message);
                            timeText.setText("");
                            Book.mapOFSelectedDatesAndTime.put(categoryModel.getCategoryName(), false);
                            //Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                /*if(timeSlots!=null && timeSlots.size()>0) {

                } else {
                    mGridView.setVisibility(View.GONE);
                }*/


            } catch (Exception e) {
                Log.e("BookFragment", e.getMessage());
            }
        }
    }

    private String getServiceTypeNamesFromCategory(CategoryModel categoryModel) {
        String serviceTypeNamesString = "";
        int iterator = 0;

        ArrayList<ServiceTypeModel> serviceTypesList = categoryModel.getServiceTypes();

        for (ServiceTypeModel stm : serviceTypesList) {
            if (iterator < (serviceTypesList.size() - 1)) {
                serviceTypeNamesString = serviceTypeNamesString.concat(stm.getServiceTypeName().concat(", "));
            } else {
                serviceTypeNamesString = serviceTypeNamesString.concat(stm.getServiceTypeName());
            }

            iterator++;
        }

        return serviceTypeNamesString;
    }

    public void setDate(String date1) {

        try {
            DateFormat formatr = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date date = formatr.parse(date1);
            dateSet = date;
            timeText.setText("");
        /*Calendar cal = GregorianCalendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth); //Set Day of the Month, 1..31
        cal.set(Calendar.MONTH,monthOfYear-1); //Set month, starts with JANUARY = 0
        cal.set(Calendar.YEAR,year); //Set year
        //System.out.println(cal.get(Calendar.DAY_OF_WEEK));
        //int dayo = cal.DAY_OF_WEEK;*/


            SimpleDateFormat format = new SimpleDateFormat("EEE", Locale.US);
            String d = format.format(date);
            //System.out.println(format.format(cal.getTime()));
            dayText.setText(d + ", ");
            SimpleDateFormat format1 = new SimpleDateFormat("MMM", Locale.US);
            String d1 = format1.format(date);
            //System.out.println(d1);
            monthText.setText(d1 + "");

            SimpleDateFormat formatNew = new SimpleDateFormat("dd", Locale.US);
            String dd = formatNew.format(date);

            SimpleDateFormat formatYear = new SimpleDateFormat("yyyy", Locale.US);
            String dYear = formatYear.format(date);

            dateText.setText(dd + "");
            yearText.setText(dYear + "");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    public void setTimeText(String time) {
        timeText.setText(time);
    }

    private void createMap() {
        mapOfTimes.put("09:00 am", "09:00 - 09:30 am");
        mapOfTimes.put("09:30 am", "09:30 - 10:00 am");
        mapOfTimes.put("10:00 am", "10:00 - 10:30 am");
        mapOfTimes.put("10:30 am", "10:30 - 11:00 am");
        mapOfTimes.put("11:00 am", "11:00 - 11:30 am");
        mapOfTimes.put("11:30 am", "11:30 - 12:00 pm");
        mapOfTimes.put("12:00 pm", "12:00 - 12:30 pm");
        mapOfTimes.put("12:30 pm", "12:30 - 01:00 pm");
        mapOfTimes.put("01:00 pm", "01:00 - 01:30 pm");
        mapOfTimes.put("01:30 pm", "01:30 - 02:00 pm");
        mapOfTimes.put("02:00 pm", "02:00 - 02:30 pm");
        mapOfTimes.put("02:30 pm", "02:30 - 03:00 pm");
        mapOfTimes.put("03:00 pm", "03:00 - 03:30 pm");
        mapOfTimes.put("03:30 pm", "03:30 - 04:00 pm");
        mapOfTimes.put("04:00 pm", "04:00 - 04:30 pm");
        mapOfTimes.put("04:30 pm", "04:30 - 05:00 pm");
        mapOfTimes.put("05:00 pm", "05:00 - 05:30 pm");
        mapOfTimes.put("05:30 pm", "05:30 - 06:00 pm");
        mapOfTimes.put("06:00 pm", "06:00 - 06:30 pm");
        mapOfTimes.put("06:30 pm", "06:30 - 07:00 pm");
        mapOfTimes.put("07:00 pm", "07:00 - 07:30 pm");
        mapOfTimes.put("07:30 pm", "07:30 - 08:00 pm");
        mapOfTimes.put("08:00 pm", "08:30 - 08:30 pm");
        mapOfTimes.put("08:30 pm", "08:30 - 09:00 pm");
    }
}
