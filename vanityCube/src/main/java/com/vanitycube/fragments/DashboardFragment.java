package com.vanitycube.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.activities.BookingSummaryActivity;
import com.vanitycube.activities.DashboardActivity;
import com.vanitycube.adapter.BodyPageAdapter;
import com.vanitycube.adapter.CustomPagerAdapterDashboard;
import com.vanitycube.adapter.DashboardAdapter;
import com.vanitycube.adapter.MyAdapter;
import com.vanitycube.adapter.PackageAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.Banner;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.CouponModel;
import com.vanitycube.model.Offer;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.RestWebServices;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DashboardFragment extends Fragment implements OnItemClickListener, OnClickListener {
    private Context context;

    private static final String TAG = "DashboardFragment";
    private static final String BANNER_ITEMS_LIST = "banner_items_list";
    private static final String OFFER_ITEMS_LIST = "offer_items_list";
    private static final String SERVICES_LIST = "services_list";
    private static final String SERVICE_NAMES_LIST = "service_names_list";
    private static final String SERVICES_MAP = "services_map";
    private static final String ALL_SERVICES = "all_services";
    private static final String USER_ID = "user_id";
    private static final String FIRST_RUN = "first_run";

    private RestWebServices mRestWebServices;
    private ProgressDialog progressDialog;

    private ListView serviceTypesListView;
    private ArrayList<ServiceModel> allServicesWithServiceTypesList;
    public HashMap<String, ArrayList<ServiceTypeModel>> allServicesWithServiceTypesMap;
    private ArrayList<String> serviceNamesList;
    private ArrayList<Banner> bannerItemsList;
    private ArrayList<Offer> offerItemsList;
    private ArrayList<ServiceTypeModel> finalSelectedServiceTypesList;
    private String serviceName;

    private GridView mGridView;
    private ViewPager viewPager;
    CustomPagerAdapterDashboard customPagerAdapter;

    private int dotsCount;

    private ImageView[] dots;
    private RecyclerView MyRecyclerView;

    public SharedPref pref;
    private Gson gson;
    public String json;
    public String userId;

    private ScrollView mainScrollView;
    private TextView serviceNameTextView, servicesAmountTextView, servicesAddedLabelTextView, numOfServicesTextView, emptyTextView, couponDiscountTextView;
    private LinearLayout serviceList;
    private CouponModel couponModel;
    private FlexboxLayout couponDiscountBarLayout;
    private TextView couponLabelTextView;
    private BodyPageAdapter bodyPageAdapter;
    private boolean firstRunDashboardFragment;
    private int finalSelectedServicesSize;
    private ArrayList<ServiceTypeModel> serviceTypesForSelectedService;
    private ArrayList<ServiceTypeModel> serviceTypesForSelectedServiceNew = new ArrayList<>();
    public ArrayList<String> serviceAddedList = new ArrayList<>();
    ArrayList<ServiceTypeModel> allServices;
    public static Map<String, Integer> mapOfPositionAdapter = new HashMap<>();
    private boolean flag_loading = false;
    private ArrayList<String> selectedServiceNameArray = new ArrayList<>();
    private String serviceId;
    private boolean flagOfScroll = false;
    private Button search;
    private EditText etSearch;
    public boolean flagOfAddItems = true;
    public LinearLayout searchLen;
    private String configNumber;
    private FloatingActionButton fab;
    private String searchString;

    public DashboardFragment() {
    }

    public void openSearch() {
        if (searchLen.getVisibility() == View.VISIBLE) {
            searchLen.setVisibility(View.GONE);
            etSearch.setText("");
            etSearch.requestFocus();
            getAdapter().getFilter().filter("");
            flagOfAddItems = true;
            View view = getActivity().getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(getActivity());
            }

            hideKeyboardFrom(getActivity(), view);

        } else {
            searchLen.setVisibility(View.VISIBLE);
            etSearch.requestFocus();

            if (etSearch.requestFocus()) {
                showKeyboard(etSearch);
                /*InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                //imm.showSoftInput(etSearch, 0);
                if (imm != null){
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                }*/

            }
        }
    }

    public void showKeyboard(final EditText ettext) {
        ettext.requestFocus();
        ettext.postDelayed(new Runnable() {
                               @Override
                               public void run() {
                                   if (getActivity() != null) {
                                       InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                       keyboard.showSoftInput(ettext, 0);
                                   }
                               }
                           }
                , 200);
    }


    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    //Parcelable is used to pass arraylist to fragments
    public static DashboardFragment newInstanceForDashboardFragment(ArrayList<Banner> bannerItemsList,
                                                                    ArrayList<Offer> offerItemsList,
                                                                    ArrayList<ServiceModel> allServicesWithServiceTypesList,
                                                                    ArrayList<String> serviceNamesList,
                                                                    HashMap<String, ArrayList<ServiceTypeModel>> allServicesWithServiceTypesMap,
                                                                    String userId, ArrayList<ServiceTypeModel> allServices, String serviceName, String searchString) {

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BANNER_ITEMS_LIST, bannerItemsList);
        bundle.putParcelableArrayList(OFFER_ITEMS_LIST, offerItemsList);
        bundle.putParcelableArrayList(SERVICES_LIST, allServicesWithServiceTypesList);
        bundle.putStringArrayList(SERVICE_NAMES_LIST, serviceNamesList);
        bundle.putSerializable(SERVICES_MAP, allServicesWithServiceTypesMap);
        bundle.putSerializable(ALL_SERVICES, allServices);
        bundle.putString(USER_ID, userId);
        bundle.putBoolean(FIRST_RUN, true);
        bundle.putString("serviceName", serviceName);
        bundle.putString("searchString", searchString);

        DashboardFragment dashboardFragment = new DashboardFragment();
        dashboardFragment.setArguments(bundle);

        return dashboardFragment;
    }

    public BodyPageAdapter getAdapter() {
        return bodyPageAdapter;
    }

    //Here we will receive the total service types list from Dashboard Activity
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRestWebServices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());

        couponModel = new CouponModel();
        bannerItemsList = new ArrayList<>();
        offerItemsList = new ArrayList<>();
        serviceTypesForSelectedService = new ArrayList<>();

        bannerItemsList = getArguments().getParcelableArrayList(BANNER_ITEMS_LIST);
        offerItemsList = getArguments().getParcelableArrayList(OFFER_ITEMS_LIST);
        allServicesWithServiceTypesList = getArguments().getParcelableArrayList(SERVICES_LIST);
        serviceNamesList = getArguments().getStringArrayList(SERVICE_NAMES_LIST);
        serviceName = getArguments().getString("serviceName");

        // noinspection unchecked
        allServicesWithServiceTypesMap = (HashMap<String, ArrayList<ServiceTypeModel>>) getArguments().getSerializable(SERVICES_MAP);
        allServices = (ArrayList<ServiceTypeModel>) getArguments().getSerializable(ALL_SERVICES);


        userId = getArguments().getString(USER_ID);

        firstRunDashboardFragment = getArguments().getBoolean(FIRST_RUN);

        finalSelectedServicesSize = 0;
        searchString = getArguments().getString("searchString");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.dashboard_content_new, parent, false);

        gson = new Gson();

        MyRecyclerView = rootView.findViewById(R.id.cardView);
        MyRecyclerView.setHasFixedSize(true);

        LinearLayoutManager MyLayoutManager = new LinearLayoutManager(getActivity());
        MyLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        MyRecyclerView.setLayoutManager(MyLayoutManager);

        mainScrollView = rootView.findViewById(R.id.mainScrollView);
        serviceTypesListView = rootView.findViewById(R.id.listViewBodyPage);
        serviceTypesListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        serviceList = rootView.findViewById(R.id.serviceList);
        fab = rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                initiateCallProcess();
            }
        });
        //serviceAddedList= new ArrayList<>();

        RelativeLayout footer = rootView.findViewById(R.id.footer);
        footer.setVisibility(View.VISIBLE);

        servicesAmountTextView = rootView.findViewById(R.id.servicesAmountTextView);
        servicesAddedLabelTextView = rootView.findViewById(R.id.servicesAddedLabelTextView);
        numOfServicesTextView = rootView.findViewById(R.id.numOfServicesTextView);
        serviceNameTextView = rootView.findViewById(R.id.serviceNameText);
        emptyTextView = rootView.findViewById(R.id.empty);
        etSearch = rootView.findViewById(R.id.etSearch);
        searchLen = rootView.findViewById(R.id.searchLen);
        getConfiguration();

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Call back the Adapter with current character to Filter
                //if(s.length()>=3)
                if (s.length() > 0) {
                    flagOfAddItems = false;
                } else {
                    flagOfAddItems = true;
                }
                getAdapter().getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //Cart button inside list
        TextView viewCartButton = rootView.findViewById(R.id.viewCartButton);
        viewCartButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoBookingSummary();
            }
        });

        mGridView = rootView.findViewById(R.id.dash_grid);

        couponDiscountBarLayout = rootView.findViewById(R.id.couponDiscountBarLayout);
        couponLabelTextView = rootView.findViewById(R.id.couponLabelTextView);
        couponDiscountTextView = rootView.findViewById(R.id.couponDiscountTextView);

        setOffersNew();
        if (!selectedServiceNameArray.contains(serviceName))
            selectedServiceNameArray.add(serviceName);
        //Select first grid item
        OptionSelectedFirst(serviceName);

        serviceTypesListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            public void onScrollStateChanged(AbsListView view, int scrollState) {


            }

            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {


                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    //flagOfScroll=false;
                    if (flag_loading == false) {
                        flag_loading = true;
                        addItems();
                    }
                } else if (!flagOfScroll) {
                    RelativeLayout cardView = (RelativeLayout) view.getChildAt(0);
                    if (cardView != null) {
                        RelativeLayout rel = (RelativeLayout) cardView.getChildAt(0);
                        if (rel.getVisibility() != View.GONE) {
                            String tag = rel.getTag().toString();
                            if (!serviceName.equalsIgnoreCase(tag))
                                selectCategory(tag);
                        }
                    }
                } else {
                    selectCategory(serviceName);
                }
            }
        });

        //Load list view, only call the adapter once
        populateListView(serviceTypesForSelectedService);

        if (searchString != null && !searchString.equals("") && searchString.equalsIgnoreCase("true")) {
            openSearch();
        }

        return rootView;
    }

    private void selectCategory(String serviceName) {

        this.serviceName = serviceName;
        FlexboxLayout layout = MyRecyclerView.findViewWithTag(serviceName);

        //int pos = MyRecyclerView.getChildLayoutPosition(layout);
        PackageAdapter.serviceName = serviceName;
        //int pos = layout.getPosi
        //int position = layout.getP
        //MyRecyclerView.scrollToPosition(position);

        if (layout != null) {
            TextView tv = (TextView) layout.getChildAt(0);
            if (tv.getText().toString().equalsIgnoreCase(serviceName)) {
                tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_orange_20dp));
                tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                //tv.setSelected(true);
            }
        }

        int i = 0;
        for (String sName : serviceNamesList) {
            if (!sName.equalsIgnoreCase(serviceName)) {
                FlexboxLayout layout1 = MyRecyclerView.findViewWithTag(sName);

                if (layout1 != null) {
                    TextView tv = (TextView) layout1.getChildAt(0);
                    //tv.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.rounded_text_view_20dp));
                    if (i == 0)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 1)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 2)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 3)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 4)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 5)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 6)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else if (i == 7)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 8)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 9)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 10)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 11)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 12)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 13)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else if (i == 14)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 15)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 16)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 17)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 18)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 19)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 20)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp));
                    tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    tv.setSelected(false);
                }
            } else {
                if (!flagOfScroll)
                    MyRecyclerView.scrollToPosition(i);
            }

            i++;
        }

        flagOfScroll = false;
    }

    private void addItems() {
        if (flagOfAddItems) {
            int i = 0;
            for (String serviceName : serviceNamesList) {

                if (!selectedServiceNameArray.contains(serviceName)) {
                    serviceTypesForSelectedService = allServicesWithServiceTypesMap.get(serviceName);
                    selectedServiceNameArray.add(serviceName);
                    serviceId = serviceTypesForSelectedService.get(0).getServiceID();
                    if (bodyPageAdapter != null) {
                        bodyPageAdapter.getServiceTypesList().addAll(serviceTypesForSelectedService);
                        bodyPageAdapter.notifyDataSetChanged();

                    /*PackageAdapter.serviceName = serviceName;
                    MyRecyclerView.scrollToPosition(i);
                    changeHeadingColorOfCategories(i,serviceName);*/
                    }
                    break;
                }
                i++;
            }
            flagOfScroll = false;
            flag_loading = false;
        }


    }

    public void getFinalSelectedServiceTypesList() {
        finalSelectedServiceTypesList = pref.getCartData();

        if (finalSelectedServiceTypesList == null) {
            finalSelectedServiceTypesList = new ArrayList<>();
        }

        finalSelectedServicesSize = finalSelectedServiceTypesList.size();
    }

    private int inList(ArrayList<ServiceTypeModel> list, ServiceTypeModel object) {
        int returnIndex = -1;
        int iterateIndex = 0;

        for (ServiceTypeModel serviceTypeObj : list) {
            if (serviceTypeObj.getServiceTypeID().equalsIgnoreCase(object.getServiceTypeID())) {
                returnIndex = iterateIndex;
                break;
            }

            iterateIndex++;
        }

        return returnIndex;
    }

    private void addServiceTypeFromCoupon(String serviceTypeId) {
        for (ServiceModel sm : allServicesWithServiceTypesList) {
            for (ServiceTypeModel stm : sm.getServiceTypes()) {
                if (serviceTypeId.equals(stm.getServiceTypeID())) {
                    if (inList(finalSelectedServiceTypesList, stm) == -1) {
                        stm.setNumOfPeople("1");
                        finalSelectedServiceTypesList.add(stm);
                        pref.putCartData(finalSelectedServiceTypesList);
                        Toast.makeText(getActivity(), "Service added", Toast.LENGTH_LONG).show();
                        updateFooter();
                    } else {
                        Toast.makeText(getActivity(), "Service already added to cart", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    public void setOffers() {
        if (offerItemsList != null && offerItemsList.size() > 0 && MyRecyclerView != null) {

            MyRecyclerView.setVisibility(View.VISIBLE);

            MyRecyclerView.setAdapter(new MyAdapter(getContext(), offerItemsList, new MyAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Offer offerItem) {
                    String thisCouponCode = offerItem.getCouponCode();
                    String thisServiceTypeId = offerItem.getServiceTypeId();

                    if (isValidString(thisCouponCode)) {
                        if (isValidString(userId)) {
                            if (finalSelectedServicesSize != 0) {
                                checkCoupon(thisCouponCode);
                            } else {
                                Toast.makeText(getActivity(), "Please add at least one service to apply coupon", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Coupon code copied!", Toast.LENGTH_LONG).show();
                            putCoupon(new CouponModel("", thisCouponCode, ""));
                            showCouponCode(thisCouponCode);
                        }
                    } else if (isValidString(thisServiceTypeId)) {
                        addServiceTypeFromCoupon(thisServiceTypeId);
                    }
                }
            }
            ));

            //initBanners();
        }
    }


    public void setOffersNew() {
        if (serviceNamesList != null && serviceNamesList.size() > 0 && MyRecyclerView != null) {

            MyRecyclerView.setVisibility(View.VISIBLE);

            PackageAdapter.serviceName = serviceName;
            MyRecyclerView.setAdapter(new PackageAdapter(getContext(), serviceNamesList, new PackageAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(String name, Integer position, View mView) {
                    //setPosition(position);

                    OptionSelected(position);
                    //MyRecyclerView.getLayoutManager().scrollToPosition(position).

                    /*TextView tv1 = (TextView) mView;
                    tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange));
                    tv1.setTextColor(getActivity().getColor(R.color.white));*/

                    /*FlexboxLayout layout = (FlexboxLayout) mView;
                    View v = layout.getChildAt(0);*/

                    //int countOfViewsinGrid = serviceNamesList.size();
                    /*TextView tv = (TextView)mView;

                    if(tv.getText().toString().equalsIgnoreCase(serviceName)) {

                    *//*for (int i = 0; i < countOfViewsinGrid; i++) {
                        if (i == position) {*//*
                        //TextView tv1 = (TextView) tv;
                        tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange_20dp));
                        tv.setTextColor(getActivity().getColor(R.color.white));
                    }else{
                        tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                        tv.setTextColor(getActivity().getColor(R.color.white));
                    }*/
                    /* } else {
                     *//*FlexboxLayout flexLayout = (FlexboxLayout) MyRecyclerView.getChildAt(i);
                            TextView textView = (TextView) flexLayout.getChildAt(0);
                            textView.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                            textView.setTextColor(getActivity().getColor(R.color.vcWhite));*//*
                        }
                    }*/

                    /*FlexboxLayout layout = (FlexboxLayout) parent.getChildAt(position);
                    View v = layout.getChildAt(0);

                    int countOfViewsinGrid = parent.getChildCount();

                    for (int i = 0; i < countOfViewsinGrid; i++) {
                        if (i == position) {
                            TextView tv1 = (TextView) v;
                            tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange));
                            tv1.setTextColor(getActivity().getColor(R.color.white));
                        } else {
                            FlexboxLayout flexLayout = (FlexboxLayout) parent.getChildAt(i);
                            TextView textView = (TextView) flexLayout.getChildAt(0);
                            textView.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view));
                            textView.setTextColor(getActivity().getColor(R.color.vcDarkGrey));
                        }
                    }*/
                    /*PackageAdapter.serviceName = serviceName;
                    int itemCount = MyRecyclerView.getAdapter().getItemCount();
                    for(int i=0;i<itemCount;i++){
                        FlexboxLayout layout = (FlexboxLayout) MyRecyclerView.getChildAt(i);
                        if(layout!=null) {
                            View v = layout.getChildAt(0);
                            TextView tv = (TextView) v;
                            if (i == position) {
                                if(tv.getText().toString().equalsIgnoreCase(serviceName)) {
                                    tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange_20dp));
                                    tv.setTextColor(getActivity().getColor(R.color.white));
                                    tv.setSelected(true);
                                }else{
                                    tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                                    tv.setTextColor(getActivity().getColor(R.color.white));
                                    tv.setSelected(false);
                                }

                            } else {
                                tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                                tv.setTextColor(getActivity().getColor(R.color.white));
                                tv.setSelected(false);
                            }
                        }else if(i==position){
                            TextView tv1 = (TextView) mView;
                            if(tv1.getText().toString().equalsIgnoreCase(serviceName)) {
                                tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange_20dp));
                                tv1.setTextColor(getActivity().getColor(R.color.white));
                                tv1.setSelected(true);
                            }else{
                                tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                                tv1.setTextColor(getActivity().getColor(R.color.white));
                                tv1.setSelected(false);
                            }
                        }
                    }*/


                }
            }
            ));


            //initBanners();
        }
    }


    private void changeHeadingColorOfCategories(int position, String serviceName) {

        FlexboxLayout layout = MyRecyclerView.findViewWithTag(serviceName);
        //int position = layout.getP
        MyRecyclerView.scrollToPosition(position);


        if (layout != null) {
            TextView tv = (TextView) layout.getChildAt(0);
            tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_orange_20dp));
            tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
            tv.setSelected(true);
        }
        int i = 0;
        for (String sName : serviceNamesList) {
            if (!sName.equalsIgnoreCase(serviceName)) {
                FlexboxLayout layout1 = MyRecyclerView.findViewWithTag(sName);

                if (layout1 != null) {
                    TextView tv = (TextView) layout1.getChildAt(0);
                    //tv.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.rounded_text_view_20dp));
                    if (i == 0)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 1)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 2)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 3)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 4)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 5)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 6)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else if (i == 7)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 8)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 9)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 10)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 11)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 12)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 13)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else if (i == 14)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_0));
                    else if (i == 15)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_1));
                    else if (i == 16)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_2));
                    else if (i == 17)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_3));
                    else if (i == 18)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_4));
                    else if (i == 19)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_5));
                    else if (i == 20)
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp_6));
                    else
                        tv.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_20dp));

                    tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                    tv.setSelected(false);
                    i++;
                }
            }
        }
        /*for(int i=0;i<itemCount;i++){
            FlexboxLayout layout = (FlexboxLayout) MyRecyclerView.findViewWithTag()
            if(layout!=null){
                TextView tv = (TextView)layout.getChildAt(0)
            }
        }*/

        /*MyRecyclerView.smoothScrollToPosition(position);
        //PackageAdapter.serviceName = serviceName;
        int itemCount = MyRecyclerView.getAdapter().getItemCount();
        for(int i=0;i<itemCount;i++){
            FlexboxLayout layout = (FlexboxLayout) MyRecyclerView.getChildAt(i);
            if(layout!=null) {
                View v = layout.getChildAt(0);
                TextView tv = (TextView) v;
                if (i == position) {
                    //if(tv.getText().toString().equalsIgnoreCase(serviceName)) {
                        tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange_20dp));
                        tv.setTextColor(getActivity().getColor(R.color.white));
                        tv.setSelected(true);
                    *//*}else{
                        tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                        tv.setTextColor(getActivity().getColor(R.color.white));
                        tv.setSelected(false);
                    }*//*

                } else {
                    tv.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                    tv.setTextColor(getActivity().getColor(R.color.white));
                    tv.setSelected(false);
                }
            }*//*else if(i==position){
                TextView tv1 = (TextView) mView;
                if(tv1.getText().toString().equalsIgnoreCase(serviceName)) {
                    tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_orange_20dp));
                    tv1.setTextColor(getActivity().getColor(R.color.white));
                    tv1.setSelected(true);
                }else{
                    tv1.setBackground(getActivity().getDrawable(R.drawable.rounded_text_view_20dp));
                    tv1.setTextColor(getActivity().getColor(R.color.white));
                    tv1.setSelected(false);
                }
            }*//*
        }*/
    }

    /*  -------------------------------------
        |
        |   CHECK COUPONS ASYNC TASK
        |
        -------------------------------------
    */

    private void putCoupon(CouponModel couponModel) {
        json = gson.toJson(couponModel);
        pref.putCouponModel(json);
    }

    private void checkCoupon(String couponCode) {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new checkCouponsAsyncTask(couponCode).execute(null, null, null);
        } else {
            Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private class checkCouponsAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String couponId, couponCode, couponAmount;

        checkCouponsAsyncTask(String couponCode) {
            this.couponCode = couponCode;
            progressDialog = new ProgressDialog(getActivity());
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Verifying Coupon");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            couponModel = mRestWebServices.checkCoupon(finalSelectedServiceTypesList, couponCode);
            return couponModel != null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    couponId = couponModel.getCouponId();
                    couponCode = couponModel.getCouponCode();
                    couponAmount = couponModel.getCouponAmount();

                    if (isValidString(couponId) && isValidString(couponCode) && isValidString(couponAmount)) {
                        putCoupon(couponModel);
                        showCouponDiscount(couponAmount);
                        Toast.makeText(getActivity(), "Coupon Applied", Toast.LENGTH_SHORT).show();
                    } else {
                        //Error message is inside coupon amount? This is absurd
                        Toast.makeText(getActivity(), couponAmount, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Sorry! coupon not available", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void getOfferCoupon() {
        try {
            json = pref.getCouponModel();

            CouponModel tempCouponModel = gson.fromJson(json, new TypeToken<CouponModel>() {
            }.getType());

            if (tempCouponModel != null) {
                couponModel = tempCouponModel;

                String couponId = couponModel.getCouponId();
                String couponCode = couponModel.getCouponCode();
                String couponAmount = couponModel.getCouponAmount();

                if (!isValidString(couponId) && isValidString(couponCode) && !isValidString(couponAmount)) {
                    showCouponCode(couponCode);
                } else {
                    showCouponDiscount(couponAmount);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void showCouponDiscount(String couponAmount) {
        couponDiscountBarLayout.setVisibility(View.VISIBLE);
        couponDiscountTextView.setText(couponAmount);
    }

    private void showCouponCode(String couponCode) {
        couponDiscountBarLayout.setVisibility(View.VISIBLE);
        couponLabelTextView.setText(getResources().getString(R.string.coupon_code_text));
        couponDiscountTextView.setText(String.valueOf(couponCode));
    }

    /*  -------------------------------------
        |
        |   POPULATE DASHBOARD OPTIONS
        |
        -------------------------------------
    */

    private void focusOnListView() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mainScrollView.smoothScrollTo(0, 780);
            }
        });
    }

    private void populateDashOptionList() {
        if (serviceNamesList != null && serviceNamesList.size() > 0) {

            DashboardAdapter dashboardAdapter = new DashboardAdapter(getActivity(), serviceNamesList);

            mGridView.setAdapter(dashboardAdapter);

            dashboardAdapter.notifyDataSetChanged();

            mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //OptionSelected(position);
                    //setPosition(position);
                    FlexboxLayout layout = (FlexboxLayout) parent.getChildAt(position);
                    View v = layout.getChildAt(0);

                    int countOfViewsinGrid = parent.getChildCount();

                    for (int i = 0; i < countOfViewsinGrid; i++) {
                        if (i == position) {
                            TextView tv1 = (TextView) v;
                            tv1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view_orange));
                            tv1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                        } else {
                            FlexboxLayout flexLayout = (FlexboxLayout) parent.getChildAt(i);
                            TextView textView = (TextView) flexLayout.getChildAt(0);
                            textView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_text_view));
                            textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.vcDarkGrey));
                        }
                    }
                }
            });

            setDynamicHeightGridView(mGridView);

        } else {
            ((DashboardActivity) getActivity()).reloadActivity(false);
        }
    }

    private void setDynamicHeightGridView(GridView gridView) {
        ListAdapter gridViewAdapter = gridView.getAdapter();

        if (gridViewAdapter == null) {
            return;
        }

        int totalHeight;
        int items = gridViewAdapter.getCount();
        int rows;

        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x;

        if (items > 3) {
            x = items / 3;
            rows = (int) (x + 2);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

    private void initBanners() {
        viewPager.setAdapter(customPagerAdapter);
    }

    //Load services for selected category type
    protected void OptionSelected(int position) {
        serviceName = serviceNamesList.get(position);
        // refreshAdapter();
        /*if(!serviceAddedList.contains(serviceName)) {
         *//**//*serviceTypesForSelectedServiceNew = allServicesWithServiceTypesMap.get(serviceName);
            serviceTypesForSelectedService.addAll(0, serviceTypesForSelectedServiceNew);
            serviceAddedList.add(serviceName);
            serviceTypesForSelectedService = allServices;
            //serviceTypesForSelectedService = allServicesWithServiceTypesMap.get(serviceName);
        }*/

        /*if(!selectedServiceNameArray.contains(serviceName)) {
            serviceTypesForSelectedService.addAll(allServicesWithServiceTypesMap.get(serviceName));
            //bodyPageAdapter.notifyDataSetChanged();
        }*/

        if (allServicesWithServiceTypesMap.get(serviceName) == null) {
            serviceName = serviceName.substring(0, 1).toUpperCase() + serviceName.substring(1);

        }

        serviceTypesForSelectedService = allServicesWithServiceTypesMap.get(serviceName);

        /*selectedServiceNameArray = new ArrayList<>();
        selectedServiceNameArray.add(serviceName);*/

        //serviceTypesForSelectedService = allServices;
        if (firstRunDashboardFragment) {
            firstRunDashboardFragment = false;
        } else {
            //focusOnListView();
            if (!selectedServiceNameArray.contains(serviceName)) {
                refreshAdapter();
                selectedServiceNameArray = new ArrayList<>();
                selectedServiceNameArray.add(serviceName);

            } else {
                setPosition(position, serviceName);
            }
            // setPosition(position);
            //focusOnListView();
        }

    }

    protected void OptionSelectedFirst(String name) {
        //serviceName = serviceNamesList.get(position);

        // refreshAdapter();
        /*if(!serviceAddedList.contains(serviceName)) {
         *//**//*serviceTypesForSelectedServiceNew = allServicesWithServiceTypesMap.get(serviceName);
            serviceTypesForSelectedService.addAll(0, serviceTypesForSelectedServiceNew);
            serviceAddedList.add(serviceName);
            serviceTypesForSelectedService = allServices;
            //serviceTypesForSelectedService = allServicesWithServiceTypesMap.get(serviceName);
        }*/

        if (allServicesWithServiceTypesMap.get(serviceName) == null) {
            serviceName = serviceName.substring(0, 1).toUpperCase() + serviceName.substring(1);

        }

        serviceTypesForSelectedService = allServicesWithServiceTypesMap.get(serviceName);
        //serviceTypesForSelectedService = allServices;
        if (firstRunDashboardFragment) {
            firstRunDashboardFragment = false;
        } else {
            //focusOnListView();
            refreshAdapter();
            // setPosition(position);
            //focusOnListView();
        }

    }

    /*  -------------------------------------
        |
        |   POPULATE LIST VIEW
        |
        -------------------------------------
    */

    private void setFinalAmountForAllServiceTypes() {
        for (ServiceModel sm : allServicesWithServiceTypesList) {
            ArrayList<ServiceTypeModel> serviceTypes = sm.getServiceTypes();

            for (ServiceTypeModel stm : serviceTypes) {
                int initialAmount = Integer.parseInt(stm.getPrice());
                int discount = Integer.parseInt(stm.getDiscount());
                int finalAmount = initialAmount - discount;
                stm.setFinalAmount(String.valueOf(finalAmount));
            }
        }
    }

    private void refreshAdapter() {
        if (bodyPageAdapter != null) {
            bodyPageAdapter.setFinalSelectedServiceTypesList(finalSelectedServiceTypesList);
            bodyPageAdapter.setServiceTypesList(serviceTypesForSelectedService);
            bodyPageAdapter.notifyDataSetChanged();
            setListViewHeightBasedOnChildren(serviceTypesListView);
            selectCategory(serviceName);
        }
    }

    private void refreshAdapterNew() {
        if (bodyPageAdapter != null) {
            bodyPageAdapter.setFinalSelectedServiceTypesList(finalSelectedServiceTypesList);
            bodyPageAdapter.setServiceTypesList(serviceTypesForSelectedService);
            bodyPageAdapter.notifyDataSetChanged();
            setListViewHeightBasedOnChildren(serviceTypesListView);
            //selectCategory(serviceName);
        }
    }

    private int getTotalServicesAmount() {
        int total = 0;

        for (ServiceTypeModel stm : finalSelectedServiceTypesList) {
            try {
                int numOfPeople = Integer.parseInt(stm.getNumOfPeople());
                int finalAmount = 0;
                if (pref.getIsUserMember())
                    finalAmount = Integer.parseInt(stm.getMembersPrice());
                else
                    finalAmount = Integer.parseInt(stm.getFinalAmount());
                total += (numOfPeople * finalAmount);
            } catch (Exception ex) {

            }
        }

        return total;
    }

    private void setTotalServicesAmount(TextView tv, int price) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");

        if (price == 0) {
            tv.setText(getResources().getString(R.string.float_zero_price));
        } else {
            tv.setText(String.valueOf(formatter.format(price)));
        }
    }

    private void showNoServices() {
        servicesAddedLabelTextView.setText(getResources().getString(R.string.no_services_added_text));
        numOfServicesTextView.setVisibility(View.GONE);
    }

    private void showServices(String numOfServices) {
        servicesAddedLabelTextView.setText(getResources().getString(R.string.services_added_text));
        numOfServicesTextView.setVisibility(View.VISIBLE);
        numOfServicesTextView.setText(numOfServices);
    }

    public void updateFooter() {
        getFinalSelectedServiceTypesList();

        String numOfServices = String.valueOf(finalSelectedServicesSize);

        if (numOfServices.equals("0")) {
            showNoServices();
        } else {
            showServices(numOfServices);
        }

        setTotalServicesAmount(servicesAmountTextView, getTotalServicesAmount());

        ((DashboardActivity) getActivity()).updateCart();
    }

    private void populateListView(ArrayList<ServiceTypeModel> list) {
        bodyPageAdapter = new BodyPageAdapter(getActivity(), serviceTypesForSelectedService, finalSelectedServiceTypesList, allServices);
        serviceTypesListView.setAdapter(bodyPageAdapter);

        //When our list is updated
        bodyPageAdapter.setOnListUpdatedListener(new BodyPageAdapter.OnListUpdatedListener() {
            @Override
            public void onListUpdated() {
                couponDiscountBarLayout.setVisibility(View.GONE);
                pref.putCouponModel("");
                updateFooter();
                //refreshAdapter();
                //setDynamicHeightGridView(mGridView);
            }
        });

        serviceTypesListView.setEmptyView(emptyTextView);
        serviceNameTextView.setText(serviceName);
        serviceNameTextView.setVisibility(View.GONE);
        // serviceList.setVisibility(View.VISIBLE);
        serviceTypesListView.setScrollContainer(true);
        setListViewHeightBasedOnChildren(serviceTypesListView);
        //serviceTypesListView.setSelection(10);
        //serviceTypesListView.smoothScrollToPosition(10);
    }

    private void setPosition(int position, String serviceName) {
        //serviceName = serviceNamesList.get(position);

        //final int counter = mapOfPositionAdapter.get(serviceName);

        /*RelativeLayout rel = (RelativeLayout) serviceTypesListView.findViewWithTag(serviceName);
        int pos = serviceTypesListView.getPositionForView(rel);
        serviceTypesListView.smoothScrollToPosition(pos);*/

        BodyPageAdapter bAdapter = (BodyPageAdapter) serviceTypesListView.getAdapter();
        ArrayList<ServiceTypeModel> serviceListModels = bAdapter.getServiceTypesList();
        //int itemCount = serviceTypesListView.getAdapter().getCount();

        int counter = 0;
        for (int i = 0; i < serviceListModels.size(); i++) {
            ServiceTypeModel sModel = serviceListModels.get(i);
            if (sModel.getServiceName().equalsIgnoreCase(serviceName)) {
                counter = i;
                flagOfScroll = true;
                break;
            }
        }
        serviceTypesListView.setSelection(counter);

        /*int itemCount = serviceTypesListView.getAdapter().getCount();

        for(int i=0;i<itemCount;i++){
            RelativeLayout layout = (RelativeLayout) serviceTypesListView.getChildAt(i);
            if(layout!=null) {
                RelativeLayout rel = (RelativeLayout) layout.getChildAt(0);
                if(rel.getVisibility()!=View.GONE) {
                    if (rel.getTag().toString().equalsIgnoreCase(serviceId)) {
                        serviceTypesListView.smoothScrollToPosition(i);
                        break;
                    }
                }
            }
        }*/


        /*new Handler().post(new Runnable() {
            @Override
            public void run() {
                serviceTypesListView.setSmoothScrollbarEnabled(true);
                serviceTypesListView.smoothScrollToPosition(counter);
            }
        });*/

        /*serviceTypesListView.post(new Runnable() {
            @Override
            public void run() {

                serviceTypesListView.setSelection(counter);
            }
        });*/


    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();

        if (listAdapter == null) {
            return;
        }

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;

        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, RelativeLayout.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
    }

    /*  -------------------------------------
        |
        |   GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    private void gotoBookingSummary() {
        Intent book_activity = new Intent(getActivity(), BookingSummaryActivity.class);
        startActivity(book_activity);
    }

    /*  -------------------------------------
        |
        |   DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onResume() {
        super.onResume();
        updateFooter();
        refreshAdapter();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }

    @Override
    public void onClick(View v) {
    }

    private void initiateCallProcess() {
        if ((Build.VERSION.SDK_INT >= 23)) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, DashboardActivity.RC_CALL_PHONE);
            } else {
                fireCallIntent();
            }
        } else {
            fireCallIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case DashboardActivity.RC_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fireCallIntent();
                } else {
                    Toast.makeText(getActivity(), "Please allow phone call permission", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void fireCallIntent() {
        try {
            Bundle parameters_google = new Bundle();
            TelephonyManager tMgr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            String mPhoneNumber = tMgr.getLine1Number();
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            parameters_google.putString("Date", date);
            parameters_google.putString("PHONE_NUMBER", mPhoneNumber);
            logGoogleEventsBooking("PHONE_TO_CC", parameters_google);
        } catch (SecurityException ex) {

        }


        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + configNumber));
        startActivity(intent);
    }

    private void getConfiguration() {
        ConfigurationModel configurationModel = gson.fromJson(pref.getConfiguration(), new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            configNumber = configurationModel.getPhoneNumber();
        } else {
            configNumber = "";
        }
    }

    public void logGoogleEventsBooking(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(getActivity());
            firebaseHelper.logEvents(eventName, bundle);
        } catch (Exception ex) {
            Log.e("Booking", ex.getMessage());
        }
    }

}