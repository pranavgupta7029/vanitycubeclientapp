package com.vanitycube.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.vanitycube.R;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.adapter.CustomPagerAdapterGallery;

public class GalleryFragment extends Fragment implements OnItemClickListener, OnClickListener {
    private ViewPager viewPager;
    private CustomPagerAdapterGallery customPagerAdapter;
    public CategoryPagerAdapter categoryPagerAdapter;

    private ImageView mGalleryImageView1, mGalleryImageView2, mGalleryImageView3, mGalleryImageView4, mGalleryImageView5, mGalleryImageView6;

    private int[] sliderImagesList = {R.drawable.hair1, R.drawable.hair2, R.drawable.face1, R.drawable.body1, R.drawable.foot1, R.drawable.foot2,
            R.drawable.foot3, R.drawable.foot4, R.drawable.make_up1, R.drawable.make_up2, R.drawable.package1};

    public GalleryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_gallery, parent, false);

        mGalleryImageView1 = rootView.findViewById(R.id.galleryImage1);
        mGalleryImageView2 = rootView.findViewById(R.id.galleryImage2);
        mGalleryImageView3 = rootView.findViewById(R.id.galleryImage3);
        mGalleryImageView4 = rootView.findViewById(R.id.galleryImage4);
        mGalleryImageView5 = rootView.findViewById(R.id.galleryImage5);
        mGalleryImageView6 = rootView.findViewById(R.id.galleryImage6);

        mGalleryImageView1.setOnClickListener(this);
        mGalleryImageView2.setOnClickListener(this);
        mGalleryImageView3.setOnClickListener(this);
        mGalleryImageView4.setOnClickListener(this);
        mGalleryImageView5.setOnClickListener(this);
        mGalleryImageView6.setOnClickListener(this);
        MainSplashActivity.changeHeader = true;

        DisplayMetrics metrics = new DisplayMetrics();

        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        customPagerAdapter = new CustomPagerAdapterGallery(getActivity(), sliderImagesList);

        viewPager = rootView.findViewById(R.id.imagePager);

        viewPager.setAdapter(customPagerAdapter);

        viewPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                int pageCount = sliderImagesList.length;

                if (position == pageCount) {
                    viewPager.setCurrentItem(0, false);
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        categoryPagerAdapter = new CategoryPagerAdapter(getActivity());

        ViewPager mViewPagerCategory = rootView.findViewById(R.id.categoryPager);
        mViewPagerCategory.setAdapter(categoryPagerAdapter);

        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }

    private void highlightService(int service) {
        int blackColorValue = ContextCompat.getColor(getContext(), R.color.vcBlack);
        int whiteColorValue = ContextCompat.getColor(getContext(), R.color.vcWhite);

        mGalleryImageView1.setBackgroundColor(blackColorValue);
        mGalleryImageView2.setBackgroundColor(blackColorValue);
        mGalleryImageView3.setBackgroundColor(blackColorValue);
        mGalleryImageView4.setBackgroundColor(blackColorValue);
        mGalleryImageView5.setBackgroundColor(blackColorValue);
        mGalleryImageView6.setBackgroundColor(blackColorValue);

        switch (service) {
            case 1:
                mGalleryImageView1.setBackgroundColor(whiteColorValue);
                break;
            case 2:
                mGalleryImageView2.setBackgroundColor(whiteColorValue);
                break;
            case 3:
                mGalleryImageView3.setBackgroundColor(whiteColorValue);
                break;
            case 4:
                mGalleryImageView4.setBackgroundColor(whiteColorValue);
                break;
            case 5:
                mGalleryImageView5.setBackgroundColor(whiteColorValue);
                break;
            case 6:
                mGalleryImageView6.setBackgroundColor(whiteColorValue);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.galleryImage1:
                highlightService(1);
                viewPager.setCurrentItem(0, true);
                break;
            case R.id.galleryImage2:
                highlightService(2);
                viewPager.setCurrentItem(2, true);
                break;
            case R.id.galleryImage3:
                highlightService(3);
                viewPager.setCurrentItem(3, true);
                break;
            case R.id.galleryImage4:
                highlightService(4);
                viewPager.setCurrentItem(4, true);
                break;
            case R.id.galleryImage5:
                highlightService(5);
                viewPager.setCurrentItem(8, true);
                break;
            case R.id.galleryImage6:
                highlightService(6);
                viewPager.setCurrentItem(10, true);
                break;
            default:
                break;
        }
    }

    class CategoryPagerAdapter extends PagerAdapter {
        Context mContext;
        LayoutInflater mLayoutInflater;

        CategoryPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return sliderImagesList.length;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.gallery_category_page, container, false);
            ImageView imageView = itemView.findViewById(R.id.galleryCategoryImage);
            imageView.setImageResource(sliderImagesList[position]);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == (arg1);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((LinearLayout) object);
        }
    }
}