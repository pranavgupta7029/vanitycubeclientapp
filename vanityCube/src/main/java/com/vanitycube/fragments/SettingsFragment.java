package com.vanitycube.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.activities.ChangePasswordActivity;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.activities.TermsActivity;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;

public class SettingsFragment extends Fragment implements OnItemClickListener, OnClickListener {
    private boolean isFacebook = false, isGoogle = false, isLoggedIn = false;
    private Context context;
    private SharedPref pref;

    public SettingsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_settings, parent, false);

        pref = new SharedPref(VcApplicationContext.getInstance());
        MainSplashActivity.changeHeader = true;

        getUser();

        if (pref.getFacebookID() != null && !(pref.getFacebookID().equalsIgnoreCase(""))) {
            isFacebook = true;
        }

        if (pref.getGoogleID() != null && !(pref.getGoogleID().equalsIgnoreCase(""))) {
            isGoogle = true;
        }

        FlexboxLayout changePasswordLayout = rootView.findViewById(R.id.changePasswordLayout);
        TextView changePasswordTextView = changePasswordLayout.findViewById(R.id.changePasswordTextView);
        TextView termsTextView = rootView.findViewById(R.id.termsTextView);
        TextView helpTextView = rootView.findViewById(R.id.helpTextView);

        if (!isLoggedIn) {
            changePasswordLayout.setVisibility(View.GONE);
        } else {
            if (!isFacebook && !isGoogle) {
                changePasswordTextView.setOnClickListener(this);
            } else {
                changePasswordTextView.setTextColor(ContextCompat.getColor(context, R.color.background));
            }
        }

        termsTextView.setOnClickListener(this);
        helpTextView.setOnClickListener(this);
        rootView.findViewById(R.id.privacyPolicyTextView).setOnClickListener(this);

        return rootView;
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        Gson gson = new Gson();
        String json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            String userId = userModel.getID();
            isLoggedIn = isValidString(userId);
        }
    }

    private void gotoTerms(int contentType) {
        Intent intent = new Intent(getActivity(), TermsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("content_type", contentType);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void gotoChangePassword() {
        Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
        intent.putExtra("changePassword", true);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        switch (v.getId()) {
            case R.id.privacyPolicyTextView:
                gotoTerms(1);
                break;

            case R.id.changePasswordTextView:
                gotoChangePassword();
                break;

            case R.id.termsTextView:
                gotoTerms(0);
                break;

            case R.id.helpTextView:
                ((MainSplashActivity) getActivity()).setTitle("Help & Support");
                HelpFragment helpfrag = new HelpFragment();
                transaction.replace(R.id.dash_content_frame, helpfrag);
                transaction.commit();
                break;

            default:
                break;
        }
    }
}