
package com.vanitycube.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.NotificationModel;

import java.util.ArrayList;
import java.util.HashMap;

public class VcDatabaseQuery {
    private static final String TAG = "VcDatabaseQuery";
    private VcDatabaseHandler m_cObjDataHandler;

    public VcDatabaseQuery() {
        m_cObjDataHandler = new VcDatabaseHandler();

    }

    public void insertUser(UserModel pObjUserModel) {
        /*
         * public static String TABLE_USER = "user"; public static String USER_ID = "id"; public static String USER_NAME =
         * "name"; public static String USER_PASSWORD = "password"; public static String USER_EMAIL = "email"; public static
         * String USER_DOB = "dob"; public static String USER_ABOUT = "about"; public static String USER_ADDRESS = "address";
         * public static String USER_NUMBER = "number"; public static String USER_DATE_CREATED = "date_created"; public
         * static String USER_PROFILEIMAGE = "profile_image";
         */
        clearTable(VcTableList.TABLE_USER);
        ContentValues initialvalues = new ContentValues();
        initialvalues.put(VcTableList.USER_ID,
                pObjUserModel.getID());
        initialvalues.put(VcTableList.USER_NAME,
                pObjUserModel.getName());
        initialvalues.put(VcTableList.USER_EMAIL,
                pObjUserModel.getEmail());
        initialvalues.put(VcTableList.USER_REFERRAL_NAME,
                pObjUserModel.getReferralName());
        initialvalues.put(VcTableList.USER_NUMBER,
                pObjUserModel.getNumber());
        initialvalues.put(VcTableList.USER_DOB,
                pObjUserModel.getDOB());
        initialvalues.put(VcTableList.USER_PROFILEIMAGE,
                pObjUserModel.getProfileImagePath());
        long rowsInserted = m_cObjDataHandler.insertContentValues(VcTableList.TABLE_USER, initialvalues);
        Log.i("ActivelyDatabaseQuery", rowsInserted + " inserted");
    }

    public boolean isExistingArea(String notificationId) {
        Cursor _cursor = null;
        try {
            String[] columns = {VcTableList.AREA_ID};
            String selection = VcTableList.AREA_ID + " = ? ";
            String[] selectionArgs = {"" + notificationId};
            String query = "SELECT area_id FROM VcTableList.TABLE_AREA WHERE area_id=" + notificationId;
            //query(table, columns, selection, selectionArgs, groupBy, having, orderBy)
            _cursor = m_cObjDataHandler.retriveQuery(query);
            if (_cursor != null)
                return _cursor.moveToNext();
        } catch (Exception e) {
            Log.e(VcTableList.TABLE_AREA, "isExistingPost()", e);
        } finally {
            _cursor.close();
        }
        return false;
    }

    /**
     * Inserting into Area Table.
     *
     * @author sanjeev
     */
    public void insertIntoAreaTable(String pQuery) {
        boolean rowsInserted = m_cObjDataHandler.executeQuery(pQuery);
        Log.i("ActivelyDatabaseQuery", rowsInserted + " :: " + pQuery);
    }

    public void truncateAreaTable(String pQuery) {
        boolean rowsInserted = m_cObjDataHandler.executeQuery(pQuery);
    }

    public String getMinAmountOfArea(String areaID) {
        String minAmount = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_ID + " = " + areaID;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            minAmount = lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_MIN_AMOUNT));
        }
        return minAmount;
    }

    /**
     * Select data from Area Table.
     *
     * @author sanjeev
     */
    public HashMap<String, String> getDataFromAreaTable() {
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        HashMap<String, String> mHash = new HashMap<String, String>();
        String areas[] = new String[lCursor.getCount()];

        if (lCursor != null && lCursor.moveToFirst()) {
            int index = 0;
            do {
                // String[] colors = getResources().getStringArray(R.array.colorList);
                areas[index] = lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_NAME));
                mHash.put(lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_NAME)),
                        lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_ID)));
                index++;
            } while (lCursor.moveToNext());
        }
        return mHash;
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(VcApplicationContext.getInstance(),
        // android.R.layout.simple_list_item_1, areas);
        // return adapter;
    }

    public HashMap<String, String> getCityListForAreaName(String area_name) {

        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_NAME + " = " + "'" + area_name + "'";
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        HashMap<String, String> mHash = new HashMap<String, String>();
//        String city[] = new String[lCursor.getCount()];

        if (lCursor != null && lCursor.moveToFirst()) {
            do {
                String cityID = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_ID));
                String lCityRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY + " WHERE " + VcTableList.CITY_ID + " = " + cityID;
                Cursor lCityCursor = m_cObjDataHandler.retriveQuery(lCityRetreiveQuery);
                if (lCityCursor != null && lCityCursor.moveToFirst()) {
                    mHash.put(lCityCursor.getString(lCityCursor.getColumnIndex(VcTableList.CITY_NAME)),
                            lCityCursor.getString(lCityCursor.getColumnIndex(VcTableList.CITY_ID)));
                }
            } while (lCursor.moveToNext());

        }
        return mHash;
    }


    public HashMap<String, String> getAreaforCity(String city_id) {
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.CITY_ID + " = " + city_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        HashMap<String, String> mHash = new HashMap<String, String>();
        String areas[] = new String[lCursor.getCount()];

        if (lCursor != null && lCursor.moveToFirst()) {
            int index = 0;
            do {
                // String[] colors = getResources().getStringArray(R.array.colorList);
                areas[index] = lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_NAME));
                mHash.put(lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_NAME)),
                        lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_ID)));
                index++;
            } while (lCursor.moveToNext());
        }
        return mHash;
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(VcApplicationContext.getInstance(),
        // android.R.layout.simple_list_item_1, areas);
        // return adapter;
    }

    public HashMap<String, String> getCityIdNameMap() {
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        HashMap<String, String> mHash = new HashMap<String, String>();
        String areas[] = new String[lCursor.getCount()];

        if (lCursor != null && lCursor.moveToFirst()) {
            int index = 0;
            do {
                // String[] colors = getResources().getStringArray(R.array.colorList);
                areas[index] = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_ID));
                mHash.put(lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_NAME)),
                        lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_ID)));
                index++;
            } while (lCursor.moveToNext());
        }
        return mHash;
        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(VcApplicationContext.getInstance(),
        // android.R.layout.simple_list_item_1, areas);
        // return adapter;
    }

    public String getHubId(String area_id) {
        String hubId = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_ID + " = " + area_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            hubId = lCursor.getString(lCursor.getColumnIndex(VcTableList.HUB_ID));
        }
        return hubId;
    }

    @SuppressWarnings("null")
    public String getCityIDForArea(String area_id) {
        String cityID = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_ID + " = " + area_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            cityID = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_ID));
        }
        return cityID;
    }

    @SuppressWarnings("null")
    public String getCityName(String city_id) {
        String cityName = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY + " WHERE " + VcTableList.CITY_ID + " = " + city_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            cityName = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_NAME));
        }
        return cityName;
    }

    // Method to get Id from names
    @SuppressWarnings("null")
    public String getCityID(String city_name) {
        String cityID = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY + " WHERE " + VcTableList.CITY_NAME + " = " + city_name;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            cityID = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_ID));
        }
        return cityID;
    }

    @SuppressWarnings("null")
    public String getAreaID(String area_name) {
        String areaID = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_NAME + " = " + area_name;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            areaID = lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_ID));
        }
        return areaID;
    }

    @SuppressWarnings("null")
    public String getStateID(String state_name) {
        String stateID = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY + " WHERE " + VcTableList.CITY_STATE_NAME + " = "
                + state_name;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            stateID = lCursor.getString(lCursor.getColumnIndex(VcTableList.STATE_ID));
        }
        return stateID;
    }

    @SuppressWarnings("null")
    public String[] getStateIdName(String city_id) {
        String stateIdName[] = {"", ""};
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_CITY + " WHERE " + VcTableList.CITY_ID + " = " + city_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            stateIdName[0] = lCursor.getString(lCursor.getColumnIndex(VcTableList.STATE_ID));
            stateIdName[1] = lCursor.getString(lCursor.getColumnIndex(VcTableList.CITY_STATE_NAME));
        }
        return stateIdName;
    }

    @SuppressWarnings("null")
    public String getAreaName(String area_id) {
        String areaName = "";
        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_AREA + " WHERE " + VcTableList.AREA_ID + " = " + area_id;
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);

        if (lCursor != null && lCursor.moveToFirst()) {
            areaName = lCursor.getString(lCursor.getColumnIndex(VcTableList.AREA_NAME));
        }
        return areaName;
    }

    public void clearAreaTable() {
        int rowDeleted = m_cObjDataHandler.emptyTable(VcTableList.TABLE_AREA);
    }

    public void clearTable(String tableName) {
        int rowDeleted = m_cObjDataHandler.emptyTable(tableName);
    }

    public void updateProfileDetailsTableProfileImage(UserModel profile) {
        ContentValues updatedValues = new ContentValues();

        updatedValues.put(VcTableList.USER_PROFILEIMAGE,
                profile.getProfileImagePath());

        System.out.println("initialvalues -->> " + updatedValues);
        long noOfRows = m_cObjDataHandler.UpdateContentValues(
                VcTableList.TABLE_USER, updatedValues);
        // Log.d(TAG + "updateAuditTable",
        // "No. of rows affected in witness table is " + noOfRows);
        // }
    }

    public void updateProfileDetailsTable(UserModel profile) {
        ContentValues updatedValues = new ContentValues();

        updatedValues.put(VcTableList.USER_NAME, profile.getName());
        updatedValues.put(VcTableList.USER_DOB, profile.getDOB());
        updatedValues.put(VcTableList.USER_EMAIL, profile.getEmail());
        updatedValues.put(VcTableList.USER_PROFILEIMAGE, profile.getProfileImagePath());

        System.out.println("initialvalues -->> " + updatedValues);
        long noOfRows = m_cObjDataHandler.UpdateContentValues(
                VcTableList.TABLE_USER, updatedValues);
        // Log.d(TAG + "updateAuditTable",
        // "No. of rows affected in witness table is " + noOfRows);
        // }
    }

    public void updateProfileDetailsTableWithRefferalCode(UserModel profile) {
        ContentValues updatedValues = new ContentValues();

        updatedValues.put(VcTableList.USER_NAME, profile.getName());
        updatedValues.put(VcTableList.USER_DOB, profile.getDOB());
        updatedValues.put(VcTableList.USER_EMAIL, profile.getEmail());
        updatedValues.put(VcTableList.USER_PROFILEIMAGE, profile.getProfileImagePath());
        updatedValues.put(VcTableList.USER_REFERRAL_NAME, profile.getReferralName());

        System.out.println("initialvalues -->> " + updatedValues);
        long noOfRows = m_cObjDataHandler.UpdateContentValues(
                VcTableList.TABLE_USER, updatedValues);
        // Log.d(TAG + "updateAuditTable",
        // "No. of rows affected in witness table is " + noOfRows);
        // }
    }

    public void removeUser() {

        int rowDeleted = m_cObjDataHandler.emptyTable(VcTableList.TABLE_USER);
        // Log.i("ActivelyDatabaseQuery", rowDeleted + " inserted");
    }

    public UserModel retreiveUser() {

        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_USER + "";
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        UserModel lObjUserModel = new UserModel();
        if (lCursor.moveToFirst()) {
            // UserModel lObjUserModel = null;
            lObjUserModel.setID(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_ID)));
            lObjUserModel.setName(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_NAME)));
            lObjUserModel.setEmail(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_EMAIL)));
            lObjUserModel.setReferralName(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_REFERRAL_NAME)));
            lObjUserModel.setNumber(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_NUMBER)));
            lObjUserModel.setDOB(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_DOB)));
            lObjUserModel.setProfileImagePath(lCursor.getString(lCursor.getColumnIndex(VcTableList.USER_PROFILEIMAGE)));

            // lObjUserModelVector.add(lObjUserModel);
        }

        return lObjUserModel;
    }

    public ArrayList<NotificationModel> retreiveNotifications() {

        Cursor lCursor = null;
        String lRetreiveQuery = "Select * FROM " + VcTableList.TABLE_NOTIFICATION + "";
        lCursor = m_cObjDataHandler.retriveQuery(lRetreiveQuery);
        ArrayList<NotificationModel> lObjNotifications = new ArrayList<NotificationModel>();
        if (lCursor != null && lCursor.moveToFirst()) {
            do {
                // UserModel lObjUserModel = null;
                NotificationModel lObjNotificationModel = new NotificationModel();
                lObjNotificationModel.setTitle(lCursor.getString(lCursor.getColumnIndex(VcTableList.NOTIFICATION_TITLE)));
                lObjNotificationModel.setMessage(lCursor.getString(lCursor.getColumnIndex(VcTableList.NOTIFICATION_MESSAGE)));
                lObjNotificationModel.setPicUrl(lCursor.getString(lCursor.getColumnIndex(VcTableList.NOTIFICATION_IMAGE)));
                lObjNotifications.add(lObjNotificationModel);

                // lObjUserModelVector.add(lObjUserModel);
            } while (lCursor.moveToNext());
        }

        return lObjNotifications;
    }

    public void insertNotification(NotificationModel pObjNotification) {
        /*
         * public static String TABLE_USER = "user"; public static String USER_ID = "id"; public static String USER_NAME =
         * "name"; public static String USER_PASSWORD = "password"; public static String USER_EMAIL = "email"; public static
         * String USER_DOB = "dob"; public static String USER_ABOUT = "about"; public static String USER_ADDRESS = "address";
         * public static String USER_NUMBER = "number"; public static String USER_DATE_CREATED = "date_created"; public
         * static String USER_PROFILEIMAGE = "profile_image";
         */
        ContentValues initialvalues = new ContentValues();
        initialvalues.put(VcTableList.NOTIFICATION_TITLE,
                pObjNotification.getTitle());
        initialvalues.put(VcTableList.NOTIFICATION_MESSAGE,
                pObjNotification.getMessage());
        initialvalues.put(VcTableList.NOTIFICATION_IMAGE,
                pObjNotification.getPicUrl());
        long rowsInserted = m_cObjDataHandler.insertContentValues(VcTableList.TABLE_NOTIFICATION, initialvalues);
        Log.i("ActivelyDatabaseQuery", rowsInserted + " inserted");
    }
}