
package com.vanitycube.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.vanitycube.constants.VcApplicationContext;

import java.util.ArrayList;

public class VcDatabaseHandler extends SQLiteOpenHelper {
    private String TAG = "VcDatabaseHandler";
    public static String DATABASE_NAME = "Actively.db";
    private static final int DATABASE_VERSION = 2;
    private SQLiteDatabase m_cDB;
    public static ArrayList<String> results = new ArrayList<String>();

    public VcDatabaseHandler() {
        super(VcApplicationContext.getInstance(), DATABASE_NAME, null,
                DATABASE_VERSION);
        try {
            m_cDB = this.getWritableDatabase();
        } catch (Exception ex) {
            m_cDB = null;
        }
    }

    public boolean createTable(String pQuery) {
        boolean lRetVal = false;
        lRetVal = executeQuery(pQuery);
        Log.i(TAG, pQuery);
        return lRetVal;
    }

    public boolean insertQuery(String pQuery) {
        boolean lRetVal = false;
        lRetVal = executeQuery(pQuery);
        return lRetVal;
    }

    public long insertContentValues(String pTableName,
                                    ContentValues initialValues) {
        /*
         * COntent values population example ContentValues initialValuesUser = new ContentValues();
         * initialValuesUser.put("pid", upid); initialValuesUser.put("username", username); initialValuesUser.put("password",
         * passwrd);
         */
        long rowsInserted = 0;
        if (m_cDB != null) {
            synchronized (m_cDB) {
                rowsInserted = m_cDB.insert(pTableName, null, initialValues);
            }
        }
        return rowsInserted;
    }

    public long UpdateContentValues(String pTableName,
                                    ContentValues initialValues) {
        /*
         * COntent values population example ContentValues initialValuesUser = new ContentValues();
         * initialValuesUser.put("pid", upid); initialValuesUser.put("username", username); initialValuesUser.put("password",
         * passwrd);
         */
        long rowsInserted = 0;
        if (m_cDB != null) {
            synchronized (m_cDB) {
                rowsInserted = m_cDB.update(pTableName, initialValues, null, null);
            }
        }
        return rowsInserted;
    }

    public int retriveCount(String pQuery) {
        return executeScalar(pQuery);
    }

    public Cursor retriveQuery(String pQuery) {
        return executeCursor(pQuery);
    }

    public boolean updateQuery(String pQuery) {
        return executeQuery(pQuery);
    }

    public boolean DeleteQuery(String pQuery) {
        return executeQuery(pQuery);
    }

    public int emptyTable(String tableName) {
        int deleted = 0;
        if (m_cDB != null) {
            synchronized (m_cDB) {
                deleted = m_cDB.delete(tableName, "1", null);
            }
        }
        return deleted;
    }

    // create, insert, delete, update Query
    public boolean executeQuery(String pQuery) {
        boolean lRetVal = false;
        try {
            if (m_cDB != null) {
                synchronized (m_cDB) {
                    m_cDB.execSQL(pQuery);
                    lRetVal = true;
                }
            }
            // closeDatabase();
        } catch (Exception ex) {
            System.out.println("executequery: " + ex.toString());
        }
        return lRetVal;
    }

    // select statement
    private Cursor executeCursor(String pQuery) {
        Cursor lRetVal = null;
        try {
            if (m_cDB != null) {
                synchronized (m_cDB) {
                    lRetVal = m_cDB.rawQuery(pQuery, null);
                }
            }
            // closeDatabase();
        } catch (Exception ex) {
            System.out.println("executeCursor " + ex.toString());
        }
        return lRetVal;
    }

    // get the count
    private int executeScalar(String pQuery) {
        int lRetVal = 0;
        Cursor lCursor = null;
        try {
            if (m_cDB != null) {
                synchronized (m_cDB) {
                    lCursor = m_cDB.rawQuery(pQuery, null);
                    if (null != lCursor) {
                        lRetVal = lCursor.getCount();
                        lCursor.close();
                        lCursor = null;
                    }
                }
            }
            // closeDatabase();
        } catch (Exception ex) {
            System.out.println("executeScalar: " + ex.toString());
        }
        return lRetVal;
    }

    @Override
    public void onCreate(SQLiteDatabase pDB) {
        m_cDB = pDB;
        createAllTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase pDB, int oldVersion, int newVersion) {
        System.out.println("Called onUpgrade()");
        m_cDB = pDB;
        try {

            if (oldVersion == 1 && newVersion == 2) {
                if(m_cDB!=null)
                    m_cDB.execSQL(VcTableList.NOTIFICATION_IMAGE_COLUMN);
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void createAllTables() {
        createTable(VcTableList.CREATE_TABLE_USER);
        createTable(VcTableList.CREATE_TABLE_AREA);
        createTable(VcTableList.CREATE_TABLE_CART);
        createTable(VcTableList.CREATE_TABLE_STATE);
        createTable(VcTableList.CREATE_TABLE_CITY);
        createTable(VcTableList.CREATE_TABLE_NOTIFICATION);

        Log.i(TAG, "Creating Table 'area' :: " + VcTableList.CREATE_TABLE_AREA);
        // createTable(VcTableList.CREATE_TABLE_COUNTRY);
        // createTable(VcTableList.CREATE_TABLE_STATE);
        // createTable(VcTableList.CREATE_TABLE_CITY);
    }
}