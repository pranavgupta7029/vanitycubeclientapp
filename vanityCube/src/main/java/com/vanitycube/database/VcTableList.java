
package com.vanitycube.database;

import android.provider.BaseColumns;

public class VcTableList implements BaseColumns {
    public static String TABLE_STATE = "state";
    public static String STATE_ID = "state_id";
    public static String STATE_NAME = "name";
    public static String STATE_STATUS = "status";

    public static final String CREATE_TABLE_STATE = "CREATE TABLE IF NOT EXISTS " + TABLE_STATE
            + "("
            + STATE_ID + " TEXT PRIMARY KEY NOT NULL,"
            + STATE_NAME + " TEXT NOT NULL,"
            + STATE_STATUS + " TEXT"
            + ")";

    public static String TABLE_CITY = "city";
    public static String CITY_ID = "city_id";
    public static String CITY_NAME = "CityName";
    public static String CITY_STATUS = "CityStatus";
    public static String CITY_STATE_NAME = "StateName";

    public static final String CREATE_TABLE_CITY = "CREATE TABLE IF NOT EXISTS " + TABLE_CITY
            + "("
            + CITY_ID + " TEXT PRIMARY KEY NOT NULL,"
            + STATE_ID + " TEXT NOT NULL,"
            + CITY_NAME + " TEXT NOT NULL,"
            + CITY_STATE_NAME + " TEXT NOT NULL"
            + ")";

    public static String TABLE_AREA = "area_table";
    public static String AREA_ID = "area_id";
    public static String AREA_NAME = "area";
    public static String HUB_ID = "hub_id";
    public static String AREA_MIN_AMOUNT = "HubAreaMinimumAmount";
    public static final String CREATE_TABLE_AREA = "CREATE TABLE IF NOT EXISTS " + TABLE_AREA
            + "("
            + AREA_ID + " TEXT NOT NULL,"
            + AREA_NAME + " TEXT NOT NULL,"
            + CITY_ID + " TEXT NOT NULL,"
            + HUB_ID + " TEXT NOT NULL,"
            + AREA_MIN_AMOUNT + " TEXT NOT NULL"
            + ")";

    public static String TABLE_USER = "user";
    public static String USER_ID = "user_id";
    public static String USER_NAME = "name";
    public static String USER_EMAIL = "email";
    public static String USER_NUMBER = "contact";
    public static String USER_DOB = "dob";
    public static String USER_GENDER = "gender";
    public static String USER_REFERRAL_NAME = "referral_name";
    public static String USER_PROFILEIMAGE = "profile_image";

    public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_USER
            + "("
            + USER_ID + " TEXT PRIMARY KEY NOT NULL,"
            + USER_NAME + " TEXT NOT NULL,"
            + USER_EMAIL + " TEXT NOT NULL,"
            + USER_NUMBER + " TEXT,"
            + USER_DOB + " TEXT,"
            + USER_REFERRAL_NAME + " TEXT,"
            + USER_PROFILEIMAGE + " TEXT"
            + ")";

    public static String TABLE_ACTIVITYLIST = "activitylist";
    public static String ACTIVITY_ID = "activityid";
    public static String ACTIVITY_NAME = "activity_name";
    public static String ACTIVITY_STATUS = "status_id";

    public static final String CREATE_TABLE_ACTIVITYLIST = "CREATE TABLE IF NOT EXISTS " + TABLE_ACTIVITYLIST
            + "("
            + ACTIVITY_ID + " TEXT PRIMARY KEY NOT NULL,"
            + ACTIVITY_NAME + " TEXT NOT NULL,"
            + ACTIVITY_STATUS + " TEXT NOT NULL,"
            + ")";

    public static String TABLE_CART = "cart";
    public static String SERVICE_TYPE_ID = "service_type_id";
    public static String SERVICE_TYPE_NAME = "ServiceTypeName";
    public static String SERVICE_ID = "ServiceId";
    public static String SERVICE_NAME = "ServiceName";
    public static String SERVICE_DESCRIPTION = "description";
    public static String SERVICE_IMAGE = "image";
    public static String SERVICE_PRICE = "price";
    public static String SERVICE_DISCOUNT = "discount";
    public static String SERVICE_TIME = "time";
    public static String SERVICE_NUM_PEOPLE = "numOfPeople";



    public static final String CREATE_TABLE_CART = "CREATE TABLE IF NOT EXISTS " + TABLE_CART
            + "("
            + SERVICE_TYPE_ID + " TEXT PRIMARY KEY NOT NULL,"
            + SERVICE_TYPE_NAME + " TEXT NOT NULL,"
            + SERVICE_ID + " TEXT NOT NULL,"
            + SERVICE_NAME + " TEXT NOT NULL,"
            + SERVICE_DESCRIPTION + " TEXT NOT NULL,"
            + SERVICE_IMAGE + " TEXT NOT NULL,"
            + SERVICE_PRICE + " TEXT NOT NULL,"
            + SERVICE_DISCOUNT + " TEXT NOT NULL,"
            + SERVICE_TIME + " TEXT NOT NULL,"
            + SERVICE_NUM_PEOPLE + " TEXT NOT NULL"
            + ")";

    public static String TABLE_NOTIFICATION = "notification";
    public static String NOTIFICATION_ID = "notification_id";
    public static String NOTIFICATION_TITLE = "title";
    public static String NOTIFICATION_MESSAGE = "message";
    public static String NOTIFICATION_IMAGE = "image_url";

    public static final String CREATE_TABLE_NOTIFICATION = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATION
            + "("
            + NOTIFICATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NOTIFICATION_TITLE + " TEXT NOT NULL,"
            + NOTIFICATION_MESSAGE + " TEXT NOT NULL,"
            + NOTIFICATION_IMAGE + " TEXT"
            + ")";


    public static final String NOTIFICATION_IMAGE_COLUMN= "ALTER TABLE "+TABLE_NOTIFICATION+" ADD COLUMN "+NOTIFICATION_IMAGE+" text";
}