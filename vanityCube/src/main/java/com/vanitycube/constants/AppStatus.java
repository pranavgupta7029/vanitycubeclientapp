package com.vanitycube.constants;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class AppStatus {
    private static AppStatus instance;

    NetworkInfo wifiInfo, mobileInfo;
    boolean connected = false;

    public static AppStatus getInstance(Context ctx) {
        Context appContext = ctx;

        if (instance == null) {
            instance = new AppStatus();
        }

        return instance;
    }

    public static boolean isOnline(Context ctx) {
        boolean connected = false;
        ConnectivityManager connectivityManager;

        try {
            connectivityManager = (ConnectivityManager) ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetInfo = connectivityManager
                    .getActiveNetworkInfo();

            connected = activeNetInfo != null && activeNetInfo.isAvailable()
                    && activeNetInfo.isConnected();

            return connected;

        } catch (Exception e) {
            System.out.println("Check Network Connectivity: " + e.getMessage());
        }

        return connected;
    }
}
