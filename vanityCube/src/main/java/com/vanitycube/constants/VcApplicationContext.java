
package com.vanitycube.constants;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.multidex.MultiDex;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.onesignal.OneSignal;
import com.vanitycube.MyNotiReceivedHandler;
import com.vanitycube.settings.ApplicationSettings;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

public class VcApplicationContext extends Application {

    private static VcApplicationContext mObjContext = null;
    private static final String TAG = VcApplicationContext.class.getName();

    @Override
    public void onCreate() {
        super.onCreate();

        if (!ApplicationSettings.TESTING) {
            Fabric.with(this, new Crashlytics());
        }

        FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/Montserrat-Regular.otf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Montserrat-Regular.otf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/Montserrat-Regular.otf");
        mObjContext = this;
        showHashKey(mObjContext);
        Log.d(TAG, "Starting Application" + this.toString());
        //UAirship.shared().getPushManager().setUserNotificationsEnabled(true);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        OneSignal.startInit(this)
                .setNotificationReceivedHandler( new MyNotiReceivedHandler() )
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

    }

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.vanitycube", PackageManager.GET_SIGNATURES); // Your package name here
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static VcApplicationContext getInstance() {
        if (null == mObjContext) {
            mObjContext = new VcApplicationContext();
        }
        return mObjContext;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
