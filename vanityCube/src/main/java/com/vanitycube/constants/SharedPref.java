package com.vanitycube.constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.model.ServiceTypeModel;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPref {
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;

    public SharedPref(Context pContext) {
        settings = PreferenceManager.getDefaultSharedPreferences(pContext);
        editor = settings.edit();
    }

    /*  -------------------------------------
        |
        |  CART PARAMETERS
        |
        -------------------------------------
    */

    public void clearCartData() {
        editor.remove("cartData");
        editor.apply();
    }

    public void putCartData(ArrayList<ServiceTypeModel> cartData) {
        Gson gson = new GsonBuilder().create();
        String cartDataString = gson.toJson(cartData);
        editor.putString("cartData", cartDataString);
        editor.commit();
    }

    public ArrayList<ServiceTypeModel> getCartData() {
        Gson gson = new GsonBuilder().create();
        String cartDataString = settings.getString("cartData", null);
        Type type = new TypeToken<ArrayList<ServiceTypeModel>>() {
        }.getType();
        return gson.fromJson(cartDataString, type);
    }

    public String getTimeStamp() {
        return settings.getString("TimeStampId", "-1");
    }

    public void putTimeStamp(String timeStampId) {
        editor.putString("TimeStampId", timeStampId);
        editor.commit();
    }

    public String getTimer() {
        return settings.getString("TimerId", "-1");
    }

    public void putTimer(String timerId) {
        editor.putString("TimerId", timerId);
        editor.commit();
    }

    /*  -------------------------------------
        |
        |  OTHER PARAMETERS
        |
        -------------------------------------
    */

    public String getConfiguration() {
        return settings.getString("Configuration", null);
    }

    public void putConfiguration(String configurationModel) {
        editor.putString("Configuration", configurationModel);
        editor.commit();
    }

    public void putGCMToken(String token) {
        editor.putString("token", token);
        editor.commit();
    }

    public String getGCMToken() {
        return settings.getString("token", null);
    }

    public void putIsLogin(boolean isLogin) {
        editor.putBoolean("isLogin", isLogin);
        editor.commit();
    }

    public boolean getIsLogin() {
        return settings.getBoolean("isLogin", false);
    }

    public void putReloadRequired(boolean reloadRequired) {
        editor.putBoolean("reloadRequired", reloadRequired);
        editor.commit();
    }

    public boolean getReloadRequired() {
        return settings.getBoolean("reloadRequired", false);
    }

    public void putOtherFragmentsActive(boolean b) {
        editor.putBoolean("OtherFragmentsActive", b);
        editor.commit();
    }

    public void putCameraIntentActive(Boolean cameraIntentActive) {
        editor.putBoolean("CameraIntentActive", cameraIntentActive);
        editor.commit();
    }

    public Boolean getCameraIntentActive() {
        return settings.getBoolean("CameraIntentActive", false);
    }

    public boolean getOtherFragmentsActive() {
        return settings.getBoolean("OtherFragmentsActive", false);
    }

    public void putAppFirstRun(Boolean firstRun) {
        editor.putBoolean("FirstRun", firstRun);
        editor.commit();
    }

    public Boolean getAppFirstRun() {
        return settings.getBoolean("FirstRun", false);
    }

    /*  -------------------------------------
        |
        |  AREA PARAMETERS
        |
        -------------------------------------
    */

    public void putAreaId(String LoginId) {
        editor.putString("AreaId", LoginId);
        editor.commit();
    }

    public String getAreaId() {
        return settings.getString("AreaId", "-1");
    }


    public void putLatitude(String latitude) {
        editor.putString("Latitude", latitude);
        editor.commit();
    }

    public String getLatitudeId() {
        return settings.getString("Latitude", "-1");
    }

    public void putLongitude(String longitude) {
        editor.putString("Longitude", longitude);
        editor.commit();
    }

    public String getLongitude() {
        return settings.getString("Longitude", "-1");
    }


    public String getCityId() {
        return settings.getString("CityId", "-1");
    }

    public void putCityId(String city_id) {
        editor.putString("CityId", city_id);
        editor.commit();
    }

    public String getCityName() {
        return settings.getString("CityName", "-1");
    }

    public void putCityName(String city_name) {
        editor.putString("CityName", city_name);
        editor.commit();
    }

    public String getHubId() {
        return settings.getString("HubId", "-1");
    }

    public void putHubId(String LoginId) {
        editor.putString("HubId", LoginId);
        editor.commit();
    }

    public String getAreaName() {
        return settings.getString("AreaName", "");
    }

    public void putAreaName(String areaName) {
        editor.putString("AreaName", areaName);
        editor.commit();
    }

    /*  -------------------------------------
        |
        |  BOOKING PARAMETERS
        |
        -------------------------------------
    */

    public String getCategoriesWithServiceTypes() {
        return settings.getString("CategoriesWithServiceTypes", "");
    }

    public void putCategoriesWithServiceTypes(String categoriesWithServiceTypes) {
        editor.putString("CategoriesWithServiceTypes", categoriesWithServiceTypes);
        editor.commit();
    }

    public String getTxnId() {
        return settings.getString("TxnId", "");
    }

    public void putTxnId(String txnId) {
        editor.putString("TxnId", txnId);
        editor.commit();
    }

    public Boolean getIsConvenienceFee() {
        return settings.getBoolean("isConvenienceFee", false);
    }

    public void putIsConvenienceFee(Boolean isConvenienceFee) {
        editor.putBoolean("isConvenienceFee", isConvenienceFee);
        editor.commit();
    }

    public String getConvenienceFee() {
        return settings.getString("ConvenienceFee", "");
    }

    public void putConvenienceFee(String convenienceFee) {
        editor.putString("ConvenienceFee", convenienceFee);
        editor.commit();
    }

    public String getCouponModel() {
        return settings.getString("CouponModel", "");
    }

    public void putCouponModel(String couponModel) {
        editor.putString("CouponModel", couponModel);
        editor.commit();
    }


    public String getUserId() {
        return settings.getString("user_id", "");
    }

    public void putUserId(String userId) {
        editor.putString("user_id", userId);
        editor.commit();
    }
    /*  -------------------------------------
        |
        |  USER PARAMETERS
        |
        -------------------------------------
    */

    public String getUserModel() {
        return settings.getString("UserCredentials", "");
    }

    public void putUserModel(String userCredentials) {
        editor.putString("UserCredentials", userCredentials);
        editor.commit();
    }

    public String getLoginId() {
        return settings.getString("LoginId", "-1");
    }

    public void putLoginId(String loginId) {
        editor.putString("LoginId", loginId);
        editor.commit();
    }

    public String getGuestAddresses() {
        return settings.getString("GuestAddress", "");
    }

    public void putGuestAddresses(String guestAddress) {
        editor.putString("GuestAddress", guestAddress);
        editor.commit();
    }

    public String getFacebookID() {
        return settings.getString("FacebookID", "");
    }

    public void putFacebookID(String facebookID) {
        editor.putString("FacebookID", facebookID);
        editor.commit();
    }

    public String getGoogleID() {
        return settings.getString("GoogleID", "");
    }

    public void putGoogleID(String googleID) {
        editor.putString("GoogleID", googleID);
        editor.commit();
    }

    public String getGuestUserPrefillPhone() {
        return settings.getString("GuestUserPrefillPhone", "");
    }

    public void putGuestUserPrefillPhone(String guestUserPrefillPhone) {
        editor.putString("GuestUserPrefillPhone", guestUserPrefillPhone);
        editor.commit();
    }

    /*  -------------------------------------
        |
        |  USER ADDRESS PARAMETERS
        |
        -------------------------------------
    */

    public String getUserSetAddresses() {
        return settings.getString("UserSetAddresses", "");
    }

    public void putUserSetAddresses(String userSetAddresses) {
        editor.putString("UserSetAddresses", userSetAddresses);
        editor.commit();
    }

    public Boolean getIsUserMember() {
        return settings.getBoolean("memberUser", false);
    }

    public void putIsUserMember(Boolean isUser) {
        editor.putBoolean("memberUser", isUser);
        editor.commit();
    }

    public String getMemberDiscount() {
        return settings.getString("memberDiscount", "");
    }

    public void putMemberDiscount(String discount) {
        editor.putString("memberDiscount", discount);
        editor.commit();
    }
}