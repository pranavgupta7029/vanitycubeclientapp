package com.vanitycube.model;

import java.util.ArrayList;

// Created by prade on 12/8/2017.

public class ExpectedDateTimeModel {
    private String date;
    private ArrayList<TimeSlotModel> timeSlots;
    private String message;

    public ExpectedDateTimeModel() {
    }

    public ExpectedDateTimeModel(String date, ArrayList<TimeSlotModel> timeSlotsList) {
        setDate(date);
        setTimeSlots(timeSlotsList);
    }

    public ExpectedDateTimeModel(String date, ArrayList<TimeSlotModel> timeSlotsList, String message) {
        setDate(date);
        setTimeSlots(timeSlotsList);
        setMessage(message);
    }

    /*  -------------------------------------
       |
       |   GETTER AND SETTER METHODS
       |
       -------------------------------------
   */

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<TimeSlotModel> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(ArrayList<TimeSlotModel> timeSlots) {
        this.timeSlots = timeSlots;
    }
}
