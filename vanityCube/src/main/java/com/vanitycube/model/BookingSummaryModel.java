package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BookingSummaryModel implements Parcelable {

    private String serviceTypeId;
    private String NumOfPeople;
    private String serviceTypeName;
    private String requiredTime;
    private String amount;

    public BookingSummaryModel() {
    }

     /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getRequiredTime() {
        return requiredTime;
    }

    public void setRequiredTime(String requiredTime) {
        this.requiredTime = requiredTime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getNumOfPeople() {
        return NumOfPeople;
    }

    public void setNumOfPeople(String numOfPeople) {
        NumOfPeople = numOfPeople;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    private BookingSummaryModel(Parcel in) {
        serviceTypeId = in.readString();
        NumOfPeople = in.readString();
        serviceTypeName = in.readString();
        requiredTime = in.readString();
        amount = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceTypeId);
        dest.writeString(NumOfPeople);
        dest.writeString(serviceTypeName);
        dest.writeString(requiredTime);
        dest.writeString(amount);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BookingSummaryModel> CREATOR = new Parcelable.Creator<BookingSummaryModel>() {
        @Override
        public BookingSummaryModel createFromParcel(Parcel in) {
            return new BookingSummaryModel(in);
        }

        @Override
        public BookingSummaryModel[] newArray(int size) {
            return new BookingSummaryModel[size];
        }
    };
}