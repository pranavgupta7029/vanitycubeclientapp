package com.vanitycube.model;
// Created by prade on 1/10/2018.

public class SelectedLocationModel {
    private String cityName;
    private String cityId;
    private String hubName;
    private String hubId;
    private String areaName;
    private String areaId;

    public SelectedLocationModel() {
    }

    public SelectedLocationModel(String cityName, String cityId,
                                 String hubName, String hubId,
                                 String areaName, String areaId) {

    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getHubName() {
        return hubName;
    }

    public void setHubName(String hubName) {
        this.hubName = hubName;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }
}
