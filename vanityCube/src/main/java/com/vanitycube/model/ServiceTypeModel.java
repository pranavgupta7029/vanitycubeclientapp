package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

// Created by prade on 11/24/2017.

public class ServiceTypeModel implements Parcelable {

    private String categoryId;
    private String categoryName;
    private String serviceTypeID;
    private String serviceTypeName;
    private String description;
    private String image;
    private String price;
    private String discount;
    private String finalAmount;
    private String time;
    private String isPackage;
    private String numOfPeople;
    private String serviceID;
    private String serviceName;
    private String percentageForMembers;
    private String membersPrice;

    public String getMembersPrice() {
        return membersPrice;
    }

    public void setMembersPrice(String membersPrice) {
        this.membersPrice = membersPrice;
    }



    public String getPercentageForMembers() {
        return percentageForMembers;
    }

    public void setPercentageForMembers(String percentageForMembers) {
        this.percentageForMembers = percentageForMembers;
    }



    public boolean isChangedCategory() {
        return changedCategory;
    }

    public void setChangedCategory(boolean changedCategory) {
        this.changedCategory = changedCategory;
    }

    private boolean changedCategory;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }



    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }


    public ServiceTypeModel() {
    }

    public ServiceTypeModel(JSONObject lJSONObject) {
        try {
            setCategoryId(lJSONObject.getString("categoryId"));
            setCategoryName(lJSONObject.getString("categoryName"));
            setServiceTypeID(lJSONObject.getString("serviceTypeId"));
            setServiceTypeName(lJSONObject.getString("serviceTypeName"));

            if (lJSONObject.optString("description").equalsIgnoreCase("")) {
                setDescription("-1");
            } else {
                setDescription(lJSONObject.getString("description"));
            }

            if (lJSONObject.optString("image").equalsIgnoreCase("")) {
                setImage("-1");
            } else {
                setImage(lJSONObject.getString("image"));
            }

            setPrice(lJSONObject.getString("price"));
            setDiscount(lJSONObject.getString("discount"));
            setFinalAmount("0");
            setTime(lJSONObject.getString("time"));
            setIsPackage(lJSONObject.getString("isPackage"));
            setNumOfPeople("0");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

     /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public String getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getIsPackage() {
        return isPackage;
    }

    public void setIsPackage(String isPackage) {
        this.isPackage = isPackage;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServiceTypeID() {
        return serviceTypeID;
    }

    public void setServiceTypeID(String serviceTypeID) {
        this.serviceTypeID = serviceTypeID;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(String numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    public JSONObject makeJSON() {
        JSONObject lJSONObject = new JSONObject();
        try {
            lJSONObject.put("serviceTypeId", serviceTypeID);
            lJSONObject.put("quantity", numOfPeople);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return lJSONObject;
    }

    //Create an instance of this model from parcel data
    public static final Parcelable.Creator<ServiceTypeModel> CREATOR = new Parcelable.Creator<ServiceTypeModel>() {

        public ServiceTypeModel createFromParcel(Parcel in) {
            return new ServiceTypeModel(in);
        }

        public ServiceTypeModel[] newArray(int size) {
            return new ServiceTypeModel[size];
        }

    };

    //Read from parcel
    public ServiceTypeModel(Parcel in) {
        this.categoryId = in.readString();
        this.categoryName = in.readString();
        this.serviceTypeID = in.readString();
        this.serviceTypeName = in.readString();
        this.description = in.readString();
        this.image = in.readString();
        this.price = in.readString();
        this.discount = in.readString();
        this.time = in.readString();
        this.isPackage = in.readString();
        this.serviceID = in.readString();
        this.serviceName = in.readString();
        this.percentageForMembers = in.readString();
        this.membersPrice = in.readString();
    }


    public ServiceTypeModel(String categoryId,String categoryName,String serviceTypeID,String serviceTypeName,String description,String image,
                            String price,String discount,String finalAmount,String time,String isPackage,String numOfPeople,String serviceID,String serviceName,String percentageForMembers,String membersPrice) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.serviceTypeID = serviceTypeID;
        this.serviceTypeName = serviceTypeName;
        this.description = description;
        this.image = image;
        this.price = price;
        this.discount = discount;
        this.finalAmount = finalAmount;
        this.time = time;
        this.isPackage = isPackage;
        this.numOfPeople = numOfPeople;
        this.serviceID = serviceID;
        this.serviceName = serviceName;
        this.percentageForMembers = percentageForMembers;
        this.membersPrice = membersPrice;
    }

    //Write to parcel
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryId);
        dest.writeString(categoryName);
        dest.writeString(serviceTypeID);
        dest.writeString(serviceTypeName);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(price);
        dest.writeString(discount);
        dest.writeString(time);
        dest.writeString(isPackage);
        dest.writeString(serviceID);
        dest.writeString(serviceName);
        dest.writeString(percentageForMembers);
        dest.writeString(membersPrice);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
