
package com.vanitycube.model;

import org.json.JSONObject;

public class UserAddressModel {
    private String addressId;
    private String address;
    private String landmark;
    private String countryName;
    private String areaId;
    private String areaName;
    private String cityId;
    private String cityName;
    private String stateId;
    private String stateName;
    private String hubId;

    public UserAddressModel() {

    }

    public UserAddressModel(String addressId, String areaId, String hubId) {
        setAddressId(addressId);
        setAreaId(areaId);
        setHubId(hubId);
    }

    public UserAddressModel(JSONObject pJSONObject) {
        setAddressId(pJSONObject.optString("user_adddress_id"));
        setAddress(pJSONObject.optString("address"));
        setLandmark(pJSONObject.optString("landmark"));
        setAreaName(pJSONObject.optString("AreaName"));
        setCityName(pJSONObject.optString("CityName"));
        setStateName(pJSONObject.optString("StateName"));
        setAreaId(pJSONObject.optString("areaId"));
        setCityId(pJSONObject.optString("cityId"));
        setHubId(pJSONObject.optString("hubId"));
        setCountryName(pJSONObject.optString("India"));
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        String address = "";

        if (getAddress() != null)
            address += getAddress() + ", ";
        if (getAreaName() != null)
            address += getAreaName() + ", ";
        if (getCityName() != null)
            address += getCityName() + ", ";
        if (getStateName() != null)
            address += getStateName() + "\n";
        if (getLandmark() != null && !getLandmark().equals(""))
            address += "Near By : " + getLandmark() + " ";

        return address;
    }
}