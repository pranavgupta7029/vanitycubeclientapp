package com.vanitycube.model;
// Created by prade on 12/27/2017.

import org.json.JSONObject;

public class LoginResponseModel {
    private String messageCode;
    private String message;
    private JSONObject data;

    public LoginResponseModel() {
    }

    public LoginResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setMessageCode(responseData.getString("messageCode"));
            setMessage(responseData.getString("message"));
            setData(responseData.optJSONObject("data"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

}
