package com.vanitycube.model;

/**
 * Created by Nitin on 08/05/18.
 */

public class LoyaltyOrderBookingModel {
    private String transactionId;
    private Boolean isOtpConfirmed;
    private String otpEntered;
    private String eligibleServiceAmount;
    private String pointsToRedeem;
    private String pointsMultiplier;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Boolean getOtpConfirmed() {
        return isOtpConfirmed;
    }

    public void setOtpConfirmed(Boolean otpConfirmed) {
        isOtpConfirmed = otpConfirmed;
    }

    public String getOtpEntered() {
        return otpEntered;
    }

    public void setOtpEntered(String otpEntered) {
        this.otpEntered = otpEntered;
    }

    public String getEligibleServiceAmount() {
        return eligibleServiceAmount;
    }

    public void setEligibleServiceAmount(String eligibleServiceAmount) {
        this.eligibleServiceAmount = eligibleServiceAmount;
    }

    public String getPointsToRedeem() {
        return pointsToRedeem;
    }

    public void setPointsToRedeem(String pointsToRedeem) {
        this.pointsToRedeem = pointsToRedeem;
    }

    public String getPointsMultiplier() {
        return pointsMultiplier;
    }

    public void setPointsMultiplier(String pointsMultiplier) {
        this.pointsMultiplier = pointsMultiplier;
    }
}
