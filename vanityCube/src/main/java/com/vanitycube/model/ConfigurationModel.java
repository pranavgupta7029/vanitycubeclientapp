package com.vanitycube.model;

//Created by prade on 12/20/2017.

import android.os.Bundle;

public class ConfigurationModel {
    private String minSupportedVersion;
    private String currentVersion;
    private String updateMessage;
    private String referralLimit;
    private String phoneNumber;
    private String inviteMessage;
    private String terms;
    private String privacyPolicy;

    public ConfigurationModel(Bundle bundle) {
        try {
            setMinSupportedVersion(bundle.getString("min_supported_version"));
            setCurrentVersion(bundle.getString("current_version"));
            setUpdateMessage(bundle.getString("update_message"));
            setReferralLimit(bundle.getString("referral_limit"));
            setPhoneNumber(bundle.getString("phone_number"));
            setInviteMessage(bundle.getString("invite_message"));
            setTerms(bundle.getString("terms_and_conditions"));
            setPrivacyPolicy(bundle.getString("privacy_policy"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*  -------------------------------------
        |
        |  GETTER & SETTER METHODS
        |
        -------------------------------------
    */

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public ConfigurationModel() {
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getMinSupportedVersion() {
        return minSupportedVersion;
    }

    public void setMinSupportedVersion(String minSupportedVersion) {
        this.minSupportedVersion = minSupportedVersion;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    public String getUpdateMessage() {
        return updateMessage;
    }

    public void setUpdateMessage(String updateMessage) {
        this.updateMessage = updateMessage;
    }

    public String getReferralLimit() {
        return referralLimit;
    }

    public void setReferralLimit(String referralLimit) {
        this.referralLimit = referralLimit;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getInviteMessage() {
        return inviteMessage;
    }

    public void setInviteMessage(String inviteMessage) {
        this.inviteMessage = inviteMessage;
    }
}
