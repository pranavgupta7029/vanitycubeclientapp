package com.vanitycube.model;
// Created by prade on 12/29/2017.

import org.json.JSONObject;

public class StandardResponseModelBooking {
    private String success;
    private String msg;

    public StandardResponseModelBooking() {
    }

    public StandardResponseModelBooking(JSONObject jsonObject) {
        try {
            //JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setSuccess(jsonObject.getString("success"));
            setMessage(jsonObject.getString("msg"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }



    public String getMessage() {
        return msg;
    }

    public void setMessage(String message) {
        this.msg = message;
    }
}
