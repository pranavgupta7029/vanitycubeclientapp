package com.vanitycube.model.staff;

import com.google.gson.annotations.Expose;
import com.vanitycube.model.CommonResponse;
import com.vanitycube.model.TimeSlotModel;
import com.vanitycube.model.TimeSlotsResponse;

import java.util.ArrayList;

/**
 * Created by swapn on 9/8/2017.
 */

public class StaffAvailabilityReponse {

    @Expose
    private StaffAvailabilityReponse.Responsedata responsedata;

    public class Responsedata extends CommonResponse {
        @Expose
        StaffData result;

        public StaffData getResult() {
            return result;
        }

        public void setResult(StaffData result) {
            this.result = result;
        }
    }

    public StaffAvailabilityReponse.Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(StaffAvailabilityReponse.Responsedata responsedata) {
        this.responsedata = responsedata;
    }

}
