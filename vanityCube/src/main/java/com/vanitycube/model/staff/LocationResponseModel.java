package com.vanitycube.model.staff;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vanitycube.model.CommonResponse;

public class LocationResponseModel implements Parcelable {

    public class Responsedata extends CommonResponse {
        @Expose
        String message;
        @SerializedName("success")
        @Expose
        GetLocationResponse result;

        //Getter and Setter of CommonResponse type
        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public GetLocationResponse getResult() {
            return result;
        }

        public void setResult(GetLocationResponse result) {
            this.result = result;
        }
    }

    private LocationResponseModel.Responsedata responsedata;

    //Getter and Setter of child class type
    public LocationResponseModel.Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(LocationResponseModel.Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    protected LocationResponseModel(Parcel in) {
        responsedata = (LocationResponseModel.Responsedata) in.readValue(LocationResponseModel.Responsedata.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(responsedata);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LocationResponseModel> CREATOR = new Parcelable.Creator<LocationResponseModel>() {
        @Override
        public LocationResponseModel createFromParcel(Parcel in) {
            return new LocationResponseModel(in);
        }

        @Override
        public LocationResponseModel[] newArray(int size) {
            return new LocationResponseModel[size];
        }
    };
}