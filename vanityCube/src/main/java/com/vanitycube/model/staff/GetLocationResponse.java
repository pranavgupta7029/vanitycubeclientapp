package com.vanitycube.model.staff;

import android.os.Parcel;
import android.os.Parcelable;

public class GetLocationResponse implements Parcelable {

    /*"areaId": "1146",
            "areaName": "Maruti Industrial Area",
            "hub_id": "7",
            "cityId": "8",
            "cityName": "Gurgaon",
            "latitude": "28.48971",
            "longitude": "77.062282",
            "distance": 0.075515829866237*/
    private String areaId;
    private String areaName;
    private String hub_id;
    private String cityId;
    private String cityName;
    private String latitude;
    private String longitude;
    private String distance;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getHub_id() {
        return hub_id;
    }

    public void setHub_id(String hub_id) {
        this.hub_id = hub_id;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    protected GetLocationResponse(Parcel in) {
        areaId = in.readString();
        areaName = in.readString();
        hub_id = in.readString();
        cityId = in.readString();
        cityName = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        distance = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(areaId);
        dest.writeString(areaName);
        dest.writeString(hub_id);
        dest.writeString(cityId);
        dest.writeString(cityName);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(distance);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GetLocationResponse> CREATOR = new Parcelable.Creator<GetLocationResponse>() {
        @Override
        public GetLocationResponse createFromParcel(Parcel in) {
            return new GetLocationResponse(in);
        }

        @Override
        public GetLocationResponse[] newArray(int size) {
            return new GetLocationResponse[size];
        }
    };
}