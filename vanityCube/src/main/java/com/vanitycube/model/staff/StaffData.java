package com.vanitycube.model.staff;

import com.google.gson.annotations.Expose;

/**
 * Created by swapn on 9/8/2017.
 */

public class StaffData {

    @Expose
    private String staffId;

    @Expose
    private String staffTime;

    public String getStaffId() {
        return staffId == null ? "" : staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffTime() {
        return staffTime == null ? "" : staffTime;
    }

    public void setStaffTime(String staffTime) {
        this.staffTime = staffTime;
    }
}
