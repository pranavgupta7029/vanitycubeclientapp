package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServiceModel implements Parcelable {

    private String serviceID;
    private String serviceName;
    private String serviceImage;
    private String membershippercentage;

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    private String totalDiscount;
    public String getMembershippercentage() {
        return membershippercentage;
    }

    public void setMembershippercentage(String membershippercentage) {
        this.membershippercentage = membershippercentage;
    }




    private ArrayList<ServiceTypeModel> serviceTypes = new ArrayList<>();

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    public ServiceModel() {

    }

    public ServiceModel(JSONObject lJSONObject) {
        try {
            if (lJSONObject.optString("serviceId").equalsIgnoreCase("")) {
                setServiceID("-1");
            } else {
                setServiceID(lJSONObject.getString("serviceId"));
            }

            setServiceName(lJSONObject.getString("serviceName"));
            setServiceImage(lJSONObject.getString("serviceImage"));
            setMembershippercentage(lJSONObject.getString("membershippercentage"));

            JSONArray jArr = lJSONObject.getJSONArray("serviceTypes");

            for (int i = 0; i < jArr.length(); i++) {
                ServiceTypeModel lServiceTypeModel = new ServiceTypeModel(jArr.getJSONObject(i));
                serviceTypes.add(lServiceTypeModel);
            }

            setServiceTypes(serviceTypes);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ServiceModel(String serviceId, String serviceName, ArrayList<ServiceTypeModel> serviceTypeList, String serviceImage,String membershippercentage,String totalDiscount) {
        this.serviceID = serviceId;
        this.serviceName = serviceName;
        this.serviceImage = serviceImage;
        this.serviceTypes = serviceTypeList;
        this.membershippercentage = membershippercentage;
        this.totalDiscount = totalDiscount;

    }

     /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public String getServiceID() {
        return serviceID;
    }

    private void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    private void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public ArrayList<ServiceTypeModel> getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(ArrayList<ServiceTypeModel> serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    private ServiceModel(Parcel in) {
        //serviceTypes = new ArrayList<>();

        serviceID = in.readString();
        serviceName = in.readString();
        serviceImage = in.readString();
        in.readList(serviceTypes, ServiceTypeModel.class.getClassLoader());
        membershippercentage = in.readString();
        totalDiscount = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceID);
        dest.writeString(serviceName);
        dest.writeString(serviceImage);
        dest.writeList(serviceTypes);
        dest.writeString(membershippercentage);
        dest.writeString(totalDiscount);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ServiceModel> CREATOR = new Parcelable.Creator<ServiceModel>() {
        @Override
        public ServiceModel createFromParcel(Parcel in) {
            return new ServiceModel(in);
        }

        @Override
        public ServiceModel[] newArray(int size) {
            return new ServiceModel[size];
        }
    };
}