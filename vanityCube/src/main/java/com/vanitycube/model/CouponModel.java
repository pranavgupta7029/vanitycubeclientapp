package com.vanitycube.model;

// Created by prade on 12/5/2017.

public class CouponModel {
    private String couponId;
    private String couponCode;
    private String couponAmount;

    public CouponModel() {
    }

    public CouponModel(String couponId, String couponCode, String couponAmount) {
        setCouponId(couponId);
        setCouponCode(couponCode);
        setCouponAmount(couponAmount);
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        this.couponAmount = couponAmount;
    }
}
