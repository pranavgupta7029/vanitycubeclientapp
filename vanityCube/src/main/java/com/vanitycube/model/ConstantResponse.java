package com.vanitycube.model;

/**
 * Created by Milind on 09-Jun-17.
 */

public class ConstantResponse {

    private Responsedata responsedata;

    public class Responsedata extends CommonResponse {
        String result;

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }
    }

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}
