package com.vanitycube.model.signinup;

public class SendOtpToUserResponse {
    private ResponseDataSendOtpToUser responsedata;

    public ResponseDataSendOtpToUser getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(ResponseDataSendOtpToUser responsedata) {
        this.responsedata = responsedata;
    }
}
