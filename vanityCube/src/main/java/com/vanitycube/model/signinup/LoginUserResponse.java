package com.vanitycube.model.signinup;

public class LoginUserResponse {
    private ResponseLogin responsedata;

    public ResponseLogin getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(ResponseLogin responsedata) {
        this.responsedata = responsedata;
    }
}
