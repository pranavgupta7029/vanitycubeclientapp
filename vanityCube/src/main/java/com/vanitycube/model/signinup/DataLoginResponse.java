package com.vanitycube.model.signinup;

public class DataLoginResponse {

   private String gender;
   private Integer user_id;
   private String contact;
   private String referral_name;
   private String name;
   private String email;
   private String profile_pic;
    private String dob;
    private Integer is_guest_user;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getIs_guest_user() {
        return is_guest_user;
    }

    public void setIs_guest_user(Integer is_guest_user) {
        this.is_guest_user = is_guest_user;
    }



    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getReferral_name() {
        return referral_name;
    }

    public void setReferral_name(String referral_name) {
        this.referral_name = referral_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
