package com.vanitycube.model.signinup;

public class OtpVerificationForNewUser {
    private ResponseDataOtpVerificationNewUser responsedata;

    public ResponseDataOtpVerificationNewUser getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(ResponseDataOtpVerificationNewUser responsedata) {
        this.responsedata = responsedata;
    }
}
