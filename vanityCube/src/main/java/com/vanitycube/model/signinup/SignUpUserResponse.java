package com.vanitycube.model.signinup;

public class SignUpUserResponse {
    private ResponseDataSignUpNewUser responsedata;

    public ResponseDataSignUpNewUser getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(ResponseDataSignUpNewUser responsedata) {
        this.responsedata = responsedata;
    }
}
