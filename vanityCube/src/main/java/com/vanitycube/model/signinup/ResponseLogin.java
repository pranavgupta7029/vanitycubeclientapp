package com.vanitycube.model.signinup;

public class ResponseLogin {
    private DataLoginResponse data;
    private String msg;
    private Integer messageCode;

    public DataLoginResponse getData() {
        return data;
    }

    public void setData(DataLoginResponse data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(Integer messageCode) {
        this.messageCode = messageCode;
    }
}
