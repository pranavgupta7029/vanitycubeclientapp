package com.vanitycube.model;

// Created by prade on 12/5/2017.

public class CouponModelBooking {
    private Double couponAmount;
    private String couponCode;
    private String couponId;

    public CouponModelBooking() {
    }

    public CouponModelBooking(String couponId, String couponCode, Double couponAmount) {
        setCouponId(couponId);
        setCouponCode(couponCode);
        setCouponAmount(couponAmount);
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Double getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(Double couponAmount) {
        this.couponAmount = couponAmount;
    }
}
