package com.vanitycube.model;
// Created by prade on 1/9/2018.

import org.json.JSONObject;

public class GetUserStateResponseModel {
    private int success;
    private JSONObject result;
    private String message;

    public GetUserStateResponseModel() {
    }

    public GetUserStateResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setSuccess(responseData.getInt("success"));
            setResult(responseData.getJSONObject("result"));
            setMessage(responseData.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public JSONObject getResult() {
        return result;
    }

    public void setResult(JSONObject result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
