package com.vanitycube.model;

import java.util.ArrayList;

public class BookingSummaryDetailModel {
    private String addressId;
    private String areaId;
    private String bookingDate;
    private String bookingTimeId;
    private String categoryId;
    private Integer convienceFee;
    private CouponModelBooking coupon;
    private int firstBooking;
    private String hubId;
    private String note;
    private int referralDiscount;
    private ArrayList<ServiceTypeModelTrimmed> services;
    private String source;
    private int paymentMode;
    private int serviceTime;
    private String userId;
    private int isGuestUser;
    private LoyaltyOrderBookingModel easyRewardzLoyalty;
    private CashBackBookingModel easyRewardzCashback;
    private Integer is_member;

    public Integer getIs_member() {
        return is_member;
    }

    public void setIs_member(Integer is_member) {
        this.is_member = is_member;
    }

    public LoyaltyOrderBookingModel getEasyRewardzLoyalty() {
        return easyRewardzLoyalty;
    }

    public void setEasyRewardzLoyalty(LoyaltyOrderBookingModel easyRewardzLoyalty) {
        this.easyRewardzLoyalty = easyRewardzLoyalty;
    }

    public CashBackBookingModel getEasyRewardzCashback() {
        return easyRewardzCashback;
    }

    public void setEasyRewardzCashback(CashBackBookingModel easyRewardzCashback) {
        this.easyRewardzCashback = easyRewardzCashback;
    }

    public BookingSummaryDetailModel() {
    }

    public BookingSummaryDetailModel(String addressId, String areaId, String bookingDate, String bookingTimeId,
                                     String categoryId, Integer convienceFee, CouponModelBooking coupon, int firstBooking,
                                     String hubId, String note, int referralDiscount, ArrayList<ServiceTypeModelTrimmed> services,
                                     String source, int paymentMode, int serviceTime, String userId, int isGuestUser, LoyaltyOrderBookingModel easyRewardzLoyalty, CashBackBookingModel easyRewardzCashback, Integer is_member) {

        setAddressId(addressId);
        setAreaId(areaId);
        setBookingDate(bookingDate);
        setBookingTimeId(bookingTimeId);
        setCategoryId(categoryId);
        setConvienceFee(convienceFee);
        setCoupon(coupon);
        setFirstBooking(firstBooking);
        setHubId(hubId);
        setNote(note);
        setReferralDiscount(referralDiscount);
        setServices(services);
        setSource(source);
        setPaymentMode(paymentMode);
        setServiceTime(serviceTime);
        setUserId(userId);
        setIsGuestUser(isGuestUser);
        setEasyRewardzLoyalty(easyRewardzLoyalty);
        setEasyRewardzCashback(easyRewardzCashback);
        setIs_member(is_member);


    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public CouponModelBooking getCoupon() {
        return coupon;
    }

    public void setCoupon(CouponModelBooking coupon) {
        this.coupon = coupon;
    }

    public int getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(int paymentMode) {
        this.paymentMode = paymentMode;
    }

    public int getIsGuestUser() {
        return isGuestUser;
    }

    public void setIsGuestUser(int isGuestUser) {
        this.isGuestUser = isGuestUser;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public String getBookingTimeId() {
        return bookingTimeId;
    }

    public void setBookingTimeId(String bookingTimeId) {
        this.bookingTimeId = bookingTimeId;
    }

    public int getFirstBooking() {
        return firstBooking;
    }

    public void setFirstBooking(int firstBooking) {
        this.firstBooking = firstBooking;
    }

    public int getReferralDiscount() {
        return referralDiscount;
    }

    public void setReferralDiscount(int referralDiscount) {
        this.referralDiscount = referralDiscount;
    }

    public Integer getConvienceFee() {
        return convienceFee;
    }

    public void setConvienceFee(Integer convienceFee) {
        this.convienceFee = convienceFee;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }

    public ArrayList<ServiceTypeModelTrimmed> getServices() {
        return services;
    }

    public void setServices(ArrayList<ServiceTypeModelTrimmed> services) {
        this.services = services;
    }
}
