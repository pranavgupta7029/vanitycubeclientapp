package com.vanitycube.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceBookingModel {
    private String serviceID = "";
    private String numOfPeople = "";

    public ServiceBookingModel() {

    }

    public ServiceBookingModel(String id, String people) {
        this.serviceID = id;
        this.numOfPeople = people;
    }

    public JSONObject makeJSON() {
        JSONObject lJSONObject = new JSONObject();
        try {
            lJSONObject.put("st_id", serviceID);
            lJSONObject.put("st_people", numOfPeople);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return lJSONObject;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(String numOfPeople) {
        this.numOfPeople = numOfPeople;
    }
}
