package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Milind on 09-Jun-17.
 */

public class ConvienenceFeeResponseMain implements Parcelable {

    private String hubAreaDistanceAmount;
    private String convenienceFee;
    private Double convenienceFeePreTax;

    public String getHubAreaDistanceAmount() {
        return hubAreaDistanceAmount;
    }

    public void setHubAreaDistanceAmount(String hubAreaDistanceAmount) {
        this.hubAreaDistanceAmount = hubAreaDistanceAmount;
    }

    public String getConvenienceFee() {
        return convenienceFee;
    }

    public void setConvenienceFee(String convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    public Double getConvenienceFeePreTax() {
        return convenienceFeePreTax;
    }

    public void setConvenienceFeePreTax(Double convenienceFeePreTax) {
        this.convenienceFeePreTax = convenienceFeePreTax;
    }

    protected ConvienenceFeeResponseMain(Parcel in) {
        hubAreaDistanceAmount = in.readString();
        convenienceFee = in.readString();
        convenienceFeePreTax = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(hubAreaDistanceAmount);
        dest.writeString(convenienceFee);
        if (convenienceFeePreTax == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(convenienceFeePreTax);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ConvienenceFeeResponseMain> CREATOR = new Parcelable.Creator<ConvienenceFeeResponseMain>() {
        @Override
        public ConvienenceFeeResponseMain createFromParcel(Parcel in) {
            return new ConvienenceFeeResponseMain(in);
        }

        @Override
        public ConvienenceFeeResponseMain[] newArray(int size) {
            return new ConvienenceFeeResponseMain[size];
        }
    };
}