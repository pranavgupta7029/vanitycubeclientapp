package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

// Created by prade on 11/26/2017.

public class CategoryModel implements Parcelable {
    private String categoryId;
    private String categoryName;
    private String categoryDate;
    private String categoryTime;
    private String categoryTimeId;
    private ArrayList<ServiceTypeModel> serviceTypes;
    private ArrayList<TimeSlotModel> timeSlots;
    private int totalServiceTime;
    private String message;

    public CategoryModel() {
    }

    public CategoryModel(String categoryId, String categoryName, String categoryDate, String categoryTime, String categoryTimeId,
                         ArrayList<ServiceTypeModel> serviceTypeList, ArrayList<TimeSlotModel> timeSlotsList, int totalServiceTime) {

        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryDate = categoryDate;
        this.categoryTime = categoryTime;
        this.categoryTimeId = categoryTimeId;
        this.serviceTypes = serviceTypeList;
        this.timeSlots = timeSlotsList;
        this.totalServiceTime = totalServiceTime;

        setCategoryId(this.categoryId);
        setCategoryName(this.categoryName);
        setCategoryDate(this.categoryDate);
        setCategoryTime(this.categoryTime);
        setCategoryTimeId(this.categoryTimeId);
        setServiceTypes(this.serviceTypes);
        setTimeSlots(this.timeSlots);
        setTotalServiceTime(this.totalServiceTime);
    }

     /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCategoryTimeId() {
        return categoryTimeId;
    }

    public void setCategoryTimeId(String categoryTimeId) {
        this.categoryTimeId = categoryTimeId;
    }

    public int getTotalServiceTime() {
        return totalServiceTime;
    }

    public void setTotalServiceTime(int totalServiceTime) {
        this.totalServiceTime = totalServiceTime;
    }

    public String getCategoryDate() {
        return categoryDate;
    }

    public void setCategoryDate(String categoryDate) {
        this.categoryDate = categoryDate;
    }

    public String getCategoryTime() {
        return categoryTime;
    }

    public void setCategoryTime(String categoryTime) {
        this.categoryTime = categoryTime;
    }

    public ArrayList<TimeSlotModel> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(ArrayList<TimeSlotModel> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<ServiceTypeModel> getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(ArrayList<ServiceTypeModel> serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    private CategoryModel(Parcel in) {
        serviceTypes = new ArrayList<>();
        categoryId = in.readString();
        in.readList(serviceTypes, null);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(categoryId);
        dest.writeList(serviceTypes);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CategoryModel> CREATOR = new Parcelable.Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };

}
