package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class BookingListModel implements Parcelable {

    private int imageId;
    private String bookingID;
    private String hubId;
    private String areaId;
    private String userId;
    private String userAddressId;
    private String amount;
    private String discount;
    private String convenienceFee;
    private String totalAmount;
    private String netAmount;
    private String igst;
    private String cgst;
    private String sgst;
    private String serviceAmount;
    private String adminExtraCharges;
    private String adminExtraDiscount;
    private String isStaffAutoAssigned;
    private String invoiceName;
    private String invoiceNum;
    private String igstI;
    private String cgstI;
    private String sgstI;
    private String serviceAmountI;
    private String invoiceNameI;
    private String invoiceNumI;
    private String firstBookingDiscount;
    private String referralDiscount;
    private String totalServiceTime;
    private String therapist;
    private String date;
    private String time;
    private String note;
    private String paymentMode;
    private String payUMoneyId;
    private String paymentStatus;
    private String approved;
    private String dateCreated;
    private String dateModified;
    private String status;
    private String invoiceDate;
    private String invoiceDateI;
    private String source;
    private String staffId;
    private String transactionEcode;
    private String transactionEstring;
    private String transactionDtime;
    private String serviceStatus;
    private String tcFlag;
    private String fdFlag;
    private String stamtFlag;
    private String vFlag;
    private String externalId;
    private String transactionId;
    private String orderId;
    private String logUserId;
    private String AmountCollected;
    private String customerSign;
    private String secretKey;
    private String bookingStatusId;
    private String bookingStatus;
    private String booktime;
    private String serviceTypeIds;
    private String serviceTypeNames;
    private String serviceTypeDescriptions;
    private String serviceTypePersons;
    private String staffIds;
    private String staffNames;

    public BookingListModel() {
    }

    public BookingListModel(JSONObject jsonObject) {
        try {
            setBookingID(jsonObject.getString("booking_id"));
            setHubId(jsonObject.getString("hub_id"));
            setAreaId(jsonObject.getString("area_id"));
            setUserId(jsonObject.getString("user_id"));
            setUserAddressId(jsonObject.getString("user_address_id"));
            setAmount(jsonObject.getString("amount"));
            setDiscount(jsonObject.getString("discount"));
            setConvenienceFee(jsonObject.getString("convience_fee"));
            setTotalAmount(jsonObject.getString("totalamount"));
            setNetAmount(jsonObject.getString("net_amount"));
            setIgst(jsonObject.getString("igst"));
            setCgst(jsonObject.getString("cgst"));
            setSgst(jsonObject.getString("sgst"));
            setServiceAmount(jsonObject.getString("service_amount"));
            setAdminExtraCharges(jsonObject.getString("admin_extra_charges"));
            setAdminExtraDiscount(jsonObject.getString("admin_extra_discount"));
            setIsStaffAutoAssigned(jsonObject.getString("is_staff_auto_assigned"));
            setInvoiceName(jsonObject.getString("invoice_name"));
            setInvoiceNum(jsonObject.getString("invoice_no"));
            setIgstI(jsonObject.getString("igst_i"));
            setCgstI(jsonObject.getString("cgst_i"));
            setSgstI(jsonObject.getString("sgst_i"));
            setServiceAmountI(jsonObject.getString("service_amount_i"));
            setInvoiceNameI(jsonObject.getString("invoice_name_i"));
            setInvoiceNumI(jsonObject.getString("invoice_no_i"));
            setFirstBookingDiscount(jsonObject.getString("firstbookingdiscount"));
            setReferralDiscount(jsonObject.getString("referral_discount"));
            setTotalServiceTime(jsonObject.getString("totalservicetime"));
            setTherapist(jsonObject.getString("therapist"));
            setDate(jsonObject.getString("date"));
            setTime(jsonObject.getString("time"));
            setNote(jsonObject.getString("note"));
            setPaymentMode(jsonObject.getString("payment_mode"));
            setPayUMoneyId(jsonObject.getString("payumoney_id"));
            setPaymentStatus(jsonObject.getString("payment_status"));
            setApproved(jsonObject.getString("approved"));
            setDateCreated(jsonObject.getString("date_created"));
            setDateModified(jsonObject.getString("date_modified"));
            setStatus(jsonObject.getString("status"));
            setInvoiceDate(jsonObject.getString("invoice_date"));
            setInvoiceDateI(jsonObject.getString("invoice_date_i"));
            setSource(jsonObject.getString("source"));
            setStaffId(jsonObject.getString("staff_id"));
            setTransactionEcode(jsonObject.getString("transaction_ecode"));
            setTransactionEstring(jsonObject.getString("transaction_estring"));
            setTransactionDtime(jsonObject.getString("transaction_dtime"));
            setServiceStatus(jsonObject.getString("service_status"));
            setTcFlag(jsonObject.getString("tcflag"));
            setFdFlag(jsonObject.getString("fdflag"));
            setStamtFlag(jsonObject.getString("stamtflag"));
            setvFlag(jsonObject.getString("vflag"));
            setExternalId(jsonObject.getString("external_id"));
            setTransactionId(jsonObject.getString("transaction_id"));
            setOrderId(jsonObject.getString("order_id"));
            setLogUserId(jsonObject.getString("log_user_id"));
            setAmountCollected(jsonObject.getString("amount_collected"));
            setCustomerSign(jsonObject.getString("customer_sign"));
            setSecretKey(jsonObject.getString("secret_key"));
            setBookingStatusId(jsonObject.getString("BookingStatusId"));
            setBookingStatus(jsonObject.getString("BookingStatus"));
            setBooktime(jsonObject.getString("BookTime"));
            setServiceTypeIds(jsonObject.getString("ServiceTypeIDs"));
            setServiceTypeNames(jsonObject.getString("ServiceTypeNames"));
            setServiceTypeDescriptions(jsonObject.getString("ServiceTypeDescriptions"));
            setStaffIds(jsonObject.getString("StaffIds"));
            setStaffNames(jsonObject.getString("StaffNames"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /*  -------------------------------------
        |
        |   GETTER & SETTER METHODS
        |
        -------------------------------------
    */

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public String getAmountCollected() {
        return AmountCollected;
    }

    public void setAmountCollected(String amountCollected) {
        AmountCollected = amountCollected;
    }

    public String getCustomerSign() {
        return customerSign;
    }

    public void setCustomerSign(String customerSign) {
        this.customerSign = customerSign;
    }

    public String getServiceTypeDescriptions() {
        return serviceTypeDescriptions;
    }

    public void setServiceTypeDescriptions(String serviceTypeDescriptions) {
        this.serviceTypeDescriptions = serviceTypeDescriptions;
    }

    public String getServiceTypePersons() {
        return serviceTypePersons;
    }

    public void setServiceTypePersons(String serviceTypePersons) {
        this.serviceTypePersons = serviceTypePersons;
    }

    public String getIsStaffAutoAssigned() {
        return isStaffAutoAssigned;
    }

    public void setIsStaffAutoAssigned(String isStaffAutoAssigned) {
        this.isStaffAutoAssigned = isStaffAutoAssigned;
    }

    public String getBooktime() {
        return booktime;
    }

    public void setBooktime(String booktime) {
        this.booktime = booktime;
    }

    public String getTransactionEstring() {
        return transactionEstring;
    }

    public void setTransactionEstring(String transactionEstring) {
        this.transactionEstring = transactionEstring;
    }

    public String getPayUMoneyId() {
        return payUMoneyId;
    }

    public void setPayUMoneyId(String payUMoneyId) {
        this.payUMoneyId = payUMoneyId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }


    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAddressId() {
        return userAddressId;
    }

    public void setUserAddressId(String userAddressId) {
        this.userAddressId = userAddressId;
    }

    public String getConvenienceFee() {
        return convenienceFee;
    }

    public void setConvenienceFee(String convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(String serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    public String getInvoiceName() {
        return invoiceName;
    }

    public void setInvoiceName(String invoiceName) {
        this.invoiceName = invoiceName;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getIgstI() {
        return igstI;
    }

    public void setIgstI(String igstI) {
        this.igstI = igstI;
    }

    public String getCgstI() {
        return cgstI;
    }

    public void setCgstI(String cgstI) {
        this.cgstI = cgstI;
    }

    public String getSgstI() {
        return sgstI;
    }

    public void setSgstI(String sgstI) {
        this.sgstI = sgstI;
    }

    public String getServiceAmountI() {
        return serviceAmountI;
    }

    public void setServiceAmountI(String serviceAmountI) {
        this.serviceAmountI = serviceAmountI;
    }

    public String getInvoiceNameI() {
        return invoiceNameI;
    }

    public void setInvoiceNameI(String invoiceNameI) {
        this.invoiceNameI = invoiceNameI;
    }

    public String getInvoiceNumI() {
        return invoiceNumI;
    }

    public void setInvoiceNumI(String invoiceNumI) {
        this.invoiceNumI = invoiceNumI;
    }

    public String getFirstBookingDiscount() {
        return firstBookingDiscount;
    }

    public void setFirstBookingDiscount(String firstBookingDiscount) {
        this.firstBookingDiscount = firstBookingDiscount;
    }

    public String getReferralDiscount() {
        return referralDiscount;
    }

    public void setReferralDiscount(String referralDiscount) {
        this.referralDiscount = referralDiscount;
    }

    public String getTotalServiceTime() {
        return totalServiceTime;
    }

    public void setTotalServiceTime(String totalServiceTime) {
        this.totalServiceTime = totalServiceTime;
    }

    public String getTherapist() {
        return therapist;
    }

    public void setTherapist(String therapist) {
        this.therapist = therapist;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getApproved() {
        return approved;
    }

    public void setApproved(String approved) {
        this.approved = approved;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceDateI() {
        return invoiceDateI;
    }

    public void setInvoiceDateI(String invoiceDateI) {
        this.invoiceDateI = invoiceDateI;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getTransactionEcode() {
        return transactionEcode;
    }

    public void setTransactionEcode(String transactionEcode) {
        this.transactionEcode = transactionEcode;
    }

    public String getTransactionDtime() {
        return transactionDtime;
    }

    public void setTransactionDtime(String transactionDtime) {
        this.transactionDtime = transactionDtime;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getTcFlag() {
        return tcFlag;
    }

    public void setTcFlag(String tcFlag) {
        this.tcFlag = tcFlag;
    }

    public String getFdFlag() {
        return fdFlag;
    }

    public void setFdFlag(String fdFlag) {
        this.fdFlag = fdFlag;
    }

    public String getStamtFlag() {
        return stamtFlag;
    }

    public void setStamtFlag(String stamtFlag) {
        this.stamtFlag = stamtFlag;
    }

    public String getvFlag() {
        return vFlag;
    }

    public void setvFlag(String vFlag) {
        this.vFlag = vFlag;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAdminExtraCharges() {
        return adminExtraCharges;
    }

    public void setAdminExtraCharges(String adminExtraCharges) {
        this.adminExtraCharges = adminExtraCharges;
    }

    public String getAdminExtraDiscount() {
        return adminExtraDiscount;
    }

    public void setAdminExtraDiscount(String adminExtraDiscount) {
        this.adminExtraDiscount = adminExtraDiscount;
    }

    public String getBookingStatusId() {
        return bookingStatusId;
    }

    public void setBookingStatusId(String bookingStatusId) {
        this.bookingStatusId = bookingStatusId;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getServiceTypeIds() {
        return serviceTypeIds;
    }

    public void setServiceTypeIds(String serviceTypeIds) {
        this.serviceTypeIds = serviceTypeIds;
    }

    public String getServiceTypeNames() {
        return serviceTypeNames;
    }

    public void setServiceTypeNames(String serviceTypeNames) {
        this.serviceTypeNames = serviceTypeNames;
    }

    public String getStaffIds() {
        return staffIds;
    }

    public void setStaffIds(String staffIds) {
        this.staffIds = staffIds;
    }

    public String getStaffNames() {
        return staffNames;
    }

    public void setStaffNames(String staffNames) {
        this.staffNames = staffNames;
    }

     /*  -------------------------------------
        |
        |   PARCEL METHODS
        |
        -------------------------------------
    */

    protected BookingListModel(Parcel in) {
        this.imageId = in.readInt();
        this.bookingID = in.readString();
        this.hubId = in.readString();
        this.areaId = in.readString();
        this.userId = in.readString();
        this.userAddressId = in.readString();
        this.amount = in.readString();
        this.discount = in.readString();
        this.convenienceFee = in.readString();
        this.totalAmount = in.readString();
        this.netAmount = in.readString();
        this.igst = in.readString();
        this.cgst = in.readString();
        this.sgst = in.readString();
        this.serviceAmount = in.readString();
        this.adminExtraCharges = in.readString();
        this.adminExtraDiscount = in.readString();
        this.isStaffAutoAssigned = in.readString();
        this.invoiceName = in.readString();
        this.invoiceNum = in.readString();
        this.igstI = in.readString();
        this.cgstI = in.readString();
        this.sgstI = in.readString();
        this.serviceAmountI = in.readString();
        this.invoiceNameI = in.readString();
        this.invoiceNumI = in.readString();
        this.firstBookingDiscount = in.readString();
        this.referralDiscount = in.readString();
        this.totalServiceTime = in.readString();
        this.therapist = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.note = in.readString();
        this.paymentMode = in.readString();
        this.payUMoneyId = in.readString();
        this.paymentStatus = in.readString();
        this.approved = in.readString();
        this.dateCreated = in.readString();
        this.dateModified = in.readString();
        this.status = in.readString();
        this.invoiceDate = in.readString();
        this.invoiceDateI = in.readString();
        this.source = in.readString();
        this.staffId = in.readString();
        this.transactionEcode = in.readString();
        this.transactionEstring = in.readString();
        this.transactionDtime = in.readString();
        this.serviceStatus = in.readString();
        this.tcFlag = in.readString();
        this.fdFlag = in.readString();
        this.stamtFlag = in.readString();
        this.vFlag = in.readString();
        this.externalId = in.readString();
        this.transactionId = in.readString();
        this.orderId = in.readString();
        this.logUserId = in.readString();
        this.AmountCollected = in.readString();
        this.customerSign = in.readString();
        this.secretKey = in.readString();
        this.bookingStatusId = in.readString();
        this.bookingStatus = in.readString();
        this.booktime = in.readString();
        this.serviceTypeIds = in.readString();
        this.serviceTypeNames = in.readString();
        this.serviceTypeDescriptions = in.readString();
        this.serviceTypePersons = in.readString();
        this.staffIds = in.readString();
        this.staffNames = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(imageId);
        dest.writeString(bookingID);
        dest.writeString(hubId);
        dest.writeString(areaId);
        dest.writeString(userId);
        dest.writeString(userAddressId);
        dest.writeString(amount);
        dest.writeString(discount);
        dest.writeString(convenienceFee);
        dest.writeString(totalAmount);
        dest.writeString(netAmount);
        dest.writeString(igst);
        dest.writeString(cgst);
        dest.writeString(sgst);
        dest.writeString(serviceAmount);
        dest.writeString(adminExtraCharges);
        dest.writeString(adminExtraDiscount);
        dest.writeString(isStaffAutoAssigned);
        dest.writeString(invoiceName);
        dest.writeString(invoiceNum);
        dest.writeString(igstI);
        dest.writeString(cgstI);
        dest.writeString(sgstI);
        dest.writeString(serviceAmountI);
        dest.writeString(invoiceNameI);
        dest.writeString(invoiceNumI);
        dest.writeString(firstBookingDiscount);
        dest.writeString(referralDiscount);
        dest.writeString(totalServiceTime);
        dest.writeString(therapist);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(note);
        dest.writeString(paymentMode);
        dest.writeString(payUMoneyId);
        dest.writeString(paymentStatus);
        dest.writeString(approved);
        dest.writeString(dateCreated);
        dest.writeString(dateModified);
        dest.writeString(status);
        dest.writeString(invoiceDate);
        dest.writeString(invoiceDateI);
        dest.writeString(source);
        dest.writeString(staffId);
        dest.writeString(transactionEcode);
        dest.writeString(transactionEstring);
        dest.writeString(transactionDtime);
        dest.writeString(serviceStatus);
        dest.writeString(tcFlag);
        dest.writeString(fdFlag);
        dest.writeString(stamtFlag);
        dest.writeString(vFlag);
        dest.writeString(externalId);
        dest.writeString(transactionId);
        dest.writeString(orderId);
        dest.writeString(logUserId);
        dest.writeString(AmountCollected);
        dest.writeString(customerSign);
        dest.writeString(secretKey);
        dest.writeString(bookingStatusId);
        dest.writeString(bookingStatus);
        dest.writeString(booktime);
        dest.writeString(serviceTypeIds);
        dest.writeString(serviceTypeNames);
        dest.writeString(serviceTypeDescriptions);
        dest.writeString(serviceTypePersons);
        dest.writeString(staffIds);
        dest.writeString(staffNames);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BookingListModel> CREATOR = new Parcelable.Creator<BookingListModel>() {
        @Override
        public BookingListModel createFromParcel(Parcel in) {
            return new BookingListModel(in);
        }

        @Override
        public BookingListModel[] newArray(int size) {
            return new BookingListModel[size];
        }
    };
}