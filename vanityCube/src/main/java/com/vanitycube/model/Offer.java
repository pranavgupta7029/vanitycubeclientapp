package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.vanitycube.settings.ApplicationSettings;

import org.json.JSONObject;

// Created by prade on 11/14/2017.

public class Offer implements Parcelable {
    private int id;
    private String name;
    private String imgURL;
    private String status;
    private String source;
    private String type;
    private String actionType;
    private String serviceTypeId;
    private String couponCode;
    private String offerLabel;


    public Offer() {
    }

    public Offer(JSONObject jsonObject) {
        try {
            setId(Integer.parseInt(jsonObject.getString("OfferId")));
            setName(jsonObject.getString("OfferName"));
            setImgURL(ApplicationSettings.OFFER_IMAGE_PATH.concat(jsonObject.getString("OfferImage")));
            setStatus(jsonObject.getString("OfferStatus"));
            setSource(jsonObject.getString("sourceDevice"));
            setType(jsonObject.getString("type"));
            setActionType(jsonObject.getString("actionType"));
            setServiceTypeId(jsonObject.getString("serviceTypeId"));
            setCouponCode(jsonObject.getString("couponCode"));
            setOfferLabel(jsonObject.getString("offerLabel"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getOfferLabel() {
        return offerLabel;
    }

    public void setOfferLabel(String offerLabel) {
        this.offerLabel = offerLabel;
    }


    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    private Offer(Parcel in) {
        id = in.readInt();
        name = in.readString();
        imgURL = in.readString();
        status = in.readString();
        source = in.readString();
        type = in.readString();
        actionType = in.readString();
        serviceTypeId = in.readString();
        couponCode = in.readString();
        offerLabel = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(imgURL);
        dest.writeString(status);
        dest.writeString(source);
        dest.writeString(type);
        dest.writeString(actionType);
        dest.writeString(serviceTypeId);
        dest.writeString(couponCode);
        dest.writeString(offerLabel);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Offer> CREATOR = new Parcelable.Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel in) {
            return new Offer(in);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };
}
