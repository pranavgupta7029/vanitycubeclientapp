package com.vanitycube.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Milind on 09-Jun-17.
 */

public class CategoryResponse {

    Responsedata responsedata;

    public class Responsedata extends CommonResponse {
        ArrayList<CategoryData> result;

        public ArrayList<CategoryData> getResult() {
            return result;
        }

        public void setResult(ArrayList<CategoryData> result) {
            this.result = result;
        }
    }

    public class CategoryData {

        @SerializedName("ServiceName")
        String serviceName;
        @SerializedName("service_id")
        String serviceId;
        @SerializedName("CategoryName")
        String categoryName;
        String status;

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getServiceId() {
            return serviceId;
        }

        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}
