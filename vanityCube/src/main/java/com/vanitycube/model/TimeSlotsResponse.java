package com.vanitycube.model;

import java.util.ArrayList;

/**
 * Created by Milind on 09-Jun-17.
 */

public class TimeSlotsResponse {

    public class Responsedata extends CommonResponse {
        ArrayList<TimeSlotModel> result;

        public ArrayList<TimeSlotModel> getResult() {
            return result == null ? new ArrayList<TimeSlotModel>() : result;
        }

        public void setResult(ArrayList<TimeSlotModel> result) {
            this.result = result;
        }
    }

    //Creating an object of this inner class
    private Responsedata responsedata;

    //Getters and Setters for this inner object
    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}
