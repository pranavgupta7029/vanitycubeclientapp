package com.vanitycube.model;

public class BodyPageModel {

    private String name;
    private String amount;
    private String time;
    private String discount;
    private String serviceCategoryId;

    public BodyPageModel() {

    }

    public BodyPageModel(String name, String amount, String time, String discount, String serviceCategoryId) {
        this.name = name;
        this.amount = amount;
        this.time = time;
        this.discount = discount;
        this.serviceCategoryId = serviceCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getServiceCategoryId() {
        return serviceCategoryId;
    }

    public void setServiceCategoryId(String serviceCategoryId) {
        this.serviceCategoryId = serviceCategoryId;
    }
}
