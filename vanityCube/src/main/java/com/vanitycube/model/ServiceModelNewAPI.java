package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ServiceModelNewAPI implements Parcelable {

    private String response;


    private ArrayList<ServiceModelResponse> data;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public ArrayList<ServiceModelResponse> getData() {
        return data;
    }

    public void setData(ArrayList<ServiceModelResponse> data) {
        this.data = data;
    }

    protected ServiceModelNewAPI(Parcel in) {
        response = in.readString();
        if (in.readByte() == 0x01) {
            data = new ArrayList<ServiceModelResponse>();
            in.readList(data, ServiceModelResponse.class.getClassLoader());
        } else {
            data = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(response);
        if (data == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(data);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ServiceModelNewAPI> CREATOR = new Parcelable.Creator<ServiceModelNewAPI>() {
        @Override
        public ServiceModelNewAPI createFromParcel(Parcel in) {
            return new ServiceModelNewAPI(in);
        }

        @Override
        public ServiceModelNewAPI[] newArray(int size) {
            return new ServiceModelNewAPI[size];
        }
    };
}