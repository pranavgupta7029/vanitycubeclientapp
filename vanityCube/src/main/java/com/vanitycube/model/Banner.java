package com.vanitycube.model;

//Created by prade on 11/14/2017.

import android.os.Parcel;
import android.os.Parcelable;

public class Banner implements Parcelable {
    private int id;
    private int order;
    private int status;
    private int target;
    private String imgURL;

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    private String actionUrl;

    public Banner() {
    }

    public Banner(int id, int order, int status, int target, String imgURL, String actionUrl) {
        this.id = id;
        this.order = order;
        this.status = status;
        this.target = target;
        this.imgURL = imgURL;
        this.actionUrl = actionUrl;
    }

    public int getId() {
        return id;
    }

    public int getOrder() {
        return order;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public int getStatus() {
        return status;
    }

    public int getTarget() {
        return target;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    private Banner(Parcel in) {
        id = in.readInt();
        order = in.readInt();
        status = in.readInt();
        target = in.readInt();
        imgURL = in.readString();
        actionUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(order);
        dest.writeInt(status);
        dest.writeInt(target);
        dest.writeString(imgURL);
        dest.writeString(actionUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Banner> CREATOR = new Parcelable.Creator<Banner>() {
        @Override
        public Banner createFromParcel(Parcel in) {
            return new Banner(in);
        }

        @Override
        public Banner[] newArray(int size) {
            return new Banner[size];
        }
    };
}
