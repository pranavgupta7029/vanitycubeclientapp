package com.vanitycube.model;
// Created by prade on 1/10/2018.

import org.json.JSONObject;

public class AddNewUserResponseModel {
    private int success;
    private String message;
    private String userId;
    private int messageCode;

    public AddNewUserResponseModel() {
    }

    public AddNewUserResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setSuccess(responseData.getInt("success"));
            setUserId(responseData.optString("userid"));
            setMessage(responseData.optString("msg"));
            setMessageCode(responseData.optInt("messageCode"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }
}
