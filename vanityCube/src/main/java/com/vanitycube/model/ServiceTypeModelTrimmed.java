package com.vanitycube.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by prade on 12/1/2017.
 */

public class ServiceTypeModelTrimmed {
    private String serviceTypeId;
    private int quantity;

    public ServiceTypeModelTrimmed() {
    }

    public ServiceTypeModelTrimmed(String serviceTypeId, int quantity) {
        setServiceTypeId(serviceTypeId);
        setQuantity(quantity);
    }

    public ServiceTypeModelTrimmed(JSONObject lJSONObject) {
        try {
            setServiceTypeId(lJSONObject.getString("serviceTypeId"));
            setQuantity(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(String serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
