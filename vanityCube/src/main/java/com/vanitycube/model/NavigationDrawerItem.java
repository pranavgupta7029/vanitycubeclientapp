package com.vanitycube.model;

import android.app.Activity;

import com.vanitycube.R;
import com.vanitycube.utilities.Strings;

import java.util.ArrayList;

/**
 * Created by prade on 11/13/2017.
 */

public class NavigationDrawerItem {

    private int id;
    private String name;
    private int icon;
    private boolean available;
    private ArrayList<NavigationDrawerItem> navigationDrawerItems;

    public NavigationDrawerItem() {
    }

    private NavigationDrawerItem(int id, String name, int icon, boolean available) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.available = available;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Strings.nullSafeString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    //Make an arraylist of available items
    public void setAvailableItems(Activity activity, boolean[] availableItems) {
        navigationDrawerItems = new ArrayList<>();

        navigationDrawerItems.add(new NavigationDrawerItem(1, activity.getString(R.string.drawer_home_text), R.drawable.drawer_home, availableItems[0]));
        navigationDrawerItems.add(new NavigationDrawerItem(2, activity.getString(R.string.drawer_notification_text), R.drawable.drawer_notifications, availableItems[1]));
        navigationDrawerItems.add(new NavigationDrawerItem(3, activity.getString(R.string.drawer_gallery_text), R.drawable.drawer_gallery, availableItems[2]));
        navigationDrawerItems.add(new NavigationDrawerItem(4, activity.getString(R.string.drawer_active_booking_text), R.drawable.drawer_active_bookings, availableItems[3]));
        navigationDrawerItems.add(new NavigationDrawerItem(5, activity.getString(R.string.history_text), R.drawable.drawer_clock, availableItems[4]));
        navigationDrawerItems.add(new NavigationDrawerItem(6, activity.getString(R.string.drawer_invite_friends_text), R.drawable.drawer_friends, availableItems[5]));
        navigationDrawerItems.add(new NavigationDrawerItem(7, activity.getString(R.string.drawer_settings_text), R.drawable.drawer_settings, availableItems[6]));
        navigationDrawerItems.add(new NavigationDrawerItem(8, activity.getString(R.string.drawer_call_us_text), R.drawable.drawer_call, availableItems[7]));
        navigationDrawerItems.add(new NavigationDrawerItem(9, activity.getString(R.string.drawer_logout_text), R.drawable.drawer_logout, availableItems[8]));
    }

    public ArrayList<NavigationDrawerItem> getAvailableItems(Activity activity) {
        ArrayList<NavigationDrawerItem> filteredList = new ArrayList<>();

        for (NavigationDrawerItem item : navigationDrawerItems) {
            if (item.isAvailable()) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }

}
