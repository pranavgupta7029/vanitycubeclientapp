package com.vanitycube.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

// Created by prade on 12/3/2017.

public class MultipleBookingResponseModel {
    private String orderId;
    private ArrayList<String> bookingIds;
    private Boolean isSet;
    private String message;

    public MultipleBookingResponseModel() {
        setSet(false);
    }

    public MultipleBookingResponseModel(String message) {
        setSet(false);
        setMessage(message);
    }

    public MultipleBookingResponseModel(JSONObject jsonObject) {
        try {
            setOrderId(jsonObject.getString("orderId"));

            String bookingIdsString = jsonObject.getString("bookingIds");

            String[] bookingIdsStringArray = bookingIdsString.split(",");

            bookingIds = new ArrayList(Arrays.asList(bookingIdsStringArray));

            setBookingIds(bookingIds);

            setSet(true);

            setMessage("");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSet() {
        return isSet;
    }

    public void setSet(Boolean set) {
        isSet = set;
    }

    public ArrayList<String> getBookingIds() {
        return bookingIds;
    }

    public void setBookingIds(ArrayList<String> bookingIds) {
        this.bookingIds = bookingIds;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
