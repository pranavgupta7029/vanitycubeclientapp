package com.vanitycube.model;

public class SearchAreaModel {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}