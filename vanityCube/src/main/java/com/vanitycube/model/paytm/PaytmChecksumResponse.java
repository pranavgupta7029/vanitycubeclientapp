package com.vanitycube.model.paytm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vanitycube.model.CommonResponse;

// Created by prade on 10/26/2017.

public class PaytmChecksumResponse {


    public class Responsedata extends CommonResponse {

        @Expose
        String message;

        @SerializedName("data")
        @Expose
        String checksum;

        //Getter and Setter of CommonResponse type
        public String getMessage() {
            return message == null ? "" : message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getChecksum() {
            return checksum == null ? "" : checksum;
        }

        public void setChecksum(String checksum) {
            this.checksum = checksum;
        }
    }

    private PaytmChecksumResponse.Responsedata responsedata;

    //Getter and Setter of child class type
    public PaytmChecksumResponse.Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(PaytmChecksumResponse.Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}
