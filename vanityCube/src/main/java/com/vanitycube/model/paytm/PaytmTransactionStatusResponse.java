package com.vanitycube.model.paytm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.vanitycube.model.CommonResponse;
import com.vanitycube.model.PaytmTransactionStatusModel;

/**
 * Created by prade on 10/26/2017.
 */

public class PaytmTransactionStatusResponse {

    //Child class, could be any name
    public class Responsedata extends CommonResponse {

        //Our response contains a key named "data" which is of PaytmTransactionStatusModel type
        @SerializedName("data")
        @Expose
        PaytmTransactionStatusModel response;

        public PaytmTransactionStatusModel getResponse() {
            return response;
        }

        public void setResponse(PaytmTransactionStatusModel response) {
            this.response = response;
        }

    }

    //Object of child class
    private PaytmTransactionStatusResponse.Responsedata responsedata;

    //Getter and Setter of return type -> child class
    public PaytmTransactionStatusResponse.Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(PaytmTransactionStatusResponse.Responsedata responsedata) {
        this.responsedata = responsedata;
    }

}
