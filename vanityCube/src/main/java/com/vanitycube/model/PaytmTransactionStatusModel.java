package com.vanitycube.model;

/**
 * Created by prade on 10/26/2017.
 */

public class PaytmTransactionStatusModel {

    private String TXNID;
    private String BANKTXNID;
    private String ORDERID;
    private String TXNAMOUNT;
    private String STATUS;
    private String TXNTYPE;
    private String GATEWAYNAME;
    private String RESPCODE;
    private String RESPMSG;
    private String BANKNAME;
    private String MID;
    private String PAYMENTMODE;
    private String REFUNDAMT;
    private String TXNDATE;

    public void setTXNID(String TXNID) {
        this.TXNID = TXNID;
    }

    public void setBANKTXNID(String BANKTXNID) {
        this.BANKTXNID = BANKTXNID;
    }

    public void setORDERID(String ORDERID) {
        this.ORDERID = ORDERID;
    }

    public void setTXNAMOUNT(String TXNAMOUNT) {
        this.TXNAMOUNT = TXNAMOUNT;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public void setTXNTYPE(String TXNTYPE) {
        this.TXNTYPE = TXNTYPE;
    }

    public void setGATEWAYNAME(String GATEWAYNAME) {
        this.GATEWAYNAME = GATEWAYNAME;
    }

    public void setRESPCODE(String RESPCODE) {
        this.RESPCODE = RESPCODE;
    }

    public void setRESPMSG(String RESPMSG) {
        this.RESPMSG = RESPMSG;
    }

    public void setBANKNAME(String BANKNAME) {
        this.BANKNAME = BANKNAME;
    }

    public void setMID(String MID) {
        this.MID = MID;
    }

    public void setPAYMENTMODE(String PAYMENTMODE) {
        this.PAYMENTMODE = PAYMENTMODE;
    }

    public void setREFUNDAMT(String REFUNDAMT) {
        this.REFUNDAMT = REFUNDAMT;
    }

    public void setTXNDATE(String TXNDATE) {
        this.TXNDATE = TXNDATE;
    }

    public String getTXNID() {
        return TXNID;
    }

    public String getBANKTXNID() {
        return BANKTXNID;
    }

    public String getORDERID() {
        return ORDERID;
    }

    public String getTXNAMOUNT() {
        return TXNAMOUNT;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public String getTXNTYPE() {
        return TXNTYPE;
    }

    public String getGATEWAYNAME() {
        return GATEWAYNAME;
    }

    public String getRESPCODE() {
        return RESPCODE;
    }

    public String getRESPMSG() {
        return RESPMSG;
    }

    public String getBANKNAME() {
        return BANKNAME;
    }

    public String getMID() {
        return MID;
    }

    public String getPAYMENTMODE() {
        return PAYMENTMODE;
    }

    public String getREFUNDAMT() {
        return REFUNDAMT;
    }

    public String getTXNDATE() {
        return TXNDATE;
    }
}
