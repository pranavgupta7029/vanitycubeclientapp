package com.vanitycube.model;
// Created by prade on 1/10/2018.

import org.json.JSONObject;

public class CheckOTPResponseModel {
    private int success;
    private String result;
    private JSONObject data;

    public CheckOTPResponseModel() {
    }

    public CheckOTPResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");

            setSuccess(responseData.getInt("success"));
            setResult(responseData.getString("result"));

            if (!responseData.isNull("data")) {
                setData(responseData.getJSONObject("data"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
