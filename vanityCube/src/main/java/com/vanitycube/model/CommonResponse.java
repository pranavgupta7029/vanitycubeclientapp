package com.vanitycube.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Milind on 09-Jun-17.
 */

public class CommonResponse {

    @Expose
    private int success;

    @Expose
    @SerializedName("Error Message")
    private String error;

    public void setSuccess(int success) {
        this.success = success;
    }

    public int getSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int isSuccess() {
        return success;
    }

}
