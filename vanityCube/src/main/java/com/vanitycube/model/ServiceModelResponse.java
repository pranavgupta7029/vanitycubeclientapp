package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ServiceModelResponse implements Parcelable {

    private Integer serviceId;


    private String serviceName;
    private String serviceImage;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(String serviceImage) {
        this.serviceImage = serviceImage;
    }

    protected ServiceModelResponse(Parcel in) {
        serviceId = in.readByte() == 0x00 ? null : in.readInt();
        serviceName = in.readString();
        serviceImage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (serviceId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(serviceId);
        }
        dest.writeString(serviceName);
        dest.writeString(serviceImage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ServiceModelResponse> CREATOR = new Parcelable.Creator<ServiceModelResponse>() {
        @Override
        public ServiceModelResponse createFromParcel(Parcel in) {
            return new ServiceModelResponse(in);
        }

        @Override
        public ServiceModelResponse[] newArray(int size) {
            return new ServiceModelResponse[size];
        }
    };
}