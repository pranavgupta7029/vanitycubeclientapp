package com.vanitycube.model;

/**
 * Created by Milind on 09-Jun-17.
 */

public class BaseModel {

    public class Responsedata extends CommonResponse {
    }

    Responsedata responsedata;

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}
