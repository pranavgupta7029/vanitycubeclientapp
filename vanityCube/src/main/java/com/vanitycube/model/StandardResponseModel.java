package com.vanitycube.model;
// Created by prade on 12/29/2017.

import org.json.JSONObject;

public class StandardResponseModel {
    private String success;
    private Boolean result;
    private String message;

    public StandardResponseModel() {
    }

    public StandardResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setSuccess(responseData.getString("success"));
            setResult(responseData.getBoolean("result"));
            setMessage(responseData.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
