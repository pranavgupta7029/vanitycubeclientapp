
package com.vanitycube.model;

public class ContactsModel {

    String fname, lName, photoPath, phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        if (phoneNumber == null)
            this.phoneNumber = "";
        else
            this.phoneNumber = phoneNumber;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        if (fname == null)
            this.fname = "";
        else
            this.fname = fname;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        if (lName == null)
            this.lName = "";
        else
            this.lName = lName;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

}
