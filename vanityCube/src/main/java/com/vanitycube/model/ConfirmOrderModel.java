package com.vanitycube.model;
// Created by prade on 1/1/2018.

import java.util.ArrayList;

public class ConfirmOrderModel {
    private String orderId;
    private ArrayList<String> bookingIds;
    private String userId;
    private String paymentMode;
    private String paymentId;
    private String success;
    private String failureCode;
    private String device;
    private String statusCode;
    private String statusMessage;
    private String responseMessage;
    private String paymentType;

    public ConfirmOrderModel() {
    }

    public ConfirmOrderModel(String orderId, ArrayList<String> bookingIds, String userId, String paymentMode, String paymentId, String success,
                             String failureCode, String device, String statusCode, String statusMessage, String responseMessage,
                             String paymentType) {
        setOrderId(orderId);
        setBookingIds(bookingIds);
        setUserId(userId);
        setPaymentMode(paymentMode);
        setPaymentId(paymentId);
        setSuccess(success);
        setFailureCode(failureCode);
        setDevice(device);
        setStatusCode(statusCode);
        setStatusMessage(statusMessage);
        setResponseMessage(responseMessage);
        setPaymentType(paymentType);
    }

    /*  -------------------------------------
        |
        |   GETTER & SETTER METHODS
        |
        -------------------------------------
    */

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public ArrayList<String> getBookingIds() {
        return bookingIds;
    }

    public void setBookingIds(ArrayList<String> bookingIds) {
        this.bookingIds = bookingIds;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getFailureCode() {
        return failureCode;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
