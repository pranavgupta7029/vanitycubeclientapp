package com.vanitycube.model.loyalty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Nitin on 09/03/18.
 */

public class AbvailablePointsResponse {

    /*"ReturnCode":"0","AvailablePoints":"132","ReferralPoints":"0","ReturnMessage":"Success.","PartnerName":[],"EasyId":[],"PointValue":"0","PointRate":"0"}"*/

    /*{“ReturnCode":"0","AvailablePoints":"166","ReferralPoints":"0","ReturnMessage":"Success.","PartnerName":"vlccUAT","EasyId":[],
    "PointValue":"166","PointRate":"1","Success":1,"ReferralCode":"VLA5FB6E"}*/

    @Expose
    @SerializedName("ReturnCode")
    private String ReturnCode;
    @Expose
    @SerializedName("AvailablePoints")
    private String AvailablePoints;
    @Expose
    @SerializedName("ReferralPoints")
    private String ReferralPoints;
    @Expose
    @SerializedName("ReturnMessage")
    private String ReturnMessage;
    @Expose
    @SerializedName("PartnerName")
    private String PartnerName;
    @Expose
    @SerializedName("EasyId")
    private ArrayList EasyId;
    @Expose
    @SerializedName("PointValue")
    private String PointValue;

    @Expose
    @SerializedName("PointRate")
    private String PointRate;

    @Expose
    @SerializedName("Success")
    private String Success;

    @Expose
    @SerializedName("ReferralCode")
    private String ReferralCode;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getReferralCode() {
        return ReferralCode;
    }

    public void setReferralCode(String referralCode) {
        ReferralCode = referralCode;
    }


    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getAvailablePoints() {
        return AvailablePoints;
    }

    public void setAvailablePoints(String availablePoints) {
        AvailablePoints = availablePoints;
    }

    public String getReferralPoints() {
        return ReferralPoints;
    }

    public void setReferralPoints(String referralPoints) {
        ReferralPoints = referralPoints;
    }

    public String getReturnMessage() {
        return ReturnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        ReturnMessage = returnMessage;
    }

    public String getPartnerName() {
        return PartnerName;
    }

    public void setPartnerName(String partnerName) {
        PartnerName = partnerName;
    }

    public ArrayList getEasyId() {
        return EasyId;
    }

    public void setEasyId(ArrayList easyId) {
        EasyId = easyId;
    }

    public String getPointValue() {
        return PointValue;
    }

    public void setPointValue(String pointValue) {
        PointValue = pointValue;
    }

    public String getPointRate() {
        return PointRate;
    }

    public void setPointRate(String pointRate) {
        PointRate = pointRate;
    }


}
