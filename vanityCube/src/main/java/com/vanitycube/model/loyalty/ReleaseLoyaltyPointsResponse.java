package com.vanitycube.model.loyalty;

/**
 * Created by Nitin on 16/03/18.
 */

public class ReleaseLoyaltyPointsResponse {

    private String ReturnCode;
    private String ReturnMessage;
    private String Success;

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getReturnMessage() {
        return ReturnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        ReturnMessage = returnMessage;
    }

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }
}
