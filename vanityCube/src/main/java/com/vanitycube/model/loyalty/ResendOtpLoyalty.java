package com.vanitycube.model.loyalty;

/**
 * Created by Nitin on 12/03/18.
 */

public class ResendOtpLoyalty {

    private String ReturnCode;
    private String ReturnMessage;

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getReturnMessage() {
        return ReturnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        ReturnMessage = returnMessage;
    }
}
