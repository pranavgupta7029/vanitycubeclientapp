package com.vanitycube.model.loyalty;

/**
 * Created by Nitin on 12/03/18.
 */

public class SubmitOtpLoyalty {

    private String ReturnCode;
    private String ReturnMessage;
    private Integer Success;
    private String BillAmountBeforeLoyalty;
    private String BillAmountAfterLoyalty;
    private String LoyaltyDiscount;

    public Integer getSuccess() {
        return Success;
    }

    public void setSuccess(Integer success) {
        Success = success;
    }

    public String getBillAmountBeforeLoyalty() {
        return BillAmountBeforeLoyalty;
    }

    public void setBillAmountBeforeLoyalty(String billAmountBeforeLoyalty) {
        BillAmountBeforeLoyalty = billAmountBeforeLoyalty;
    }

    public String getBillAmountAfterLoyalty() {
        return BillAmountAfterLoyalty;
    }

    public void setBillAmountAfterLoyalty(String billAmountAfterLoyalty) {
        BillAmountAfterLoyalty = billAmountAfterLoyalty;
    }

    public String getLoyaltyDiscount() {
        return LoyaltyDiscount;
    }

    public void setLoyaltyDiscount(String loyaltyDiscount) {
        LoyaltyDiscount = loyaltyDiscount;
    }

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getReturnMessage() {
        return ReturnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        ReturnMessage = returnMessage;
    }
}
