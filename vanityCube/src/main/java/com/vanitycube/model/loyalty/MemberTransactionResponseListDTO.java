package com.vanitycube.model.loyalty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nitin on 09/03/18.
 */

public class MemberTransactionResponseListDTO {

    /*            "ReturnCode": "0",
            "BillNo": "ff38ce93-94af-4877-be1f-79842ae797d0",
            "BillDate": "2018-03-16T00:00:00",
            "TotalBilledAmount": "1785.0000",
            "Mobile": "9540590200",
            "TotalAccruedPoints": "0.0000",
            "TotalRedeemPoints": "10.0000",
            "EnrolledDate": "0001-01-01T00:00:00",
            "IsEnableReferralAccruals": "false",
            "UserName": "nitinSaraswat",
            "CardNumber": [],
            "IsVoucher": "false",
            "IsRefunded": "true",
            "Code": [],
            "Brand": "VLCC Rewardz Club",
            "Narration": [],
            "AccruedPoints": "20",
            "MemberShipCardNumber": [],
            "IsPointType": "false",
            "Tier": "false",
            "RecordCount": "7",
            "StoreCode": "Demo",
            "EasyPointTypeId": "2",
            "BookingId": "32138",
            "FormattedBookingId": "VC0032138"*/


    @Expose
    @SerializedName("ReturnCode")
    private String ReturnCode;
    @Expose
    @SerializedName("BillNo")
    private String BillNo;
    @Expose
    @SerializedName("BillDate")
    private String BillDate;
    @Expose
    @SerializedName("TotalBilledAmount")
    private String TotalBilledAmount;
    @Expose
    @SerializedName("TotalAccruedPoints")
    private String TotalAccruedPoints;
    @Expose
    @SerializedName("TotalRedeemPoints")
    private String TotalRedeemPoints;
    @Expose
    @SerializedName("Brand")
    private String Brand;
    @Expose
    @SerializedName("BookingId")
    private String BookingId;

    @Expose
    @SerializedName("FormattedBookingId")
    private String FormattedBookingId;

    public String getBookingId() {
        return BookingId;
    }

    public void setBookingId(String bookingId) {
        BookingId = bookingId;
    }

    public String getFormattedBookingId() {
        return FormattedBookingId;
    }

    public void setFormattedBookingId(String formattedBookingId) {
        FormattedBookingId = formattedBookingId;
    }

    public String getTotalAccruedPoints() {
        return TotalAccruedPoints;
    }

    public void setTotalAccruedPoints(String totalAccruedPoints) {
        TotalAccruedPoints = totalAccruedPoints;
    }

    public String getTotalRedeemPoints() {
        return TotalRedeemPoints;
    }

    public void setTotalRedeemPoints(String totalRedeemPoints) {
        TotalRedeemPoints = totalRedeemPoints;
    }

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String billNo) {
        BillNo = billNo;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String billDate) {
        BillDate = billDate;
    }

    public String getTotalBilledAmount() {
        return TotalBilledAmount;
    }

    public void setTotalBilledAmount(String totalBilledAmount) {
        TotalBilledAmount = totalBilledAmount;
    }


    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }
}
