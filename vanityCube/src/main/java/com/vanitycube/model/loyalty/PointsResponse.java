package com.vanitycube.model.loyalty;

import com.google.gson.annotations.Expose;

/**
 * Created by Nitin on 09/03/18.
 */

public class PointsResponse {
    @Expose
    private Integer pointsRedeemable;
    @Expose
    private Integer loyaltyRedemptionPercentage;
    @Expose
    private Integer loyaltyMinBookingAmount;
    @Expose
    private Integer loyaltyMaxRedemption;
    /*@Expose
    private Integer eligibleServiceAmount;*/
    @Expose
    private Integer loyaltyPointsMultiplier;
    @Expose
    private Integer availablePoints;
    @Expose
    private Integer cartValue;
    @Expose
    private String message;

    public Integer getAvailablePoints() {
        return availablePoints;
    }

    public Integer getCartValue() {
        return cartValue;
    }

    public void setCartValue(Integer cartValue) {
        this.cartValue = cartValue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAvailablePoints(Integer availablePoints) {

        this.availablePoints = availablePoints;
    }

    public Integer getPointsRedeemable() {
        return pointsRedeemable;
    }

    public void setPointsRedeemable(Integer pointsRedeemable) {
        this.pointsRedeemable = pointsRedeemable;
    }

    public Integer getLoyaltyRedemptionPercentage() {
        return loyaltyRedemptionPercentage;
    }

    public void setLoyaltyRedemptionPercentage(Integer loyaltyRedemptionPercentage) {
        this.loyaltyRedemptionPercentage = loyaltyRedemptionPercentage;
    }

    public Integer getLoyaltyMinBookingAmount() {
        return loyaltyMinBookingAmount;
    }

    public void setLoyaltyMinBookingAmount(Integer loyaltyMinBookingAmount) {
        this.loyaltyMinBookingAmount = loyaltyMinBookingAmount;
    }

    public Integer getLoyaltyMaxRedemption() {
        return loyaltyMaxRedemption;
    }

    public void setLoyaltyMaxRedemption(Integer loyaltyMaxRedemption) {
        this.loyaltyMaxRedemption = loyaltyMaxRedemption;
    }

    /*public Integer getEligibleServiceAmount() {
        return eligibleServiceAmount;
    }

    public void setEligibleServiceAmount(Integer eligibleServiceAmount) {
        this.eligibleServiceAmount = eligibleServiceAmount;
    }*/

    public Integer getLoyaltyPointsMultiplier() {
        return loyaltyPointsMultiplier;
    }

    public void setLoyaltyPointsMultiplier(Integer loyaltyPointsMultiplier) {
        this.loyaltyPointsMultiplier = loyaltyPointsMultiplier;
    }
}
