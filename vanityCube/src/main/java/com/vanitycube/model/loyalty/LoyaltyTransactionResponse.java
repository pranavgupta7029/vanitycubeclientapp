package com.vanitycube.model.loyalty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Nitin on 09/03/18.
 */

public class LoyaltyTransactionResponse {

    @Expose
    @SerializedName("MemberTransactionResponseListDTO")
    private ArrayList<MemberTransactionResponseListDTO> memberTransactionResponseListDTO;
    @Expose
    @SerializedName("Success")
    private Integer Success;

    public ArrayList<MemberTransactionResponseListDTO> getMemberTransactionResponseListDTO() {
        return memberTransactionResponseListDTO;
    }

    public void setMemberTransactionResponseListDTO(ArrayList<MemberTransactionResponseListDTO> memberTransactionResponseListDTO) {
        this.memberTransactionResponseListDTO = memberTransactionResponseListDTO;
    }

    public Integer getSuccess() {
        return Success;
    }

    public void setSuccess(Integer success) {
        Success = success;
    }
}
