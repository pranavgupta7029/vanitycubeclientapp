package com.vanitycube.model.loyalty;

import com.google.gson.annotations.Expose;
import com.vanitycube.model.CommonResponse;

/**
 * Created by Nitin on 09/03/18.
 */

public class ReedemableLoyaltyPointsResponse {
    @Expose
    private com.vanitycube.model.loyalty.ReedemableLoyaltyPointsResponse.Responsedata responsedata;

    public class Responsedata extends CommonResponse {
        @Expose
        PointsResponse result;

        public PointsResponse getResult() {
            return result;
        }

        public void setResult(PointsResponse result) {
            this.result = result;
        }
    }

    public com.vanitycube.model.loyalty.ReedemableLoyaltyPointsResponse.Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(com.vanitycube.model.loyalty.ReedemableLoyaltyPointsResponse.Responsedata responsedata) {
        this.responsedata = responsedata;
    }
}



