package com.vanitycube.model.loyalty;

import java.util.ArrayList;

/**
 * Created by Nitin on 12/03/18.
 */

public class BlockLoyaltyPoints {


    private String ReturnCode;
    private String EasyPoints;
    private String CashWorthPoints;
    private String BalanceToPay;
    private String BillAmountPoints;
    private String IsPointsPlusCash;
    private String ReturnMessage;
    private ArrayList Vouchers;
    private String BillNo;
    private String IsRedeemWithOutOTP;
    private String BillAmountBeforeLoyalty;
    private String BillAmountAfterLoyalty;
    private String LoyaltyDiscount;

    public String getBillAmountBeforeLoyalty() {
        return BillAmountBeforeLoyalty;
    }

    public void setBillAmountBeforeLoyalty(String billAmountBeforeLoyalty) {
        BillAmountBeforeLoyalty = billAmountBeforeLoyalty;
    }

    public String getBillAmountAfterLoyalty() {
        return BillAmountAfterLoyalty;
    }

    public void setBillAmountAfterLoyalty(String billAmountAfterLoyalty) {
        BillAmountAfterLoyalty = billAmountAfterLoyalty;
    }

    public String getLoyaltyDiscount() {
        return LoyaltyDiscount;
    }

    public void setLoyaltyDiscount(String loyaltyDiscount) {
        LoyaltyDiscount = loyaltyDiscount;
    }

    public String getReturnCode() {
        return ReturnCode;
    }

    public void setReturnCode(String returnCode) {
        ReturnCode = returnCode;
    }

    public String getEasyPoints() {
        return EasyPoints;
    }

    public void setEasyPoints(String easyPoints) {
        EasyPoints = easyPoints;
    }

    public String getCashWorthPoints() {
        return CashWorthPoints;
    }

    public void setCashWorthPoints(String cashWorthPoints) {
        CashWorthPoints = cashWorthPoints;
    }

    public String getBalanceToPay() {
        return BalanceToPay;
    }

    public void setBalanceToPay(String balanceToPay) {
        BalanceToPay = balanceToPay;
    }

    public String getBillAmountPoints() {
        return BillAmountPoints;
    }

    public void setBillAmountPoints(String billAmountPoints) {
        BillAmountPoints = billAmountPoints;
    }

    public String getIsPointsPlusCash() {
        return IsPointsPlusCash;
    }

    public void setIsPointsPlusCash(String isPointsPlusCash) {
        IsPointsPlusCash = isPointsPlusCash;
    }

    public String getReturnMessage() {
        return ReturnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        ReturnMessage = returnMessage;
    }

    public ArrayList getVouchers() {
        return Vouchers;
    }

    public void setVouchers(ArrayList vouchers) {
        Vouchers = vouchers;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String billNo) {
        BillNo = billNo;
    }

    public String getIsRedeemWithOutOTP() {
        return IsRedeemWithOutOTP;
    }

    public void setIsRedeemWithOutOTP(String isRedeemWithOutOTP) {
        IsRedeemWithOutOTP = isRedeemWithOutOTP;
    }
}
