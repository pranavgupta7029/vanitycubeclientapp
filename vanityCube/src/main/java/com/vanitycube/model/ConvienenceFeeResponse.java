package com.vanitycube.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Milind on 09-Jun-17.
 */

public class ConvienenceFeeResponse implements Parcelable {

    private Responsedata responsedata;

    public class Responsedata extends CommonResponse {
        ConvienenceFeeResponseMain result;

        public ConvienenceFeeResponseMain getResult() {
            return result;
        }

        public void setResult(ConvienenceFeeResponseMain result) {
            this.result = result;
        }
    }

    public Responsedata getResponsedata() {
        return responsedata;
    }

    public void setResponsedata(Responsedata responsedata) {
        this.responsedata = responsedata;
    }

    protected ConvienenceFeeResponse(Parcel in) {
        responsedata = (Responsedata) in.readValue(Responsedata.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(responsedata);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ConvienenceFeeResponse> CREATOR = new Parcelable.Creator<ConvienenceFeeResponse>() {
        @Override
        public ConvienenceFeeResponse createFromParcel(Parcel in) {
            return new ConvienenceFeeResponse(in);
        }

        @Override
        public ConvienenceFeeResponse[] newArray(int size) {
            return new ConvienenceFeeResponse[size];
        }
    };
}