package com.vanitycube.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by swapn on 9/1/2017.
 */

public class TimeSlotModel {

    @SerializedName("timeId")
    @Expose
    private String slotId;

    @SerializedName("time")
    @Expose
    private String slotName;

    private boolean isChecked;

    public TimeSlotModel(String slotId, String slotName) {
        setSlotId(slotId);
        setSlotName(slotName);
    }

    public TimeSlotModel(JSONObject lJsonObject) {
        try {
            setSlotId(lJsonObject.getString("timeId"));
            setSlotName(lJsonObject.getString("time"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public String getSlotId() {
        return slotId;
    }

    private void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getSlotName() {
        return slotName != null ? slotName : "";
    }

    private void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
