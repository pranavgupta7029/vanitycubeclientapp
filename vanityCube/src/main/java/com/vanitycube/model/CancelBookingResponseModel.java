package com.vanitycube.model;
// Created by prade on 1/3/2018.

import org.json.JSONObject;

public class CancelBookingResponseModel {
    private int success;
    private String bookingDetails;

    public CancelBookingResponseModel() {
    }

    public CancelBookingResponseModel(JSONObject jsonObject) {
        try {
            JSONObject responseData = jsonObject.getJSONObject("responsedata");
            setSuccess(Integer.parseInt(responseData.getString("success")));
            setBookingDetails(responseData.getString("bookingDetails"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public String getBookingDetails() {
        return bookingDetails;
    }

    public void setBookingDetails(String bookingDetails) {
        this.bookingDetails = bookingDetails;
    }
}
