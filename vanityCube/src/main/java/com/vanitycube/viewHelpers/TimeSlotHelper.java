package com.vanitycube.viewHelpers;


import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.TimeSlotModel;

import java.util.ArrayList;


/**
 * Created by SWAPNIL on 10/16/2015.
 */
public class TimeSlotHelper {

    private View slotBlockView;
    private TextView slot1, slot2, slot3;
    private Activity activity;

    private SlotBlockHelperCallbackInterface slotBlockHelperCallbackInterface;

    public TimeSlotHelper(Activity activity, SlotBlockHelperCallbackInterface slotBlockHelperCallbackInterface) {
        this.slotBlockHelperCallbackInterface = slotBlockHelperCallbackInterface;
        this.activity = activity;

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View subCategoryLayout = layoutInflater.inflate(R.layout.time_slot, null, false);

        slotBlockView = subCategoryLayout.findViewById(R.id.time_slot_layout);
        slot1 = (TextView) slotBlockView.findViewById(R.id.time_slot_1_text_view);
        slot2 = (TextView) slotBlockView.findViewById(R.id.time_slot_2_text_view);
        slot3 = (TextView) slotBlockView.findViewById(R.id.time_slot_3_text_view);
    }

    public View updateSlot(final ArrayList<TimeSlotModel> slotModels, final int block) {
        enableSlots(slot1, true);

        if (slotModels.size() > block * 3 + 1) {
            enableSlots(slot2, true);
        }

        if (slotModels.size() > block * 3 + 2) {
            enableSlots(slot3, true);
        }

        slot1.setText(slotModels.get(block * 3).getSlotName());

        if (slotModels.size() > (block * 3 + 1)) {
            slot2.setText(slotModels.get(block * 3 + 1).getSlotName());
        } else {
            slot2.setVisibility(View.INVISIBLE);
        }

        if (slotModels.size() > (block * 3 + 2)) {
            slot3.setText(slotModels.get(block * 3 + 2).getSlotName());
        } else {
            slot3.setVisibility(View.INVISIBLE);
        }

        slot1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!slot1.isEnabled()) {
                    return;
                }

                int pos = block * 3;
                slotBlockHelperCallbackInterface.slotClicked(slotModels.get(pos));
            }
        });
        slot2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!slot2.isEnabled()) {
                    return;
                }

                int pos = block * 3 + 1;
                slotBlockHelperCallbackInterface.slotClicked(slotModels.get(pos));
            }
        });
        slot3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!slot3.isEnabled()) {
                    return;
                }

                int pos = block * 3 + 2;
                slotBlockHelperCallbackInterface.slotClicked(slotModels.get(pos));
            }
        });

        return slotBlockView;
    }

    private void enableSlots(TextView textView, boolean isEnabled) {
        textView.setEnabled(isEnabled);
        textView.setBackgroundResource(isEnabled ? R.drawable.corner_radius_button_white : R.drawable.corner_radius_button_disabled);
        textView.setPadding(dp2px(10), dp2px(5), dp2px(10), dp2px(5));
    }

    public void toggleSlot(TextView view, ArrayList<TimeSlotModel> slotModels, int pos, boolean isChecked) {
        if (pos >= slotModels.size()) {
            return;
        }

        view.setPadding(dp2px(10), dp2px(5), dp2px(10), dp2px(5));
        view.setText(slotModels.get(pos).getSlotName());
        slotModels.get(pos).setChecked(isChecked);
        view.setBackgroundResource(view.isEnabled() ? (!isChecked ? R.drawable.corner_radius_button_white : R.drawable.corner_radius_button_red) : R.drawable.corner_radius_button_disabled);
        view.setTextColor(ContextCompat.getColor(activity, isChecked ? R.color.white : R.color.dark_red));
        view.setPadding(dp2px(10), dp2px(5), dp2px(10), dp2px(5));
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, activity.getResources().getDisplayMetrics());
    }

    public interface SlotBlockHelperCallbackInterface {
        void slotClicked(TimeSlotModel bookingSlot);
    }

    public TextView getSlot1() {
        return slot1;
    }

    public void setSlot1(TextView slot1) {
        this.slot1 = slot1;
    }

    public TextView getSlot2() {
        return slot2;
    }

    public void setSlot2(TextView slot2) {
        this.slot2 = slot2;
    }

    public TextView getSlot3() {
        return slot3;
    }

    public void setSlot3(TextView slot3) {
        this.slot3 = slot3;
    }
}