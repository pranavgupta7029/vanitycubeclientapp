package com.vanitycube.viewHelpers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.vanitycube.R;
import com.vanitycube.adapter.PackageAdapter;

/**
 * Created by Nitin on 25/10/17.
 */

public class PackageViewHolder extends RecyclerView.ViewHolder {
    public View mView;

    public TextView coverImageView;
    public FlexboxLayout fLayout;

    public PackageViewHolder(View v) {
        super(v);
        mView = v;
        coverImageView = v.findViewById(R.id.dashText);
        fLayout = v.findViewById(R.id.gridCapsule);
    }

    public void bind(final String name, final Integer position, final PackageAdapter.OnItemClickListener listener) {
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(name, position, coverImageView);
                coverImageView.setTag(name);
                fLayout.setTag(name);
            }
        });
    }

}

