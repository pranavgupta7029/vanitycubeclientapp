package com.vanitycube.viewHelpers;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.adapter.MyAdapter;
import com.vanitycube.model.Offer;

/**
 * Created by Nitin on 25/10/17.
 */

public class MyViewHolder extends RecyclerView.ViewHolder {
    public View mView;

    public TextView coverImageView;

    public MyViewHolder(View v) {
        super(v);
        mView = v;
        coverImageView = v.findViewById(R.id.dashText);
    }

    public void bind(final Offer offerItem, final MyAdapter.OnItemClickListener listener) {
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(offerItem);
            }
        });
    }

}

