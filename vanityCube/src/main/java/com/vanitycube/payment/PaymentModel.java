
package com.vanitycube.payment;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentModel implements Parcelable {
    private String txnid;
    private String key;
    private String amount;
    private String firstname;
    private String email;
    private String phone;
    private String productinfo;
    private String baseurl = "https://secure.payu.in";
    private String merchant_key = "RZ4oum";
    private String salt = "zmlJaxId";
    private String surl = "http://vanitycube.in/vcube/public/default/index/paysuccess";
    private String furl = "http://vanitycube.in/vcube/public/default/index/paysuccess";

    public PaymentModel() {
    }

    public PaymentModel(String txnid, String amount, String firstname, String phone, String email, String productinfo) {
        this.txnid = txnid;
        this.amount = amount;
        this.firstname = firstname;
        this.email = email;
        this.phone = phone;
        this.productinfo = productinfo;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProductinfo() {
        return productinfo;
    }

    public void setProductinfo(String productinfo) {
        this.productinfo = productinfo;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public String getMerchant_key() {
        return merchant_key;
    }

    public String getSalt() {
        return salt;
    }

    public String getSurl() {
        return surl;
    }

    public String getFurl() {
        return furl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    protected PaymentModel(Parcel in) {
        txnid = in.readString();
        key = in.readString();
        amount = in.readString();
        firstname = in.readString();
        email = in.readString();
        phone = in.readString();
        productinfo = in.readString();
        baseurl = in.readString();
        salt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(txnid);
        dest.writeString(key);
        dest.writeString(amount);
        dest.writeString(firstname);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(productinfo);
        dest.writeString(baseurl);
        dest.writeString(salt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PaymentModel> CREATOR = new Parcelable.Creator<PaymentModel>() {
        @Override
        public PaymentModel createFromParcel(Parcel in) {
            return new PaymentModel(in);
        }

        @Override
        public PaymentModel[] newArray(int size) {
            return new PaymentModel[size];
        }
    };
}