
package com.vanitycube.payment;

import android.content.Context;
import android.util.Log;

import com.vanitycube.settings.ApplicationSettings;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class PaymentWebService {
    private static final String TAG = "PaymentWebService";
    private final int SUCCESS_RESPONSE = 200;
    private static final String URL = ApplicationSettings.PAY_U_MONEY_URL;
    private Context mContext;

    public PaymentWebService(Context context) {
        mContext = context;
        createPayPostRequest();
    }

    @SuppressWarnings("deprecation")
    private boolean createPayPostRequest() {
        boolean isResponse = false;
        HttpPost httpPost = null;
        HttpResponse response = null;
        StringEntity se = null;
        String lData = createPaymentJSONString();

        HttpClient client = new DefaultHttpClient();
        client = sslClient(client);
        try {
            httpPost = new HttpPost(URL);

            se = new StringEntity(lData);
            se.setContentType("application/json;charset=UTF-8");
            httpPost.setEntity(se);
            response = client.execute(httpPost);
            if (response.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
                String serverResponse = getHttpResponseStructure(response);
                JSONTokener tokener = new JSONTokener(serverResponse);
                try {
                    JSONObject jsonObject = new JSONObject(tokener);
                    JSONObject responseDataJson = jsonObject.getJSONObject("responsedata");

                    String statusCode = responseDataJson.getString("success");
                    Log.i(TAG, " Status Code :: " + statusCode);
                    if (("1").equalsIgnoreCase(statusCode)) {
                        // mUserProfile.setUserDetails(jsonObjectResponse);
                        isResponse = true;

                    } else {
                        isResponse = false;

                    }
                } catch (JSONException ex) {
                    Log.e(TAG, ex.getMessage());
                    isResponse = false;
                } catch (Exception ex) {
                    Log.e(TAG, ex.getMessage());
                    isResponse = false;
                }
                // isResponse = true;
            } else {
                isResponse = false;
            }
        } catch (IllegalArgumentException ie) {
            Log.e(TAG, ie.getMessage());
            isResponse = false;
        } catch (Exception ie) {
            Log.e(TAG, ie.getMessage());
            isResponse = false;
        }
        return isResponse;
    }

    private String createPaymentJSONString() {
        JSONObject lJSONObject = new JSONObject();
        try {
            lJSONObject.put("key", "");
            lJSONObject.put("txnid", "");
            lJSONObject.put("amount", "");
            lJSONObject.put("productinfo", "");
            lJSONObject.put("firstname", "");
            lJSONObject.put("email", "");
            lJSONObject.put("phone", "");
            lJSONObject.put("surl", "");
            lJSONObject.put("furl", "");
            lJSONObject.put("hash", "");

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return lJSONObject.toString();
    }

    @SuppressWarnings("deprecation")
    public String getHttpResponseStructure(HttpResponse pHttpResponseObj) {
        String lResponseString = null;
        if (pHttpResponseObj.getStatusLine().getStatusCode() == SUCCESS_RESPONSE) {
            Reader inputStreamReaderObj = null;
            try {
                inputStreamReaderObj = new BufferedReader(new InputStreamReader(pHttpResponseObj.getEntity().getContent(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG + "RestWebServices", e.getMessage());
            } catch (IllegalStateException e) {
                Log.e(TAG + "RestWebServices", e.getMessage());
            } catch (IOException e) {
                Log.e(TAG + "RestWebServices", e.getMessage());
            }
            StringBuilder httpResponseDataBuilder = new StringBuilder();
            char[] buf = new char[1000];
            int l = 0;
            while (l >= 0) {
                httpResponseDataBuilder.append(buf, 0, l);
                try {
                    l = inputStreamReaderObj.read(buf);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            lResponseString = httpResponseDataBuilder.toString();
            Log.i(TAG, "Rest webservice :: " + httpResponseDataBuilder.toString());
            // System.out.println("builder.toString() >> " + httpResponseDataBuilder.toString());
        }
        return lResponseString;
    }

    @SuppressWarnings("deprecation")
    private HttpClient sslClient(HttpClient client) {
        try {
            X509TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] xcs,
                                               String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs,
                                               String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(null, new TrustManager[]{tm}, null);
            // SSLSocketFactory ssf = new SSLSocketFactory(ctx);
            // ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            ClientConnectionManager ccm = client.getConnectionManager();
            SchemeRegistry sr = ccm.getSchemeRegistry();
            // sr.register(new Scheme("https", ssf, 443));
            return new DefaultHttpClient(ccm, client.getParams());
        } catch (Exception ex) {
            return null;
        }
    }
}
