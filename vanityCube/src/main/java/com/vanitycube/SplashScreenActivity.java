package com.vanitycube;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.vanitycube.activities.FeedbackActivity;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.activities.SearchAreaListActivity;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;

public class SplashScreenActivity extends Activity {
    private RestWebServices mRestWebservices;
    private SharedPref pref;
    private String areaId = "-1";
    private String hubId = "-1";
    private Uri data;
    private String lastWord = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash_screen);

        mRestWebservices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());

        pref.putReloadRequired(false);
        pref.putOtherFragmentsActive(false);

        areaId = pref.getAreaId();
        hubId = pref.getHubId();

        Intent intent = getIntent();

        if (intent != null) {
            //get action
            String action = intent.getAction();
            //get data
            data = intent.getData();
            if (data != null) {
                System.out.print(data.toString());
                if (data.toString() != null) {
                    lastWord = getLastWord(data.toString());
                }
            }
        }

        //Get configuration call
        getConfiguration();
    }

    private void getConfiguration() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new ConfigurationAsyncTask().execute(null, null, null);
        } else {
            showNoInternetDialog();
        }
    }

    private void proceed() {
        int SPLASH_TIME_OUT = 2000;

        if (!areaId.equalsIgnoreCase("-1") && !hubId.equalsIgnoreCase("-1")) {
            //Handler
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Intent i = new Intent(SplashScreenActivity.this, DashboardActivity.class);
                    Intent i = new Intent(SplashScreenActivity.this, MainSplashActivity.class);
                    if (data != null && lastWord != null)
                        i.putExtra("scheme", lastWord);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent(SplashScreenActivity.this, SearchAreaListActivity.class);
                    if (data != null && lastWord != null)
                        i.putExtra("scheme", lastWord);
                    pref.putAppFirstRun(true);
                    startActivity(i);
                    //finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    /*  -------------------------------------
        |
        |  GET FEEDBACK ASYNC TASK
        |
        -------------------------------------
    */

    class IsFeedbackAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;
        String[] bookingIdTimeDate;

        IsFeedbackAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(Void... url) {
            try {
                bookingIdTimeDate = mRestWebservices.isFeedback();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return bookingIdTimeDate;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            if (result[0].length() > 0) {
                Intent feedback = new Intent(SplashScreenActivity.this, FeedbackActivity.class);
                feedback.putExtra("booking_id", result);
                startActivityForResult(feedback, 21);
            } else {
                proceed();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GET CONFIGURATION ASYNC TASK
        |
        -------------------------------------
    */

    class ConfigurationAsyncTask extends AsyncTask<Void, Void, Bundle> {
        ConfigurationAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bundle doInBackground(Void... url) {
            return mRestWebservices.getConfiguration();
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);
            if (result.getBoolean("success", false)) {
                try {
                    int currentVersion = Integer.parseInt(result.getString("current_version"));//28
                    int minSupportedVersion = Integer.parseInt(result.getString("min_supported_version"));//28
                    String updateMessage = result.getString("update_message");
                    checkIfUpdateRequired(currentVersion, minSupportedVersion, updateMessage);
                } catch (Exception e) {
                    new IsFeedbackAsyncTask().execute(null, null, null);
                }
            } else {
                new IsFeedbackAsyncTask().execute(null, null, null);
            }
        }
    }

    private void checkIfUpdateRequired(int currentVersion, int minSupportedVersion, String updateMessage) {
        float currentAppVersion = getCurrentAppVersion();//47

        if (currentAppVersion < (float) minSupportedVersion) {
            showUpdateAppDialog(false, updateMessage);
        } else if (currentAppVersion < (float) currentVersion) {
            showUpdateAppDialog(true, updateMessage);
        } else {
            proceed();
        }
    }

    private int getCurrentAppVersion() {
        int versionCode = -1;
        try {
            Context context = getApplicationContext();
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_ACTIVITIES).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionCode;
    }

    private void showUpdateAppDialog(boolean showRemindMeLaterButton, String updateMessage) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.upgrade_available_dialog_title));
        dialogBuilder.setMessage(updateMessage);

        dialogBuilder.setPositiveButton(getString(R.string.upgrade_available_dialog_positive_button_text), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ApplicationSettings.MARKET_URL)));
                finish();
            }
        });

        if (!showRemindMeLaterButton) {
            dialogBuilder.setNegativeButton(R.string.exit_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }

        if (showRemindMeLaterButton) {
            dialogBuilder.setNeutralButton(getString(R.string.upgrade_available_dialog_neutral_button_text), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    new IsFeedbackAsyncTask().execute(null, null, null);
                }
            });
        }

        dialogBuilder.setCancelable(false);
        dialogBuilder.show();
    }

    public void reloadActivity() {
        try {
            Intent reloadIntent = getIntent();
            reloadIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            reloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (reloadIntent != null) {
                startActivity(reloadIntent);
                finish();
            }
        } catch (Exception ex) {

        }
    }

    private void showNoInternetDialog() {
        String noInternetMessage = getResources().getString(R.string.no_internet_message);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.error_text));
        dialogBuilder.setMessage(noInternetMessage);

        dialogBuilder.setPositiveButton(getString(R.string.reload_text), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                reloadActivity();
            }
        });

        dialogBuilder.setNegativeButton(R.string.exit_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        dialogBuilder.setCancelable(false);
        dialogBuilder.show();
    }

    private String getLastWord(String word) {
        //String testString = "This is a sentence";
        String[] parts = word.split("/");
        String lastWord = parts[parts.length - 1];
        System.out.println(lastWord);
        if (lastWord.equalsIgnoreCase("Hair_and_Chemical"))
            lastWord = "Hair & Chemical";
        return lastWord;
    }

}
