package com.vanitycube.dbmodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.vanitycube.database.VcTableList;

import org.json.JSONObject;

public class CityModel implements Parcelable {
    private String city_id;
    private String state_id;
    private String name;
    private String stateName;

    public CityModel() {

    }

    public CityModel(JSONObject lObj_cityJSONObject) {

        setCityId(lObj_cityJSONObject
                .optString(VcTableList.CITY_ID));
        setStateId(lObj_cityJSONObject
                .optString(VcTableList.STATE_ID));
        setName(lObj_cityJSONObject
                .optString(VcTableList.CITY_NAME));
        setStateName(lObj_cityJSONObject
                .optString("StateName"));
    }

    public String getCityId() {
        return this.city_id;
    }

    public void setCityId(String city_id) {
        this.city_id = city_id;
    }

    public String getStateId() {
        return state_id;
    }

    public void setStateId(String state_id) {
        this.state_id = state_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {

        String cityDetail = "";
        if (city_id != null)
            cityDetail = cityDetail + "  " + city_id;
        if (state_id != null)
            cityDetail = cityDetail + "  " + state_id;
        if (name != null)
            cityDetail = cityDetail + "  " + name;

        Log.i("Country Details Here", cityDetail);
        return cityDetail;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }


    protected CityModel(Parcel in) {
        city_id = in.readString();
        state_id = in.readString();
        name = in.readString();
        stateName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city_id);
        dest.writeString(state_id);
        dest.writeString(name);
        dest.writeString(stateName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CityModel> CREATOR = new Parcelable.Creator<CityModel>() {
        @Override
        public CityModel createFromParcel(Parcel in) {
            return new CityModel(in);
        }

        @Override
        public CityModel[] newArray(int size) {
            return new CityModel[size];
        }
    };
}