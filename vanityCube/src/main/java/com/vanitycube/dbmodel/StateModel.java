
package com.vanitycube.dbmodel;

import android.util.Log;

import com.vanitycube.database.VcTableList;

import org.json.JSONObject;

public class StateModel {

    private String state_id;
    private String name;
    private String status;

    public StateModel() {

    }

    public StateModel(JSONObject lObj_stateJSONObject) {
        // User Details.

        setState_id(lObj_stateJSONObject
                .optString(VcTableList.STATE_ID));
        setName(lObj_stateJSONObject
                .optString(VcTableList.STATE_NAME));
        setStatus(lObj_stateJSONObject
                .optString(VcTableList.STATE_STATUS));

    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {

        String stateDetail = "";
        if (state_id != null)
            stateDetail = stateDetail + "  " + state_id;
        if (name != null)
            stateDetail = stateDetail + "  " + name;
        if (status != null)
            stateDetail = stateDetail + "  " + status;

        Log.i("State Details Here", stateDetail);
        return stateDetail;
    }

}
