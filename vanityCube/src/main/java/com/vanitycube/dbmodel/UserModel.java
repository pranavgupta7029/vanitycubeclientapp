package com.vanitycube.dbmodel;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

public class UserModel implements Parcelable {
    private String id;
    private String name;
    private String email;
    private String number;
    private String profileImagePath;
    private String referralName;
    private String dob;
    private String hubId;
    private String areaId;
    private String areaName;
    private String cityName;
    private String cityId;
    private String facebookId;
    private String googleId;
    private Boolean isGuest;
    private Boolean isNumberVerified;
    private String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }



    public UserModel() {
    }

    public UserModel(JSONObject userDetailsJSONObject) {
        try {
            setID(userDetailsJSONObject.optString("user_id"));

            setName(userDetailsJSONObject.isNull("name") ? "" : userDetailsJSONObject.optString("name"));

            setEmail(userDetailsJSONObject.optString("email"));

            setNumber(userDetailsJSONObject.optString("contact"));

            setProfileImagePath(userDetailsJSONObject.optString("profile_pic"));

            setReferralName(userDetailsJSONObject.optString("referral_name"));

            setDOB(userDetailsJSONObject.optString("dob"));

            setHub_id(userDetailsJSONObject.optString("hub_id"));

            setArea_id(userDetailsJSONObject.optString("area_id"));

            setArea_name(userDetailsJSONObject.optString("area_name"));

            setCity_id(userDetailsJSONObject.optString("city_id"));

            setCity_name(userDetailsJSONObject.optString("city_name"));

            setFacebookId("");

            setGoogleId("");

            setGuest(false);

            setNumberVerified(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /*  -------------------------------------
        |
        |   GETTER AND SETTER METHODS
        |
        -------------------------------------
    */

    public Boolean getNumberVerified() {
        return isNumberVerified;
    }

    public void setNumberVerified(Boolean numberVerified) {
        isNumberVerified = numberVerified;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public Boolean getGuest() {
        return isGuest;
    }

    public void setGuest(Boolean guest) {
        isGuest = guest;
    }

    public String getArea_name() {
        return areaName;
    }

    public void setArea_name(String areaName) {
        this.areaName = areaName;
    }

    public String getCity_name() {
        return cityName;
    }

    public void setCity_name(String cityName) {
        this.cityName = cityName;
    }

    public String getCity_id() {
        return cityId;
    }

    public void setCity_id(String cityId) {
        this.cityId = cityId;
    }

    public String getHub_id() {
        return hubId;
    }

    public void setHub_id(String hubId) {
        this.hubId = hubId;
    }

    public String getArea_id() {
        return areaId;
    }

    public void setArea_id(String areaId) {
        this.areaId = areaId;
    }

    public String getDOB() {
        return dob;
    }

    public void setDOB(String dob) {
        this.dob = dob;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }

    public String getProfileImagePath() {
        return profileImagePath;
    }

    public void setProfileImagePath(String profileImagePath) {
        this.profileImagePath = profileImagePath;
    }

    @Override
    public String toString() {

        String lUserDetails = "";

        if (id != null) {
            lUserDetails += " UserId: " + id;
        }

        if (name != null) {
            lUserDetails += " name: " + name;
        }

        if (email != null) {
            lUserDetails += " email: " + email;
        }

        if (number != null) {
            lUserDetails += " Contact Number: " + number;
        }

        if (referralName != null) {
            lUserDetails += " Referral Code: " + referralName;
        }

        if (profileImagePath != null) {
            lUserDetails += " profileImagePath: " + profileImagePath;
        }

        if (dob != null) {
            lUserDetails += " DOB: " + dob;
        }

        return lUserDetails;
    }

    /*  -------------------------------------
        |
        |   PARCELABLE METHODS
        |
        -------------------------------------
    */

    //Create an instance of this model from parcel data
    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {

        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }

    };

    //Read from parcel
    public UserModel(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.email = in.readString();
        this.number = in.readString();
        this.profileImagePath = in.readString();
        this.referralName = in.readString();
        this.dob = in.readString();
        this.hubId = in.readString();
        this.areaId = in.readString();
        this.areaName = in.readString();
        this.cityName = in.readString();
        this.cityId = in.readString();
        this.facebookId = in.readString();
        this.googleId = in.readString();
        this.isGuest = in.readByte() != 0;
        this.isNumberVerified = in.readByte() != 0;
    }

    //Write to parcel
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(number);
        dest.writeString(profileImagePath);
        dest.writeString(referralName);
        dest.writeString(dob);
        dest.writeString(hubId);
        dest.writeString(areaId);
        dest.writeString(areaName);
        dest.writeString(cityName);
        dest.writeString(cityId);
        dest.writeString(facebookId);
        dest.writeString(googleId);
        dest.writeByte((byte) (isGuest ? 1 : 0));
        dest.writeByte((byte) (isNumberVerified ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }
}