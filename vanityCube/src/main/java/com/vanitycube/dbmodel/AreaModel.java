package com.vanitycube.dbmodel;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.vanitycube.database.VcTableList;

import org.json.JSONObject;


public class AreaModel implements Parcelable {

    private String city_id;
    private String area_id;
    private String name;
    private String city;
    private String hub_id;
    private String hub;
    private String minAmount;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHub_id() {
        return hub_id;
    }

    public void setHub_id(String hub_id) {
        this.hub_id = hub_id;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public AreaModel() {

    }

    public AreaModel(JSONObject lObj_cityJSONObject) {
        // User Details.
        setCityId(lObj_cityJSONObject
                .optString(VcTableList.CITY_ID));
        setArea_id(lObj_cityJSONObject
                .optString(VcTableList.AREA_ID));
        setName(lObj_cityJSONObject
                .optString(VcTableList.AREA_NAME));
        setCity(lObj_cityJSONObject.optString("city"));
        setHub_id(lObj_cityJSONObject.optString("hub_id"));
        setHub(lObj_cityJSONObject.optString("hub"));
        setMinAmount(lObj_cityJSONObject.optString("HubAreaMinimumAmount"));
    }


    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getCityId() {
        return this.city_id;
    }

    public void setCityId(String city_id) {
        this.city_id = city_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {

        String cityDetail = "";
        if (city_id != null)
            cityDetail = cityDetail + "  " + city_id;
        if (area_id != null)
            cityDetail = cityDetail + "  " + area_id;
        if (name != null)
            cityDetail = cityDetail + "  " + name;

        Log.i("City Details Here", cityDetail);
        return cityDetail;
    }


    protected AreaModel(Parcel in) {
        city_id = in.readString();
        area_id = in.readString();
        name = in.readString();
        city = in.readString();
        hub_id = in.readString();
        hub = in.readString();
        minAmount = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(city_id);
        dest.writeString(area_id);
        dest.writeString(name);
        dest.writeString(city);
        dest.writeString(hub_id);
        dest.writeString(hub);
        dest.writeString(minAmount);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AreaModel> CREATOR = new Parcelable.Creator<AreaModel>() {
        @Override
        public AreaModel createFromParcel(Parcel in) {
            return new AreaModel(in);
        }

        @Override
        public AreaModel[] newArray(int size) {
            return new AreaModel[size];
        }
    };
}