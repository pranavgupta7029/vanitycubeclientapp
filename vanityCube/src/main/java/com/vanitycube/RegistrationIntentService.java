package com.vanitycube;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.webservices.RestWebServices;

import java.io.IOException;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationService";
    private static final String[] TOPICS = {"global"};
    private Bundle b;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "onHandleIntent called");

        try {
            synchronized (TAG) {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken("975362665118", GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                sendRegistrationToServer(token);
                subscribeTopics(token);
            }
        } catch (Exception e) {
            Log.i(TAG, "Failed to complete token refresh", e);
        }

        Intent registrationComplete = new Intent("registrationComplete");
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(String token) {
        SharedPref pref = new SharedPref(VcApplicationContext.getInstance());
        pref.putGCMToken(token);
        Log.i("GCM Token", "Token is: " + token);
        UpdateDeviceTokenAsyncTask updateDeviceTokenAsyncTask = new UpdateDeviceTokenAsyncTask();
        updateDeviceTokenAsyncTask.execute();
    }

    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }

    private class UpdateDeviceTokenAsyncTask extends AsyncTask<Void, Void, Bundle> {
        private RestWebServices mRestWebservices;

        UpdateDeviceTokenAsyncTask() {
            mRestWebservices = new RestWebServices();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bundle doInBackground(Void... url) {
            return b;
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);
        }
    }
}
