package com.vanitycube;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vanitycube.activities.FeedbackActivity;
import com.vanitycube.activities.MainSplashActivity;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.model.NotificationModel;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GcmMessageHandler extends IntentService {

    private static final String TAG = "GCMIntentService";
    private Context context;

    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {

            final Bundle extras = intent.getExtras();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            String messageType = gcm.getMessageType(intent);
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                    Log.e("GCM", "Send error: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                    Log.e("GCM", "Deleted messages on server: " + extras.toString());
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                    sendNotification(extras);
                }
            }
            GcmBroadcastReceiver.completeWakefulIntent(intent);

        } catch (Exception ex) {
            Log.d(TAG, "Title: " + ex.getMessage());
        }
    }

    private void sendNotification(Bundle extras) {
        onMessageReceived(extras);
    }

    public void onMessageReceived(Bundle data) {
        String message = data.getString("message");
        String title = data.getString("title");
        String type = data.getString("type");

        if (message != null) {

            Log.d(TAG, "Title: " + title);
            Log.d(TAG, "Message: " + message);

            if (type != null && type.equalsIgnoreCase("feedback")) {
                String id = data.getString("booking_id");
                String time = data.getString("booking_time");
                String date = data.getString("booking_date");
                if (id != null && time != null && date != null)
                    sendFeedbackNotification(title, id, time, date);
            } else {
                sendNotification(title, message);
                VcDatabaseQuery mDbQuery = new VcDatabaseQuery();
                NotificationModel lObjNotification = new NotificationModel();
                lObjNotification.setTitle(title);
                lObjNotification.setMessage(message);
                mDbQuery.insertNotification(lObjNotification);
            }
        } /*else if(data!=null){
            try {
                //Bundle bund = data.getBundle("")
                String scheme = data.getString("com.urbanairship.actions");
                String url = data.getString("com.urbanairship.style");
                String text = data.getString("com.urbanairship.push.ALERT");
                String action=null;
                String picture=null;
                if (scheme != null) {
                    JSONObject jObj = new JSONObject(scheme);
                    action = jObj.getString("^d");
                    if(url!=null) {
                        JSONObject jOBjPicture = new JSONObject(url);
                        if(jOBjPicture!=null)
                            picture = jOBjPicture.getString("big_picture");
                    }
                }

                if(picture!=null && action!=null){
                    sendCustomNotification(text,action,picture);
                } else if(text!=null && action!=null){
                    sendNotificationText(text,action);
                }

                //Set<String> keyset = data.keySet();
                //data.
                System.out.print("hello");
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }*/
    }

    private void sendNotification(String title, String message) {
        Intent intent = new Intent(this, MainSplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("notification", true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(41 /* ID of notification */, notificationBuilder.build());
    }

    private void sendNotificationText(String message, String url) {
        Intent intent = new Intent(this, MainSplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("scheme", url);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(getNotificationIcon())
                .setContentTitle("VanityCube")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(41 /* ID of notification */, notificationBuilder.build());
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.logo_vc_white : R.drawable.huge;
    }

    private void sendFeedbackNotification(String title, String booking_id, String booking_time, String booking_date) {
        Intent intent = new Intent(this, FeedbackActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        String[] bookingIdTImeDate = {booking_id, booking_time, booking_date};
        intent.putExtra("booking_id", bookingIdTImeDate);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.huge)
                .setContentTitle(title)
                .setContentText("We would love to hear your feedback.")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(41 /* ID of notification */, notificationBuilder.build());
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    private void sendCustomNotification(String text, String scheme, String url) {


        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, MainSplashActivity.class);
        notificationIntent.putExtra("scheme", scheme);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent =
                PendingIntent.getActivity(context, 0,
                        notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap bitmap = getBitmapfromUrl(url);
        //String message="Hello Notification with image";
        String title = "VanityCube";
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();

        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setContentText(text)
                .setContentIntent(intent)
                .setSmallIcon(icon)
                .setWhen(when)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap).setSummaryText(text))
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

// Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);
    }


}