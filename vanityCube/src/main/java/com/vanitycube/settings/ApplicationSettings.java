package com.vanitycube.settings;

public class ApplicationSettings {

    // URL Constants
    public static final boolean LOCAL = false;
    public static final boolean TESTING = false;

    // VC Server
    //public static final String BASE_URL = LOCAL ? "http://dev.vanitycube.in/vcube/public/" : "https://vanitycube.in/vcube/public/";
    //public static final String BASE_URL_PAY = LOCAL ? "http://dev.vanitycube.in/" : "https://vanitycube.in/";
    public static final String VANITYCUBE_API_URL = "http://api.vanitycube.in/services";
    //public static final String URL_VCLUB = "http://staging.vanitycube.in/v-club?userId="; //staging
    public static final String URL_VCLUB = "https://vanitycube.in/v-club?userId="; //production

    public static final String BASE_URL = LOCAL ? "http://staging.vanitycube.in/vcube/public/" : "https://vanitycube.in/vcube/public/";
    public static final String BASE_URL_PAY = LOCAL ? "http://staging.vanitycube.in/" : "https://vanitycube.in/";

    public static final String APPLICATION_SERVICE_URL = BASE_URL + "b2b/wsdl/rest";
    public static final String PROFILE_IMAGE_PATH = BASE_URL + "img/users/";
    public static final String OFFER_IMAGE_PATH = BASE_URL + "img/offers/";
    //public static final String BANNER_IMAGE_PATH = BASE_URL + "vanitycube_uploads/";
    public static final String PAY_U_MONEY_URL = BASE_URL + "b2b/wsdl/rest";
    public static final String PARAM_API_DATA = "apiData";
    public static final String PARAM_REWARD_TYPE = "rewardType";


    // Other Constants
    public static final String BOOKING_ID_PREFIX = "VC00";

    // MAX Service Time in Hours
    public static final int MAX_SERVICE_TIME = 18;

    // Turn on/off logging to increase application performance.
    public static final boolean APPLICATION_LOGGING = true;


    // PlayStore URL for application
    public static final String MARKET_URL = "https://play.google.com/store/apps/details?id=com.vanitycube";
    public static final String UPDATE_LOCATION_URL = APPLICATION_SERVICE_URL;
    public static final String PAGE_CATEGORY = "category_select";
    public static final String PAGE_CART = "cart";
    public static final String PAGE_BOOKING_DATE = "booking_date_select";
    public static final String PAGE_ADDRESS = "address_select";
    public static final String PAGE_BOOKING_SUMMARY = "booking_summary";
    public static final String PAGE_PAYMENT_SUCCESS = "payment_success";
    public static final String PARAM_METHOD = "method";
    public static final String PARAM_KEY = "key";
    public static final String KEY_CONVENIENCE_FEE = "convience_fee";
    public static final String PARAM_DATA = "data";
    public static final String METHOD_UPDATE_USER_LOCATION = "updateUserLocation";
    public static final String METHOD_GET_CONSTANT = "getConstant";
    public static final String METHOD_GET_CONVIENENCE_FEES = "getConvenienceFee";
    public static final String METHOD_GET_EXPECTED_DATE_TIME_SLOTS = "getExpectedDateTimeSlotsForMultipleCategoryBooking";
    public static final String METHOD_GET_TIME_SLOTS_BY_DATE = "getTimeSlotsForMultipleCategoryBooking";
    public static final String METHOD_GET_LOCATION = "getNearestLocationByGeoCoordinates";

    // PayTm Creds
//    public static final String PAYTM_MERCHANT_ID = !LOCAL ? "Vanity98131214595194" : "Vanity32414746155297";
//    public static final String PAYTM_INDUSTRY_TYPE_ID = !LOCAL ? "Retail" : "Retail109";
//    public static final String PAYTM_CHANNEL_ID = !LOCAL ? "WAP" : "WAP";
//    public static final String PAYTM_WEBSITE = !LOCAL ? "APP_STAGING" : "VanityWAP";
//    public static final String PAYTM_CALLBACK_URL = !LOCAL ? "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp" : "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    public static final String PAYTM_MERCHANT_ID = LOCAL ? "Vanity98131214595194" : "Vanity32414746155297";
    public static final String PAYTM_INDUSTRY_TYPE_ID = LOCAL ? "Retail" : "Retail109";
    public static final String PAYTM_CHANNEL_ID = LOCAL ? "WAP" : "WAP";
    public static final String PAYTM_WEBSITE = LOCAL ? "APP_STAGING" : "VanityWAP";
    public static final String PAYTM_CALLBACK_URL = LOCAL ? "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp" : "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    public static final String PAYTM_CHECKSUM_URL = BASE_URL_PAY + "api/v1/getpaytmchecksum";
    public static final String PAYTM_TRANSACTION_STATUS_URL = BASE_URL_PAY + "api/v1/gettransactionstatus";
    public static final String PAYTM_TRANSACTION_URL = LOCAL ? "https://pguat.paytm.com/oltp-web/processTransaction" : "https://secure.paytm.in/oltp-web/processTransaction";

    public static final String PAYTM_MERCHANT_KEY = LOCAL ? "kn63KcAOzECFwLv#" : "qEU_88BBlRW58QjH";


}