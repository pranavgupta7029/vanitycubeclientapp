package com.vanitycube;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.model.NotificationModel;

public class MyNotiReceivedHandler implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(OSNotification notification) {
        String message = notification.payload.body;
        String title = notification.payload.title;
        String urlPic = notification.payload.bigPicture;
        String customKey;

        if (message != null) {

            //sendNotification(title, message);
            VcDatabaseQuery mDbQuery = new VcDatabaseQuery();
            NotificationModel lObjNotification = new NotificationModel();
            lObjNotification.setTitle(title);
            lObjNotification.setMessage(message);
            lObjNotification.setPicUrl(urlPic);
            mDbQuery.insertNotification(lObjNotification);
            /*customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);*/
        }
    }
}

