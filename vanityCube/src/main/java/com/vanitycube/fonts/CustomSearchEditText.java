package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

//Created by prade on 10/10/2017.

public class CustomSearchEditText extends EditText {
    public CustomSearchEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomSearchEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomSearchEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Regular.otf");
            setTypeface(tf);
        }
    }
}
