
package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class VCButton extends Button {
    public VCButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public VCButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VCButton(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/Montserrat-Regular.otf");
            setTypeface(tf);
        }
    }
}
