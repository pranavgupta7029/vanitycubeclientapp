
package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class VCSplashTextView extends TextView {
    public VCSplashTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public VCSplashTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VCSplashTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/CinzelDecorative-Bold.ttf");
            setTypeface(tf);

        }
    }
}
