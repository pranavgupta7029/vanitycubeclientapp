/*
    This is our custom TextView
    which extends the base TextView
*/
package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class VCTextView extends TextView {

    public VCTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public VCTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VCTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            //Defining a custom Type Face
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat-Regular.otf");
            setTypeface(tf);
        }
    }

}
