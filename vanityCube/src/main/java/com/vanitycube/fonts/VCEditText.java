
package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class VCEditText extends EditText {
    public VCEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public VCEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VCEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                    "fonts/Montserrat-Regular.otf");
            setTypeface(tf);
        }
    }
}
