package com.vanitycube.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by pradeep on 9/28/2017.
 */

public class FACaretDown extends TextView {

    public FACaretDown(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public FACaretDown(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FACaretDown(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/fontawesome-webfont.ttf");
            setTypeface(tf);
        }
    }

}