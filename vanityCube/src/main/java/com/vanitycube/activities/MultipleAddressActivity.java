package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.UserAddressListAdapter;
import com.vanitycube.adapter.UserAddressListAdapter.userAddressInterface;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

public class MultipleAddressActivity extends Activity implements OnClickListener, userAddressInterface {
    userAddressInterface mObjuserAddressInterface;

    private String TAG;
    public static String mUserName, mUserId;
    private RestWebServices mRestWebservices;
    private ArrayList<UserAddressModel> mUserAddresses;
    private ArrayList<UserAddressModel> mUserSetAddresses;
    private ListView mAddressListView;
    private String json;
    private Calendar startTime;
    private SharedPref pref;
    private TextView mEmptyAddressView, cartQuantityTextView;
    private Gson gson;
    private Bundle bundle;
    private UserModel userModel;
    private String userId;

    private boolean cameFromSettings, isGuestUser;
    private int RC_UPDATE_ADDRESS = 102;
    private int RC_ADD_ADDRESS = 103;
    public static boolean flagOfAddressAdd = false;
    public static boolean flagOfEmptyAddress = true;
    public MultipleAddressActivity multipleAddressActivity;
    public boolean addressSelectedFlag = false;
    UserAddressListAdapter mAddressListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.add_new_address);

        TAG = getResources().getString(R.string.multiple_address_activity_tag);

        mRestWebservices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_ADDRESS);

        mUserAddresses = new ArrayList<>();

        mUserSetAddresses = new ArrayList<>();

        gson = new Gson();

        userModel = new UserModel();
        multipleAddressActivity = this;

        //Set header
        setHeader();

        setCartQuantity();

        //Get intent params
        getBundle();

        mObjuserAddressInterface = MultipleAddressActivity.this;

        mAddressListView = findViewById(R.id.listViewAddressSummary);
        mEmptyAddressView = findViewById(R.id.emptyAddressView);

        RelativeLayout mAddAddressLayout = findViewById(R.id.addNewAddressLayout);
        mAddAddressLayout.setOnClickListener(this);

        TextView mAddAddressTextView = findViewById(R.id.addNewAddressButton);
        mAddAddressTextView.setOnClickListener(this);

        //Load user credentials
        getUser();

        //Load addresses
        loadAddresses();
        setFooter();
    }

    private void setCartQuantity() {
        ArrayList<ServiceTypeModel> list = pref.getCartData();

        if (list != null) {
            cartQuantityTextView.setText(list.size() + "");
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            userId = userModel.getID();

            if (isValidString(userId)) {
                isGuestUser = userModel.getGuest();
            }
        }
    }

    private void getBundle() {
        bundle = getIntent().getExtras();

        if (bundle != null) {
            mUserId = bundle.getString("user_id");
            mUserName = bundle.getString("user_name");
            cameFromSettings = bundle.getBoolean("came_from_settings");
        }
    }

    private void setHeader() {
        //Set left arrow
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);

        LinearLayout headerFirstButton = findViewById(R.id.headerFirstButton);
        headerFirstButton.setOnClickListener(this);

        TextView mHeaderText = findViewById(R.id.headerText);

        ImageView mHeaderImage = findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderImage.setBackgroundResource(R.drawable.header_body);
        mHeaderText.setText(R.string.address_info_text);

        FloatingActionButton addLayout = findViewById(R.id.fab);
        addLayout.setVisibility(View.VISIBLE);
        addLayout.setOnClickListener(this);

        cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);

        Button addButton = findViewById(R.id.addNewAddressButton);
        addButton.setBackgroundResource(R.drawable.plusadd);
        addButton.setOnClickListener(this);
    }

    class getAddressesAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;

        getAddressesAsyncTask() {
            progressDialog = new ProgressDialog(MultipleAddressActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Getting Addresses");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            try {
                mUserAddresses = mRestWebservices.getAllAddresses();
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }

            return mUserAddresses.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                populateAddressList();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  DELETE ADDRESS API CALL
        |
        -------------------------------------
    */

    class deleteAddressAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String mAddressId;
        boolean isRemoved;

        deleteAddressAsyncTask(String addressId) {
            progressDialog = new ProgressDialog(MultipleAddressActivity.this);
            mAddressId = addressId;
        }

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Removing Address");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            try {
                isRemoved = mRestWebservices.removeAddress(mAddressId);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }

            return isRemoved;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    Toast.makeText(MultipleAddressActivity.this, "Address removed successfully", Toast.LENGTH_SHORT).show();
                    loadAddresses();
                } else {
                    Toast.makeText(MultipleAddressActivity.this, "An error occurred, please try again", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void getChild(int position) {
        View view1;
        int n = mAddressListView.getCheckedItemCount();
        int m = mAddressListView.getChildCount();

        /*for(int i=0;i<mAddressListView.getCount();i++){
            if(i==position)
                continue;
            else{
                CardView view =  (CardView)mAddressListView.getAdapter().getView(position, mAddressListView.getChildAt(i), mAddressListView);
                //CardView view = (CardView) mAddressListView.getChildAt(i);
                if (view != null){
                    LinearLayout len = (LinearLayout) view.getChildAt(0);
                    if(len!=null) {
                        RelativeLayout rel = (RelativeLayout) len.getChildAt(1);
                        if(rel!=null) {
                            RadioButton rad = (RadioButton) rel.getChildAt(0);
                            if(rad!=null) {
                                if(i!=position)
                                    rad.setChecked(false);
                                mAddressListAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                }else{
                    RadioButton radio = (RadioButton) mAddressListView.getAdapter().getView(position, mAddressListView.getChildAt(i), mAddressListView).findViewById(R.id.radio);
                    radio.setChecked(false);
                }

            }
        }*/
        for (int i = 0; i < mAddressListView.getCount(); i++) {
            if (i == position) {
                CardView view = (CardView) mAddressListView.getChildAt(i);
                if (view != null) {
                    LinearLayout len = (LinearLayout) view.getChildAt(0);
                    if (len != null) {
                        RelativeLayout rel = (RelativeLayout) len.getChildAt(1);
                        if (rel != null) {
                            RadioButton rad = (RadioButton) rel.getChildAt(0);
                            if (rad != null) {
                                if (i != position)
                                    rad.setChecked(true);
                                mAddressListAdapter.notifyDataSetChanged();
                            }

                        }
                    }
                }
                continue;
            }
            CardView view = (CardView) mAddressListView.getChildAt(i);
            if (view != null) {
                LinearLayout len = (LinearLayout) view.getChildAt(0);
                if (len != null) {
                    RelativeLayout rel = (RelativeLayout) len.getChildAt(1);
                    if (rel != null) {
                        RadioButton rad = (RadioButton) rel.getChildAt(0);
                        if (rad != null) {
                            if (i != position)
                                rad.setChecked(false);
                            mAddressListAdapter.notifyDataSetChanged();
                        }

                    }
                }
            }
        }
    }

    private void setFooter() {
        TextView FooterButton = findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText(R.string.next_text);
        LinearLayout footerLayout = findViewById(R.id.submitBtn);
        footerLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUserAddresses.size() != 0 && addressSelectedFlag)
                    gotoBook();
                else
                    Toast.makeText(MultipleAddressActivity.this, "Please select/add an address", Toast.LENGTH_SHORT).show();

            }
        });
    }

    //Populate the address list
    public void populateAddressList() {
        mAddressListAdapter = new UserAddressListAdapter(MultipleAddressActivity.this, mUserAddresses, mObjuserAddressInterface, isGuestUser, multipleAddressActivity);

        mAddressListView.setEmptyView(mEmptyAddressView);
        mAddressListView.setAdapter(mAddressListAdapter);
        /*mAddressListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!cameFromSettings) {
                    String selectedCityId = mUserAddresses.get(position).getCityId();
                    String currentSetCityId = pref.getCityId();

                    if (currentSetCityId.equals(selectedCityId)) {
                        setUserAddressParams(mUserAddresses, position);
                        gotoBook();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please select an address with same city", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });*/

        if (mUserAddresses != null && mUserAddresses.size() <= 0) {
            //flagOfEmptyAddress = true;
            if (flagOfEmptyAddress)
                gotoAddAddressForAdding();
        }


    }

    /*  -------------------------------------
        |
        |   GUEST USER ADDRESS ADD/DELETE
        |
        -------------------------------------
    */

    public void deleteAddressForGuest(String mUserAddressId) {
        for (UserAddressModel userAddress : mUserAddresses) {
            if (userAddress.getAddressId().equals(mUserAddressId)) {
                mUserAddresses.remove(userAddress);
            }
        }

        json = gson.toJson(mUserAddresses);
        pref.putGuestAddresses(json);
        populateAddressList();
    }

    /*  -------------------------------------
        |
        |   NORMAL USER ADDRESS ADD/DELETE
        |
        -------------------------------------
    */

    private void addAddressActivity() {
        gotoAddAddressForAdding();
    }

    public void loadAddresses() {
        ArrayList<UserAddressModel> tempList;

        json = pref.getUserSetAddresses();
        tempList = gson.fromJson(json, new TypeToken<ArrayList<UserAddressModel>>() {
        }.getType());

        if (tempList != null) {
            mUserSetAddresses = new ArrayList<>(tempList);
        }

        //Load list of addresses saved by the user
        if (isGuestUser) {
            ArrayList<UserAddressModel> mGuestUserAddresses;

            json = pref.getGuestAddresses();

            mGuestUserAddresses = gson.fromJson(json, new TypeToken<ArrayList<UserAddressModel>>() {
            }.getType());

            if (mGuestUserAddresses != null) {
                mUserAddresses = new ArrayList<>(mGuestUserAddresses);
            }

            populateAddressList();
        } else {
            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                new getAddressesAsyncTask().execute(null, null, null);
            } else {
                Toast.makeText(MultipleAddressActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
            }
        }

        //Get list of addresses chosen by the user

    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void gotoBook() {
        Intent payment = new Intent(MultipleAddressActivity.this, Book.class);
        startActivity(payment);
    }

    public void setUserAddressParams(ArrayList<UserAddressModel> mUserAddresses, int position) {
        String addressId = mUserAddresses.get(position).getAddressId();
        String areaId = mUserAddresses.get(position).getAreaId();
        String hubId = mUserAddresses.get(position).getHubId();

        UserAddressModel lUserAddressModel = new UserAddressModel(addressId, areaId, hubId);

        //Check if already set, else add to the list
        if (mUserSetAddresses.size() > 0) {
            mUserSetAddresses.set(0, lUserAddressModel);
        } else {
            mUserSetAddresses.add(lUserAddressModel);
        }

        json = gson.toJson(mUserSetAddresses);
        pref.putUserSetAddresses(json);
    }

    public void gotoAddAddressForUpdate(String state, String city, String area, String areaId, String landmark, String address, String addressId) {
        Intent updateAddressIntent = new Intent(MultipleAddressActivity.this, AddressAddDeleteActivity.class);

        bundle = new Bundle();

        bundle.putString("state", state);
        bundle.putString("city", city);
        bundle.putString("area", area);
        bundle.putString("areaId", areaId);
        bundle.putString("landmark", landmark);
        bundle.putString("address", address);
        bundle.putString("addressId", addressId);
        bundle.putBoolean("updateAddress", true);

        updateAddressIntent.putExtras(bundle);

        startActivityForResult(updateAddressIntent, RC_UPDATE_ADDRESS);
    }

    public void gotoAddAddressForAdding() {
        Intent lObjAddAddressIntent = new Intent(MultipleAddressActivity.this, AddressAddDeleteActivity.class);
        lObjAddAddressIntent.putExtra("return_address", true);
        flagOfEmptyAddress = false;
        startActivityForResult(lObjAddAddressIntent, RC_ADD_ADDRESS);
    }

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_ADD_ADDRESS || requestCode == RC_UPDATE_ADDRESS) {
            if (resultCode == RESULT_OK) {
                loadAddresses();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.empty:
            case R.id.fab:
                if (mUserAddresses.size() < 4) {
                    addAddressActivity();
                } else {
                    Toast.makeText(MultipleAddressActivity.this, "You cannot add more than 4 addresses, " +
                            "please delete an address to add a new one", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.headerFirstButton:
            case R.id.headerMenu:
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void updateAddress(String state, String city, String area, String areaId, String landmark, String address, String addressId) {
        gotoAddAddressForUpdate(state, city, area, areaId, landmark, address, addressId);
    }

    @Override
    public void removeAddress(String mAddressId) {
        if (isGuestUser) {
            deleteAddressForGuest(mAddressId);
        } else {
            new deleteAddressAsyncTask(mAddressId).execute(null, null, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
        if (flagOfAddressAdd) {
            finish();
            startActivity(getIntent(), bundle);
            flagOfAddressAdd = false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_ADDRESS);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
