package com.vanitycube.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.BookAdapter;
import com.vanitycube.adapter.BookPagerAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.customViews.SlidingTabLayout;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.CategoryModel;
import com.vanitycube.model.ExpectedDateTimeModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.TimeSlotModel;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Book extends FragmentActivity implements OnClickListener, BookAdapter.BookAdapterInterface {
    private String TAG;
    private Calendar startTime;
    private ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes;
    public static ArrayList<CategoryModel> categoriesWithServiceTypesList;
    public ArrayList<Boolean> fieldsBlank;

    private ArrayList<Integer> categoryIdsList;
    private ArrayList<Integer> addressAreaIdsList;
    private ArrayList<Integer> serviceTimesList;

    private ArrayList<TimeSlotModel> timeSlots;
    private SharedPref pref;
    private RestWebServices mRestWebServices;
    private Gson gson;
    private String json;
    private String userId;
    private ListView multipleCategoriesListView;
    private BookAdapter bookAdapter;
    private TextView cartQuantityTextView;

    private boolean cartSet;
    private int currPos = 0;
    private int expectedDateTimeIterator = 0;
    private ViewPager viewPager;
    private BookPagerAdapter mTabsAdapter;
    private SlidingTabLayout mSlidingTabs;
    private ArrayList<String> titleList = new ArrayList<>();
    private HashMap<String, CategoryModel> mapCategoryModelByName = new HashMap<>();
    public static HashMap<String, Boolean> mapOFSelectedDatesAndTime = new HashMap<>();
    private String validationText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_book);

        TAG = getResources().getString(R.string.book_activity_tag);

        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebServices = new RestWebServices();
        gson = new Gson();

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_BOOKING_DATE);

        RelativeLayout mainView = findViewById(R.id.mainView);
        RelativeLayout emptyCartView = findViewById(R.id.emptyCartView);

        cartSet = false;

        //Get user
        getUser();

        mFinalSelectedServiceTypes = pref.getCartData();

        categoriesWithServiceTypesList = new ArrayList<>();
        fieldsBlank = new ArrayList<>();
        categoryIdsList = new ArrayList<>();
        addressAreaIdsList = new ArrayList<>();
        serviceTimesList = new ArrayList<>();
        timeSlots = new ArrayList<>();
        multipleCategoriesListView = findViewById(R.id.multipleCategoryBookingListView);

        setHeader();

        setFooter();


        if (mFinalSelectedServiceTypes != null && !mFinalSelectedServiceTypes.isEmpty()) {
            cartSet = true;
            buildCategoriesModel();
            loadAddressAreaIds();
            loadServiceTimes();
            settingIds();
            settingViewPager();
            //getExpectedDateTimeSlots();
        } else {
            mainView.setVisibility(View.GONE);
            emptyCartView.setVisibility(View.VISIBLE);
        }


    }

    private void settingViewPager() {
        try {

            /*if (PrefrenceManager.getAppParam(this, PrefrenceManager.USER_ROLE).equalsIgnoreCase(ApiConstants.ROLE_DEALER_GATE_KEEPER)) {
                titleList.add("REQUEST");
                titleList.add("COMPLETED");
            } else if (PrefrenceManager.getAppParam(this, PrefrenceManager.USER_ROLE).equalsIgnoreCase(ApiConstants.ROLE__DEALER_SALES_EXECUTIVE)) {
                titleList.add("ENQUIRY");
                titleList.add("INVENTORY");
            }*/


            mTabsAdapter = new BookPagerAdapter(this.getSupportFragmentManager(), Book.this, titleList, viewPager, mapCategoryModelByName, addressAreaIdsList, serviceTimesList);
            mSlidingTabs.setDistributeEvenly(true);
            int count = titleList.size();
            viewPager.setOffscreenPageLimit(count);


            mSlidingTabs.setSelected(true);
            mSlidingTabs.setBackgroundColor(ContextCompat.getColor(this, R.color.vcOrange));
            mSlidingTabs.setSelectedIndicatorColors(ContextCompat.getColor(this, R.color.white));
            viewPager.setAdapter(mTabsAdapter);
            mTabsAdapter.notifyDataSetChanged();

            mSlidingTabs.setViewPager(viewPager);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void settingIds() {
        mSlidingTabs = (SlidingTabLayout) findViewById(R.id.transaction_sliding_tabs);
        viewPager = (ViewPager) findViewById(R.id.transaction_view_pager);
    }

    private void setHeader() {
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);

        findViewById(R.id.headerFirstButton).setOnClickListener(this);
        mMenuButton.setOnClickListener(this);

        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);

        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText("Select Date and Time");
        cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);
        setCartQuantity();
    }

    private void setCartQuantity() {
        ArrayList<ServiceTypeModel> list = pref.getCartData();

        if (list != null) {
            cartQuantityTextView.setText(list.size() + "");
        }
    }

    private void setFooter() {
        TextView FooterButton = findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText(R.string.next_text);
        LinearLayout footerLayout = findViewById(R.id.submitBtn);
        footerLayout.setOnClickListener(this);
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();
        }
    }

    /*  -------------------------------------
        |
        |  LOAD CATEGORIES, SERVICE TIMES & AREA IDS
        |
        -------------------------------------
    */

    private void buildCategoriesModel() {
        try {
            ArrayList<ServiceTypeModel> serviceTypesList;

            for (ServiceTypeModel stm : mFinalSelectedServiceTypes) {
                String categoryId = stm.getCategoryId();

                //If unique category found
                if (categoryId != null && !categoryIdsList.contains(Integer.parseInt(categoryId))) {
                    serviceTypesList = new ArrayList<>();
                    int totalServiceTime = Integer.parseInt(stm.getTime());
                    serviceTypesList.add(stm);
                    if (categoryId != null)
                        categoryIdsList.add(Integer.parseInt(categoryId));
                    String categoryName = stm.getCategoryName();
                    CategoryModel ctm = new CategoryModel(categoryId, categoryName, "", "", "", serviceTypesList, timeSlots, totalServiceTime);
                    titleList.add(categoryName);
                    categoriesWithServiceTypesList.add(ctm);
                    mapCategoryModelByName.put(categoryName, ctm);
                    mapOFSelectedDatesAndTime.put(categoryName, false);
                    fieldsBlank.add(true);
                } else {
                    for (CategoryModel cm : categoriesWithServiceTypesList) {
                        //Enter into the category where this service belongs
                        if (categoryId.equals(cm.getCategoryId())) {
                            serviceTypesList = cm.getServiceTypes();
                            int totalServiceTime = 0;
                            serviceTypesList.add(stm);
                            cm.setServiceTypes(serviceTypesList);

                            for (ServiceTypeModel sm : serviceTypesList) {
                                totalServiceTime += Integer.parseInt(sm.getTime());
                            }

                            cm.setTotalServiceTime(totalServiceTime);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("BOOK", ex.getMessage());
        }

    }

    private void loadAddressAreaIds() {
        ArrayList<UserAddressModel> list;
        gson = new Gson();
        json = pref.getUserSetAddresses();
        list = gson.fromJson(json, new TypeToken<ArrayList<UserAddressModel>>() {
        }.getType());

        if (list != null) {
            for (UserAddressModel uam : list) {
                addressAreaIdsList.add(Integer.parseInt(uam.getAreaId()));
            }
        }
    }

    private void loadServiceTimes() {
        ArrayList<ServiceTypeModel> list;
        int serviceTime = 0;

        for (CategoryModel ctm : categoriesWithServiceTypesList) {
            list = ctm.getServiceTypes();
            for (ServiceTypeModel stm : list) {
                serviceTime += Integer.parseInt(stm.getTime());
            }
            serviceTimesList.add(serviceTime);
        }
    }

    private void populateMultipleCategoryBookingList() {
        bookAdapter = new BookAdapter(Book.this, categoriesWithServiceTypesList, this);
        multipleCategoriesListView.setAdapter(bookAdapter);
    }

    @Override
    public void onListUpdated(String date, int index) {
        currPos = index;
        categoriesWithServiceTypesList.get(currPos).setCategoryDate(date);
        getAvailableTimeSlotsSingleCategory(categoriesWithServiceTypesList.get(currPos).getCategoryId(), date);
    }

    @Override
    public void onAllFieldsSet(String time, String timeId, int index) {
        if (!time.equalsIgnoreCase("selectTime")) {
            currPos = index;
            categoriesWithServiceTypesList.get(currPos).setCategoryTime(time);
            categoriesWithServiceTypesList.get(currPos).setCategoryTimeId(timeId);
            fieldsBlank.set(currPos, false);
            refreshAdapter();
        } else {
            fieldsBlank.set(index, true);
        }
    }

    private boolean allFieldsNotBlank() {
        for (boolean b : fieldsBlank) if (b) return false;
        return true;
    }

    private boolean isAllSelected() {
        boolean b = false;
        for (String s : titleList) {
            if (mapOFSelectedDatesAndTime.get(s)) {
                b = true;
                validationText = "";
                continue;
            } else {
                validationText = s;
                return false;
            }
        }
        return b;
    }

    public void refreshAdapter() {
        bookAdapter.setCategoriesList(categoriesWithServiceTypesList);
        bookAdapter.notifyDataSetChanged();
    }

    private void getExpectedDateTimeSlots() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new getExpectedTimeSlotsAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private void getAvailableTimeSlotsSingleCategory(String categoryId, String dateString) {
        JSONObject data = new JSONObject();
        CategoryModel requestedCategory = new CategoryModel();

        for (CategoryModel ctm : categoriesWithServiceTypesList) {
            if (ctm.getCategoryId().equals(categoryId)) {
                requestedCategory = ctm;
                break;
            }
        }

        ArrayList<Integer> category = new ArrayList<>();
        category.add(Integer.parseInt(categoryId));

        ArrayList<Integer> time = new ArrayList<>();
        time.add(requestedCategory.getTotalServiceTime());

        ArrayList<String> date = new ArrayList<>();
        date.add(dateString);
        ArrayList<String> dateFormated = new ArrayList<>();
        /*for(String d:date){
            dateFormated.add(formatDate(d));
        }*/

        try {
            data.put("categoryIds", new JSONArray(category));
            data.put("areaIds", new JSONArray(addressAreaIdsList));
            data.put("servicesTime", new JSONArray(time));
            data.put("dates", new JSONArray(date));
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new getAvailableTimeSlotsByDateAsyncTask(data.toString()).execute(null, null, null);
        } else {
            Toast.makeText(this, "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |  GET EXPECTED TIME SLOTS ASYNC TASK
        |
        -------------------------------------
    */

    private String getServiceTypeNamesFromCategory(CategoryModel categoryModel) {
        String serviceTypeNamesString = "";
        int iterator = 0;

        ArrayList<ServiceTypeModel> serviceTypesList = categoryModel.getServiceTypes();

        for (ServiceTypeModel stm : serviceTypesList) {
            if (iterator < (serviceTypesList.size() - 1)) {
                serviceTypeNamesString = serviceTypeNamesString.concat(stm.getServiceTypeName().concat(", "));
            } else {
                serviceTypeNamesString = serviceTypeNamesString.concat(stm.getServiceTypeName());
            }

            iterator++;
        }

        return serviceTypeNamesString;
    }

    class getExpectedTimeSlotsAsyncTask extends AsyncTask<Void, Void, ExpectedDateTimeModel> {
        ProgressDialog progressDialog;
        JSONObject data = new JSONObject();
        ExpectedDateTimeModel expectedDateTimeModel = new ExpectedDateTimeModel();

        getExpectedTimeSlotsAsyncTask() {
            progressDialog = new ProgressDialog(Book.this);

            try {
                ArrayList<Integer> category = new ArrayList<>();
                category.add(Integer.parseInt(categoriesWithServiceTypesList.get(expectedDateTimeIterator).getCategoryId()));

                ArrayList<Integer> time = new ArrayList<>();
                time.add(categoriesWithServiceTypesList.get(expectedDateTimeIterator).getTotalServiceTime());

                data.put("categoryIds", new JSONArray(category));
                data.put("areaIds", new JSONArray(addressAreaIdsList));
                data.put("servicesTime", new JSONArray(time));
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Getting available time slots");
            progressDialog.show();
        }

        @Override
        protected ExpectedDateTimeModel doInBackground(Void... url) {
            expectedDateTimeModel = mRestWebServices.getExpectedTimeSlots(data.toString());
            return expectedDateTimeModel;
        }

        @Override
        protected void onPostExecute(ExpectedDateTimeModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (expectedDateTimeIterator <= (categoriesWithServiceTypesList.size() - 1)) {
                    CategoryModel categoryModel = categoriesWithServiceTypesList.get(expectedDateTimeIterator);
                    String date = expectedDateTimeModel.getDate();
                    /*if(date!=null && !date.equals("")) {
                        try {
                            SimpleDateFormat mdyFormat = new SimpleDateFormat("dd-MM-yyyy");
                            SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");

                            // Format the date to Strings
                            String dmy = new SimpleDateFormat("yyyy-MM-dd").parse(date).toString();

                            date = mdyFormat.format(new Date(dmy));
                        }catch(Exception exDate){
                            date = expectedDateTimeModel.getDate();
                        }

                    }*/
                    /*Date dateNew = new Date(date);
                    date = sd.format(dateNew);*/
                    ArrayList<TimeSlotModel> timeSlots = expectedDateTimeModel.getTimeSlots();
                    String message = expectedDateTimeModel.getMessage();

                    if (isValidString(date) && timeSlots != null && timeSlots.size() != 0) {
                        categoryModel.setCategoryDate(date);
                        categoryModel.setCategoryTime(timeSlots.get(0).getSlotName());
                        categoryModel.setCategoryTimeId(timeSlots.get(0).getSlotId());
                        categoryModel.setTimeSlots(timeSlots);
                        fieldsBlank.set(expectedDateTimeIterator, false);
                    } else {
                        if (isValidString(message)) {
                            String serviceTypeNames = getServiceTypeNamesFromCategory(categoryModel);
                            message = message.replace("({{serviceTypesName}})", serviceTypeNames);
                            if (message != null)
                                categoryModel.setMessage(message);
                        }
                    }

                    categoriesWithServiceTypesList.set(expectedDateTimeIterator, categoryModel);

                    //Move to the next category
                    expectedDateTimeIterator++;

                    if (expectedDateTimeIterator <= (categoriesWithServiceTypesList.size() - 1)) {
                        getExpectedDateTimeSlots();
                    } else {
                        populateMultipleCategoryBookingList();
                    }
                }
            } catch (Exception e) {
                //Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  GET AVAILABLE TIME SLOTS ASYNC TASK
        |
        -------------------------------------
    */

    class getAvailableTimeSlotsByDateAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        String data = "";
        boolean result = false;

        getAvailableTimeSlotsByDateAsyncTask(String data) {
            progressDialog = new ProgressDialog(Book.this);
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Getting available time slots");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            timeSlots = new ArrayList<>();
            timeSlots = mRestWebServices.getAvailableTimeSlotsByDate(data);


            if (timeSlots.size() != 0) result = true;
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    categoriesWithServiceTypesList.get(currPos).setTimeSlots(timeSlots);

                    refreshAdapter();
                    fieldsBlank.set(currPos, false);
                } else {
                    categoriesWithServiceTypesList.get(currPos).setTimeSlots(timeSlots);
                    refreshAdapter();
                    fieldsBlank.set(currPos, true);
                    Toast.makeText(Book.this, "We are going busy, please select another date or change service", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void gotoBookingSummaryDetail() {
        if (isAllSelected()) {
            Intent payment = new Intent(Book.this, BookingSummaryDetailActivity.class);

            gson = new Gson();

            /*for(CategoryModel cm: categoriesWithServiceTypesList){
                String date = cm.getCategoryDate();
                cm.setCategoryDate(date);
            }*/

            json = gson.toJson(categoriesWithServiceTypesList);
            pref.putCategoriesWithServiceTypes(json);

            startActivity(payment);
        } else {
            Toast.makeText(this, "Please Select Date and time for " + validationText, Toast.LENGTH_SHORT).show();
        }
    }

    public static String formatDate(String date) {
        String parsedDate = "";
        try {

            Date initDate = new SimpleDateFormat("dd-mm-yyyy").parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
            parsedDate = formatter.format(initDate);

            return parsedDate;
        } catch (Exception ex) {
            return date;
        }
    }


    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.book, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerFirstButton:
            case R.id.headerMenu:
                finish();
                break;

            case R.id.submitBtn:
            case R.id.footerButton:
                if (cartSet) {
                    Long tsLong = System.currentTimeMillis();
                    pref.putTxnId(userId.concat(tsLong.toString()));
                    //pref.putTimer(categoriesWithServiceTypesList.get(0).getCategoryTimeId());
                    //Long tsLong = System.currentTimeMillis();
                    Long timeStamp = tsLong / 1000;
                    pref.putTimeStamp(timeStamp.toString());
                    pref.putTimer("180");
                    gotoBookingSummaryDetail();
                }

                break;

            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_BOOKING_DATE);
    }

}