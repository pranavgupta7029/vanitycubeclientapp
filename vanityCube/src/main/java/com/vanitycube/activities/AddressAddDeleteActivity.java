package com.vanitycube.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.AreaModel;
import com.vanitycube.dbmodel.CityModel;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AddressAddDeleteActivity extends Activity implements OnClickListener {
    private String TAG;
    private String headerTextString;

    AutoCompleteTextView mObjCityEditText, mObjAreaEditText;
    EditText mObjAreaEditTextDisabled, mObjLandmarkEditText, mObjFulladdressEditText, mObjStateEditText;
    TextView mFooterButton, cartQuantityTextView;
    String state, stateId, city, cityId, hubId, area, areaId, landmark, address, addressId;

    private String oldState, oldCity, oldArea, oldAreaId, oldLandmark, oldAddress, oldAddressId, areaText;
    private boolean updateAddress = false;
    private RestWebServices mRestWebservices;
    private SharedPref pref;
    private Gson gson;
    private String json;
    private boolean isGuestUser;
    private String areaName;
    public ProgressDialog progressDialog;
    private Context context;
    private Button locationButton;
    boolean flagOfLocation = false;

    // New changes
    ArrayList<CityModel> mCityModelArrayList;
    ArrayList<AreaModel> mAreaModelArrayList;
    private Calendar startTime;
    private ArrayList<UserAddressModel> mUserSetAddresses;


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.ACCESS_FINE_LOCATION};
    Double latitude, longitude;
    FusedLocationProviderClient mFusedLocationProviderClient;
    public Location mLastKnownLocation;


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(AddressAddDeleteActivity.this, "Cannot get location", Toast.LENGTH_SHORT).show();
                } else {
                    verifyStoragePermissions(AddressAddDeleteActivity.this);
                }
            }
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity the activity from which permissions are checked
     */
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {

            getDeviceLocation();
            /*LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // get the last know location from your location manager.
            Location location= locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
           // Toast.makeText(AddressAddDeleteActivity.this, "Latitude = "+location.getLatitude() +" and longitude = "+location.getLongitude(), Toast.LENGTH_SHORT).show();
            //getLocation(location.getLatitude(),location.getLongitude());
            if(location!=null)
                getGeoAddress(location.getLatitude()+"",location.getLongitude()+"");
            else
                Toast.makeText(AddressAddDeleteActivity.this,"Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
*/
        }
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = (Location) task.getResult();
                        if (mLastKnownLocation != null) {
                            flagOfLocation = true;
                            getGeoAddress(mLastKnownLocation.getLatitude() + "", mLastKnownLocation.getLongitude() + "");
                        } else {
                            Toast.makeText(AddressAddDeleteActivity.this, "Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(AddressAddDeleteActivity.this, "Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.new_address_screen);

        TAG = getResources().getString(R.string.address_add_delete_activity_tag);

        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebservices = new RestWebServices();
        gson = new Gson();
        areaName = pref.getAreaName();
        progressDialog = new ProgressDialog(this);
        context = AddressAddDeleteActivity.this;
        mUserSetAddresses = new ArrayList<>();

        // Set strings
        headerTextString = "Add Address";
        state = "";
        stateId = "";
        city = "";
        cityId = "";
        hubId = "";
        area = "";
        areaId = "";
        landmark = "";
        address = "";
        addressId = "";
        oldState = "";
        oldCity = "";
        oldArea = "";
        oldAreaId = "";
        oldLandmark = "";
        oldAddress = "";
        oldAddressId = "";
        areaText = "";

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_ADDRESS);

        //Get user
        getUser();

        //Get bundle
        getBundle();

        mObjStateEditText = findViewById(R.id.state);
        mObjStateEditText.setEnabled(false);
        mObjCityEditText = findViewById(R.id.city);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        locationButton = findViewById(R.id.locationButton);
        locationButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyStoragePermissions(AddressAddDeleteActivity.this);
            }
        });

        mObjCityEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && (area.equalsIgnoreCase("") || !areaText.equalsIgnoreCase(mObjAreaEditText.getText().toString()))) {
                    Toast.makeText(AddressAddDeleteActivity.this, "Invalid area selection. Select \"others\" if area not found.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        mObjFulladdressEditText = findViewById(R.id.fulladdress_edittext);
        mObjLandmarkEditText = findViewById(R.id.landmark_edittext);
        mObjAreaEditText = findViewById(R.id.autoCompleteTextView1);
        mObjAreaEditTextDisabled = findViewById(R.id.autoCompleteTextView1Disabled);
        mFooterButton = findViewById(R.id.footerButton);
        mFooterButton.setOnClickListener(this);

        findViewById(R.id.common_footer_layout).setOnClickListener(this);

        //Set header
        setHeader();

        setCartQuantity();
        //mObjAreaEditText.setText(areaName);


        mObjFulladdressEditText.requestFocus();

        //Prefill the values
        if (updateAddress) preFill();
        if (!updateAddress) {
            String lat = pref.getLatitudeId();
            String longi = pref.getLongitude();
            if (lat != null && !lat.equals("-1"))
                getGeoAddress(lat, longi);
            else
                Toast.makeText(this, "Please select manually", Toast.LENGTH_SHORT).show();
        }
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetAllCitiesAsyncTask().execute(null, null, null);
            new GetAllAreasAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(VcApplicationContext.getInstance(), "Please Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void getGeoAddress(String lat, String longi) {
        if (lat != null && !lat.equalsIgnoreCase("-1")) {
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(longi), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                //mObjFulladdressEditText.setText(address+"");
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                //String country = addresses.get(0).getCountryName();

                address = address.replaceAll(city + ",", "");
                address = address.replaceAll(state, "");
                address = address.replaceAll(postalCode + ",", "");
                address = address.replaceAll(country, "");
                address = address.replaceAll("  ", "");
                mObjFulladdressEditText.setText(address.trim() + "");
                mObjStateEditText.setText(state);
                latitude = Double.valueOf(lat);
                longitude = Double.valueOf(longi);
                if (flagOfLocation) {
                    flagOfLocation = false;
                    new AddressAddDeleteActivity.GetLocationAsyncTask().execute(null, null, null);
                }
                //mObjAreaEditText.setText();

            } catch (Exception ex) {
                System.out.print(ex.getMessage());
            }
        }
    }

    private void setCartQuantity() {
        ArrayList<ServiceTypeModel> list = pref.getCartData();

        if (list != null) {
            cartQuantityTextView.setText(list.size() + "");
        }
    }

    private void setHeader() {
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        findViewById(R.id.headerFirstButton).setOnClickListener(this);
        mMenuButton.setOnClickListener(this);
        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderImage.setBackgroundResource(R.drawable.header_body);
        mHeaderText.setText(headerTextString);
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;


        oldState = bundle.getString("state");
        oldCity = bundle.getString("city");
        oldArea = bundle.getString("area");
        oldAreaId = bundle.getString("areaId");
        oldLandmark = bundle.getString("landmark");
        oldAddress = bundle.getString("address");
        oldAddressId = bundle.getString("addressId");
        updateAddress = bundle.getBoolean("updateAddress");
        if (updateAddress)
            headerTextString = "Update Address";
        //Setting the params
        state = oldState;
        city = oldCity;
        area = oldArea;
        areaId = oldAreaId;
        landmark = oldLandmark;
        address = oldAddress;
        addressId = oldAddressId;
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            String userId = userModel.getID();

            if (isValidString(userId)) {
                isGuestUser = userModel.getGuest();
            }
        }
    }
    /*  -------------------------------------
        |
        |  UPDATE ADDRESS API CALL
        |
        -------------------------------------
    */

    class updateAddressAsyncTask extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        private String landmark;
        private String address;
        private String userAddressId;

        updateAddressAsyncTask(String landmark, String address, String userAddressId) {
            progressDialog = new ProgressDialog(AddressAddDeleteActivity.this);
            this.landmark = landmark;
            this.address = address;
            this.userAddressId = userAddressId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Updating Address");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... arg0) {
            String apiResult = "";
            try {
                apiResult = mRestWebservices.updateAddress(this.landmark, this.address, this.userAddressId);
            } catch (JSONException e) {
                Log.e(TAG, "<<AddressAddDelete Exception>>" + e.getMessage());
            }

            return apiResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                // Enable Footer button after the stuff is done
                mFooterButton.setEnabled(true);

                if (result.equalsIgnoreCase("true")) {
                    Toast.makeText(AddressAddDeleteActivity.this, "Address updated successfully", Toast.LENGTH_SHORT).show();
                    goBack(true);
                } else {
                    Toast.makeText(AddressAddDeleteActivity.this, "Address could not be updated. Reason :" + result, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, "<<AddressAddDelete Exception>>" + e.getMessage());
            }
        }
    }

    private JSONObject createGuestUserAddressJSON(String addressId, String state, String city, String cityId, String area, String areaId,
                                                  String landmark, String address, String hubId, String country) throws JSONException {
        JSONObject jsonObject = new JSONObject();


        jsonObject.put("user_adddress_id", addressId);
        jsonObject.put("address", address);
        jsonObject.put("landmark", landmark);
        jsonObject.put("AreaName", area);
        jsonObject.put("CityName", city);
        jsonObject.put("StateName", state);
        jsonObject.put("areaId", areaId);
        jsonObject.put("cityId", cityId);
        jsonObject.put("hubId", hubId);
        jsonObject.put("country", country);

        return jsonObject;
    }

    public void addGuestUserAddress(JSONObject jsonObject) {
        ArrayList<UserAddressModel> guestUserAddressesArray = new ArrayList<>();
        UserAddressModel mGuestUserAddressModel = new UserAddressModel(jsonObject);
        guestUserAddressesArray.add(mGuestUserAddressModel);
        json = gson.toJson(guestUserAddressesArray);
        pref.putGuestAddresses(json);
    }

    /*  -------------------------------------
        |
        |  ADD ADDRESS API CALL
        |
        -------------------------------------
    */

    class addAddressAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;

        addAddressAsyncTask() {
            progressDialog = new ProgressDialog(AddressAddDeleteActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Adding Address");
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(Void... arg0) {
            String[] successResult = {"0", "Unknown Error!"};

            try {
                successResult = mRestWebservices.addAddress(stateId, cityId, areaId, landmark, address);
            } catch (JSONException e) {
                Log.e(TAG, "<<AddressAddDelete Exception>>" + e.getMessage());
            }

            return successResult;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                // Enable Footer button after the stuff is done
                mFooterButton.setEnabled(true);

                if (result[0].equalsIgnoreCase("1")) {
                    Toast.makeText(AddressAddDeleteActivity.this, "Address added successfully", Toast.LENGTH_SHORT).show();

                    addressId = result[1];

                    if (isGuestUser) {
                        //Store address in custom model for guest user
                        String country = "India";

                        JSONObject mGuestUserAddressJSON = createGuestUserAddressJSON(addressId, state, city, cityId, area, areaId, landmark,
                                address, hubId, country);

                        addGuestUserAddress(mGuestUserAddressJSON);
                    }
                    if (result[2] != null && !result[2].toString().equals("")) {
                        JSONObject jsonObject = new JSONObject(result[2].toString());
                        String selectedCityId = jsonObject.getString("cityId");
                        String currentSetCityId = pref.getCityId();

                        if (currentSetCityId.equals(selectedCityId)) {
                            setUserAddressParams(jsonObject);
                            gotoBook();
                        } else {
                            Toast.makeText(getApplicationContext(), "Please select an address with same city", Toast.LENGTH_SHORT).show();
                        }

                        //goBack(true);
                    }
                } else {
                    Toast.makeText(AddressAddDeleteActivity.this, "Address could not be added. Reason :" + result[1], Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, "<<AddressAddDelete Exception>>" + e.getMessage());
            }
        }
    }

    private void setUserAddressParams(JSONObject jObj) {
        try {
            String addressId = jObj.getString("user_adddress_id");
            String areaId = jObj.getString("areaId");
            String hubId = jObj.getString("hubId");

            UserAddressModel lUserAddressModel = new UserAddressModel(addressId, areaId, hubId);

            //Check if already set, else add to the list
            if (mUserSetAddresses.size() > 0) {
                mUserSetAddresses.set(0, lUserAddressModel);
            } else {
                mUserSetAddresses.add(lUserAddressModel);
            }

            json = gson.toJson(mUserSetAddresses);
            pref.putUserSetAddresses(json);
        } catch (Exception ex) {
            Log.e("setUserAddressParams", ex.getMessage());
        }
    }

    public void gotoBook() {
        Intent payment = new Intent(AddressAddDeleteActivity.this, Book.class);
        startActivity(payment);
        MultipleAddressActivity.flagOfAddressAdd = true;
        MultipleAddressActivity.flagOfEmptyAddress = false;
        finish();
    }

    public boolean isValidPopulatedString(String string) {
        Boolean result = false;

        if (string != null && string.length() > 0 && (!string.equalsIgnoreCase(""))) {
            result = true;
        }

        return result;
    }

    private void sendAddress() {
        mFooterButton.setEnabled(false);
        landmark = mObjLandmarkEditText.getText().toString();
        address = mObjFulladdressEditText.getText().toString();

        if (updateAddress) {
            //If all params are set, then fire API
            //if (isValidPopulatedString(landmark)) {
            if (isValidPopulatedString(address)) {
                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    new updateAddressAsyncTask(landmark, address, oldAddressId).execute(null, null, null);
                } else {
                    showToast("Check your internet connection");
                    mFooterButton.setEnabled(true);
                }
            } else {
                showToast("Address must not be blank");
                mFooterButton.setEnabled(true);
            }
            /*} else {
                showToast("Landmark must not be blank");
                mFooterButton.setEnabled(true);
            }*/
        } else {
            //If all params are set, then fire API
            if (isValidPopulatedString(state) && isValidPopulatedString(stateId)) {
                if (isValidPopulatedString(city) && isValidPopulatedString(cityId)) {
                    if (isValidPopulatedString(area) && isValidPopulatedString(areaId)) {
                        //if (isValidPopulatedString(landmark)) {
                        if (isValidPopulatedString(address)) {
                            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                new addAddressAsyncTask().execute(null, null, null);
                            } else {
                                showToast("Check your internet connection");
                                mFooterButton.setEnabled(true);
                            }
                        } else {
                            showToast("Address must not be blank");
                            mFooterButton.setEnabled(true);
                        }
                       /* } else {
                            showToast("Landmark must not be blank");
                            mFooterButton.setEnabled(true);
                        }*/
                    } else {
                        showToast("Area must not be blank");
                        mFooterButton.setEnabled(true);
                    }
                } else {
                    showToast("City must be valid");
                    mFooterButton.setEnabled(true);
                }
            } else {
                showToast("State must be valid");
                mFooterButton.setEnabled(true);
            }
        }

    }

    private void showToast(String pToastMessage) {
        Toast.makeText(AddressAddDeleteActivity.this, pToastMessage, Toast.LENGTH_SHORT).show();
    }

    private class GetAllAreasAsyncTask extends AsyncTask<Void, Void, Bundle> {
        ProgressDialog progressDialog;

        GetAllAreasAsyncTask() {
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Getting Area...");
            progressDialog.show();
        }

        @Override
        protected Bundle doInBackground(Void... params) {

            return mRestWebservices.getAllAreasNew();
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);

            if (result != null && result.getBoolean("success", true)) {
                mAreaModelArrayList = result.getParcelableArrayList("areas");
            }

            if (mAreaModelArrayList == null) {
                Toast.makeText(AddressAddDeleteActivity.this, "Areas not available", Toast.LENGTH_SHORT).show();
                return;
            }

            final HashMap<String, AreaModel> lNewAreaNameHash = new HashMap<>();

            for (AreaModel lModel : mAreaModelArrayList) {
                lNewAreaNameHash.put(lModel.getName() + ", " + lModel.getCity(), lModel);
            }

            final String[] areas = lNewAreaNameHash.keySet().toArray(new String[lNewAreaNameHash.size()]);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(AddressAddDeleteActivity.this, android.R.layout.simple_list_item_1, areas);

            mObjAreaEditText.setAdapter(adapter);
            mObjAreaEditText.setThreshold(1);

            mObjAreaEditText.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView lTextView = (TextView) view;

                    if (lTextView != null) {
                        AreaModel lSelectedModel = lNewAreaNameHash.get(lTextView.getText().toString());
                        cityId = lSelectedModel.getCityId();
                        city = lSelectedModel.getCity();
                        hubId = lSelectedModel.getHub_id();
                        area = lSelectedModel.getName();
                        areaId = lSelectedModel.getArea_id();

                        for (CityModel lCityModel : mCityModelArrayList) {
                            if (lCityModel.getCityId().equalsIgnoreCase(cityId)) {
                                state = lCityModel.getStateName();
                                stateId = lCityModel.getStateId();
                                mObjStateEditText.setText(state);
                            }
                        }

                        mObjCityEditText.setText(city);
                        mObjAreaEditText.setText(area);
                    }
                }
            });


            for (int i = 0; i < areas.length; i++) {
                if (areas[i].contains(areaName)) {
                    mObjAreaEditText.setText(areas[i]);
                    AreaModel lSelectedModel = lNewAreaNameHash.get(areas[i]);
                    cityId = lSelectedModel.getCityId();
                    city = lSelectedModel.getCity();
                    hubId = lSelectedModel.getHub_id();
                    area = lSelectedModel.getName();
                    areaId = lSelectedModel.getArea_id();

                    for (CityModel lCityModel : mCityModelArrayList) {
                        if (lCityModel.getCityId().equalsIgnoreCase(cityId)) {
                            state = lCityModel.getStateName();
                            stateId = lCityModel.getStateId();
                            mObjStateEditText.setText(state);
                        }
                    }

                    mObjCityEditText.setText(city);
                    mObjAreaEditText.setText(area);
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                    break;
                }
            }

        }
    }

    private class GetAllCitiesAsyncTask extends AsyncTask<Void, Void, Bundle> {
        @Override
        protected Bundle doInBackground(Void... params) {
            return mRestWebservices.getCityListNew();
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);
            if (result.getBoolean("success", false)) {
                mCityModelArrayList = result.getParcelableArrayList("cities");
            }
        }
    }

    //Pre fill all the text fields in case of update address
    public void preFill() {
        mObjFulladdressEditText.setText(oldAddress);
        mObjLandmarkEditText.setText(oldLandmark);
        mObjAreaEditText.setVisibility(View.VISIBLE);
        //mObjAreaEditTextDisabled.setVisibility(View.VISIBLE);
        disableEditText(mObjAreaEditTextDisabled);
        mObjAreaEditTextDisabled.setText(oldArea);
        areaName = oldArea;
        mObjCityEditText.setText(oldCity);
        mObjStateEditText.setText(oldState);
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
        MultipleAddressActivity.flagOfEmptyAddress = false;
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.common_footer_layout:
            case R.id.footerButton:
                sendAddress();
                break;

            case R.id.headerFirstButton:
            case R.id.headerMenu:
                goBack(false);
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_ADDRESS);
    }

    private class GetLocationAsyncTask extends AsyncTask<Void, Void, Map<String, String>> {
        Map<String, String> resultArrayList;


        GetLocationAsyncTask() {

        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null) {
                progressDialog.setMessage("Getting Location...");
                progressDialog.show();
            }

        }

        protected Map<String, String> doInBackground(Void... url) {
            RestWebServices restWebServices = new RestWebServices();
            return restWebServices.getLocation(latitude, longitude);
        }

        protected void onPostExecute(Map<String, String> result) {
            super.onPostExecute(resultArrayList);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                if (result.size() > 0) {
                    resultArrayList = result;
                    cityId = resultArrayList.get("cityId");
                    city = resultArrayList.get("cityName");
                    hubId = resultArrayList.get("hub_id");
                    area = resultArrayList.get("areaName");
                    areaId = resultArrayList.get("areaId");
                    mObjAreaEditText.setText(resultArrayList.get("areaName"));
                    mObjCityEditText.setText(resultArrayList.get("cityName"));
                    mObjAreaEditTextDisabled.setText(resultArrayList.get("areaName"));

                    //mObjStateEditText.setText(resultArrayList.get(""));

                    /*"areaName": "Maruti Industrial Area",
                            "hub_id": "7",
                            "cityId": "8",
                            "cityName": "Gurgaon",
                            "latitude": "28.48971",
                            "longitude": "77.062282",
                            "distance": 0.10877255733206*/

                    //pref.putCityId(resultArrayList.get("cityId"));
                    ////pref.putCityName(resultArrayList.get("cityName"));
                    //searchCityEditText.setText(resultArrayList.get("cityName"));


                    //pref.putAreaId(resultArrayList.get("areaId"));
                    //pref.putHubId(resultArrayList.get("hub_id"));
                    //pref.putAreaName(resultArrayList.get("areaName"));
                    //pref.clearCartData();
                    //updateLocation(resultArrayList.get("areaId"), resultArrayList.get("hub_id"));
                    //createViewpager();
                } else {
                    Toast.makeText(AddressAddDeleteActivity.this, "Sorry We didn't serve in this area at this moment....", Toast.LENGTH_SHORT).show();
                }

                //new DashboardActivity.GetOffersAsyncTask().execute(null, null, null);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

}