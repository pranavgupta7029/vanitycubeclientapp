package com.vanitycube.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vanitycube.R;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setHeader();
        setFooter();
    }

    private void setHeader() {
        Button mMenuButton = (Button) findViewById(R.id.headerMenu);
        mMenuButton.setVisibility(View.VISIBLE);
        Button mBackButton = (Button) findViewById(R.id.headerArrow);
        mBackButton.setVisibility(View.GONE);
        TextView mHeaderText = (TextView) findViewById(R.id.headerText);
        mHeaderText.setText("SettingsActivity");
        ImageView mHeaderImage = (ImageView) findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
    }

    private void setFooter() {
        TextView mFooterButton = (TextView) findViewById(R.id.footerButton);
        mFooterButton.setVisibility(View.GONE);
        //mFooterButton.setText("Is FOOTER required?");
//    	Button mHelpButton = (Button) findViewById(R.id.footerHelp);
//    	mHelpButton.setVisibility(View.GONE);
//    	Button mFavButton = (Button) findViewById(R.id.footerFavourites);
//    	mFavButton.setVisibility(View.GONE);
//    	Button mCalenderButton = (Button) findViewById(R.id.footerCalender);
//    	mCalenderButton.setVisibility(View.GONE);
//    	Button mUserButton = (Button) findViewById(R.id.footerUser);
//    	mUserButton.setVisibility(View.GONE);
//    	Button mHomeButton = (Button) findViewById(R.id.footerHome);
//    	mHomeButton.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
