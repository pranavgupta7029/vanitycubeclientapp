package com.vanitycube.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.AddNewUserResponseModel;
import com.vanitycube.model.LoginResponseModel;
import com.vanitycube.model.signinup.SendOtpToUserResponse;
import com.vanitycube.model.signinup.SignUpUserResponse;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpActivity extends AppCompatActivity implements OnClickListener, ServerResponseListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks,DatePickerDialog.OnDateSetListener {
    private String TAG;

    private EditText userNameEditText, userEmailEditText, userMobileEditText, userPasswordEditText, userReferralCodeEditText;
    private ImageView mTermsCheck;

    private RestWebServices mRestWebservices;
    private SharedPref pref;
    private Gson gson;

    private SignInButton mGoogleSignUpBtn;
    private RadioButton mRadioMale;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;

    private UserModel userModel;
    private boolean mChecked, dashboardLogin, bookingLogin, memberLogin;

    private static final int RC_GOOGLE_LOG_IN = 501;
    private static final int RC_FACEBOOK_SIGN_UP = 504;
    private static final int RC_GOOGLE_SIGN_UP = 505;
    private static final int RC_OTP = 506;
    private LinearLayout nameLen, emaiLen, passwordLen, referalLen, sexLen, continueLen, signUpLen,anniLen,dobLen;
    private Button continueBtn;
    private RelativeLayout tandcLen;
    private boolean isUserNotExist = false;
    private String userPhone;
    ProgressDialog progressDialog;
    private ApiManager apiManager;
    private static final String SEND_OTP_TO_USER = "sendOTPToUser";
    private static final String NEW_USER_SIGN_UP = "newUseerSignUp";
    private Date dateSet;
    private TextView anniversayDate,birthDate;
    private String anniDate,bDate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_signup_new_design);

        TAG = getResources().getString(R.string.sign_up_page_activity_tag);
        mChecked = true;

        mRestWebservices = new RestWebServices();
        gson = new Gson();
        pref = new SharedPref(VcApplicationContext.getInstance());
        progressDialog = new ProgressDialog(SignUpActivity.this);

        //Get user
        getUser();

        //This is instantiated only once, keep it here only
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        //Check login type
        dashboardLogin = getIntent().getBooleanExtra("dashboard_login", false);
        bookingLogin = getIntent().getBooleanExtra("booking_login", false);
        memberLogin = getIntent().getBooleanExtra("membership_login", false);
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
        isUserNotExist = getIntent().getBooleanExtra("sentOtp", false);
        userPhone = getIntent().getStringExtra("phone");

        //Facebook Sign Up
        /*TextView lFacebook = findViewById(R.id.SignUpFacebook);
        lFacebook.setOnClickListener(this);
        initializeFacebookSdk();

        //Google Sign Up
        mGoogleSignUpBtn = findViewById(R.id.SignUpGoogle);
        setGooglePlusButtonText(mGoogleSignUpBtn);*/

        init();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        if(view.getTag()!=null && view.getTag().equalsIgnoreCase("DatepickerdialogAnni")) {
            String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
            String d = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            anniDate = d;
            anniversayDate.setText(date);
        }else if(view.getTag()!=null && view.getTag().equalsIgnoreCase("DatepickerdialogBirth")) {
            String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
            String d = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            bDate = d;
            birthDate.setText(date);
        }
    }


    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (Objects.equals(TAG, SEND_OTP_TO_USER) && responseObj instanceof SendOtpToUserResponse) {
            SendOtpToUserResponse sendOtpToUserResponse = (SendOtpToUserResponse) responseObj;

            if (sendOtpToUserResponse.getResponsedata().getSuccess() == 1) {

                Toast.makeText(SignUpActivity.this, "User Exist! Please Log in.", Toast.LENGTH_SHORT).show();
                Intent signin_activity = new Intent(SignUpActivity.this, SignInPageActivity.class);
                if (dashboardLogin) signin_activity.putExtra("dashboard_login", true);
                else if (bookingLogin) signin_activity.putExtra("booking_login", true);
                else if (memberLogin) signin_activity.putExtra("membership_login", true);
                signin_activity.putExtra("sentOtp", true);
                signin_activity.putExtra("phone", userMobileEditText.getText().toString());
                startActivity(signin_activity);
                finish();
            } else {
                openVisiBility();
            }

        } else if (Objects.equals(TAG, NEW_USER_SIGN_UP) && responseObj instanceof SignUpUserResponse) {
            SignUpUserResponse signUpUserResponse = (SignUpUserResponse) responseObj;

            if (signUpUserResponse.getResponsedata().getSuccess() == 1) {

                    logCart(userNameEditText.getText().toString(), userEmailEditText.getText().toString(),userMobileEditText.getText().toString());
                    Intent int_next = new Intent(SignUpActivity.this, OtpActivity.class);

                    if (dashboardLogin) {
                        int_next.putExtra("dashboard_login", true);
                    } else if (bookingLogin) {
                        int_next.putExtra("booking_login", true);
                    }

                    UserModel userModel = new UserModel();
                    userModel.setID(signUpUserResponse.getResponsedata().getUserId());
                    userModel.setName(userNameEditText.getText().toString());
                    userModel.setEmail(userEmailEditText.getText().toString());
                    userModel.setNumber(userMobileEditText.getText().toString());
                    userModel.setProfileImagePath("");
                    userModel.setReferralName(userReferralCodeEditText.getText().toString());
                    userModel.setGuest(false);
                    userModel.setNumberVerified(false);
                    userModel.setDOB(birthDate.getText().toString());

                    int_next.putExtra("user_model", userModel);
                    startActivityForResult(int_next, RC_OTP);
                } else {
                        Toast.makeText(SignUpActivity.this, signUpUserResponse.getResponsedata().getMsg(), Toast.LENGTH_LONG).show();
                }


        }else {
            Toast.makeText(SignUpActivity.this, "Something went wrong! Please try again...", Toast.LENGTH_SHORT).show();
        }


        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void positiveResponse(String TAG, String response) {
        String popints = "";

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {

        if (SignUpActivity.this.isDestroyed() || SignUpActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
            return;
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    private void sendOtpToUser(String pContact) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(SignUpActivity.this, SignUpActivity.this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SendOtpToUserResponse.class);

        HashMap<String, String> params = new HashMap<>();
        //params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SENDOTP_TO_USER);

        params.put("otp", pContact);

        apiManager.getStringPostResponse(SEND_OTP_TO_USER, ApplicationSettings.BASE_URL_PAY + RestWebServices.METHOD_SENDOTP_TO_USER, params);
    }

    private void signUpUser() {
        if(progressDialog!=null) {
            progressDialog.setMessage("Processing...");
            if(!progressDialog.isShowing())
                progressDialog.show();
        }

        apiManager = new ApiManager(SignUpActivity.this, SignUpActivity.this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SignUpUserResponse.class);

        HashMap<String, String> params = new HashMap<>();
        //params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SENDOTP_TO_USER);

        params.put("name", userNameEditText.getText().toString());
        params.put("email", userEmailEditText.getText().toString());
        params.put("contact", userMobileEditText.getText().toString());
        params.put("password", userPasswordEditText.getText().toString());
        params.put("refferal", userReferralCodeEditText.getText().toString());
        if(anniDate!=null)
            params.put("doa", anniDate);
        else
            params.put("doa", "");
        if(bDate!=null)
            params.put("dob", bDate);
        else
            params.put("dob", "");
        apiManager.getStringPostResponse(NEW_USER_SIGN_UP, ApplicationSettings.BASE_URL_PAY + RestWebServices.METHOD_SIGNUP_NEW_USER, params);
    }



    private void init() {
        userNameEditText = findViewById(R.id.userNameEditText);
        userEmailEditText = findViewById(R.id.userEmailEditText);
        userPasswordEditText = findViewById(R.id.userPasswordEditText);
        userMobileEditText = findViewById(R.id.userContactEditText);
        userReferralCodeEditText = findViewById(R.id.userReferralCodeEditText);
        LinearLayout mTermsCheckLayout = findViewById(R.id.check_terms_conditions_layout);
        mTermsCheck = findViewById(R.id.check_terms_conditions);
        TextView mTerms = findViewById(R.id.terms_link);
        Button mSignUpButton = findViewById(R.id.signup_button_signup);
        TextView mLoginButton = findViewById(R.id.loginHereTextView);
        mTermsCheckLayout.setOnClickListener(this);
        mTermsCheck.setOnClickListener(this);
        mTerms.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
        anniLen = findViewById(R.id.anniLen);
        dobLen = findViewById(R.id.dobLen);
        anniversayDate = findViewById(R.id.dateAnniversary);
        birthDate = findViewById(R.id.dateBirth);

        mRadioMale = findViewById(R.id.radioMale);
        nameLen = findViewById(R.id.nameLen);
        emaiLen = findViewById(R.id.emaiLen);
        passwordLen = findViewById(R.id.passwordLen);
        referalLen = findViewById(R.id.referalLen);
        sexLen = findViewById(R.id.sexLen);
        tandcLen = findViewById(R.id.tandcLen);
        continueLen = findViewById(R.id.continueLen);
        signUpLen = findViewById(R.id.signUpLen);
        continueBtn = findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPhone();
            }
        });

        if (isUserNotExist) {
            userMobileEditText.setText(userPhone);
            openVisiBility();
        }

        anniversayDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SignUpActivity.this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
                long currentTimeInMillis = System.currentTimeMillis();
                Calendar timeout = Calendar.getInstance();
                timeout.setTimeInMillis(currentTimeInMillis);
                dpd.setMaxDate(timeout);
                dpd.setAccentColor(Color.parseColor("#f36f21"));
                dpd.setTitle("Select Date");
                dpd.show(getFragmentManager(), "DatepickerdialogAnni");

            }
        });

        birthDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SignUpActivity.this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
                long currentTimeInMillis = System.currentTimeMillis();
                Calendar timeout = Calendar.getInstance();
                timeout.setTimeInMillis(currentTimeInMillis);
                dpd.setMaxDate(timeout);
                dpd.setAccentColor(Color.parseColor("#f36f21"));
                dpd.setTitle("Select Date");
                dpd.show(getFragmentManager(), "DatepickerdialogBirth");
            }
        });

        //Google Sign In
        /*mGoogleSignUpBtn = findViewById(R.id.SignUpGoogle);
        mGoogleSignUpBtn.setOnClickListener(this);*/
    }

    private void openVisiBility() {
        emaiLen.setVisibility(View.VISIBLE);
        nameLen.setVisibility(View.VISIBLE);
        passwordLen.setVisibility(View.VISIBLE);
        referalLen.setVisibility(View.VISIBLE);
        sexLen.setVisibility(View.VISIBLE);
        tandcLen.setVisibility(View.VISIBLE);
        continueLen.setVisibility(View.VISIBLE);
        signUpLen.setVisibility(View.VISIBLE);
        continueLen.setVisibility(View.GONE);
        anniLen.setVisibility(View.VISIBLE);
        dobLen.setVisibility(View.VISIBLE);
    }

    private void checkPhone() {
        if (userMobileEditText.getText() != null && !userMobileEditText.getText().toString().equals("") && phoneValidator(userMobileEditText.getText().toString())) {
            sendOtpToUser(userMobileEditText.getText().toString());
        } else {
            Toast.makeText(SignUpActivity.this, "Please enter a valid mobile number", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            String userId = userModel.getID();

            if (isValidString(userId)) {
                boolean isGuestUser = userModel.getGuest();
            }
        }
    }

    /*  -------------------------------------
        |
        |   FACEBOOK SIGN IN
        |
        -------------------------------------
    */

    public void initializeFacebookSdk() {
        FacebookSdk.sdkInitialize(VcApplicationContext.getInstance());
        callbackManager = CallbackManager.Factory.create();
    }


    public void doFacebookLogin() {
        try {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));

            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Bundle b = new Bundle();
                    b.putString("fields", "id,first_name,email,gender,last_name,birthday");

                    new GraphRequest(AccessToken.getCurrentAccessToken(), "/me", b, HttpMethod.GET,
                            new GraphRequest.Callback() {
                                public void onCompleted(GraphResponse response) {
                                    JSONObject responseJson = response.getJSONObject();

                                    if (responseJson != null) {
                                        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                            String email = responseJson.optString("email");
                                            new LoginAsyncTask(email, "", true, responseJson).execute(null, null, null);
                                        } else {
                                            Toast.makeText(SignUpActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                    ).executeAsync();
                }

                @Override
                public void onCancel() {
                    Log.e(TAG, "Login attempt canceled.");
                    Log.i(TAG, "onCancel: onCompleted");
                }

                @Override
                public void onError(FacebookException e) {
                    Log.i(TAG, "onError: onCompleted");
                    Log.e(TAG, "Login failed :: Reason :: " + e.getMessage());
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*  -------------------------------------
        |
        |   GOOGLE SIGN IN
        |
        -------------------------------------
    */

    public void doGoogleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        this.startActivityForResult(signInIntent, RC_GOOGLE_LOG_IN);
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended:");
    }

    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();

            String personName;
            String givenName;

            if (acct != null) {
                givenName = acct.getGivenName();
                String email = acct.getEmail();
                personName = acct.getDisplayName();
                String personID = acct.getId();

                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    new LoginAsyncTask(personName, givenName, email, "", personID, true);
                } else {
                    Toast.makeText(SignUpActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not be available.
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), 1);
        dialog.show();
    }

    private void setGooglePlusButtonText(SignInButton signInButton) {
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setTextSize(15);
                tv.setTypeface(null, Typeface.NORMAL);
                tv.setText(R.string.login_with_google_text);
                return;
            }
        }
    }

    /*  -------------------------------------
        |
        |   LOGIN ASYNC TASK
        |
        -------------------------------------
    */

    private class LoginAsyncTask extends AsyncTask<Void, Void, LoginResponseModel> {
        private ProgressDialog progressDialog;
        private String userId, userName, password, email, personName, personId, givenName, responseMessage, googleId, facebookId;
        private boolean isLogin, isGoogle, isFacebook;
        private JSONObject responseJson;

        LoginAsyncTask(String pUserName, String pPassword, boolean isFacebook, JSONObject responseJson) {
            this.userName = pUserName;
            this.password = pPassword;
            this.isFacebook = isFacebook;
            this.responseJson = responseJson;

            if (this.responseJson != null) {
                facebookId = this.responseJson.optString("id");
            } else {
                facebookId = "";
            }

            progressDialog = new ProgressDialog(SignUpActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        LoginAsyncTask(String personName, String givenName, String email, String password, String personId, boolean isGoogle) {
            this.personName = personName;
            this.givenName = givenName;
            this.email = email;
            this.password = password;
            this.isGoogle = isGoogle;
            this.personId = personId;

            googleId = this.personId;

            progressDialog = new ProgressDialog(SignUpActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Signing In");
            progressDialog.show();
        }

        @Override
        protected LoginResponseModel doInBackground(Void... url) {
            if (isGoogle) {
                return mRestWebservices.loginGoogle(this.email, password);
            } else {
                return mRestWebservices.login(userName, password, isFacebook);
            }
        }

        @Override
        protected void onPostExecute(LoginResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                String messageCode = result.getMessageCode();
                responseMessage = result.getMessage();
                JSONObject data = result.getData();

                if (messageCode.equals("1") && data != null && data.length() != 0) {
                    UserModel userModel = new UserModel(data);

                    userModel.setFacebookId(facebookId);
                    userModel.setGoogleId(googleId);

                    pref.putUserModel(gson.toJson(userModel));
                    pref.putReloadRequired(true);
                    pref.putGuestAddresses("");

                    Map<String, Object> afEventValue = new HashMap<>();
                    afEventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
                    AppsFlyerLib.trackEvent(VcApplicationContext.getInstance(), AFInAppEventType.LOGIN, afEventValue);
                    new FireBaseHelper(SignUpActivity.this).logLogin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

                    goBack(true);
                } else {
                    if (isFacebook) {
                        Intent signUpFacebook = new Intent(SignUpActivity.this, SignUpFacebookActivity.class);
                        signUpFacebook.putExtra("id", responseJson.optString("id"));
                        signUpFacebook.putExtra("first_name", responseJson.optString("first_name"));
                        signUpFacebook.putExtra("email", responseJson.optString("email"));
                        signUpFacebook.putExtra("gender", responseJson.optString("gender"));
                        signUpFacebook.putExtra("last_name", responseJson.optString("last_name"));
                        signUpFacebook.putExtra("birthday", responseJson.optString("birthday"));
                        startActivityForResult(signUpFacebook, RC_FACEBOOK_SIGN_UP);

                    } else if (isGoogle) {
                        Intent signUpGoogle = new Intent(SignUpActivity.this, SignUpGoogleActivity.class);
                        signUpGoogle.putExtra("id", googleId);
                        signUpGoogle.putExtra("personName", this.personName);
                        signUpGoogle.putExtra("email", this.email);
                        startActivityForResult(signUpGoogle, RC_GOOGLE_SIGN_UP);
                    } else {
                        if (isValidString(responseMessage)) {
                            Toast.makeText(SignUpActivity.this, responseMessage, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignUpActivity.this, "Login failed, please try again", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |   SIGNUP ASYNC TASK
        |
        -------------------------------------
    */

    private void logCart(String givenName, String email, String phoneNumber) {
        AppEventsLogger logger;
        logger = AppEventsLogger.newLogger(SignUpActivity.this);
        Bundle parameters = new Bundle();
        parameters.putString("USER_NAME", givenName);
        parameters.putString("USER_EMAIL", email);
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        parameters.putString("Date", date);
        parameters.putString("USER_PHONE", phoneNumber);
        logger.logEvent("EVENT_USER_REGISTRATION", parameters);

        parameters = new Bundle();

        parameters.putString("user_name", givenName);
        parameters.putString("user_phone", email);
        parameters.putString("user_email", phoneNumber);
        logGoogleEvents(FirebaseAnalytics.Event.SIGN_UP, parameters);

    }

    public void logGoogleEvents(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(this);
            firebaseHelper.logEvents(eventName, bundle);
        } catch (Exception ex) {
            Log.e("PackageSelected", ex.getMessage());
        }
    }


    class SignUpAsyncTask extends AsyncTask<Void, Void, AddNewUserResponseModel> {
        ProgressDialog progressDialog;
        String name, email, password, mobileNumber, referralCode, gender;

        SignUpAsyncTask(String pName, String pUserEmail, String pPassword, String userMobileEditText, String userReferralCodeEditText) {
            this.name = pName;
            this.email = pUserEmail;
            this.password = pPassword;
            this.mobileNumber = userMobileEditText;
            this.referralCode = userReferralCodeEditText;

            if (mRadioMale.isChecked()) {
                gender = "1";
            } else {
                gender = "2";
            }

            progressDialog = new ProgressDialog(SignUpActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Sign Up");
            progressDialog.show();
        }

        @Override
        protected AddNewUserResponseModel doInBackground(Void... url) {
            AddNewUserResponseModel addNewUserResponseModel = new AddNewUserResponseModel();

            try {
                addNewUserResponseModel = mRestWebservices.addNewUser(this.name, this.email, this.password, this.mobileNumber, this.referralCode, false, gender);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }

            return addNewUserResponseModel;
        }

        @Override
        protected void onPostExecute(AddNewUserResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                int success = result.getSuccess();
                String userId = result.getUserId();
                String responseMessage = result.getMessage();

                if (success == 1) {
                    logCart(name, email, mobileNumber);
                    Intent int_next = new Intent(SignUpActivity.this, OtpActivity.class);

                    if (dashboardLogin) {
                        int_next.putExtra("dashboard_login", true);
                    } else if (bookingLogin) {
                        int_next.putExtra("booking_login", true);
                    }

                    if(memberLogin){
                        int_next.putExtra("membership_login", true);
                    }

                    UserModel userModel = new UserModel();
                    userModel.setID(userId);
                    userModel.setName(name);
                    userModel.setEmail(email);
                    userModel.setNumber(mobileNumber);
                    userModel.setProfileImagePath("");
                    userModel.setReferralName(referralCode);
                    userModel.setGuest(false);
                    userModel.setNumberVerified(false);

                    int_next.putExtra("user_model", userModel);
                    startActivityForResult(int_next, RC_OTP);
                } else {
                    if (isValidString(responseMessage)) {
                        Toast.makeText(SignUpActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(SignUpActivity.this, "An error occurred, please try again", Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private boolean emailValidator(String pEmail) {
        String regex = "^(.+)@(.+)\\.(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pEmail);
        return matcher.matches();
    }

    private boolean phoneValidator(String pMobile) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        Pattern pattern = Pattern.compile(regexStr);
        Matcher matcher2 = pattern.matcher(pMobile);
        return matcher2.matches();
    }

    private void signUp() {
        String userNameEditTextString = userNameEditText.getText().toString().trim();
        String userEmailEditTextString = userEmailEditText.getText().toString().trim();
        String userPasswordEditTextString = userPasswordEditText.getText().toString().trim();
        String userMobileEditTextString = userMobileEditText.getText().toString().trim();
        String userReferralCodeEditTextString = userReferralCodeEditText.getText().toString().trim();

        if (!emailValidator(userEmailEditTextString)) {
            Toast.makeText(SignUpActivity.this, "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!phoneValidator(userMobileEditTextString)) {
            Toast.makeText(SignUpActivity.this, "Please enter a valid mobile number.", Toast.LENGTH_SHORT).show();
            return;
        }

        if ((userNameEditTextString == null || userNameEditTextString.equalsIgnoreCase("") || userNameEditTextString.length() == 0)
                && (userPasswordEditTextString != null || userPasswordEditTextString.equalsIgnoreCase("") || userPasswordEditTextString.length() == 0)
                && (userMobileEditTextString != null || userMobileEditTextString.equalsIgnoreCase("") || userMobileEditTextString.length() == 0)) {

            Toast.makeText(SignUpActivity.this, "Please fill in all the fields.", Toast.LENGTH_LONG).show();
        }

        if (!mChecked) {
            Toast.makeText(SignUpActivity.this, "Please Agree with Terms and Conditions.", Toast.LENGTH_LONG).show();
            return;
        }

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            signUpUser();
           // new SignUpAsyncTask(userNameEditTextString, userEmailEditTextString, userPasswordEditTextString, userMobileEditTextString, userReferralCodeEditTextString).execute(null, null, null);
        } else {
            Toast.makeText(SignUpActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();
        intBack.putExtra("dashboard_login", dashboardLogin);
        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    private void gotoTerms() {
        Intent intent = new Intent(SignUpActivity.this, TermsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("content_type", 0);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_GOOGLE_LOG_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }

        if (requestCode == RC_FACEBOOK_SIGN_UP || requestCode == RC_GOOGLE_SIGN_UP || requestCode == RC_OTP) {
            if (resultCode == RESULT_OK) {
                goBack(true);
            }
        }
    }

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {

            case R.id.loginHereTextView:
                Intent loginActivity = new Intent(getApplicationContext(), SignInPageActivity.class);
                startActivity(loginActivity);
                finish();
                break;

            case R.id.signup_button_signup:
                signUp();
                break;

            case R.id.check_terms_conditions_layout:
            case R.id.check_terms_conditions:
                mChecked = !mChecked;

                if (mChecked) {
                    mTermsCheck.setBackgroundResource(R.drawable.check_box_with);
                } else {
                    mTermsCheck.setBackgroundResource(R.drawable.check_box_without);
                }
                break;

            case R.id.terms_link:
                gotoTerms();
                break;

            default:
                break;

        }
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }
}
