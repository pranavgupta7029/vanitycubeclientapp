package com.vanitycube.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.model.BookingListModel;
import com.vanitycube.settings.ApplicationSettings;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class ActiveBookingActivity extends Activity implements OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.active_booking);

        Bundle bundle = this.getIntent().getExtras();

        BookingListModel bookingListModel = bundle.getParcelable("booking_list_model");

        String bookingId = bookingListModel.getBookingID();
        String bookingServiceTypeNames = bookingListModel.getServiceTypeNames();
        String bookingTime = bookingListModel.getBooktime();
        String bookingDate = bookingListModel.getDate();
        String bookingAmount = bookingListModel.getAmount();
        //String bookingStatus = bookingListModel.getStatus();
        String bookingStatus = bookingListModel.getBookingStatus();

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        int intAmount = Integer.valueOf(bookingAmount);

        TextView bookingIdTextView = findViewById(R.id.bookingIdTextView);
        TextView bookingDateTextView = findViewById(R.id.bookingDateTextView);
        TextView bookingTimeTextView = findViewById(R.id.bookingTimeTextView);
        TextView bookingAmountTextView = findViewById(R.id.bookingAmountTextView);
        TextView bookingStatusTextView = findViewById(R.id.bookingStatusTextView);
        TextView bookingServiceTypeNamesTextView = findViewById(R.id.bookingServiceTypeNamesTextView);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat slotDateFormat = new SimpleDateFormat("dd MMM-yyyy");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat givenFormat = new SimpleDateFormat("yyyy-MM-dd");

        java.util.Date lDate = null;

        try {
            lDate = givenFormat.parse(bookingDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (lDate != null) {
            bookingDate = slotDateFormat.format(lDate);
        }


        bookingIdTextView.setText(ApplicationSettings.BOOKING_ID_PREFIX.concat(String.valueOf(bookingId)));
        bookingDateTextView.setText(String.valueOf(bookingDate));
        bookingTimeTextView.setText(String.valueOf(bookingTime));
        bookingAmountTextView.setText(formatter.format(intAmount));

        /*if(bookingStatus.equals("1")) {
            bookingStatus = "Confirmed";
        } else {
            bookingStatus = "Cancelled";
        }*/

        bookingStatusTextView.setText(bookingStatus);
        bookingServiceTypeNamesTextView.setText(String.valueOf(bookingServiceTypeNames));

        setHeader();
    }

    private void setHeader() {
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);

        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        findViewById(R.id.cartLayout).setVisibility(View.GONE);

        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText(getResources().getString(R.string.booking_detail_text));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /*private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }*/

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerFirstButton:
            case R.id.headerMenu:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
