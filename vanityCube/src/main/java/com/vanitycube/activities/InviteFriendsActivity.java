package com.vanitycube.activities;

//Created by prade on 12/20/2017.

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.fonts.VCEditText;
import com.vanitycube.fonts.VCTextView;
import com.vanitycube.model.ConfigurationModel;

public class InviteFriendsActivity extends Activity implements View.OnClickListener {
    private Context context;
    private static final int REQUEST_CONTACT = 0, PICK_CONTACT = 1;
    private SharedPref pref;
    private Gson gson;
    private String inviteMessage;
    private String inviteePhone;
    private VCEditText chooseContactEditText, inviteMessageEditText;
    public VCTextView headerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_invite_friends);

        context = InviteFriendsActivity.this;

        gson = new Gson();
        pref = new SharedPref(VcApplicationContext.getInstance());

        View customHeaderLayout = findViewById(R.id.header);
        headerText = customHeaderLayout.findViewById(R.id.headerText);
        headerText.setVisibility(View.VISIBLE);
        headerText.setText(getResources().getString(R.string.activity_invite_friends_name));
        customHeaderLayout.findViewById(R.id.cartLayout).setVisibility(View.GONE);

        LinearLayout headerMenuButton = customHeaderLayout.findViewById(R.id.headerFirstButton);
        headerMenuButton.setVisibility(View.GONE);

        chooseContactEditText = findViewById(R.id.chooseContactEditText);
        inviteMessageEditText = findViewById(R.id.inviteMessageEditText);

        VCTextView submitButton = findViewById(R.id.sendInviteButton);

        //Get configuration data
        getConfiguration();

        chooseContactEditText.setOnClickListener(this);
        submitButton.setOnClickListener(this);
    }

    private void sendInvite() {
        inviteMessage = inviteMessageEditText.getText().toString();

        if (!inviteMessage.equals("")) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(inviteePhone, null, inviteMessage, null, null);
                Toast.makeText(getApplicationContext(), "Invite Sent!", Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Invite failed, please try again later!", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(VcApplicationContext.getInstance(), "Invite message cannot be empty", Toast.LENGTH_SHORT).show();

        }
    }

    private void getConfiguration() {
        String json = pref.getConfiguration();
        ConfigurationModel configurationModel = gson.fromJson(json, new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            inviteMessage = configurationModel.getInviteMessage();
            inviteMessageEditText.setText(inviteMessage);
        }
    }

    private void contactPicked(Intent data) {
        Cursor cursor;

        try {
            Uri uri = data.getData();
            cursor = getContentResolver().query(uri, null, null, null, null);

            if (cursor != null) {
                cursor.moveToFirst();
                int phoneIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                //int  nameIndex =cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                inviteePhone = cursor.getString(phoneIndex);
                //String inviteeName = cursor.getString(nameIndex);
                chooseContactEditText.setText(inviteePhone);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pickContact(View v) {
        final Activity activity = InviteFriendsActivity.this;

        if ((Build.VERSION.SDK_INT >= 23)) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_CONTACTS}, REQUEST_CONTACT);
            } else {
                firePickContactIntent();
            }
        } else {
            firePickContactIntent();
        }

    }

    private void firePickContactIntent() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, PICK_CONTACT);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICK_CONTACT:
                if (resultCode == RESULT_OK) {
                    contactPicked(data);
                }

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CONTACT:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    firePickContactIntent();
                } else {
                    Toast.makeText(context, "Operation cancelled", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.chooseContactEditText:
                pickContact(v);
                break;

            case R.id.sendInviteButton:
                sendInvite();
                break;

            default:
                break;
        }
    }
}
