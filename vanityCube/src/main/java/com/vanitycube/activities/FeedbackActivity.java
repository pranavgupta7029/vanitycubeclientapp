
package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class FeedbackActivity extends Activity implements OnClickListener {
    Button mObjRatingButton;
    RatingBar mObjAppRatingBar;
    private Button mMenuButton;
    private TextView mHeaderText;
    private ImageView mHeaderImage;
    float mRatingValue = 5.0f;
    String booking_id = "";
    String[] bookingIdTimeDate;
    private RestWebServices mRestWebservices;
    private EditText feedbackText;
    private TextView mFeedbackDate, mFeedbackTIme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.feedback);
        mRestWebservices = new RestWebServices();
        //booking_id = getIntent().getStringExtra("booking_id");
        bookingIdTimeDate = getIntent().getStringArrayExtra("booking_id");
        mFeedbackDate = (TextView) findViewById(R.id.feedbackDate);
        mFeedbackTIme = (TextView) findViewById(R.id.feedbackTime);
        String lDateString = bookingIdTimeDate[2];
        SimpleDateFormat slotDateFormat = new SimpleDateFormat("dd MMM-yyyy");
        SimpleDateFormat givenFormat = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date lDate = null;
        try {
            lDate = givenFormat.parse(lDateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (lDate != null) {
            lDateString = slotDateFormat.format(lDate);
        }

        mFeedbackDate.setText(lDateString);
        mFeedbackTIme.setText(bookingIdTimeDate[1]);
        setHeader();
        TextView footer = (TextView) findViewById(R.id.footerButton);
        footer.setOnClickListener(this);
        feedbackText = (EditText) findViewById(R.id.feedbackText);
        mObjAppRatingBar = (RatingBar) findViewById(R.id.app_rating_bar);
        mObjAppRatingBar.setRating(5);
        mObjAppRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                // TODO Auto-generated method stub
                mRatingValue = rating;
                int lrateValue = (int) mRatingValue;
                String ratingText = lrateValue + "/" + 5;

            }
        });
    }

    private void setHeader() {
        // TODO Auto-generated method stub
        mMenuButton = (Button) findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setVisibility(View.INVISIBLE);
        mMenuButton.setOnClickListener(this);
        mMenuButton.setOnClickListener(this);
        mHeaderText = (TextView) findViewById(R.id.headerText);
        mHeaderText.setText("Feedback");
        mHeaderImage = (ImageView) findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderImage.setBackgroundResource(R.drawable.header_body);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerMenu:
                //finish();
                break;
            case R.id.footerButton:
                submitFeedback();
                break;
            default:
                break;
        }
        // TODO Auto-generated method stub
        // switch (v.getId()) {
        // case R.id.submit_rating_Button:
        // // new PostRateAsyncTask().execute(null, null, null);
        // break;
        // case R.id.back_button_imageview:
        // finish();
        // break;
        // default:
        // break;
        // }
    }

    private void submitFeedback() {
        String text = feedbackText.getText().toString();
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new SubmitFeedbackAsyncTask(bookingIdTimeDate[0], String.valueOf(mRatingValue), text).execute(null, null, null);
        } else {
            Toast.makeText(FeedbackActivity.this,
                    "Check your internet connection", Toast.LENGTH_LONG)
                    .show();
        }
    }

    class SubmitFeedbackAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        boolean isSubmitted = false;
        String bookingID = "";
        String bookingId, rating, text;

        public SubmitFeedbackAsyncTask(String bookingId, String rating, String text) {
            this.bookingId = bookingId;
            this.rating = rating;
            this.text = text;
            progressDialog = new ProgressDialog(FeedbackActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Submitting feedback");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            // TODO Ashish : add appropriate logic here
            boolean isDone = false;
            try {
                isSubmitted = mRestWebservices.submitFeedback(bookingId, rating, text);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return isSubmitted;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            if (result) {
                finish();
            } else {
                Toast.makeText(FeedbackActivity.this, "We are facing some problem submitting your review. Please try later",
                        Toast.LENGTH_LONG).show();
                finish();
                // mRestWebservices.showAlertDialog(OtpActivity.this, "OTP not sent",
                // "Please try again later...",
                // false);
            }
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        Toast.makeText(FeedbackActivity.this, "Please submit the feedback to move forward!",
                Toast.LENGTH_SHORT).show();
    }
}
