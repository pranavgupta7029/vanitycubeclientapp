package com.vanitycube.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.BookingSummaryDetailModel;
import com.vanitycube.model.CashBackBookingModel;
import com.vanitycube.model.CategoryModel;
import com.vanitycube.model.ConfirmOrderModel;
import com.vanitycube.model.CouponModel;
import com.vanitycube.model.CouponModelBooking;
import com.vanitycube.model.LoyaltyOrderBookingModel;
import com.vanitycube.model.MultipleBookingResponseModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.model.ServiceTypeModelTrimmed;
import com.vanitycube.model.StandardResponseModelBooking;
import com.vanitycube.model.UserAddressModel;
import com.vanitycube.model.loyalty.AbvailablePointsResponse;
import com.vanitycube.model.loyalty.BlockLoyaltyPoints;
import com.vanitycube.model.loyalty.ReedemableLoyaltyPointsResponse;
import com.vanitycube.model.loyalty.ReleaseLoyaltyPointsResponse;
import com.vanitycube.model.loyalty.ResendOtpLoyalty;
import com.vanitycube.model.loyalty.SubmitOtpLoyalty;
import com.vanitycube.model.paytm.PaytmChecksumResponse;
import com.vanitycube.model.paytm.PaytmTransactionStatusResponse;
import com.vanitycube.payment.PaymentModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.ConfirmDialogHelper;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BookingSummaryDetailActivity extends Activity implements OnClickListener, ServerResponseListener, PaymentResultListener {
    private String TAG;
    private static final String BOOKING_SUMMARY_DETAIL_PAGE = "booking_summary_detail_page";
    private static final String GET_ABVAILABLE_LOYALTY_POINTS = "easyRewardzGetAvailablePointsAction";
    private static final String GET_ABVAILABLE_CASHBACK_POINTS = "getAvailablePoitsCashBack";
    private static final String GET_ABVAILABLE_REEDEMEBALE_LOYALTY_POINTS = "getEasyRewardzPointsDetails";
    private static final String BLOCK_LOYALTY_POINTS = "easyRewardzCheckRedemptionPointsAction";
    private static final String RESEND_LOYALTY_POINTS = "easyRewardzResendOtpAction";
    private static final String SUBMIT_LOYALTY_POINTS = "easyRewardzResendOtpAction";
    private static final String RELEASE_LOYALTY_POINTS = "easyRewardzReleaseRedemptionPointsAction";
    private static final String LOYALTY_REWARD_TYPE = "loyalty";
    private static final String CASHBACK_REWARD_TYPE = "cashback";


    Handler handler = new Handler();
    int delay = 1000;
    private Long timeStamp;
    private int timer;
    private Runnable runnable;
    private boolean isBookingAllowed;
    private LinearLayout roundedTimerLayout;
    private TextView roundedTimerTextView;
    private AlertDialog.Builder builder;
    private AlertDialog dialog1;

    private LinearLayout cashPaymentLayout, onlinePaymentLayout, discountLayout, convienceFeeLayout, couponDiscountLayout, gstLayout;
    private LinearLayout loyaltyPointLen, loyaltyOtpLen, redeemLen, loyaltyDiscountLayout, cashBackPointLen, redeemCashBackLen, cashBackOtpLen, cashBackDiscountLayout;
    private CheckBox useLoyaltyCb, useCashBackCb;
    private EditText couponEditText;
    private ImageView mcashimage, monlineimage;
    private EditText noteEditText;
    private HashMap<String, String> paramMap;
    private Calendar startTime;
    private AlertDialog dialog;
    private ApiManager apiManager;
    private RestWebServices mRestWebservices;
    private SharedPref pref;
    private Gson gson;
    private UserModel userModel;
    private String checkYourInternet, userName, userPhone, userEmail;
    private Boolean isGuestUser, couponAlreadyApplied, isConvenienceFee;
    private String json, userId, orderId;
    private ArrayList<UserAddressModel> userSetAddresses;
    private ArrayList<CategoryModel> categoriesWithServiceTypesList;
    private ArrayList<String> serviceTypeIdsList;
    private ArrayList<BookingSummaryDetailModel> bookingSummaryDetailList;
    private String firstBookingDiscount = "0";
    private String referralDiscount = "0";
    private String note;
    private CouponModel couponModel;
    private ArrayList<ServiceTypeModel> finalSelectedServiceTypes;
    private double initialAmount, convenienceFeeAmount, finalAmount;
    private Double totalDiscount;
    private ConfirmOrderModel confirmOrderModel;
    private ArrayList<String> bookingIds;
    private int paymentModeCode = 1;
    private String paymentMode;
    private EditText loyaltyPointsEditText, otpLoyaltyEditText, cashBackPointsEditText, otpCashBackEditText;
    private int payselect = 1;
    ProgressDialog progressDialog;
    private Integer redeemableLoyaltyPoints = 0;
    private Integer isOtpConfirmedLoyalty = 0, isOtpConfirmedCashBack = 0;
    private Integer maxLoyaltyPoints = 0, maxCashBachPoints = 0;
    private TextView couponApplyTextView, loyaltyDiscountTextView, cartQuantityTextView, gstTextView, memPriceDisText;
    private String loyaltyDiscount = "0", cashBackDiscount = "0";
    private String REWARD_TYPE = null;
    private RelativeLayout cuoponRelView, addMemberRel, membershipDiscountText;
    private String loyaltyTransactionCode = null, cashBackTransactionCode = null;
    private Double GSTAmount = 0.0;
    private String totalamountEligbleForLoyalty;
    private String totalamountEligbleForCashback;
    private RelativeLayout cuonAppliedRel;
    private TextView appliedCouponTextview, percentage;
    private ImageView removeCouponImg;

    private RestWebServices restWebServices;
    private Boolean isToWebView = false;


    private TextView FooterButton, amountTextView, discountTextView, convienceFeeTextView, couponAmountTextView, couponConditionTextView,
            couponErrorTextView, finalAmountTextView, tCash, tOnline;

    private TextView loyaltyTextUse, editPoints, submitOtpButton, resendOTPButton, redeemButton, cashBackTextUse, redeemCashBackButton, editCashBackPoints,
            submitOtpCashBackButton, resendOTPCashBackButton, cashBackDiscountTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_booking_summary_detail);

        TAG = getResources().getString(R.string.booking_summary_detail_activity_tag);

        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebservices = new RestWebServices();
        gson = new Gson();
        checkYourInternet = getResources().getString(R.string.check_your_internet_text);


        userSetAddresses = new ArrayList<>();
        categoriesWithServiceTypesList = new ArrayList<>();
        serviceTypeIdsList = new ArrayList<>();
        bookingSummaryDetailList = new ArrayList<>();
        bookingIds = new ArrayList<>();
        finalSelectedServiceTypes = new ArrayList<>();

        couponModel = new CouponModel();

        initialAmount = 0.0;
        finalAmount = 0.0;
        totalDiscount = 0.0;

        couponAlreadyApplied = false;

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_BOOKING_SUMMARY);
        userModel = new UserModel();

        //Amount layouts
        discountLayout = findViewById(R.id.discountLayout);
        discountLayout.setVisibility(View.GONE);
        convienceFeeLayout = findViewById(R.id.convienceFeeLayout);
        couponDiscountLayout = findViewById(R.id.couponDiscountLayout);
        restWebServices = new RestWebServices();

        //Amount views
        amountTextView = findViewById(R.id.amountTextView);
        discountTextView = findViewById(R.id.discountTextView);
        convienceFeeTextView = findViewById(R.id.convenienceFeeTextView);
        couponEditText = findViewById(R.id.couponEditText);
        couponAmountTextView = findViewById(R.id.couponAmountTextView);
        couponConditionTextView = findViewById(R.id.couponConditionTextView);
        couponErrorTextView = findViewById(R.id.couponErrorTextView);
        finalAmountTextView = findViewById(R.id.finalAmountTextView);
        percentage = findViewById(R.id.percentage);

        //Payment mode views
        mcashimage = findViewById(R.id.cashimage);
        monlineimage = findViewById(R.id.onlineImage);
        mcashimage.setBackgroundResource(R.drawable.cash);
        monlineimage.setBackgroundResource(R.drawable.card);
        cashPaymentLayout = findViewById(R.id.cashlayout);
        onlinePaymentLayout = findViewById(R.id.onlineLayout);
        tCash = findViewById(R.id.cashtext);
        tOnline = findViewById(R.id.onlineText);
        noteEditText = findViewById(R.id.bookingSummaryNote);

        //Payment mode listeners
        tCash.setOnClickListener(this);
        tOnline.setOnClickListener(this);
        cashPaymentLayout.setOnClickListener(this);
        onlinePaymentLayout.setOnClickListener(this);
        findViewById(R.id.couponApplyTextView).setOnClickListener(this);
        gstLayout = (LinearLayout) findViewById(R.id.gstLayout);
        gstTextView = (TextView) findViewById(R.id.gstTextView);
        cuonAppliedRel = (RelativeLayout) findViewById(R.id.cuonAppliedRel);
        appliedCouponTextview = (TextView) findViewById(R.id.appliedCouponTextview);
        removeCouponImg = (ImageView) findViewById(R.id.removeCouponImg);
        removeCouponImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                removeCoupon();
                appliedCouponTextview.setVisibility(View.GONE);
            }
        });

        membershipDiscountText = findViewById(R.id.membershipDiscountText);

        memPriceDisText = findViewById(R.id.memPriceDisText);

        addMemberRel = (RelativeLayout) findViewById(R.id.addMemberRel);
        if (pref.getIsUserMember()) {
            addMemberRel.setVisibility(View.GONE);
            membershipDiscountText.setVisibility(View.VISIBLE);
            if (pref.getMemberDiscount() != null && !pref.getMemberDiscount().equals(""))
                memPriceDisText.setText("Your Total V Club saving is " + getString(R.string.Rs) + " " + pref.getMemberDiscount());
        } else {
            membershipDiscountText.setVisibility(View.GONE);
            addMemberRel.setVisibility(View.VISIBLE);
            if (pref.getMemberDiscount() != null && !pref.getMemberDiscount().equals("")) {
                percentage.setText(pref.getMemberDiscount());
            }
            addMemberRel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {
                        isToWebView = true;
                        Intent i = new Intent(BookingSummaryDetailActivity.this, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        startActivity(i);

                    }
                }
            });

        }


        //Get data
        getUser();
        getFinalSelectedServiceTypes();
        setHeader();
        setFooter();
        getConvenienceFee();
        getCategoriesList();
        getOfferCoupon();


        if (!isGuestUser) {
            getDiscounts();
        }

        loyaltyTextUse = (TextView) findViewById(R.id.loyaltyTextUse);
        cashBackTextUse = (TextView) findViewById(R.id.cashBackTextUse);
        editPoints = (TextView) findViewById(R.id.editPoints);
        editCashBackPoints = (TextView) findViewById(R.id.editCashBackPoints);
        submitOtpButton = (TextView) findViewById(R.id.submitOtpButton);
        submitOtpCashBackButton = (TextView) findViewById(R.id.submitOtpCashBackButton);

        resendOTPButton = (TextView) findViewById(R.id.resendOTPButton);
        resendOTPCashBackButton = (TextView) findViewById(R.id.resendOTPCashBackButton);

        loyaltyOtpLen = (LinearLayout) findViewById(R.id.loyaltyOtpLen);
        cashBackOtpLen = (LinearLayout) findViewById(R.id.cashBackOtpLen);

        loyaltyPointLen = (LinearLayout) findViewById(R.id.loyaltyPointLen);

        cashBackPointLen = (LinearLayout) findViewById(R.id.cashBackPointLen);
        redeemLen = (LinearLayout) findViewById(R.id.redeemLen);
        redeemCashBackLen = (LinearLayout) findViewById(R.id.redeemCashBackLen);
        redeemButton = (TextView) findViewById(R.id.redeemButton);
        redeemCashBackButton = (TextView) findViewById(R.id.redeemCashBackButton);
        progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        cuoponRelView = (RelativeLayout) findViewById(R.id.cuoponRelView);

        couponApplyTextView = ((TextView) findViewById(R.id.couponApplyTextView));
        couponEditText = ((EditText) findViewById(R.id.couponEditText));
        loyaltyDiscountLayout = (LinearLayout) findViewById(R.id.loyaltyDiscountLayout);
        loyaltyDiscountTextView = (TextView) findViewById(R.id.loyaltyDiscountTextView);


        cashBackDiscountTextView = (TextView) findViewById(R.id.cashBackDiscountTextView);
        cashBackDiscountLayout = (LinearLayout) findViewById(R.id.cashBackDiscountLayout);


        loyaltyPointsEditText = (EditText) findViewById(R.id.loyaltyPointsEditText);
        cashBackPointsEditText = (EditText) findViewById(R.id.cashBackPointsEditText);
        otpLoyaltyEditText = (EditText) findViewById(R.id.otpLoyaltyEditText);
        otpCashBackEditText = (EditText) findViewById(R.id.otpCashBackEditText);

        useLoyaltyCb = (CheckBox) findViewById(R.id.useLoyaltyCb);
        useCashBackCb = (CheckBox) findViewById(R.id.useCashBackCb);
        buildMultipleBookingModelList();
        Checkout.preload(getApplicationContext());
        useLoyaltyCb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (useLoyaltyCb.isChecked()) {
                    REWARD_TYPE = LOYALTY_REWARD_TYPE;

                    getAvailableReedemeblePoints(userModel.getID(), LOYALTY_REWARD_TYPE);

                } else {

                    if (isOtpConfirmedLoyalty != null && isOtpConfirmedLoyalty == 1) {
                        releaseLoyaltyPoints(userModel.getID());
                    }
                    loyaltyPointLen.setVisibility(View.GONE);
                    redeemLen.setVisibility(View.GONE);
                    loyaltyOtpLen.setVisibility(View.GONE);
                    if (Integer.valueOf(loyaltyDiscount) > 0) {
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        String discount = "0";
                        if (discountTextView.getText() != null)
                            discount = discountTextView.getText().toString().replaceAll(",", "");
                        double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
                        totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(discount) + Double.valueOf(loyaltyDiscount);
                        double total = serviceAmount + Double.valueOf(loyaltyDiscount);
                        if (total < 0) {
                            total = 0;
                        }
                        //finalAmountTextView.setText(formatter.format(total));
                        loyaltyDiscountLayout.setVisibility(View.GONE);
                        loyaltyDiscountTextView.setText("0");
                        loyaltyDiscount = "0";
                        isOtpConfirmedLoyalty = 0;
                        setGstForloyalty();
                    }
                }
            }
        });
        useCashBackCb.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (useCashBackCb.isChecked()) {
                    REWARD_TYPE = CASHBACK_REWARD_TYPE;
                    getAvailableReedemeblePoints(userModel.getID(), CASHBACK_REWARD_TYPE);
                    //cuoponRelView.setVisibility(View.GONE);
                    makeCouponNotAvailable();


                } else {
                    if (isOtpConfirmedCashBack != null && isOtpConfirmedCashBack == 1) {
                        releaseLoyaltyPoints(userModel.getID());
                    }
                    makeCouponAvailable();
                    cuoponRelView.setVisibility(View.VISIBLE);
                    cashBackPointLen.setVisibility(View.GONE);
                    redeemCashBackLen.setVisibility(View.GONE);
                    cashBackOtpLen.setVisibility(View.GONE);
                    if (Integer.valueOf(cashBackDiscount) > 0) {
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        String discount = "0";
                        if (discountTextView.getText() != null)
                            discount = discountTextView.getText().toString().replaceAll(",", "");
                        double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
                        totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(discount) + Double.valueOf(loyaltyDiscount);
                        double total = serviceAmount + Double.valueOf(cashBackDiscount);
                        if (total < 0) {
                            total = 0;
                        }
                        //finalAmountTextView.setText(formatter.format(total));
                        /*int serviceAmount = Integer.valueOf(totalText.getText().toString().replace(",", ""));
                         *//*totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount)+Integer.valueOf(coupon_amount)+Integer.valueOf(loyaltyDiscount);*//*
                        int total = serviceAmount + Integer.valueOf(cashBackDiscount);
                        if(total<0){
                            total = 0;
                        }
                        totalText.setText(formatter.format(total));*/
                        cashBackDiscountLayout.setVisibility(View.GONE);
                        cashBackDiscountTextView.setText("0");
                        cashBackDiscount = "0";
                        isOtpConfirmedCashBack = 0;
                        setGstForCashBack();
                    }
                }
            }
        });


        editPoints.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                editPoints.setVisibility(View.GONE);
                loyaltyPointsEditText.setEnabled(true);
                loyaltyPointsEditText.setBackgroundColor(Color.WHITE);
                redeemButton.setEnabled(true);
                redeemButton.setClickable(true);
                REWARD_TYPE = LOYALTY_REWARD_TYPE;
                releaseLoyaltyPoints(userModel.getID());
                loyaltyOtpLen.setVisibility(View.GONE);
                redeemButton.setBackground(getDrawable(R.drawable.vc_color_list));

                submitOtpButton.setEnabled(true);
                submitOtpButton.setBackground(getDrawable(R.drawable.vc_color_list));
                resendOTPButton.setEnabled(true);
                resendOTPButton.setBackground(getDrawable(R.drawable.vc_color_list));
                otpLoyaltyEditText.setEnabled(true);
                otpLoyaltyEditText.setBackgroundColor(Color.WHITE);
                otpLoyaltyEditText.setText("");
                isOtpConfirmedLoyalty = 0;
                if (Integer.valueOf(loyaltyDiscount) > 0) {
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String discount = "0";
                    if (discountTextView.getText() != null)
                        discount = discountTextView.getText().toString().replaceAll(",", "");
                    double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
                    totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(discount) + Double.valueOf(loyaltyDiscount);
                    double total = serviceAmount + Double.valueOf(loyaltyDiscount);
                    if (total < 0) {
                        total = 0;
                    }
                    //finalAmountTextView.setText(formatter.format(total));
                    /*int serviceAmount = Integer.valueOf(totalText.getText().toString().replace(",", ""));
                     *//*totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount)+Integer.valueOf(coupon_amount)+Integer.valueOf(loyaltyDiscount);*//*
                    int total = serviceAmount + Integer.valueOf(loyaltyDiscount);
                    if(total<0){
                        total = 0;
                    }
                    totalText.setText(formatter.format(total));*/
                    loyaltyDiscountLayout.setVisibility(View.GONE);
                    loyaltyDiscountTextView.setText("0");
                    loyaltyDiscount = "0";
                    isOtpConfirmedLoyalty = 0;
                    setGst();
                }
                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });

        editCashBackPoints.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                editCashBackPoints.setVisibility(View.GONE);
                cashBackPointsEditText.setEnabled(true);
                cashBackPointsEditText.setBackgroundColor(Color.WHITE);
                redeemCashBackButton.setEnabled(true);
                redeemCashBackButton.setClickable(true);
                REWARD_TYPE = CASHBACK_REWARD_TYPE;
                releaseLoyaltyPoints(userModel.getID());
                cashBackOtpLen.setVisibility(View.GONE);
                redeemCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));

                submitOtpCashBackButton.setEnabled(true);
                submitOtpCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));
                resendOTPCashBackButton.setEnabled(true);
                resendOTPCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));
                otpCashBackEditText.setEnabled(true);
                otpCashBackEditText.setBackgroundColor(Color.WHITE);
                otpCashBackEditText.setText("");
                isOtpConfirmedCashBack = 0;
                if (Integer.valueOf(cashBackDiscount) > 0) {
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    String discount = "0";
                    if (discountTextView.getText() != null)
                        discount = discountTextView.getText().toString().replaceAll(",", "");
                    double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
                    totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(discount) + Double.valueOf(loyaltyDiscount);
                    double total = serviceAmount + Double.valueOf(cashBackDiscount);
                    if (total < 0) {
                        total = 0;
                    }
                    //finalAmountTextView.setText(formatter.format(total));
                    /*int serviceAmount = Integer.valueOf(totalText.getText().toString().replace(",", ""));
                     *//*totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount)+Integer.valueOf(coupon_amount)+Integer.valueOf(loyaltyDiscount);*//*
                    int total = serviceAmount + Integer.valueOf(cashBackDiscount);
                    if(total<0){
                        total = 0;
                    }
                    totalText.setText(formatter.format(total));*/
                    cashBackDiscountLayout.setVisibility(View.GONE);
                    cashBackDiscountTextView.setText("0");
                    cashBackDiscount = "0";
                    isOtpConfirmedCashBack = 0;
                    setGst();
                }
                //cashBackOtpLen.setVisibility(View.VISIBLE);
            }
        });


        redeemButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loyaltyPointsEditText.getText() != null && !loyaltyPointsEditText.getText().toString().equalsIgnoreCase("")) {
                    REWARD_TYPE = LOYALTY_REWARD_TYPE;
                    blockLoyaltyPoints(userModel.getID());

                } else {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Please Enter number of loyalty points, you want to redeem", Toast.LENGTH_SHORT).show();
                }
                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });

        redeemCashBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (cashBackPointsEditText.getText() != null && !cashBackPointsEditText.getText().toString().equalsIgnoreCase("")) {
                    REWARD_TYPE = CASHBACK_REWARD_TYPE;
                    blockLoyaltyPoints(userModel.getID());

                } else {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Please Enter number of loyalty points, you want to redeem", Toast.LENGTH_SHORT).show();
                }
                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });

        submitOtpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpLoyaltyEditText.getText() != null && !otpLoyaltyEditText.getText().toString().equals("")) {
                    submitOtpButton.setEnabled(false);
                    submitOtpButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    resendOTPButton.setEnabled(false);
                    resendOTPButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    otpLoyaltyEditText.setEnabled(false);
                    otpLoyaltyEditText.setBackgroundColor(Color.TRANSPARENT);
                    REWARD_TYPE = LOYALTY_REWARD_TYPE;
                    submitLoyaltyOTP(userModel.getID());

                } else {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                }
                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });

        submitOtpCashBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (otpCashBackEditText.getText() != null && !otpCashBackEditText.getText().toString().equals("")) {
                    submitOtpCashBackButton.setEnabled(false);
                    submitOtpCashBackButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    resendOTPCashBackButton.setEnabled(false);
                    resendOTPCashBackButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    otpCashBackEditText.setEnabled(false);
                    otpCashBackEditText.setBackgroundColor(Color.TRANSPARENT);
                    REWARD_TYPE = CASHBACK_REWARD_TYPE;
                    submitLoyaltyOTP(userModel.getID());

                } else {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                }
                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });


        resendOTPButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                REWARD_TYPE = LOYALTY_REWARD_TYPE;
                resendOtpLoyalty(userModel.getID());

                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });

        resendOTPCashBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                REWARD_TYPE = CASHBACK_REWARD_TYPE;
                resendOtpLoyalty(userModel.getID());

                //loyaltyOtpLen.setVisibility(View.VISIBLE);
            }
        });


        //By default select online payment mode
        onlinePaymentLayout.performClick();
        getAvailablePointsLoyalty(userModel.getID(), LOYALTY_REWARD_TYPE);
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getConvenienceFee() {
        isConvenienceFee = pref.getIsConvenienceFee();
        if (isConvenienceFee) {
            convenienceFeeAmount = Double.parseDouble(pref.getConvenienceFee());
        }
    }

    private void getCategoriesList() {
        ArrayList<CategoryModel> tempList;
        json = pref.getCategoriesWithServiceTypes();
        tempList = gson.fromJson(json, new TypeToken<ArrayList<CategoryModel>>() {
        }.getType());

        if (tempList != null) {
            categoriesWithServiceTypesList = new ArrayList<>(tempList);
        }
    }

    private void getOfferCoupon() {
        try {
            json = pref.getCouponModel();
            CouponModel tempCouponModel = gson.fromJson(json, new TypeToken<CouponModel>() {
            }.getType());

            if (tempCouponModel != null) {
                couponModel = tempCouponModel;
                String couponId = couponModel.getCouponId();
                String couponCode = couponModel.getCouponCode();
                String couponAmount = couponModel.getCouponAmount();

                if (!isValidString(couponId) && isValidString(couponCode) && !isValidString(couponAmount)) {
                    couponEditText.setText(couponCode);
                    checkCoupon(couponCode);
                } else {
                    showCouponAmount(couponAmount);
                }

                couponAlreadyApplied = true;
            }
        } catch (Exception e) {
            Log.e(TAG, "<<DashboardFragment Exception>>" + e.getMessage());
        }
    }

    private void showCouponAmount(String couponAmount) {
        if (isValidString(couponAmount)) {
            couponDiscountLayout.setVisibility(View.VISIBLE);
            couponAmountTextView.setText(couponAmount);
        }
    }

    private void removeCouponAmount(String couponAmount) {
        if (isValidString(couponAmount)) {
            couponDiscountLayout.setVisibility(View.GONE);
            couponAmountTextView.setText(couponAmount);
        }
    }

    private void getFinalSelectedServiceTypes() {
        finalSelectedServiceTypes = pref.getCartData();

        if (finalSelectedServiceTypes == null) {
            finalSelectedServiceTypes = new ArrayList<>();
        }
        //getLoyalityPointsJsonRequest(userModel.getID());
    }

    private void getUser() {
        json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            userId = userModel.getID();
            pref.putUserId(userId);

            if (isValidString(userId)) {
                isGuestUser = userModel.getGuest();
                userName = userModel.getName();
                userPhone = userModel.getNumber();
                userEmail = userModel.getEmail();
            }
        }
    }

    private void getDiscounts() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new getDiscountsAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private void setFooter() {
        FooterButton = findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText(getString(R.string.pay_text));
        LinearLayout footerLayout = findViewById(R.id.common_footer_layout);
        footerLayout.setOnClickListener(this);
    }

    private void setCartQuantity() {
        ArrayList<ServiceTypeModel> list = pref.getCartData();

        if (list != null) {
            cartQuantityTextView.setText(list.size() + "");
        }
    }

    private void setHeader() {
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);
        findViewById(R.id.headerFirstButton).setOnClickListener(this);
        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText(getString(R.string.booking_summary_text));
        cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);
        setCartQuantity();
    }

    public void buildMultipleBookingModelList() {
        ArrayList<UserAddressModel> tempUserAddressModelList;
        ArrayList<ServiceTypeModel> tempServiceModelList;

        //Get list of addresses chosen by the user
        json = pref.getUserSetAddresses();
        tempUserAddressModelList = gson.fromJson(json, new TypeToken<ArrayList<UserAddressModel>>() {
        }.getType());

        if (tempUserAddressModelList != null) {
            userSetAddresses = new ArrayList<>(tempUserAddressModelList);
        }

        String addressId = userSetAddresses.get(0).getAddressId();
        String areaId = userSetAddresses.get(0).getAreaId();
        int isGuestUserInteger = 0;

        for (CategoryModel cm : categoriesWithServiceTypesList) {
            ArrayList<ServiceTypeModelTrimmed> services = new ArrayList<>();
            ArrayList<ServiceTypeModel> serviceTypeModelsList;
            serviceTypeModelsList = cm.getServiceTypes();

            for (ServiceTypeModel sm : serviceTypeModelsList) {
                serviceTypeIdsList.add(sm.getServiceTypeID());
                if (pref.getIsUserMember()) {
                    initialAmount += (Double.parseDouble(sm.getMembersPrice()) * Double.parseDouble(sm.getNumOfPeople()));
                } else {
                    initialAmount += (Double.parseDouble(sm.getFinalAmount()) * Double.parseDouble(sm.getNumOfPeople()));
                }
            }

            String bookingDate = cm.getCategoryDate();
            String bookingTimeId = cm.getCategoryTimeId();
            String categoryId = cm.getCategoryId();
            String hubId = pref.getHubId();
            tempServiceModelList = cm.getServiceTypes();
            int serviceTime = cm.getTotalServiceTime();

            if (isGuestUser) {
                isGuestUserInteger = 1;
            }

            //Construct trimmed service type model
            for (ServiceTypeModel stm : tempServiceModelList) {
                ServiceTypeModelTrimmed stmt = new ServiceTypeModelTrimmed(stm.getServiceTypeID(), Integer.parseInt(stm.getNumOfPeople()));
                services.add(stmt);
            }
            Integer convFee = 0;
            if (isConvenienceFee)
                convFee = 1;
            else
                convFee = 0;

            CouponModelBooking couponModelBooking = new CouponModelBooking();

            //if(couponModel!=null){
            if (couponModel.getCouponAmount() != null)
                couponModelBooking.setCouponAmount(Double.parseDouble(couponModel.getCouponAmount()));
            else
                couponModelBooking.setCouponAmount(null);
            couponModelBooking.setCouponCode(couponModel.getCouponCode());
            couponModelBooking.setCouponId(couponModel.getCouponId());
            //}

            Integer is_member = 0;
            if (pref.getIsUserMember()) {
                is_member = 1;
            } else {
                is_member = 0;
            }

            BookingSummaryDetailModel lBookingSummaryDetailModel = new BookingSummaryDetailModel(addressId, areaId, bookingDate, bookingTimeId,
                    categoryId, convFee, couponModelBooking, 0, hubId, note, 0, services, "App", paymentModeCode,
                    serviceTime, userId, isGuestUserInteger, null, null, is_member);

            bookingSummaryDetailList.add(lBookingSummaryDetailModel);
        }

        String couponAmount = couponModel.getCouponAmount();

        //Set initial amount
        if (isValidString(couponAmount)) {
            finalAmount = initialAmount - Double.parseDouble(couponAmount);
        } else {
            finalAmount = initialAmount;
        }

        if (couponAmount != null && !couponAmount.equals("")) {
            if (Double.parseDouble(couponAmount) >= 0.0) {
                makeCashBackNotAvailable();
                cuonAppliedRel.setVisibility(View.VISIBLE);
                appliedCouponTextview.setText(couponModel.getCouponCode());
            }
        }

        //setAmounts();
        setGSTAmount(initialAmount);
    }

    public void setAmounts() {
        //amountTextView.setText(String.valueOf(initialAmount));
        finalAmountTextView.setText(String.valueOf(finalAmount));
        setGst();
    }

    private void setGSTAmount(double amount) {
        double amt = amount / 1.18;
        amountTextView.setText("" + new DecimalFormat("##.##").format(amt));
        double gst = amt * 18 / 100;
        gstTextView.setText(new DecimalFormat("##.##").format(gst) + "");
        gstLayout.setVisibility(View.VISIBLE);
        finalAmountTextView.setText(String.valueOf(finalAmount));
        //return null;
    }

    public void updatePayInCategories() {
        for (BookingSummaryDetailModel bsdm : bookingSummaryDetailList) {
            bsdm.setPaymentMode(paymentModeCode);
            bsdm.setNote(note);
        }
    }

    /*  -------------------------------------
        |
        |  ONLINE PAYMENT DIALOG BOX
        |
        -------------------------------------
    */

    public void onlinePayDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingSummaryDetailActivity.this);
        //final String[] clients = new String[]{"Credit/Debit Card", "Mobikwik Wallet", "Paytm"};
        final String[] clients = new String[]{"Credit/Debit Card", "Net Banking", "Paytm"};

        builder.setSingleChoiceItems(clients, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (!isBookingAllowed) {
                    dialog.dismiss();
                    Toast.makeText(BookingSummaryDetailActivity.this, "Please select the time slot again to continue.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    switch (which) {
                        //Credit or Debit Card
                        case 0:
                            paymentModeCode = 4;

                            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                new AddBookingAsyncTask(paymentModeCode, paymentMode).execute(null, null, null);
                            } else {
                                Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
                            }

                            break;

                        //Mobikwik Wallet
                        case 1:
                            paymentModeCode = 2;

                            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                new AddBookingAsyncTask(paymentModeCode, paymentMode).execute(null, null, null);
                            } else {
                                Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
                            }

                            break;

                        //Paytm
                        case 2:
                            paymentModeCode = 3;

                            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                new AddBookingAsyncTask(paymentModeCode, paymentMode).execute(null, null, null);
                            } else {
                                Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
                            }

                            break;
                    }
                }
            }
        });

        builder.setTitle("Choose a payment method");
        dialog = builder.create();
        dialog.show();
    }

    /*  -------------------------------------
        |
        |  GET DISCOUNTS ASYNC TASK
        |
        -------------------------------------
    */

    public void setDiscountInCategories() {
        for (BookingSummaryDetailModel bsdm : bookingSummaryDetailList) {
            bsdm.setFirstBooking(Integer.parseInt(firstBookingDiscount));
            bsdm.setReferralDiscount(Integer.parseInt(referralDiscount));
        }
    }

    private class getDiscountsAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;

        getDiscountsAsyncTask() {
            progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Getting Discounts");
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(Void... url) {
            String[] discounts;
            discounts = mRestWebservices.getUserDiscountDetails();
            return discounts;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if (result.length != 0) {
                    firstBookingDiscount = result[0];
                    referralDiscount = result[1];

                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    totalDiscount += (Double.parseDouble(firstBookingDiscount) + Double.parseDouble(referralDiscount));
                    double amount = Double.parseDouble(finalAmountTextView.getText().toString().replaceAll(",", ""));

                    if (totalDiscount >= amount) {
                        totalDiscount = amount;
                    }

                    double total = amount - totalDiscount;

                    // Setting discount text
                    if (totalDiscount > 0) {
                        discountLayout.setVisibility(View.VISIBLE);
                        discountTextView.setText(formatter.format(totalDiscount));
                    }

                    if (isConvenienceFee) {
                        convienceFeeLayout.setVisibility(View.VISIBLE);
                        convienceFeeTextView.setText(String.valueOf(convenienceFeeAmount));
                        total += convenienceFeeAmount;
                    } else {
                        convienceFeeLayout.setVisibility(View.GONE);
                    }

                    finalAmountTextView.setText(formatter.format(total));
                    String lAmountString = formatter.format(Integer.parseInt(amountTextView.getText().toString().replaceAll(",", "")));
                    amountTextView.setText(lAmountString);
                    setDiscountInCategories();
                } else {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Oops, no discounts applicable on this order!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |   CHECK COUPONS ASYNC TASK
        |
        -------------------------------------
    */

    private void checkCoupon(String coupon) {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new checkCouponsAsyncTask(coupon).execute(null, null, null);
        } else {
            Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private void setCouponModelInCategories(CouponModel couponModel) {
        for (BookingSummaryDetailModel iterator : bookingSummaryDetailList) {

            CouponModelBooking couponModelBooking = new CouponModelBooking();
            if (couponModel != null) {
                couponModelBooking.setCouponAmount(Double.parseDouble(couponModel.getCouponAmount()));
                couponModelBooking.setCouponCode(couponModel.getCouponCode());
                couponModelBooking.setCouponId(couponModel.getCouponId());
            }
            iterator.setCoupon(couponModelBooking);

        }
    }

    private void clearCoupon() {
        pref.putCouponModel("");
        couponAlreadyApplied = false;
    }

    private class checkCouponsAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String couponId, couponCode, couponAmount;
        CouponModel couponModel;
        ProgressDialog progressDialog;

        checkCouponsAsyncTask(String coupon) {
            couponCode = coupon;
            this.couponModel = new CouponModel();
            progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Verifying Coupon");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            couponModel = mRestWebservices.checkCoupon(finalSelectedServiceTypes, couponCode);
            return (couponModel != null);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if (result) {
                    couponId = couponModel.getCouponId();
                    couponCode = couponModel.getCouponCode();
                    couponAmount = couponModel.getCouponAmount();


                    if (isValidString(couponId) && isValidString(couponCode) && isValidString(couponAmount)) {
                        json = gson.toJson(couponModel);
                        pref.putCouponModel(json);

                        setCouponModelInCategories(couponModel);
                        showCouponAmount(couponAmount);
                        cuonAppliedRel.setVisibility(View.VISIBLE);
                        appliedCouponTextview.setVisibility(View.VISIBLE);
                        appliedCouponTextview.setText(couponModel.getCouponCode() + "");

                        if (couponAlreadyApplied) {
                            couponAlreadyApplied = false;
                        } else {
                            Toast.makeText(BookingSummaryDetailActivity.this, "Coupon applied", Toast.LENGTH_LONG).show();
                        }
                        calculateFinalAmount(couponAmount, couponModel.getCouponCode(), true);
                    } else {
                        couponConditionTextView.setVisibility(View.GONE);
                        couponErrorTextView.setVisibility(View.VISIBLE);
                        couponErrorTextView.setText(couponAmount);
                        clearCoupon();
                        calculateFinalAmount(couponAmount, couponModel.getCouponCode(), false);
                    }


                } else {

                    if (couponAlreadyApplied) {
                        couponAlreadyApplied = false;
                    } else {
                        Toast.makeText(BookingSummaryDetailActivity.this, "Sorry! coupon not available", Toast.LENGTH_LONG).show();
                    }

                    //Remove coupon
                    pref.putCouponModel("");
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private void removeCoupon() {
        /*couponConditionTextView.setVisibility(View.GONE);
        //couponErrorTextView.setVisibility(View.VISIBLE);
        couponErrorTextView.setText("0.0");*/

        removeCouponAmount("0.0");
        /*if(couponModel!=null)
            totalDiscount = totalDiscount - Double.parseDouble(couponModel.getCouponAmount());*/
        clearCoupon();
        calculateFinalAmount("0.0", "", false);
        makeCashBackAvailable();
        couponEditText.setText("");
        cuonAppliedRel.setVisibility(View.GONE);


    }

    private void logCartCoupons(String code, String amount) {
        try {
            AppEventsLogger logger;
            logger = AppEventsLogger.newLogger(BookingSummaryDetailActivity.this);
            Bundle parameters = new Bundle();
            parameters.putString("COUPON_AMOUNT", amount);
            parameters.putString("COUPON_CODE", code);
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            parameters.putString("Date", date);
            logger.logEvent("EVENT_COUPON_APPLIED", parameters);

            parameters = new Bundle();

            parameters.putString("coupon_amount", amount);
            parameters.putString("coupon_code", code);
            logGoogleEventsCoupon(FirebaseAnalytics.Event.PRESENT_OFFER, parameters);
        } catch (Exception e) {

        }
    }

    public void logGoogleEventsCoupon(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(this);
            firebaseHelper.logEvents(eventName, bundle);
        } catch (Exception ex) {
            Log.e("PackageSelected", ex.getMessage());
        }
    }

    /*  -------------------------------------
        |
        |  CALCULATE FINAL AMOUNT
        |
        -------------------------------------
    */

    private void calculateFinalAmount(String couponAmount, String code, boolean flag) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        //String couponAmount = couponModel.getCouponAmount();
        if (!isValidString(couponAmount)) {
            couponAmount = "0";
        } else {
            if (flag)
                logCartCoupons(code, couponAmount);
        }
        totalDiscount += Double.parseDouble(firstBookingDiscount) + Double.parseDouble(referralDiscount) - Double.parseDouble(couponAmount) - Double.parseDouble(loyaltyDiscount);
        double serviceAmount = Double.parseDouble(amountTextView.getText().toString().replace(",", ""));
        if (totalDiscount >= serviceAmount) {
            totalDiscount = serviceAmount;
        }
        double total = serviceAmount - totalDiscount;
        total = total - Double.parseDouble(loyaltyDiscount) - Double.parseDouble(cashBackDiscount);

        if (isConvenienceFee) {
            String convenienceFee = pref.getConvenienceFee();
            convienceFeeLayout.setVisibility(View.VISIBLE);
            convienceFeeTextView.setText(convenienceFee);
            total += Integer.parseInt(convenienceFee);
        } else {
            convienceFeeLayout.setVisibility(View.GONE);
        }

        /*discountLayout.setVisibility(View.VISIBLE);
        discountTextView.setText(formatter.format(totalDiscount));*/
        finalAmount = total;
        if (Double.parseDouble(couponAmount) > 0) {
            makeCashBackNotAvailable();
        } else {
            makeCashBackAvailable();
        }
        //setAmounts();
        setGstForCoupon();
    }

    /*  -------------------------------------
        |
        |   ADD BOOKING ASYNC TASK
        |
        -------------------------------------
    */

    private void addPaymentModeToBookingList(int mode) {
        for (BookingSummaryDetailModel bsdm : bookingSummaryDetailList) {
            bsdm.setPaymentMode(mode);
        }
    }


    private void logCart(ArrayList<BookingSummaryDetailModel> bsdm, String success, String bookingId, String orderId) {

        try {
            Gson gson = new GsonBuilder().create();
            JsonArray jsonArray = gson.toJsonTree(bsdm).getAsJsonArray();
            String data = jsonArray.toString();
            AppEventsLogger logger;
            logger = AppEventsLogger.newLogger(BookingSummaryDetailActivity.this);
            Bundle parameters = new Bundle();
            Bundle parameters_google = new Bundle();
            parameters.putString("BOOKING_DATA", data);
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            parameters.putString("Date", date);
            if (success.equalsIgnoreCase("true")) {
                parameters.putString("BOOKING_STATUS", "CONFIRMED");
                parameters.putString("BOOKING_IDS", bookingId);
                parameters.putString("ORDER_ID", orderId);
                parameters_google.putString("booking_status", "confirmed");
                parameters_google.putString("BOOKING_IDS", bookingId);
                parameters_google.putString("ORDER_ID", orderId);
                logger.logEvent("EVENT_BOOKING_SUCCESS", parameters);
            } else {
                parameters_google.putString("booking_status", "failed");
                parameters.putString("BOOKING_STATUS", "FAILED");
                logger.logEvent("EVENT_BOOKING_FAILED", parameters);
            }
            logGoogleEventsBooking(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, parameters_google);
        } catch (Exception e) {

        }
    }

    public void logGoogleEventsBooking(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(this);
            firebaseHelper.logEvents(eventName, bundle);
        } catch (Exception ex) {
            Log.e("Booking", ex.getMessage());
        }
    }

    private class AddBookingAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private MultipleBookingResponseModel multipleBookingResponseModel;
        private int paymentModeCode;
        private String paymentMode, responseMessage;
        private ProgressDialog progressDialog;

        AddBookingAsyncTask(int paymentModeCode, String paymentMode) {
            this.paymentModeCode = paymentModeCode;
            this.paymentMode = paymentMode;
            multipleBookingResponseModel = new MultipleBookingResponseModel();
            progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        }

        @Override
        protected void onPreExecute() {
            bookingIds.clear();
            progressDialog.setMessage("Adding Booking");
            progressDialog.show();
            progressDialog.setCancelable(false);
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            if (bookingSummaryDetailList.size() > 0) {
                addPaymentModeToBookingList(paymentModeCode);


                /*isOtpConfirmedLoyalty+"", loyaltyTransactionCode, otpLoyaltyEditText.getText().toString(),
                        isOtpConfirmedCashBack+"",cashBackTransactionCode,otpCashBackEditText.getText().toString()*/
                for (BookingSummaryDetailModel bsdm : bookingSummaryDetailList) {
                   /* bsdm.setPaymentMode(paymentModeCode);
                    bsdm.setNote(note);*/

                    if (isOtpConfirmedLoyalty != null && isOtpConfirmedLoyalty == 1) {
                        LoyaltyOrderBookingModel loyaltyOrderBookingModel = new LoyaltyOrderBookingModel();
                        loyaltyOrderBookingModel.setTransactionId(loyaltyTransactionCode);
                        loyaltyOrderBookingModel.setOtpConfirmed(true);
                        //loyaltyOrderBookingModel.setOtpEntered(otpLoyaltyEditText.getText().toString());
                        loyaltyOrderBookingModel.setOtpEntered("");
                        loyaltyOrderBookingModel.setEligibleServiceAmount(totalamountEligbleForLoyalty);
                        loyaltyOrderBookingModel.setPointsToRedeem(loyaltyPointsEditText.getText().toString().replaceAll(",", ""));
                        loyaltyOrderBookingModel.setPointsMultiplier("1");
                        bsdm.setEasyRewardzLoyalty(loyaltyOrderBookingModel);
                    }

                    if (isOtpConfirmedCashBack != null && isOtpConfirmedCashBack == 1) {

                        CashBackBookingModel cashBackBookingModel = new CashBackBookingModel();
                        cashBackBookingModel.setTransactionId(cashBackTransactionCode);
                        cashBackBookingModel.setOtpConfirmed(true);
                        //cashBackBookingModel.setOtpEntered(otpCashBackEditText.getText().toString());
                        cashBackBookingModel.setOtpEntered("");
                        cashBackBookingModel.setEligibleServiceAmount(totalamountEligbleForCashback);
                        cashBackBookingModel.setPointsToRedeem(cashBackPointsEditText.getText().toString().replaceAll(",", ""));
                        cashBackBookingModel.setPointsMultiplier("1");
                        bsdm.setEasyRewardzCashback(cashBackBookingModel);
                    }
                }


                multipleBookingResponseModel = mRestWebservices.addBooking(bookingSummaryDetailList);
            }

            return multipleBookingResponseModel.getSet();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                if (FooterButton != null) {
                    FooterButton.setEnabled(true);
                    FooterButton.setClickable(true);
                    //FooterButton.setVisibility(View.VISIBLE);
                }

                if (result) {
                    try {
                        Gson gson = new GsonBuilder().create();
                        JsonArray jsonArray = gson.toJsonTree(multipleBookingResponseModel.getBookingIds()).getAsJsonArray();
                        String data = jsonArray.toString();


                        orderId = multipleBookingResponseModel.getOrderId();
                        bookingIds = multipleBookingResponseModel.getBookingIds();
                        logCart(bookingSummaryDetailList, "true", data, orderId);
                        long time = System.currentTimeMillis();
                        String paymentId = orderId.concat("_").concat(String.valueOf(time));
                        String success = "1";
                        String failureCode = "";
                        String statusCode = "";
                        String statusMessage = "TXN_SUCCESS";

                        responseMessage = "Txn_Successful";

                        confirmOrderModel = new ConfirmOrderModel(orderId, bookingIds, userId, paymentMode, paymentId, success,
                                failureCode, "Android", statusCode, statusMessage, responseMessage, "");

                        final double total = Double.parseDouble(finalAmountTextView.getText().toString().replace(",", ""));

                        switch (paymentModeCode) {
                            case 0:
                                gotoBookingConfirmation(orderId);
                                break;
                            case 4:
                                confirmOrderModel.setPaymentType("Razorpay");
                                startPaymentModelForCard(orderId, total, userName, userPhone, userEmail);
                                break;
                            case 2:
                                //confirmOrderModel.setPaymentType("Mobikwik");
                                //gotoMobikwikActivity(orderId);
                                confirmOrderModel.setPaymentType("Razorpay");
                                startPaymentModelForCard(orderId, total, userName, userPhone, userEmail);
                                break;
                            case 3:
                                confirmOrderModel.setPaymentType("Paytm");
                                startPaytmTransaction(userId, total);
                                break;
                        }
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }

                } else {
                    responseMessage = multipleBookingResponseModel.getMessage();
                    logCart(bookingSummaryDetailList, "false", "", "");

                    if (!isValidString(responseMessage)) {
                        responseMessage = "Booking failed! We are having a busy day, please try again after some time";
                    }

                    Toast.makeText(BookingSummaryDetailActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }

    }

    /*  -------------------------------------
        |
        |   START A NEW PAYTM TRANSACTION
        |
        -------------------------------------
    */

    public void startPaytmTransaction(String customerId, double total) {
        //Initialize paytm credentials
        String orderId = confirmOrderModel.getOrderId();
        String paytmMerchantID = ApplicationSettings.PAYTM_MERCHANT_ID;
        String paytmIndustryTypeID = ApplicationSettings.PAYTM_INDUSTRY_TYPE_ID;
        String paytmChannelID = ApplicationSettings.PAYTM_CHANNEL_ID;
        String paytmWebsite = ApplicationSettings.PAYTM_WEBSITE;
        String transactionAmount = String.valueOf(total);
//        String paytmCallbackURL = !ApplicationSettings.LOCAL ? ApplicationSettings.PAYTM_CALLBACK_URL : ApplicationSettings.PAYTM_CALLBACK_URL + orderId;
        String paytmCallbackURL = ApplicationSettings.LOCAL ? ApplicationSettings.PAYTM_CALLBACK_URL : ApplicationSettings.PAYTM_CALLBACK_URL + orderId;

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            paramMap = new HashMap<>();
            paramMap.put("MID", paytmMerchantID);
            paramMap.put("ORDER_ID", orderId);
            paramMap.put("CUST_ID", customerId);
            paramMap.put("INDUSTRY_TYPE_ID", paytmIndustryTypeID);
            paramMap.put("CHANNEL_ID", paytmChannelID);
            // paramMap.put("TXN_AMOUNT", "1");
            paramMap.put("TXN_AMOUNT", transactionAmount);
            paramMap.put("WEBSITE", paytmWebsite);
            paramMap.put("CALLBACK_URL", paytmCallbackURL);
            paramMap.put("TEST_MODE", "true");
            paramMap.put("PAYTM_MERCHANT_KEY", ApplicationSettings.PAYTM_MERCHANT_KEY);

            apiManager = new ApiManager(BookingSummaryDetailActivity.this, BookingSummaryDetailActivity.this);
            apiManager.doJsonParsing();
            apiManager.setClassTypeForJson(PaytmChecksumResponse.class);
            apiManager.getStringGetResponse(BOOKING_SUMMARY_DETAIL_PAGE, ApplicationSettings.PAYTM_CHECKSUM_URL, paramMap);

        } else {
            Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private void completePayTmTransaction(String paytmChecksumHash) {
//        PaytmPGService paytmPGService = !ApplicationSettings.LOCAL ? PaytmPGService.getStagingService() : PaytmPGService.getProductionService();
        PaytmPGService paytmPGService = ApplicationSettings.LOCAL ? PaytmPGService.getStagingService() : PaytmPGService.getProductionService();

        paramMap.remove("TEST_MODE");
        paramMap.put("CHECKSUMHASH", paytmChecksumHash);

        PaytmOrder Order = new PaytmOrder(paramMap);
        paytmPGService.initialize(Order, null);

        try {
            paytmPGService.startPaymentTransaction(this, true, true, new PaytmPaymentTransactionCallback() {

                @Override
                public void someUIErrorOccurred(String inErrorMessage) {
                    Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onTransactionResponse(Bundle inResponse) {
                    String statusMessage = inResponse.getString("STATUS");
                    confirmOrderModel.setStatusMessage(statusMessage);

                    if (statusMessage.equals("TXN_SUCCESS")) {
                        paramMap.remove("CUST_ID");
                        paramMap.remove("INDUSTRY_TYPE_ID");
                        paramMap.remove("CHANNEL_ID");
                        paramMap.remove("TXN_AMOUNT");
                        paramMap.remove("WEBSITE");
                        paramMap.remove("CALLBACK_URL");
                        paramMap.remove("CHECKSUMHASH");
                        paramMap.put("TEST_MODE", "true");
                        getPaytmTransactionStatus(paramMap);
                    } else {
                        Toast.makeText(getBaseContext(), "Payment Transaction Failed, Please Try Again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void networkNotAvailable() {
                    Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                }

                @Override
                public void clientAuthenticationFailed(String inErrorMessage) {
                    Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                    Log.d("LOG", "Payment Transaction Failed " + inErrorMessage);
                }

                @Override
                public void onBackPressedCancelTransaction() {
                }

                @Override
                public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                    Toast.makeText(getBaseContext(), "Payment Transaction Cancelled ", Toast.LENGTH_LONG).show();
                }

            });
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void getPaytmTransactionStatus(HashMap<String, String> paramMap) {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            apiManager = new ApiManager(BookingSummaryDetailActivity.this, BookingSummaryDetailActivity.this);
            apiManager.doJsonParsing();
            apiManager.setClassTypeForJson(PaytmTransactionStatusResponse.class);
            apiManager.getStringGetResponse(BOOKING_SUMMARY_DETAIL_PAGE, ApplicationSettings.PAYTM_TRANSACTION_STATUS_URL, paramMap);
        } else {
            Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |   API RESPONSES
        |
        -------------------------------------
    */

    @Override
    public void positiveResponse(String TAG, String response) {
        String popints = "";
        if (TAG == GET_ABVAILABLE_LOYALTY_POINTS) {

            JSONObject jsonObject = null;
            try {


                //response = response.substring(1, response.length()-1);
                //response = response.replaceAll("\\\\","");

                jsonObject = new JSONObject(response);

                if (jsonObject.getString("ReturnMessage").equalsIgnoreCase("Success.")) {
                    useLoyaltyCb.setVisibility(View.VISIBLE);
                    popints = jsonObject.getString("AvailablePoints");
                    useLoyaltyCb.setText("Use your " + popints + " EasyRewardz loyalty points");
                } else {
                    useLoyaltyCb.setVisibility(View.GONE);
                }

            } catch (Exception ex) {
                System.out.print(ex.getMessage());
            }


        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }


    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (Objects.equals(TAG, BOOKING_SUMMARY_DETAIL_PAGE) && responseObj instanceof PaytmChecksumResponse) {
            PaytmChecksumResponse paytmChecksumResponse = (PaytmChecksumResponse) responseObj;

            if (paytmChecksumResponse.getResponsedata().isSuccess() == 1) {
                String checksum = paytmChecksumResponse.getResponsedata().getChecksum();
                completePayTmTransaction(checksum);
            } else {
                Toast.makeText(BookingSummaryDetailActivity.this, "Error: Checksum generation failed, " + "Please try again", Toast.LENGTH_SHORT).show();
            }

        } else if (Objects.equals(TAG, BOOKING_SUMMARY_DETAIL_PAGE) && responseObj instanceof PaytmTransactionStatusResponse) {
            PaytmTransactionStatusResponse paytmTransactionStatusResponse = (PaytmTransactionStatusResponse) responseObj;

            if (paytmTransactionStatusResponse.getResponsedata().isSuccess() == 1) {
                pref.clearCartData();
                dialog.dismiss();
                confirmOrderModel.setPaymentId(paytmTransactionStatusResponse.getResponsedata().getResponse().getTXNID());
                new ConfirmBookingAsyncTask(confirmOrderModel, true).execute(null, null, null);
            } else {
                confirmOrderModel.setFailureCode("01");
                confirmOrderModel.setSuccess("0");
                confirmOrderModel.setStatusMessage("TXN_UNSUCCESSFUL");
                confirmOrderModel.setResponseMessage("Txn not verified");
                new ConfirmBookingAsyncTask(confirmOrderModel, false).execute(null, null, null);
                Toast.makeText(BookingSummaryDetailActivity.this, "Error: Transaction verification failed, Please try again", Toast.LENGTH_SHORT).show();
            }
        } else if (TAG == GET_ABVAILABLE_REEDEMEBALE_LOYALTY_POINTS && responseObj instanceof ReedemableLoyaltyPointsResponse) {

            ReedemableLoyaltyPointsResponse reedemableLoyaltyPointsResponse = (ReedemableLoyaltyPointsResponse) responseObj;

            if (reedemableLoyaltyPointsResponse.getResponsedata().isSuccess() == 1) {
                redeemableLoyaltyPoints = reedemableLoyaltyPointsResponse.getResponsedata().getResult().getPointsRedeemable();
                if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                    if (redeemableLoyaltyPoints <= 0) {
                        Toast.makeText(BookingSummaryDetailActivity.this, "Your redeemable points " + redeemableLoyaltyPoints, Toast.LENGTH_SHORT).show();
                        loyaltyPointLen.setVisibility(View.GONE);
                        redeemLen.setVisibility(View.GONE);
                        loyaltyOtpLen.setVisibility(View.GONE);
                        useLoyaltyCb.setChecked(false);
                        couponEditText.setFocusable(true);
                        couponEditText.setEnabled(true);
                        couponEditText.setCursorVisible(true);
                        //couponEditText.setKeyListener(null);
                        couponEditText.setBackgroundColor(Color.WHITE);

                        couponApplyTextView.setClickable(true);
                        couponApplyTextView.setEnabled(true);
                        couponApplyTextView.setCursorVisible(true);
                        couponApplyTextView.setBackground(getDrawable(R.drawable.vc_color_list));

                    } else {
                        loyaltyPointsEditText.setText(reedemableLoyaltyPointsResponse.getResponsedata().getResult().getPointsRedeemable() + "");
                        loyaltyTextUse.setText("of " + reedemableLoyaltyPointsResponse.getResponsedata().getResult().getAvailablePoints() + " redeemable points of this booking");
                        loyaltyPointLen.setVisibility(View.VISIBLE);
                        redeemLen.setVisibility(View.VISIBLE);
                        maxLoyaltyPoints = reedemableLoyaltyPointsResponse.getResponsedata().getResult().getPointsRedeemable();
                        loyaltyPointsEditText.setEnabled(true);
                        loyaltyPointsEditText.setBackgroundColor(Color.WHITE);
                        redeemButton.setEnabled(true);
                        redeemButton.setBackground(ContextCompat.getDrawable(BookingSummaryDetailActivity.this, R.drawable.vc_color_list));
                        redeemButton.setClickable(true);

                    }
                } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                    if (redeemableLoyaltyPoints <= 0) {
                        Toast.makeText(BookingSummaryDetailActivity.this, "Your redeemable points " + redeemableLoyaltyPoints, Toast.LENGTH_SHORT).show();
                        cashBackPointLen.setVisibility(View.GONE);
                        redeemCashBackLen.setVisibility(View.GONE);
                        cashBackOtpLen.setVisibility(View.GONE);
                        useCashBackCb.setChecked(false);
                        makeCouponAvailable();
                        /*couponEditText.setFocusable(true);
                        couponEditText.setEnabled(true);
                        couponEditText.setCursorVisible(true);
                        //couponEditText.setKeyListener(null);
                        couponEditText.setBackgroundColor(Color.WHITE);

                        couponApplyTextView.setClickable(true);
                        couponApplyTextView.setEnabled(true);
                        couponApplyTextView.setCursorVisible(true);
                        couponApplyTextView.setBackground(getDrawable(R.drawable.vc_color_list));*/

                    } else {
                        cashBackPointsEditText.setText(reedemableLoyaltyPointsResponse.getResponsedata().getResult().getPointsRedeemable() + "");
                        cashBackTextUse.setText("of " + reedemableLoyaltyPointsResponse.getResponsedata().getResult().getAvailablePoints() + " redeemable points of this booking");
                        cashBackPointLen.setVisibility(View.VISIBLE);
                        redeemCashBackLen.setVisibility(View.VISIBLE);
                        maxCashBachPoints = reedemableLoyaltyPointsResponse.getResponsedata().getResult().getPointsRedeemable();
                        cashBackPointsEditText.setEnabled(true);
                        cashBackPointsEditText.setBackgroundColor(Color.WHITE);
                        redeemCashBackButton.setEnabled(true);
                        redeemCashBackButton.setBackground(ContextCompat.getDrawable(BookingSummaryDetailActivity.this, R.drawable.vc_color_list));
                        redeemCashBackButton.setClickable(true);
                    }
                }

                //completePayTmTransaction(paytmChecksumResponse.getResponsedata().getChecksum());
            }
        } else if (TAG == GET_ABVAILABLE_LOYALTY_POINTS && responseObj instanceof AbvailablePointsResponse) {

            AbvailablePointsResponse abvailablePointsResponse = (AbvailablePointsResponse) responseObj;

            if (abvailablePointsResponse.getReturnMessage().equalsIgnoreCase("Success.")) {
                useLoyaltyCb.setVisibility(View.VISIBLE);
                useLoyaltyCb.performClick();


                useLoyaltyCb.setText("Use your " + abvailablePointsResponse.getAvailablePoints() + " VLCC Reward Club points");

            } else {
                useLoyaltyCb.setVisibility(View.GONE);
            }

            getAvailablePointsCashBack(userModel.getID(), CASHBACK_REWARD_TYPE);


        } else if (TAG == GET_ABVAILABLE_CASHBACK_POINTS && responseObj instanceof AbvailablePointsResponse) {

            AbvailablePointsResponse abvailablePointsResponse = (AbvailablePointsResponse) responseObj;

            if (abvailablePointsResponse.getReturnMessage().equalsIgnoreCase("Success.")) {
                useCashBackCb.setVisibility(View.VISIBLE);
                useCashBackCb.performClick();


                useCashBackCb.setText("Use your " + abvailablePointsResponse.getAvailablePoints() + " VLCC Cash Back points");

            } else {
                useCashBackCb.setVisibility(View.GONE);
            }


        } else if (TAG == BLOCK_LOYALTY_POINTS && responseObj instanceof BlockLoyaltyPoints) {

            BlockLoyaltyPoints blockLoyaltyPoints = (BlockLoyaltyPoints) responseObj;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            if (blockLoyaltyPoints.getReturnMessage().equalsIgnoreCase("Success.")) {
                if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {

                    loyaltyTransactionCode = blockLoyaltyPoints.getBillNo();
                    loyaltyPointsEditText.setEnabled(false);
                    loyaltyPointsEditText.setBackgroundColor(Color.TRANSPARENT);
                    redeemButton.setEnabled(false);
                    redeemButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    redeemButton.setClickable(false);
                    if (blockLoyaltyPoints.getIsRedeemWithOutOTP() != null && blockLoyaltyPoints.getIsRedeemWithOutOTP().equalsIgnoreCase("true")) {
                        isOtpConfirmedLoyalty = 1;
                        if (blockLoyaltyPoints.getLoyaltyDiscount() != null && !blockLoyaltyPoints.getLoyaltyDiscount().equals("")) {
                            loyaltyDiscount = blockLoyaltyPoints.getLoyaltyDiscount();
                        } else {
                            loyaltyDiscount = "0";
                        }
                        if (Integer.valueOf(loyaltyDiscount) > 0) {
                            loyaltyDiscountLayout.setVisibility(View.VISIBLE);
                            loyaltyDiscountTextView.setText(formatter.format(Integer.valueOf(loyaltyDiscount)));
                        }
                        addLoyaltyDiscount(blockLoyaltyPoints.getBillAmountAfterLoyalty());
                    } else {
                        loyaltyOtpLen.setVisibility(View.VISIBLE);
                        editPoints.setVisibility(View.VISIBLE);

                        Toast.makeText(BookingSummaryDetailActivity.this, "Otp sent successfully to registered mobile number", Toast.LENGTH_SHORT).show();
                    }

                } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {

                    cashBackTransactionCode = blockLoyaltyPoints.getBillNo();
                    cashBackPointsEditText.setEnabled(false);
                    cashBackPointsEditText.setBackgroundColor(Color.TRANSPARENT);
                    redeemCashBackButton.setEnabled(false);
                    redeemCashBackButton.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
                    redeemCashBackButton.setClickable(false);
                    if (blockLoyaltyPoints.getIsRedeemWithOutOTP() != null && blockLoyaltyPoints.getIsRedeemWithOutOTP().equalsIgnoreCase("true")) {
                        isOtpConfirmedCashBack = 1;
                        if (blockLoyaltyPoints.getLoyaltyDiscount() != null && !blockLoyaltyPoints.getLoyaltyDiscount().equals("")) {
                            cashBackDiscount = blockLoyaltyPoints.getLoyaltyDiscount();
                        } else {
                            cashBackDiscount = "0";
                        }
                        if (Integer.valueOf(cashBackDiscount) > 0) {
                            cashBackDiscountLayout.setVisibility(View.VISIBLE);
                            cashBackDiscountTextView.setText(formatter.format(Integer.valueOf(cashBackDiscount)));
                            //total = total - Integer.valueOf(loyaltyDiscount);
                        }
                /*useLoyaltyCb.setClickable(false);
                useLoyaltyCb.setEnabled(false);*/
                        addCashBackDiscount(blockLoyaltyPoints.getBillAmountAfterLoyalty());
                    } else {
                        cashBackOtpLen.setVisibility(View.VISIBLE);
                        editCashBackPoints.setVisibility(View.VISIBLE);

                        Toast.makeText(BookingSummaryDetailActivity.this, "Otp sent successfully to registered mobile number", Toast.LENGTH_SHORT).show();
                    }
                }

                //useLoyaltyCb.setText("Use your " + blockLoyaltyPoints.getAvailablePoints() + " EasyRewardz loyalty points");
            } else {
                if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                    loyaltyOtpLen.setVisibility(View.GONE);
                    loyaltyPointsEditText.setEnabled(true);
                    loyaltyPointsEditText.setBackgroundColor(Color.WHITE);
                    editPoints.setVisibility(View.GONE);
                    redeemButton.setEnabled(true);
                    redeemButton.setClickable(true);
                    redeemButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    Toast.makeText(BookingSummaryDetailActivity.this, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
                } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                    cashBackOtpLen.setVisibility(View.GONE);
                    cashBackPointsEditText.setEnabled(true);
                    cashBackPointsEditText.setBackgroundColor(Color.WHITE);
                    editCashBackPoints.setVisibility(View.GONE);
                    redeemCashBackButton.setEnabled(true);
                    redeemCashBackButton.setClickable(true);
                    redeemCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    Toast.makeText(BookingSummaryDetailActivity.this, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (TAG == RELEASE_LOYALTY_POINTS && responseObj instanceof ReleaseLoyaltyPointsResponse) {

            ReleaseLoyaltyPointsResponse releaseLoyaltyPointsResponse = (ReleaseLoyaltyPointsResponse) responseObj;

            if (releaseLoyaltyPointsResponse.getReturnMessage().equalsIgnoreCase("Success.")) {
                useLoyaltyCb.setVisibility(View.VISIBLE);


                //useLoyaltyCb.setText("Use your " + abvailablePointsResponse.getAvailablePoints() + " EasyRewardz loyalty points");

            } else {
                //useLoyaltyCb.setVisibility(View.GONE);
            }


        } else if (TAG == RESEND_LOYALTY_POINTS && responseObj instanceof ResendOtpLoyalty) {

            ResendOtpLoyalty resendOtpLoyalty = (ResendOtpLoyalty) responseObj;

            if (resendOtpLoyalty.getReturnMessage().equalsIgnoreCase("Success.")) {
                Toast.makeText(BookingSummaryDetailActivity.this, "OTP sent successfully.", Toast.LENGTH_SHORT).show();

                //useLoyaltyCb.setText("Use your " + blockLoyaltyPoints.getAvailablePoints() + " EasyRewardz loyalty points");
            } else {
                //loyaltyOtpLen.setVisibility(View.GONE);
                Toast.makeText(BookingSummaryDetailActivity.this, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
            }
        } else if (TAG == SUBMIT_LOYALTY_POINTS && responseObj instanceof SubmitOtpLoyalty) {

            SubmitOtpLoyalty submitOtpLoyalty = (SubmitOtpLoyalty) responseObj;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            if (submitOtpLoyalty.getReturnMessage().equalsIgnoreCase("Success.")) {

                if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                    Toast.makeText(BookingSummaryDetailActivity.this, "VLCC Rewards Club points Redeemed  successfully.", Toast.LENGTH_SHORT).show();
                    if (submitOtpLoyalty.getLoyaltyDiscount() != null && !submitOtpLoyalty.getLoyaltyDiscount().equals("")) {
                        loyaltyDiscount = submitOtpLoyalty.getLoyaltyDiscount();
                    } else {
                        loyaltyDiscount = "0";
                    }
                    if (Integer.valueOf(loyaltyDiscount) > 0) {
                        loyaltyDiscountLayout.setVisibility(View.VISIBLE);
                        loyaltyDiscountTextView.setText(formatter.format(Integer.valueOf(loyaltyDiscount)));
                        //total = total - Integer.valueOf(loyaltyDiscount);
                    }
                /*useLoyaltyCb.setClickable(false);
                useLoyaltyCb.setEnabled(false);*/
                    addLoyaltyDiscount(submitOtpLoyalty.getBillAmountAfterLoyalty());
                } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                    Toast.makeText(BookingSummaryDetailActivity.this, "VLCC CashBack points Redeemed  successfully.", Toast.LENGTH_SHORT).show();
                    if (submitOtpLoyalty.getLoyaltyDiscount() != null && !submitOtpLoyalty.getLoyaltyDiscount().equals("")) {
                        cashBackDiscount = submitOtpLoyalty.getLoyaltyDiscount();
                    } else {
                        cashBackDiscount = "0";
                    }
                    if (Integer.valueOf(cashBackDiscount) > 0) {
                        cashBackDiscountLayout.setVisibility(View.VISIBLE);
                        cashBackDiscountTextView.setText(formatter.format(Integer.valueOf(cashBackDiscount)));
                        //total = total - Integer.valueOf(loyaltyDiscount);
                    }
                /*useLoyaltyCb.setClickable(false);
                useLoyaltyCb.setEnabled(false);*/
                    addCashBackDiscount(submitOtpLoyalty.getBillAmountAfterLoyalty());
                }


                //amountText.setText(formatter.format(serviceAmount));
                //useLoyaltyCb.setText("Use your " + blockLoyaltyPoints.getAvailablePoints() + " EasyRewardz loyalty points");
            } else {

                if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                    isOtpConfirmedLoyalty = 0;

                    loyaltyDiscount = "0";
                    loyaltyDiscountLayout.setVisibility(View.GONE);
                    submitOtpButton.setEnabled(true);
                    submitOtpButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    resendOTPButton.setEnabled(true);
                    resendOTPButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    otpLoyaltyEditText.setEnabled(true);
                    otpLoyaltyEditText.setBackgroundColor(Color.WHITE);
                } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                    cashBackDiscount = "0";
                    cashBackDiscountLayout.setVisibility(View.GONE);
                    isOtpConfirmedCashBack = 0;
                    submitOtpCashBackButton.setEnabled(true);
                    submitOtpCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    resendOTPCashBackButton.setEnabled(true);
                    resendOTPCashBackButton.setBackground(getDrawable(R.drawable.vc_color_list));
                    otpCashBackEditText.setEnabled(true);
                    otpCashBackEditText.setBackgroundColor(Color.WHITE);
                }


                //loyaltyOtpLen.setVisibility(View.GONE);
                Toast.makeText(BookingSummaryDetailActivity.this, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
            }
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    private void addLoyaltyDiscount(String amountAfterLoyaltyDiscount) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
        String discount = "0";
        if (discountTextView.getText() != null)
            discount = discountTextView.getText().toString();
        totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(discount) + Double.valueOf(loyaltyDiscount);
        double total = serviceAmount - Double.valueOf(loyaltyDiscount);
        if (total < 0) {
            total = 0;
        }
        //finalAmountTextView.setText(formatter.format(total));
        //int serviceAmount = Integer.valueOf(totalText.getText().toString().replace(",", ""));
        /*totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount)+Integer.valueOf(coupon_amount)+Integer.valueOf(loyaltyDiscount);*/
        /*int total = serviceAmount - Integer.valueOf(loyaltyDiscount);
        if(total<0){
            total = 0;
        }
        totalText.setText(formatter.format(total));*/
        isOtpConfirmedLoyalty = 1;
        setGst();

    }

    private void addCashBackDiscount(String amountAfterLoyaltyDiscount) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        double serviceAmount = Double.valueOf(finalAmountTextView.getText().toString().replace(",", ""));
        String discount = "0";
        if (discountTextView.getText() != null)
            discount = discountTextView.getText().toString();
        totalDiscount = Double.valueOf(firstBookingDiscount) + Double.valueOf(referralDiscount) + Double.valueOf(loyaltyDiscount);
        double total = serviceAmount - Double.valueOf(cashBackDiscount);
        if (total < 0) {
            total = 0;
        }
        //finalAmountTextView.setText(formatter.format(total));
        //int serviceAmount = Integer.valueOf(totalText.getText().toString().replace(",", ""));
        /*totalDiscount = Integer.valueOf(firstBookingDiscount) + Integer.valueOf(referralDiscount)+Integer.valueOf(coupon_amount)+Integer.valueOf(loyaltyDiscount);*/
        // int total = serviceAmount - Integer.valueOf(cashBackDiscount);
        /*if(total<0){
            total = 0;
        }
        totalText.setText(formatter.format(total));*/
        isOtpConfirmedCashBack = 1;
        setGstForCashBack();

    }


    @Override
    public void negativeResponse(String TAG, String errorResponse) {
        if (!TAG.contains("easyRewardz")) {
            if (confirmOrderModel != null) {
                confirmOrderModel.setFailureCode("02");
                confirmOrderModel.setSuccess("0");
                confirmOrderModel.setStatusMessage("TXN_UNSUCCESSFUL");
                confirmOrderModel.setResponseMessage("No response from paytm server");
                //new ConfirmBookingAsyncTask(confirmOrderModel, false).execute(null, null, null);
                Toast.makeText(BookingSummaryDetailActivity.this, "Error:" + errorResponse, Toast.LENGTH_SHORT).show();
                System.out.print(errorResponse);
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
            }
        }
        if (BookingSummaryDetailActivity.this.isDestroyed() || BookingSummaryDetailActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
            return;
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }


    }

    /*  -------------------------------------
        |
        |   CARD PAYMENT USING RAZORPAY
        |
        -------------------------------------
    */

    public void startPaymentModelForCard(String orderId, Double total, String userName, String userNumber, String userEmail) {
        PaymentModel paymentModel = new PaymentModel(orderId, String.valueOf(total.intValue()), userName, userNumber, userEmail, "VanityCube Services");
        startPayment(paymentModel);
    }

    public void startPayment(PaymentModel paymentModel) {
        final String public_key = ApplicationSettings.LOCAL ? "rzp_test_JMxk245VODdMXV" : "rzp_live_McSvz0wFJfvtg7";
        final Activity activity = BookingSummaryDetailActivity.this;
        final Checkout checkout = new Checkout();
        checkout.setPublicKey(public_key);

        try {
            JSONObject options = new JSONObject(
                    "{" +
                            "name: 'VanityCube'," +
                            "description: 'Cart'," +
                            "image: ''," +
                            "currency: 'INR' " +
                            "}"
            );

            options.put("amount", String.valueOf(Integer.parseInt(paymentModel.getAmount()) * 100));
            options.put("name", paymentModel.getFirstname());
            options.put("prefill", new JSONObject("{email: " + paymentModel.getEmail() + ", contact: " + paymentModel.getPhone() + "}"));
            options.put("theme", new JSONObject("{color: '#FA8072'}"));
            checkout.open(activity, options);

        } catch (Exception e) {
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.getMessage());
        }
        //onPaymentSuccess("razorPayiD_test");
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            dialog.dismiss();
            confirmOrderModel.setPaymentId(razorpayPaymentID);
            new ConfirmBookingAsyncTask(confirmOrderModel, true).execute(null, null, null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            dialog.dismiss();
            confirmOrderModel.setResponseMessage(response);
            confirmOrderModel.setFailureCode(String.valueOf(code));
            confirmOrderModel.setSuccess("0");
            confirmOrderModel.setStatusMessage("TXN_UNSUCCESSFUL");
            new ConfirmBookingAsyncTask(confirmOrderModel, false).execute(null, null, null);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /*  -------------------------------------
        |
        |   CONFIRM BOOKING ASYNC TASK
        |
        -------------------------------------
    */

    private class ConfirmBookingAsyncTask extends AsyncTask<Void, Void, StandardResponseModelBooking> {
        ProgressDialog progressDialog;
        StandardResponseModelBooking response;
        ConfirmOrderModel confirmOrderModel;
        Boolean paymentSuccess;

        ConfirmBookingAsyncTask(ConfirmOrderModel confirmOrderModel, Boolean paymentSuccess) {
            this.confirmOrderModel = confirmOrderModel;
            this.paymentSuccess = paymentSuccess;
            progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Confirming");
            progressDialog.show();
        }

        @Override
        protected StandardResponseModelBooking doInBackground(Void... url) {
            if (isValidString(orderId) && isValidString(confirmOrderModel.getPaymentId())) {
                response = mRestWebservices.confirmBooking(confirmOrderModel);
            }
            return response;
        }

        @Override
        protected void onPostExecute(StandardResponseModelBooking result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if (result.getSuccess().equalsIgnoreCase("1")) {
                    if (paymentSuccess) {
                        Toast.makeText(BookingSummaryDetailActivity.this, "Booking confirmed", Toast.LENGTH_LONG).show();
                        gotoBookingConfirmation(confirmOrderModel.getOrderId());
                    }
                } else {
                    String message = result.getMessage();
                    if (!isValidString(message)) {
                        message = "Unable to confirm booking, please try again";
                    }
                    Toast.makeText(BookingSummaryDetailActivity.this, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void gotoMobikwikActivity(String orderId) {
        String url = ApplicationSettings.BASE_URL_PAY + "order/details/pay?orderId=" + orderId + "&selectedWallet=mobikwik";
        Intent onlinePayIntent = new Intent(BookingSummaryDetailActivity.this, MobiKwikActivity.class);
        onlinePayIntent.putExtra("url", url);
        onlinePayIntent.putExtra("orderId", orderId);
        startActivityForResult(onlinePayIntent, 1);
    }

    public void goBack(boolean mode, boolean selectTimeSlot) {
        Intent intBack = new Intent();

        if (selectTimeSlot) {
            intBack.putExtra("select_time_slot", "yes");
        }

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    private void gotoBookingConfirmation(String orderId) {
        Intent bookIntent = new Intent(BookingSummaryDetailActivity.this, BookingConfirmationActivity.class);
        bookIntent.putStringArrayListExtra("bookingIds", confirmOrderModel.getBookingIds());
        bookIntent.putExtra("orderId", orderId);
        bookIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(bookIntent);
        finish();
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerMenu:
                finish();
                break;

            case R.id.common_footer_layout:
            case R.id.footerButton:

                if (!isBookingAllowed) {
                    //Toast.makeText(BookingSummaryDetailActivity.this, "Please select the time slot again to continue.", Toast.LENGTH_SHORT).show();
                    //Confirmation dialog to select a new timer
                    chooseTimerDialog();

                    return;
                }
                note = noteEditText.getText().toString();
                updatePayInCategories();

                switch (paymentMode) {
                    case "Cash":
                        paymentModeCode = 0;
                        if (FooterButton != null) {
                            FooterButton.setEnabled(false);
                            FooterButton.setClickable(false);
                            //FooterButton.setVisibility(View.GONE);
                        }
                        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                            new AddBookingAsyncTask(paymentModeCode, paymentMode).execute(null, null, null);
                        } else {
                            Toast.makeText(BookingSummaryDetailActivity.this, checkYourInternet, Toast.LENGTH_LONG).show();
                        }

                        break;

                    case "Online":
                        onlinePayDialog();
                        break;

                    default:
                        break;
                }

                break;

            case R.id.cashtext:
            case R.id.cashlayout:
                paymentModeCode = 0;
                paymentMode = getResources().getString(R.string.cash_payment_mode_text);

                cashPaymentLayout.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.magenta));
                tCash.setTextColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.white));
                mcashimage.setBackgroundResource(R.drawable.cash_white);

                //Uncheck online
                onlinePaymentLayout.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.white));
                tOnline.setTextColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.magenta));
                monlineimage.setBackgroundResource(R.drawable.card);
                FooterButton.setText(getString(R.string.book_text));

                break;

            case R.id.onlineText:
            case R.id.onlineLayout:
                paymentMode = getResources().getString(R.string.online_payment_mode_text);

                onlinePaymentLayout.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.magenta));
                tOnline.setTextColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.white));
                monlineimage.setBackgroundResource(R.drawable.card_white);

                //Uncheck cash
                cashPaymentLayout.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.white));
                tCash.setTextColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.magenta));
                mcashimage.setBackgroundResource(R.drawable.cash);

                break;

            case R.id.couponApplyTextView:
                String couponCode = couponEditText.getText().toString();

                if (couponAlreadyApplied) {
                    Toast.makeText(BookingSummaryDetailActivity.this, "Coupon already applied", Toast.LENGTH_LONG).show();
                } else {
                    couponConditionTextView.setVisibility(View.VISIBLE);
                    couponErrorTextView.setVisibility(View.GONE);

                    if (isValidString(couponCode)) {
                        checkCoupon(couponCode);
                    }
                }

                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //For Mobikwik
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                orderId = data.getStringExtra("orderId");
                dialog.dismiss();
                gotoBookingConfirmation(orderId);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(BookingSummaryDetailActivity.this, "Payment Failed", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        }
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //For Mobikwik
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                //Move to the final activity
                Intent bookIntent = new Intent(BookingSummaryDetailActivity.this, BookingConfirmationActivity.class);
                booking_id = data.getStringExtra("booking_id");

                //We will clear the cart here
                pref.clearCartData();
                bookIntent.putExtra("booking_id", booking_id);
                startActivity(bookIntent);
                finish();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                confirmBooking("-1", "Payment cancelled", "0");
                dialog.dismiss();
            }
        }

    }*/

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
        setupTimer();
        if (!pref.getIsUserMember()) {
            isToWebView = false;
            checkIsMember();
        }
    }


    private void checkIsMember() {
        if (userId != null && !userId.equalsIgnoreCase("")) {
            /*isMember = true;
            pref.putIsUserMember(true);*/
            new GetMemberAsyncTask().execute(null, null, null);
        } else {
            getUser();
            if (userId != null) {
                /*isMember = true;
                pref.putIsUserMember(true);*/
                new GetMemberAsyncTask().execute(null, null, null);

            } else {
                pref.putIsUserMember(false);
                //populateBookingList();
            }
        }

    }

    private class GetMemberAsyncTask extends AsyncTask<Void, Void, Boolean> {
        boolean isSubmitted = false;
        ProgressDialog progressDialog;


        GetMemberAsyncTask() {
            progressDialog = new ProgressDialog(BookingSummaryDetailActivity.this);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Processing...");
                progressDialog.show();
            }

        }

        protected Boolean doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            try {
                isSubmitted = restWebServices.getIsUserMember(userId);
            } catch (Exception ex) {

            }
            return isSubmitted;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                pref.putIsUserMember(result);

                //loadDashFrag();
                //populateBookingList();
                if (result)
                    reloadActivity();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void reloadActivity() {
        Intent reloadIntent = getIntent();
        startActivity(reloadIntent);
        finish();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_BOOKING_SUMMARY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void releaseLoyaltyPoints(String userId) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(ReleaseLoyaltyPointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_RELEASE_LOYALTY_POINTS);
        String data = releaseLoyaltyRequestParam(userId, loyaltyTransactionCode);
        params.put(ApplicationSettings.PARAM_API_DATA, data);

        apiManager.getStringPostResponse(RELEASE_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private void getAvailablePointsLoyalty(String userId, String rewardType) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(AbvailablePointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_AVAILABLE_POINTS);
        params.put(ApplicationSettings.PARAM_API_DATA, "[\"" + userId + "\"]");
        params.put(ApplicationSettings.PARAM_REWARD_TYPE, rewardType);

        apiManager.getStringGetResponse(GET_ABVAILABLE_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private void getAvailablePointsCashBack(String userId, String rewardType) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(AbvailablePointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_AVAILABLE_POINTS);
        params.put(ApplicationSettings.PARAM_API_DATA, "[\"" + userId + "\"]");
        params.put(ApplicationSettings.PARAM_REWARD_TYPE, rewardType);

        apiManager.getStringGetResponse(GET_ABVAILABLE_CASHBACK_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private void getAvailableReedemeblePoints(String userId, String rewardType) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(ReedemableLoyaltyPointsResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_AVAILABLE_REEDEMEBALE_POINTS);
        String data = getLoyalityPointsJsonRequest(userId);
        params.put(ApplicationSettings.PARAM_API_DATA, data);
        params.put(ApplicationSettings.PARAM_REWARD_TYPE, rewardType);
        apiManager.getStringPostResponse(GET_ABVAILABLE_REEDEMEBALE_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private void blockLoyaltyPoints(String userId) {


        if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
            Integer redeemPoints = Integer.parseInt(loyaltyPointsEditText.getText().toString().replaceAll(",", ""));
            if (maxLoyaltyPoints > 0 && maxLoyaltyPoints >= redeemPoints) {

                progressDialog.setMessage("Processing...");
                progressDialog.show();

                apiManager = new ApiManager(this, this);

                apiManager.doJsonParsing(true);
                apiManager.setClassTypeForJson(BlockLoyaltyPoints.class);

                HashMap<String, String> params = new HashMap<>();
                params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_BLOCK_LOYALTY_POINTS);
                String data = createBlockLoyaltyPointsRequestParams(userId);
                params.put(ApplicationSettings.PARAM_API_DATA, data);
                params.put(ApplicationSettings.PARAM_REWARD_TYPE, REWARD_TYPE);

                apiManager.getStringPostResponse(BLOCK_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
            } else {
                Toast.makeText(BookingSummaryDetailActivity.this, "You can Redeem maximum of " + maxLoyaltyPoints + " loyalty points", Toast.LENGTH_SHORT).show();
            }
        } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
            Integer redeemPoints = Integer.parseInt(cashBackPointsEditText.getText().toString().replaceAll(",", ""));
            if (maxCashBachPoints > 0 && maxCashBachPoints >= redeemPoints) {

                progressDialog.setMessage("Processing...");
                progressDialog.show();

                apiManager = new ApiManager(this, this);

                apiManager.doJsonParsing(true);
                apiManager.setClassTypeForJson(BlockLoyaltyPoints.class);

                HashMap<String, String> params = new HashMap<>();
                params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_BLOCK_LOYALTY_POINTS);
                String data = createBlockLoyaltyPointsRequestParams(userId);
                params.put(ApplicationSettings.PARAM_API_DATA, data);
                params.put(ApplicationSettings.PARAM_REWARD_TYPE, REWARD_TYPE);

                apiManager.getStringPostResponse(BLOCK_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
            } else {
                Toast.makeText(BookingSummaryDetailActivity.this, "You can Redeem maximum of " + maxCashBachPoints + " cashback points", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void resendOtpLoyalty(String userId) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(ResendOtpLoyalty.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_RESEND_LOYALTY_POINTS);
        String data = resendLoyaltyRequestParam(loyaltyTransactionCode);
        params.put(ApplicationSettings.PARAM_API_DATA, data);

        apiManager.getStringPostResponse(RESEND_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private String resendLoyaltyRequestParam(String loyaltyTransactionCode) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", loyaltyTransactionCode);
            } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", cashBackTransactionCode);
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return jsonObject.toString();
    }

    private String releaseLoyaltyRequestParam(String userId, String loyaltyTransactionCode) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userId", userId);
            if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", loyaltyTransactionCode);
            } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", cashBackTransactionCode);
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return jsonObject.toString();
    }

    private void submitLoyaltyOTP(String userId) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SubmitOtpLoyalty.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SUBMITOTP_LOYALTY_POINTS);
        String data = submitOtpLoyaltyRequestParams(userId);
        params.put(ApplicationSettings.PARAM_API_DATA, data);
        //params.put(ApplicationSettings.PARAM_REWARD_TYPE, REWARD_TYPE);
        apiManager.getStringPostResponse(SUBMIT_LOYALTY_POINTS, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    private String submitOtpLoyaltyRequestParams(String userId) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", loyaltyTransactionCode);
                jsonObject.put("userId", userId);
                jsonObject.put("smsCode", otpLoyaltyEditText.getText().toString());
            } else if (REWARD_TYPE.equalsIgnoreCase(CASHBACK_REWARD_TYPE)) {
                jsonObject.put("loyaltyTransactionCode", cashBackTransactionCode);
                jsonObject.put("userId", userId);
                jsonObject.put("smsCode", otpCashBackEditText.getText().toString());
            }
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
        return jsonObject.toString();
    }

    private String createBlockLoyaltyPointsRequestParams(String userId) {
        JSONArray mainArray = new JSONArray();
        JSONObject mainJsonObject = new JSONObject();
        try {

            Map<String, ArrayList<ServiceTypeModel>> stm = getSortedList();
            for (String key : stm.keySet()) {
                JSONObject jObj = new JSONObject();
                ArrayList<ServiceTypeModel> list = stm.get(key);
                JSONArray jsonArray = new JSONArray();
                if (list != null) {
                    jObj.put("categoryId", key);
                    JSONObject jsonObject = null;
                    for (ServiceTypeModel listNew : list) {
                        jsonObject = new JSONObject();
                        jsonObject.put("categoryId", listNew.getCategoryId());
                        jsonObject.put("serviceId", listNew.getServiceID());
                        jsonObject.put("serviceTypeId", listNew.getServiceTypeID());
                        jsonObject.put("quantity", listNew.getNumOfPeople());
                        jsonArray.put(jsonObject);
                    }
                    jObj.put("services", jsonArray);
                    mainArray.put(jObj);

                }
            }

            mainJsonObject.put("userId", userId);
            double amt = Double.parseDouble(amountTextView.getText().toString().replaceAll(",", "")) - getTotalDiscount();
            if (REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE)) {
                mainJsonObject.put("loyaltyPoints", loyaltyPointsEditText.getText().toString().replaceAll(",", ""));
                totalamountEligbleForLoyalty = Math.round(amt) + "";
            } else {
                mainJsonObject.put("loyaltyPoints", cashBackPointsEditText.getText().toString().replaceAll(",", ""));
                totalamountEligbleForCashback = Math.round(amt) + "";
            }

            mainJsonObject.put("totalAmount", amt);
            mainJsonObject.put("bookingServices", mainArray);
            String couponAmt = "";
            if (couponAmountTextView.getText() != null) {
                couponAmt = couponAmountTextView.getText().toString().replaceAll(",", "");
            }
            mainJsonObject.put("couponAmount", couponAmt);//coupon_amount
            mainJsonObject.put("referralDiscount", referralDiscount);//referralDiscount
            mainJsonObject.put("firstBookingDiscount", firstBookingDiscount);//firstBookingDiscount
            mainJsonObject.put("isOtp", 0);
            /*JSONObject jObjectMainArray = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (ServiceTypeModel serviceModel : finalSelectedServiceTypes) {
                JSONObject jObj = new JSONObject();
                jObj.put("categoryId", serviceModel.getCategoryId());
                jObj.put("serviceId",serviceModel.getServiceID());
                jObj.put("serviceTypeId",serviceModel.getServiceTypeID());
                jObj.put("quantity",serviceModel.getNumOfPeople());
                jsonArray.put(jObj);
            }
            jObjectMainArray.put("services",jsonArray);
            //jObjectMainArray.put("categoryId",mServiceModel.get(0).getCategoryId());
            mainArray.put(jObjectMainArray);
            mainJsonObject.put("userId",userId);
            if(REWARD_TYPE.equalsIgnoreCase(LOYALTY_REWARD_TYPE))
                mainJsonObject.put("loyaltyPoints",loyaltyPointsEditText.getText().toString());
            else
                mainJsonObject.put("loyaltyPoints",cashBackPointsEditText.getText().toString());
            //mainJsonObject.put("totalAmount",totalText.getText().toString().replaceAll(",",""));
            mainJsonObject.put("bookingServices",mainArray);
            mainJsonObject.put("couponAmount","");//coupon_amount
            mainJsonObject.put("referralDiscount","");//referralDiscount
            mainJsonObject.put("firstBookingDiscount","");//firstBookingDiscount*/

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }



        /*{"userId":"36788","bookingServices":[{"categoryId":"1","services":[{"categoryId":"1","serviceId":"5","serviceTypeId":"252","quantity":"1"}]}],
            "couponAmount":"","firstBookingDiscount":"0","referralDiscount":"0"}*/
        return mainJsonObject.toString();
    }


    private String getLoyalityPointsJsonRequest(String userId) {

        /*{“userId":"36792","loyaltyPoints":"10","totalAmount":"800","bookingServices":[{"categoryId":"1","services":[{"categoryId":"1","serviceId":"5","serviceTypeId":"252"" +
                ","quantity":"1"}]}]" +
                ","couponAmount":"","firstBookingDiscount":"0","referralDiscount":"0"}*/
        JSONArray mainArray = new JSONArray();
        JSONObject mainJsonObject = new JSONObject();


        try {

            Map<String, ArrayList<ServiceTypeModel>> stm = getSortedList();
            for (String key : stm.keySet()) {
                JSONObject jObj = new JSONObject();
                ArrayList<ServiceTypeModel> list = stm.get(key);
                JSONArray jsonArray = new JSONArray();
                if (list != null) {
                    jObj.put("categoryId", key);
                    JSONObject jsonObject = null;
                    for (ServiceTypeModel listNew : list) {
                        jsonObject = new JSONObject();
                        jsonObject.put("categoryId", listNew.getCategoryId());
                        jsonObject.put("serviceId", listNew.getServiceID());
                        jsonObject.put("serviceTypeId", listNew.getServiceTypeID());
                        jsonObject.put("quantity", listNew.getNumOfPeople());
                        jsonArray.put(jsonObject);
                    }
                    jObj.put("services", jsonArray);
                    mainArray.put(jObj);

                }
            }

            mainJsonObject.put("userId", userId);
            mainJsonObject.put("bookingServices", mainArray);
            /*mainJsonObject.put("couponAmount","");//coupon_amount
            mainJsonObject.put("referralDiscount","");//referralDiscount
            mainJsonObject.put("firstBookingDiscount","");//firstBookingDiscount*/

            String couponAmt = "";
            if (couponAmountTextView.getText() != null) {
                couponAmt = couponAmountTextView.getText().toString().replaceAll(",", "");
            }
            mainJsonObject.put("couponAmount", couponAmt);//coupon_amount
            mainJsonObject.put("referralDiscount", referralDiscount);//referralDiscount
            mainJsonObject.put("firstBookingDiscount", firstBookingDiscount);//firstBookingDiscount


            /*for(ServiceTypeModel st:finalSelectedServiceTypes){
                JSONObject jObj = new JSONObject();
                JSONObject jsonObject = new JSONObject();
                ArrayList<ServiceTypeModel> list = stm.get(st.getCategoryId());

                if(list!=null){
                    jObj.put("categoryId",st.getCategoryId());

                    for(ServiceTypeModel listNew:list){
                        jsonObject.put("categoryId",listNew.getCategoryId());
                        jsonObject.put("serviceId",listNew.getServiceID());
                        jsonObject.put("serviceTypeId",listNew.getServiceTypeID());
                        jsonObject.put("quantity",listNew.getNumOfPeople());
                        jsonArray.put(jsonObject);
                    }
                }
            }*/



            /*for (ServiceTypeModel serviceModel : finalSelectedServiceTypes) {
                JSONObject jObj = new JSONObject();
                jObj.put("categoryId", serviceModel.getCategoryId());
                jObj.put("serviceId",serviceModel.getServiceID());
                jObj.put("serviceTypeId",serviceModel.getServiceTypeID());
                jObj.put("quantity",serviceModel.getNumOfPeople());
                jsonArray.put(jObj);
            }
            jObjectMainArray.put("services",jsonArray);
            //jObjectMainArray.put("categoryId",mServiceModel.get(0).getServiceCategoryId());
            mainArray.put(jObjectMainArray);
            mainJsonObject.put("userId",userId);
            mainJsonObject.put("bookingServices",mainArray);
            mainJsonObject.put("couponAmount","");//coupon_amount
            mainJsonObject.put("referralDiscount","");//referralDiscount
            mainJsonObject.put("firstBookingDiscount","");//firstBookingDiscount*/

        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }


        /*try {
            JSONObject jObjectMainArray = new JSONObject();

            JSONArray jsonArray = new JSONArray();

            for (ServiceTypeModel serviceModel : finalSelectedServiceTypes) {
                JSONObject jObj = new JSONObject();
                jObj.put("categoryId", serviceModel.getCategoryId());
                jObj.put("serviceId",serviceModel.getServiceID());
                jObj.put("serviceTypeId",serviceModel.getServiceTypeID());
                jObj.put("quantity",serviceModel.getNumOfPeople());
                jsonArray.put(jObj);
            }
            jObjectMainArray.put("services",jsonArray);
            //jObjectMainArray.put("categoryId",mServiceModel.get(0).getServiceCategoryId());
            mainArray.put(jObjectMainArray);
            mainJsonObject.put("userId",userId);
            mainJsonObject.put("bookingServices",mainArray);
            mainJsonObject.put("couponAmount","");//coupon_amount
            mainJsonObject.put("referralDiscount","");//referralDiscount
            mainJsonObject.put("firstBookingDiscount","");//firstBookingDiscount

        }catch(Exception ex){
            System.out.print(ex.getMessage());
        }*/



        /*{"userId":"36788","bookingServices":[{"categoryId":"1","services":[{"categoryId":"1","serviceId":"5","serviceTypeId":"252","quantity":"1"}]}],
            "couponAmount":"","firstBookingDiscount":"0","referralDiscount":"0"}*/
        return mainJsonObject.toString();
    }

    private Map<String, ArrayList<ServiceTypeModel>> getSortedList() {
        ArrayList<ServiceTypeModel> list = new ArrayList<>();
        Map<String, ArrayList<ServiceTypeModel>> map = new HashMap<>();
        for (ServiceTypeModel stm : finalSelectedServiceTypes) {
            ArrayList<ServiceTypeModel> listStm = new ArrayList<>();
            if (map.containsKey(stm.getCategoryId())) {
                // listStm.add(stm);
                map.get(stm.getCategoryId()).add(stm);
            } else {

                listStm.add(stm);
                map.put(stm.getCategoryId(), listStm);
            }
        }
        return map;
    }

    protected void chooseTimerDialog() {
        ConfirmDialogHelper.with(BookingSummaryDetailActivity.this).confirmNoCancel("Time Slot Expired", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Kill this activity and pass intent back to previous activity
                Intent selectTimer = new Intent();
                selectTimer.putExtra("select_time_slot", "yes");
                setResult(RESULT_OK, selectTimer);
                finish();
            }
        }, "Please select the time slot again to continue.");
    }

    private void setupTimer() {
        try {
            timeStamp = Long.parseLong(pref.getTimeStamp());
            timer = Integer.parseInt(pref.getTimer());
        } catch (Exception e) {
            timeStamp = System.currentTimeMillis() / 1000;
            timer = 120;
        }

        runnable = new Runnable() {
            public void run() {
                roundedTimerLayout = (LinearLayout) findViewById(R.id.rounded_timer_layout);
                Long currentTimeStamp = System.currentTimeMillis() / 1000;
                roundedTimerLayout = (LinearLayout) findViewById(R.id.rounded_timer_layout);
                roundedTimerTextView = (TextView) findViewById(R.id.roundedTimerTextView);
                roundedTimerLayout.setVisibility(View.VISIBLE);
                int timeRemaining = (int) (timeStamp + timer - currentTimeStamp);
                int minutes = timeRemaining / 60;
                int seconds = timeRemaining % 60;

                if (timeRemaining >= 0) {
                    roundedTimerTextView.setText((minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds));
                    //Set footer button
                    FooterButton.setText("Book");
                    handler.postDelayed(this, delay);
                    isBookingAllowed = true;
                } else {
                    //Confirmation dialog to select a new timer
                    chooseTimerDialog();
                    isBookingAllowed = false;
                    handler.removeCallbacks(runnable);
                }

            }
        };
        handler.postDelayed(runnable, delay);
    }

    private void setGst() {
        double initialAmt = Double.parseDouble(amountTextView.getText().toString().replaceAll(",", ""));
        /*double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString());
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString());
        double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString());
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString());*/

        double totalDiscountAmount = getTotalDiscount();
        double taxableAmt = initialAmt - totalDiscountAmount;

        double gst = taxableAmt * 18 / 100;
        GSTAmount = gst;
        gstTextView.setText("" + new DecimalFormat("##.##").format(gst));
        DecimalFormat formatter = new DecimalFormat("##.##");
        String val = formatter.format(taxableAmt + gst) + "";
        finalAmountTextView.setText(val);
    }

    private void setGstForloyalty() {
        double initialAmt = Double.parseDouble(amountTextView.getText().toString().replaceAll(",", ""));
        /*double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString());
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString());
        double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString());
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString());*/

        double totalDiscountAmount = getTotalDiscountForLoyalty();
        double taxableAmt = initialAmt - totalDiscountAmount;

        double gst = taxableAmt * 18 / 100;
        GSTAmount = gst;
        gstTextView.setText("" + new DecimalFormat("##.##").format(gst));
        DecimalFormat formatter = new DecimalFormat("##.##");
        String val = formatter.format(taxableAmt + gst) + "";
        finalAmountTextView.setText(val);
    }

    private void setGstForCashBack() {
        double initialAmt = Double.parseDouble(amountTextView.getText().toString().replaceAll(",", ""));
        double totalDiscountAmount = getTotalDiscountForCashBack();
        double taxableAmt = initialAmt - totalDiscountAmount;
        double gst = taxableAmt * 18 / 100;
        GSTAmount = gst;
        gstTextView.setText("" + new DecimalFormat("##.##").format(gst));
        DecimalFormat formatter = new DecimalFormat("##.##");
        String val = formatter.format(taxableAmt + gst) + "";
        finalAmountTextView.setText(val);
        couponDiscountLayout.setVisibility(View.GONE);
    }

    private void setGstForCoupon() {
        double initialAmt = Double.parseDouble(amountTextView.getText().toString().replaceAll(",", ""));
        /*double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString());
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString());
        double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString());
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString());*/

        double totalDiscountAmount = getTotalDiscountForCuopon();
        double taxableAmt = initialAmt - totalDiscountAmount;

        double gst = taxableAmt * 18 / 100;
        GSTAmount = gst;
        gstTextView.setText("" + new DecimalFormat("##.##").format(gst));
        DecimalFormat formatter = new DecimalFormat("##.##");
        String val = formatter.format(taxableAmt + gst) + "";
        finalAmountTextView.setText(val);
        cashBackDiscountLayout.setVisibility(View.GONE);
    }

    private Double getTotalDiscount() {
        double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString().replaceAll(",", ""));
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString().replaceAll(",", ""));
        //double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString().replaceAll(",", ""));
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString().replaceAll(",", ""));
        double dis = 0.0;
        if (useCashBackCb.isChecked()) {
            dis = cashBackAmt;
        } else {
            dis = cuopnAmt;
        }
        double totalDiscountAmount = dis + loyaltyAmt + Double.parseDouble(referralDiscount) + Double.parseDouble(firstBookingDiscount);
        return totalDiscountAmount;
    }

    private Double getTotalDiscountForLoyalty() {
        double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString().replaceAll(",", ""));
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString().replaceAll(",", ""));
        //double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString().replaceAll(",", ""));
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString().replaceAll(",", ""));
        double dis = 0.0;
        if (useCashBackCb.isChecked()) {
            dis = cashBackAmt;
        } else {
            dis = cuopnAmt;
        }
        double totalDiscountAmount = dis + loyaltyAmt + Double.parseDouble(referralDiscount) + Double.parseDouble(firstBookingDiscount);
        return totalDiscountAmount;
    }

    private Double getTotalDiscountForCashBack() {
        double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString().replaceAll(",", ""));
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString().replaceAll(",", ""));
        //double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString().replaceAll(",", ""));
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString().replaceAll(",", ""));
        double totalDiscountAmount = cashBackAmt + loyaltyAmt + Double.parseDouble(referralDiscount) + Double.parseDouble(firstBookingDiscount);
        return totalDiscountAmount;
    }

    private Double getTotalDiscountForCuopon() {
        double convFeeAmt = Double.parseDouble(convienceFeeTextView.getText().toString().replaceAll(",", ""));
        double cuopnAmt = Double.parseDouble(couponAmountTextView.getText().toString().replaceAll(",", ""));
        //double discAmt = Double.parseDouble(discountTextView.getText().toString());
        double cashBackAmt = Double.parseDouble(cashBackDiscountTextView.getText().toString().replaceAll(",", ""));
        double loyaltyAmt = Double.parseDouble(loyaltyDiscountTextView.getText().toString().replaceAll(",", ""));
        double totalDiscountAmount = cuopnAmt + loyaltyAmt + Double.parseDouble(referralDiscount) + Double.parseDouble(firstBookingDiscount);
        return totalDiscountAmount;
    }

    private void makeCouponNotAvailable() {
        couponEditText.setEnabled(false);

        couponEditText.setBackgroundColor(Color.TRANSPARENT);

        couponApplyTextView.setClickable(false);
        couponApplyTextView.setEnabled(false);
        couponApplyTextView.setCursorVisible(false);

        couponApplyTextView.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
    }

    private void makeCouponAvailable() {
        couponEditText.setEnabled(true);
        //couponEditText.setCursorVisible(true);
        //couponEditText.setKeyListener(null);
        couponEditText.setBackgroundColor(Color.WHITE);

        couponApplyTextView.setClickable(true);
        couponApplyTextView.setEnabled(true);
        couponApplyTextView.setCursorVisible(true);
        //couponApplyTextView.setKeyListener(BookingSummaryDetailActivity.this);
        couponApplyTextView.setBackground(getDrawable(R.drawable.vc_color_list));
    }

    private void makeCashBackNotAvailable() {
        useCashBackCb.setEnabled(false);
        useCashBackCb.setBackgroundColor(ContextCompat.getColor(BookingSummaryDetailActivity.this, R.color.grey));
    }

    private void makeCashBackAvailable() {
        useCashBackCb.setEnabled(true);
        useCashBackCb.setBackground(null);
    }


}