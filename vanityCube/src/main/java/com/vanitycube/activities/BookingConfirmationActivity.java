package com.vanitycube.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class BookingConfirmationActivity extends Activity implements OnClickListener {
    private Calendar startTime;
    private SharedPref pref;
    private boolean isGuestUser;
    private Gson gson;
    private UserModel userModel;
    private String userName;
    private String userNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.booking_confirmation);

        gson = new Gson();
        pref = new SharedPref(VcApplicationContext.getInstance());
        userModel = new UserModel();

        StringBuilder idsConcatenated = new StringBuilder();

        getUser();

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_PAYMENT_SUCCESS);

        ArrayList<String> bookingIds;

        bookingIds = getIntent().getStringArrayListExtra("bookingIds");
        String orderId = getIntent().getStringExtra("orderId");

        /*TextView mactivebook = findViewById(R.id.activebook);
        mactivebook.setOnClickListener(this);*/

        TextView userNameTextView = findViewById(R.id.userNameTextView);
        TextView mID = findViewById(R.id.orderIdTextView);

        String bookingPrefix = ApplicationSettings.BOOKING_ID_PREFIX;

        for (int i = 0; i < bookingIds.size(); i++) {
            String bookingId = bookingPrefix.concat(bookingIds.get(i));
            if (i != (bookingIds.size() - 1)) {
                bookingId += ",";
            }
            idsConcatenated.append(bookingId);
        }

        mID.setText(String.valueOf(idsConcatenated.toString()));
        mID.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BookingConfirmationActivity.this, MainSplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("showActiveBookings", true);
                startActivity(intent);
                finish();
                //break;
            }
        });

        if (isGuestUser) {
            userNameTextView.setText(userNumber);
        } else {
            userNameTextView.setText(userName);
        }

        if (orderId != null) {

            if (orderId.equalsIgnoreCase("-1")) {
                TextView payFail = findViewById(R.id.paymentFailed);
                payFail.setVisibility(View.VISIBLE);

                mID.setVisibility(View.GONE);

                TextView b1 = findViewById(R.id.bookingidtext);
                b1.setVisibility(View.GONE);

                LinearLayout l1 = findViewById(R.id.bookingOrderStatusLayout);
                l1.setVisibility(View.GONE);

                TextView b2 = findViewById(R.id.thankYouText);
                b2.setText(R.string.something_went_wrong_text);

            } else {

                pref.clearCartData();
                pref.putCouponModel("");
                pref.putGuestAddresses("");

                //Remove guest user flag and make guest user a normal user
                if (isGuestUser) {
                    userModel.setName(userNumber);
                    userModel.setEmail("");
                    userModel.setDOB("");
                    userModel.setReferralName("");
                    userModel.setGuest(false);
                    userModel.setNumberVerified(false);
                }

                pref.putUserModel(gson.toJson(userModel));

                /*TextView orderIdTextView = findViewById(R.id.orderIdTextView);
                orderIdTextView.setText(bookingPrefix.concat(orderId));*/
            }
        }

        setHeader();

        setFooter();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            String userId = userModel.getID();

            if (isValidString(userId)) {
                isGuestUser = userModel.getGuest();
                userName = userModel.getName();
                userNumber = userModel.getNumber();
            }
        }
    }

    private void setHeader() {
        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        Button mHeaderMenu = findViewById(R.id.headerMenu);
        mHeaderMenu.setVisibility(View.GONE);
        mHeaderText.setText(R.string.booking_confirmation_text);
        findViewById(R.id.cartLayout).setVisibility(View.GONE);
    }

    private void setFooter() {
        TextView FooterButton = findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText(R.string.go_to_home_text);
        FooterButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.footerButton:
                startDashBoardActivity();
                break;

            case R.id.activebook:
                Intent intent = new Intent(BookingConfirmationActivity.this, MainSplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("showActiveBookings", true);
                startActivity(intent);
                finish();
                break;

            default:
                break;
        }
    }

    private void startDashBoardActivity() {
        Intent dash = new Intent(BookingConfirmationActivity.this, MainSplashActivity.class);
        dash.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(dash);
        finish();
    }

    @Override
    public void onBackPressed() {
        startDashBoardActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_PAYMENT_SUCCESS);
    }

}
