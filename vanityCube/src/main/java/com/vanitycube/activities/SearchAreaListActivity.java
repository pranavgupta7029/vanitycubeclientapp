package com.vanitycube.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.GooglePlacesAutocompleteAdapter;
import com.vanitycube.adapter.SearchAreaAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.dbmodel.AreaModel;
import com.vanitycube.dbmodel.CityModel;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.BaseModel;
import com.vanitycube.model.SearchAreaModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.NaturalOrderComparator;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchAreaListActivity extends Activity implements OnClickListener, ServerResponseListener {
    private String TAG, checkYourInternet;
    private static final String UPDATE_LOCATION = "update_location";
    private static final String GET_LOCATION = "get_location";

    private RestWebServices mRestWebServices;
    private SharedPref pref;
    private Gson gson;

    private ListView commonListView;
    private ArrayList<SearchAreaModel> cityAreaNamesModelList;
    private HashMap<String, String> cityNameIdHash, areaNameIdHash, hubNameIdHash;
    private SearchAreaAdapter searchAreaAdapter;
    private String userId, selectedCityId, selectedCityName;
    private EditText searchCityEditText, searchAreaEditText;
    private Button locationText;

    private ArrayList<CityModel> allCitiesList;
    private ArrayList<AreaModel> allAreasList;

    private boolean appFirstRun;
    private RestWebServices restWebServices;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    Double latitude, longitude;
    public ProgressDialog progressDialog;
    public String scheme = null;
    private LocationManager locationManager;
    FusedLocationProviderClient mFusedLocationProviderClient;
    public Location mLastKnownLocation;
    private GooglePlacesAutocompleteAdapter dataAdapter;
    private TextView edEnterLocation, skip;
    private ListView listView;
    String lat, longi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_area_activity);

        mRestWebServices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());
        gson = new Gson();

        //If app is launched afresh
        appFirstRun = pref.getAppFirstRun();
        restWebServices = new RestWebServices();
        progressDialog = new ProgressDialog(SearchAreaListActivity.this);
        commonListView = findViewById(R.id.searcharealist);
        locationText = (Button) findViewById(R.id.locationButton);
        skip = (TextView) findViewById(R.id.skip);
        lat = pref.getLatitudeId();
        longi = pref.getLongitude();
        if(skip!=null) {
            skip.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (lat != null && longi != null) {
                        finish();
                    } else {
                        Toast.makeText(SearchAreaListActivity.this, "Please select a location", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        TAG = getResources().getString(R.string.search_area_list_activity_tag);
        checkYourInternet = getResources().getString(R.string.check_your_internet_text);

        getUser();

        setHeader();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        Intent intent = getIntent();
        String from = "";
        if (intent != null && intent.getExtras() != null) {
            //get action
            scheme = intent.getExtras().getString("scheme");
            from = intent.getExtras().getString("fromBooking");
            if (scheme != null)
                System.out.print(scheme);
        }
        if (from != null && !from.equals("")) {

        } else {
            verifyStoragePermissions(SearchAreaListActivity.this);
        }

        if (locationText != null) {
            locationText.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    verifyStoragePermissions(SearchAreaListActivity.this);
                }
            });
        }

        cityAreaNamesModelList = new ArrayList<>();
        edEnterLocation = (TextView) findViewById(R.id.edEnterLocation);
        //listView = (ListView) findViewById(R.id.listView1);
        //edEnterLocation.setPaintFlags(edEnterLocation.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        edEnterLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                findPlace();
            }
        });

        /*dataAdapter = new GooglePlacesAutocompleteAdapter(SearchAreaListActivity.this, R.layout.list_item);

        listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String str = (String) adapterView.getItemAtPosition(i);

                Toast.makeText(SearchAreaListActivity.this, str, Toast.LENGTH_SHORT).show();
                dataAdapter.notifyDataSetInvalidated();
                listView.setVisibility(View.GONE);

            }
        });

        //enables filtering for the contents of the given ListView
        listView.setTextFilterEnabled(true);

        edEnterLocation.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()>3) {
                    listView.setVisibility(View.VISIBLE);
                    dataAdapter.getFilter().filter(s.toString());
                }else{
                    listView.setVisibility(View.GONE);
                }
            }
        });*/


        //loadCities();
        /*PlaceAutocompleteFragment places = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        places.setHint("Search Location");
        places.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Toast.makeText(getApplicationContext(), place.getName() + "lang" + place.getLatLng(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {

                Toast.makeText(getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();

            }
        });*/
    }

    public void findPlace() {
        try {
            Intent intent =
                    new PlaceAutocomplete
                            .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, 1);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                // retrive the data by using getPlace() method.
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.e("Tag", "Place: " + place.getLatLng() + place.getPhoneNumber());
                //Toast.makeText(SearchAreaListActivity.this, place.getLatLng() + "", Toast.LENGTH_SHORT).show();
                if (place.getLatLng() != null) {
                    LatLng latlng = place.getLatLng();
                    Double lat = latlng.latitude;
                    Double longi = latlng.longitude;
                    this.lat = latlng.latitude + "";
                    this.longi = latlng.longitude + "";
                    getLocation(lat, longi);
                }
                //getLocation(place.getLatLng());

                /*((TextView) findViewById(R.id.searched_address))
                        .setText(place.getName()+",\n"+
                                place.getAddress() +"\n" + place.getPhoneNumber());*/

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.e("Tag", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(SearchAreaListActivity.this, "Cannot get location", Toast.LENGTH_SHORT).show();
                } else {
                    verifyStoragePermissions(SearchAreaListActivity.this);
                }
            }
        }
    }

    /**
     * Checks if the app has permission to write to device location
     * <p/>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity the activity from which permissions are checked
     */
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        } else {

            getDeviceLocation();
            /*LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            // get the last know location from your location manager.
            Location location= getLastKnownLocation();//locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
           // Toast.makeText(SearchAreaListActivity.this, "Latitude = "+location.getLatitude() +" and longitude = "+location.getLongitude(), Toast.LENGTH_SHORT).show();
            if(location==null){
                location= locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if(location!=null)
                getLocation(location.getLatitude(),location.getLongitude());
            else
                getLastLocationNewMethod();*/
            //Toast.makeText(this,"Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
        }
    }


    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            Task locationResult = mFusedLocationProviderClient.getLastLocation();
            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = (Location) task.getResult();
                        if (mLastKnownLocation != null) {
                            lat = mLastKnownLocation.getLatitude() + "";
                            longi = mLastKnownLocation.getLongitude() + "";
                            getLocation(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
                        }

                    } else {
                        Toast.makeText(SearchAreaListActivity.this, "Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }



    /*private void getLastLocationNewMethod(){
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            //getAddress(location);
                            getLocation(location.getLatitude(),location.getLongitude());
                        }else{
                            Toast.makeText(SearchAreaListActivity.this,"Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(SearchAreaListActivity.this,"Couldn't get location please select manually", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                });
    }*/



    /*private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        String providers = mLocationManager.getBestProvider(criteria,false);
        Location bestLocation = null;
        bestLocation = mLocationManager.getLastKnownLocation(providers);
        *//*for (String provider : providers) {
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }*//*
        return bestLocation;
    }*/


    private void loadCities() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetAllCitiesAsyncTask().execute(null, null, null);
        } else {
            showNoInternetDialog();
        }
    }

    private void loadAreas() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetAllAreasAsyncTask().execute(null, null, null);
        } else {
            showNoInternetDialog();
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();
        }
    }

    private void setHeader() {
        Button mHeaderMenu = findViewById(R.id.headerMenu);
        mHeaderMenu.setVisibility(View.GONE);

        //Set the back arrow image
        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        mHeaderImage.setBackgroundResource(R.drawable.header_body);
        mHeaderText.setText("Select Location");
        mHeaderText.setVisibility(View.VISIBLE);
        findViewById(R.id.cartLayout).setVisibility(View.GONE);

        //Make visible Select City and Select Area
        LinearLayout lSearchLayout = findViewById(R.id.newSelectCityAreaLayout);
        lSearchLayout.setVisibility(View.GONE);
        searchCityEditText = findViewById(R.id.cityEditText);
        searchAreaEditText = findViewById(R.id.areaEditText);

        if (!(pref.getCityId().equalsIgnoreCase("-1")))
            searchCityEditText.setText(pref.getCityName());

        searchCityEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setCityAdapter();
                }
            }
        });

        //If area found
        if (!(pref.getAreaId().equalsIgnoreCase("-1"))) {
            searchAreaEditText.setText(pref.getAreaName());
        }

        searchAreaEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (pref.getCityId().equalsIgnoreCase("-1")) setCityAdapter();
                    else setAreaAdapter(pref.getCityId());
                }
            }
        });

        //Watcher for any changes in the edit text
        TextWatcher tw = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isValidString(String.valueOf(s)) && searchAreaAdapter != null)
                    searchAreaAdapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        searchCityEditText.addTextChangedListener(tw);
        searchAreaEditText.addTextChangedListener(tw);

    }

    @SuppressWarnings("unchecked")
    private void setCityAdapter() {
        searchCityEditText.requestFocus();

        if (allCitiesList == null) {
            return;
        }

        cityNameIdHash = new HashMap<>();

        //Populate cityNameId hash map
        for (CityModel cityModel : allCitiesList) {
            cityNameIdHash.put(cityModel.getName(), cityModel.getCityId());
        }

        String[] cities = cityNameIdHash.keySet().toArray(new String[cityNameIdHash.size()]);
        Arrays.sort(cities);

        List<String> cityNamesList = Arrays.asList(cities);
        Collections.shuffle(cityNamesList);
        Collections.sort(cityNamesList, new NaturalOrderComparator());

        //Flushing
        cityAreaNamesModelList.clear();

        for (String i : cityNamesList) {
            SearchAreaModel searchAreaModel = new SearchAreaModel();
            searchAreaModel.setName(i);
            cityAreaNamesModelList.add(searchAreaModel);
        }

        searchAreaAdapter = new SearchAreaAdapter(SearchAreaListActivity.this, cityAreaNamesModelList);

        commonListView.setAdapter(searchAreaAdapter);

        commonListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView lTextView = (TextView) view;
                selectedCityId = cityNameIdHash.get(lTextView.getText().toString());
                selectedCityName = lTextView.getText().toString();
                pref.putCityId(selectedCityId);
                pref.putCityName(selectedCityName);
                searchCityEditText.setText(selectedCityName);
                setAreaAdapter(selectedCityId);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void setAreaAdapter(final String city_id) {
        searchAreaEditText.requestFocus();

        if (allAreasList == null) return;

        areaNameIdHash = new HashMap<>();
        hubNameIdHash = new HashMap<>();

        for (AreaModel lModel : allAreasList) {
            if (lModel.getCityId().equalsIgnoreCase(city_id))
                areaNameIdHash.put(lModel.getName(), lModel.getArea_id());
            hubNameIdHash.put(lModel.getName(), lModel.getHub_id());
        }

        String[] areas = areaNameIdHash.keySet().toArray(new String[areaNameIdHash.size()]);
        Arrays.sort(areas);
        List<String> scrambled = Arrays.asList(areas);
        Collections.shuffle(scrambled);
        System.out.println("Scrambled: " + scrambled);
        Collections.sort(scrambled, new NaturalOrderComparator());
        cityAreaNamesModelList.clear();

        for (String i : scrambled) {
            SearchAreaModel notify = new SearchAreaModel();
            notify.setName(i);
            cityAreaNamesModelList.add(notify);
        }

        searchAreaAdapter = new SearchAreaAdapter(SearchAreaListActivity.this, cityAreaNamesModelList);
        commonListView.setAdapter(searchAreaAdapter);

        commonListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView lTextView = (TextView) view;
                String areaID = areaNameIdHash.get(lTextView.getText().toString());
                String hubID = hubNameIdHash.get(lTextView.getText().toString());
                updateLocation(areaID, hubID);

                pref.putAreaId(areaID);
                pref.putHubId(hubID);
                pref.putAreaName(lTextView.getText().toString());
                pref.clearCartData();
            }
        });
    }

    private void updateLocation(String areaID, String hubID) {
        JSONObject data = new JSONObject();

        try {
            data.put("area_id", areaID);
            data.put("hub_id", hubID);
            data.put("user_id", userId);
        } catch (JSONException e) {
            //Log.e("SearchAreaListException Exception:", e.getMessage());
        }

        ApiManager apiManager = new ApiManager(this, this);
        apiManager.doJsonParsing();
        apiManager.setClassTypeForJson(BaseModel.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, ApplicationSettings.METHOD_UPDATE_USER_LOCATION);
        params.put(ApplicationSettings.PARAM_DATA, data.toString());

        apiManager.getStringGetResponse(UPDATE_LOCATION, ApplicationSettings.UPDATE_LOCATION_URL, params);
    }

    private void getLocation(Double lat, Double longi) {
        latitude = lat;
        longitude = longi;
        pref.putLatitude(latitude + "");
        pref.putLongitude(longitude + "");
        new GetLocationAsyncTask().execute(null, null, null);

    }


    private class GetLocationAsyncTask extends AsyncTask<Void, Void, Map<String, String>> {
        Map<String, String> resultArrayList;


        GetLocationAsyncTask() {

        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Getting Location...");
                progressDialog.show();
            }

        }

        protected Map<String, String> doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            return restWebServices.getLocation(latitude, longitude);
        }

        protected void onPostExecute(Map<String, String> result) {
            super.onPostExecute(resultArrayList);

            try {
                if (progressDialog != null) {
                    /*progressDialog.dismiss();
                    progressDialog = null;*/
                }
                if (result.size() > 0) {
                    resultArrayList = result;

                    /*"areaName": "Maruti Industrial Area",
                            "hub_id": "7",
                            "cityId": "8",
                            "cityName": "Gurgaon",
                            "latitude": "28.48971",
                            "longitude": "77.062282",
                            "distance": 0.10877255733206*/

                    pref.putCityId(resultArrayList.get("cityId"));
                    pref.putCityName(resultArrayList.get("cityName"));
                    searchCityEditText.setText(resultArrayList.get("cityName"));


                    pref.putAreaId(resultArrayList.get("areaId"));
                    pref.putHubId(resultArrayList.get("hub_id"));
                    pref.putAreaName(resultArrayList.get("areaName"));
                    pref.clearCartData();
                    updateLocation(resultArrayList.get("areaId"), resultArrayList.get("hub_id"));
                    //createViewpager();
                } else {
                    Toast.makeText(SearchAreaListActivity.this, "Sorry We didnt serve in this area at this moment....", Toast.LENGTH_SHORT).show();
                }

                //new DashboardActivity.GetOffersAsyncTask().execute(null, null, null);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  API RESPONSES
        |
        -------------------------------------
    */

    @Override
    public void positiveResponse(String TAG, String response) {
    }

    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (appFirstRun) {
            appFirstRun = false;
            pref.putAppFirstRun(appFirstRun);
        }
        /*if (Objects.equals(TAG, GET_LOCATION) && responseObj instanceof LocationResponseModel) {
            LocationResponseModel locationResponseModel = (LocationResponseModel) responseObj;

            if (locationResponseModel.getResponsedata().isSuccess() == 1) {
                pref.putCityId(locationResponseModel.getResponsedata().getResult().getCityId());
                pref.putCityName(locationResponseModel.getResponsedata().getResult().getCityName());
                searchCityEditText.setText(locationResponseModel.getResponsedata().getResult().getCityName());


                pref.putAreaId(locationResponseModel.getResponsedata().getResult().getAreaId());
                pref.putHubId(locationResponseModel.getResponsedata().getResult().getHub_id());
                pref.putAreaName(locationResponseModel.getResponsedata().getResult().getAreaName());
                pref.clearCartData();
                //updateLocation(locationResponseModel.getResponsedata().getResult().getAreaId(), locationResponseModel.getResponsedata().getResult().getHub_id());
            } else {
                Toast.makeText(SearchAreaListActivity.this, "Error: Checksum generation failed, " + "Please try again", Toast.LENGTH_SHORT).show();
            }

        }else {*/
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        gotoDashboard(true);
        //}
    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {
        showNegativeResponseDialog();
    }

    private void showNegativeResponseDialog() {
        String negativeResponseMessage = getResources().getString(R.string.negative_response_message);

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.error_text));
        dialogBuilder.setMessage(negativeResponseMessage);

        if (appFirstRun) {
            dialogBuilder.setPositiveButton(getString(R.string.reload_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    reloadActivity();
                }
            });

            dialogBuilder.setNegativeButton(R.string.exit_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            dialogBuilder.setPositiveButton(getString(R.string.retry_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    reloadActivity();
                }
            });

            dialogBuilder.setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    gotoDashboard(false);
                }
            });
        }

        dialogBuilder.setCancelable(false);
        if (!((Activity) SearchAreaListActivity.this).isFinishing() || !((Activity) SearchAreaListActivity.this).isDestroyed()) {
            //show dialog
            dialogBuilder.show();
        }


    }

    private void showNoInternetDialog() {
        String noInternetMessage = getResources().getString(R.string.no_internet_message);

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.error_text));
        dialogBuilder.setMessage(noInternetMessage);

        dialogBuilder.setPositiveButton(getString(R.string.reload_text), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                reloadActivity();
            }
        });

        if (appFirstRun) {
            dialogBuilder.setPositiveButton(getString(R.string.reload_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    reloadActivity();
                }
            });

            dialogBuilder.setNegativeButton(R.string.exit_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else {
            dialogBuilder.setPositiveButton(getString(R.string.retry_text), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    reloadActivity();
                }
            });

            dialogBuilder.setNegativeButton(R.string.cancel_text, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    gotoDashboard(false);
                }
            });
        }

        dialogBuilder.setCancelable(false);
        dialogBuilder.show();
    }

    /*  -------------------------------------
        |
        |  GET ALL CITIES ASYNC TASK
        |
        -------------------------------------
    */

    private class GetAllCitiesAsyncTask extends AsyncTask<Void, Void, Bundle> {
        ProgressDialog progressDialog;

        GetAllCitiesAsyncTask() {
            progressDialog = new ProgressDialog(SearchAreaListActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading Cities");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            VcDatabaseQuery vcDatabaseQuery = new VcDatabaseQuery();
            vcDatabaseQuery.clearAreaTable();
        }

        @Override
        protected Bundle doInBackground(Void... params) {
            return mRestWebServices.getCityListNew();
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);
            if (SearchAreaListActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
                return;
            }

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (result.getBoolean("success", false)) {
                allCitiesList = result.getParcelableArrayList("cities");
                loadAreas();
            } else {
                showNegativeResponseDialog();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GET ALL AREAS ASYNC TASK
        |
        -------------------------------------
    */

    private class GetAllAreasAsyncTask extends AsyncTask<Void, Void, Bundle> {
        GetAllAreasAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bundle doInBackground(Void... params) {
            return mRestWebServices.getAllAreasNew();
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);

            if (result != null && result.getBoolean("success", true)) {
                allAreasList = result.getParcelableArrayList("areas");

                String selectedCityId = pref.getCityId();

                //If we already have a city selected
                if (selectedCityId.equalsIgnoreCase("-1")) {
                    //Let user select city
                    setCityAdapter();
                } else {
                    //Auto select city and let user select area
                    setAreaAdapter(selectedCityId);
                }
            } else {
                showNegativeResponseDialog();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void reloadActivity() {
        Intent reloadIntent = getIntent();
        startActivity(reloadIntent);
        finish();
    }

    public void gotoDashboard(Boolean mode) {
        //Clear first run flag
        pref.putAppFirstRun(false);

        //Intent intent = new Intent(SearchAreaListActivity.this, DashboardActivity.class);
        Intent intent = new Intent(SearchAreaListActivity.this, MainSplashActivity.class);
        intent.putExtra("scheme", scheme);
        //startActivity(i);

        if (mode) {
            setResult(RESULT_OK, intent);
            finish();
        } else {
            setResult(RESULT_CANCELED, intent);
            finish();
        }

        startActivity(intent);
        finish();
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.footerButton:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        String areaId = pref.getAreaId();
        String hubId = pref.getHubId();

        if (appFirstRun) {
            finish();
        } else {
            if (!areaId.equalsIgnoreCase("-1") && !hubId.equalsIgnoreCase("-1")) {
                gotoDashboard(true);
            } else {
                gotoDashboard(false);
            }
        }
    }

}