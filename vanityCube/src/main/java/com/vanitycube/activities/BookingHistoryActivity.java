package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.adapter.BookingListAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.BookingListModel;
import com.vanitycube.model.CancelBookingResponseModel;
import com.vanitycube.webservices.RestWebServices;

import java.util.ArrayList;

public class BookingHistoryActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RestWebServices mRestWebServices;
    private ArrayList<BookingListModel> bookingsList;
    private ListView bookingsListView;
    private Boolean activeBookings;
    public BookingListAdapter notifyAdapter;
    private TextView emptyTextView;
    private String checkYourInternet;
    private SharedPref pref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_booking_history);
        MainSplashActivity.changeHeader = true;

        context = BookingHistoryActivity.this;

        bookingsListView = findViewById(R.id.bookingHistoryListView);
        emptyTextView = findViewById(R.id.empty);

        checkYourInternet = getResources().getString(R.string.check_your_internet_text);
        mRestWebServices = new RestWebServices();
        bookingsList = new ArrayList<>();
        activeBookings = getIntent().getExtras().getBoolean("active_bookings");
        pref = new SharedPref(VcApplicationContext.getInstance());

        setHeader();
        getActiveBookings();
    }

    private void setHeader() {
        RelativeLayout headerLayout = findViewById(R.id.header);
        Button headerMenuButton = headerLayout.findViewById(R.id.headerMenu);
        headerMenuButton.setBackgroundResource(R.drawable.arrow_left);
        headerLayout.findViewById(R.id.headerFirstButton).setOnClickListener(this);
        headerMenuButton.setOnClickListener(this);
        TextView mHeaderText = headerLayout.findViewById(R.id.headerText);
        headerLayout.findViewById(R.id.cartLayout).setVisibility(View.GONE);
        mHeaderText.setText(R.string.booking_history_activity_tag);
    }

    private void populateBookingList(Boolean activeBookings) {

        //bookingsList = new ArrayList<>();

        /*notifyAdapter = new BookingListAdapter(BookingHistoryActivity.this, bookingsList, activeBookings);
        bookingsListView.setEmptyView(emptyTextView);
        bookingsListView.setAdapter(notifyAdapter);*/

        if (bookingsList.size() > 0) {
            notifyAdapter = new BookingListAdapter(BookingHistoryActivity.this, bookingsList, activeBookings);
            bookingsListView.setEmptyView(emptyTextView);
            if (bookingsList.size() > 0)
                emptyTextView.setVisibility(View.GONE);
            bookingsListView.setAdapter(notifyAdapter);
        } else {
            emptyTextView.setVisibility(View.VISIBLE);
        }

       /* notifyAdapter = new NotificationAdapter(getActivity(), activities);
        activitiesList.setEmptyView(empty);
        activitiesList.setAdapter(notifyAdapter);*/

        bookingsListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        notifyAdapter.setOnCancelBookingClickedListener(new BookingListAdapter.BookingListAdapterInterface() {

            @Override
            public void onCancelBookingClicked(String bookingId) {
                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    new CancelBookingAsyncTask(bookingId).execute(null, null, null);
                } else {
                    Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onItemClicked(BookingListModel bookingListModel) {
                gotoActiveBooking(bookingListModel);
            }

        });
    }

    /*  -------------------------------------
        |
        |  CANCEL BOOKING ASYNC TASK
        |
        -------------------------------------
    */

    class CancelBookingAsyncTask extends AsyncTask<Void, Void, CancelBookingResponseModel> {
        ProgressDialog progressDialog;
        CancelBookingResponseModel cancelBookingResponseModel;
        String bookingId;

        CancelBookingAsyncTask(String bookingId) {
            progressDialog = new ProgressDialog(context);
            this.bookingId = bookingId;
            cancelBookingResponseModel = new CancelBookingResponseModel();
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Cancelling");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected CancelBookingResponseModel doInBackground(Void... url) {
            return mRestWebServices.cancelBooking(bookingId);
        }

        @Override
        protected void onPostExecute(CancelBookingResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                int success = result.getSuccess();
                String message = result.getBookingDetails();

                if (success == 1) {
                    Toast.makeText(context, "Booking cancelled successfully", Toast.LENGTH_LONG).show();
                    getActiveBookings();
                } else {
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GET ACTIVE BOOKING ASYNC TASK
        |
        -------------------------------------
    */
    public void getActiveBookings() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetBookingsAsyncTask(activeBookings).execute(null, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    class GetBookingsAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        boolean activeBookings;

        GetBookingsAsyncTask(boolean activeBookings) {
            progressDialog = new ProgressDialog(context);
            this.activeBookings = activeBookings;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            if (activeBookings) {
                bookingsList = mRestWebServices.getActiveBooking();
            } else {
                bookingsList = mRestWebServices.getHistoryBooking();
            }

            return bookingsList.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                populateBookingList(activeBookings);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    private void gotoActiveBooking(BookingListModel bookingListModel) {
        Intent intent = new Intent(context, ActiveBookingActivity.class);
        intent.putExtra("booking_list_model", bookingListModel);
        startActivity(intent);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerFirstButton:
            case R.id.headerMenu:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //pref.putReloadRequired(true);
        /*Intent i = new Intent(BookingHistoryActivity.this,MainSplashActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);*/
        finish();
    }
}
