package com.vanitycube.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;

public class MobiKwikActivity extends Activity implements View.OnClickListener {
    private String orderId;
    private Boolean success = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mobikwik);

        //Firebase
        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_BOOKING_SUMMARY);

        //Set header
        setHeader();

        //Fetch intent params
        String url = getIntent().getStringExtra("url");
        orderId = getIntent().getStringExtra("orderId");
        loadWebview(url);

    }

    private void setHeader() {
        Button mMenuButton = (Button) findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);
        findViewById(R.id.headerFirstButton).setOnClickListener(this);
        TextView mHeaderText = (TextView) findViewById(R.id.headerText);
        ImageView mHeaderImage = (ImageView) findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText(R.string.online_payment_text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerMenu:
                confirmClose();
                break;
        }
    }

    public void confirmClose() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Close")
                .setMessage("Do you really want to end the current session and return to previous screen? " +
                        "Note that this will end the current transaction if taking place.")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                }).setNegativeButton(android.R.string.no, null).show();
    }

    public void loadWebview(String url) {
        WebView webview = (WebView) this.findViewById(R.id.onlinePaymentView);
        webview.requestFocusFromTouch();
        webview.setVerticalScrollBarEnabled(true);
        webview.setHorizontalScrollBarEnabled(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.indexOf("complete?orderId=" + orderId) > -1) goBack(success);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }
        });

        if (webview != null) {
            webview.loadUrl(url);
        }
    }

    public void goBack(Boolean success) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("orderId", orderId);

        if (success) {
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else {
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
    }
}