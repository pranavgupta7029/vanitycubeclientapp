package com.vanitycube.activities;

// Created by prade on 1/4/2018.

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.ConfigurationModel;

public class TermsActivity extends Activity implements View.OnClickListener {
    private SharedPref pref;
    private Gson gson;
    private String content;
    private WebView termsWebView;
    private int contentType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_terms);

        pref = new SharedPref(VcApplicationContext.getInstance());
        gson = new Gson();

        Bundle bundle = getIntent().getExtras();

        String activityTitle;

        if (bundle != null) {
            contentType = bundle.getInt("content_type");
        } else {
            contentType = 0;
        }

        getConfiguration();

        termsWebView = findViewById(R.id.termsWebView);

        if (contentType == 0) {
            activityTitle = getResources().getString(R.string.terms_and_conditions_text);
        } else {
            activityTitle = getResources().getString(R.string.privacy_policy_text);
        }

        setHeader(activityTitle);

        setHtmlInWebView(content);
    }

    private void getConfiguration() {
        String json = pref.getConfiguration();

        ConfigurationModel configurationModel = gson.fromJson(json, new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            if (contentType == 0) {
                content = configurationModel.getTerms();
            } else {
                content = configurationModel.getPrivacyPolicy();
            }
        } else {
            content = "";
        }
    }

    private void setHeader(String title) {
        RelativeLayout headerLayout = findViewById(R.id.header);
        Button headerMenuButton = headerLayout.findViewById(R.id.headerMenu);
        headerMenuButton.setBackgroundResource(R.drawable.arrow_left);
        headerLayout.findViewById(R.id.headerFirstButton).setOnClickListener(this);
        headerMenuButton.setOnClickListener(this);
        TextView mHeaderText = headerLayout.findViewById(R.id.headerText);
        headerLayout.findViewById(R.id.cartLayout).setVisibility(View.GONE);
        mHeaderText.setText(title);
    }

    private void setHtmlInWebView(String htmlStringified) {
        termsWebView.loadData(htmlStringified, "text/html", null);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerFirstButton:
            case R.id.headerMenu:
                onBackPressed();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
