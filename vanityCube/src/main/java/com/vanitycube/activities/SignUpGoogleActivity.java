package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Created by prade on 11/6/2017.

public class SignUpGoogleActivity extends Activity implements View.OnClickListener {
    private EditText contactEditText, referralEditText;
    private RestWebServices restWebservices;
    private boolean checked = false;
    private ImageView termsCheck;
    private String personName, email, googleId;
    private RadioButton radioButtonMale;
    private int RC_OTP = 606;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_up_google);
        init();
    }

    private void init() {
        restWebservices = new RestWebServices();

        TextView mSignUpText = findViewById(R.id.googleSignUpText);
        contactEditText = findViewById(R.id.contact_signup_google);
        referralEditText = findViewById(R.id.referral_google);

        LinearLayout mGenderLayout = findViewById(R.id.genderLinearLayout);
        googleId = getIntent().getStringExtra("google_id");
        personName = getIntent().getStringExtra("personName");
        email = getIntent().getStringExtra("email");

        String mGender = "";

        if (mGender.equalsIgnoreCase("")) {
            mGenderLayout.setVisibility(View.GONE);
        }

        mSignUpText.setText(getResources().getString(R.string.hi_prefix).concat(personName));

        LinearLayout termsCheckLayout = findViewById(R.id.check_terms_conditions_layout);
        termsCheck = findViewById(R.id.check_terms_conditions);

        TextView mTerms = findViewById(R.id.terms_link);
        Button mSignUpButton = findViewById(R.id.signup_button_signup);
        TextView mLoginButton = findViewById(R.id.login_signup_textview);

        termsCheckLayout.setOnClickListener(this);
        termsCheck.setOnClickListener(this);
        mTerms.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);

        radioButtonMale = findViewById(R.id.radioMale);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sign_up_facebook, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    class SignUpAsyncTask extends AsyncTask<Void, Void, String[]> {
        ProgressDialog progressDialog;
        String name, userEmail, password, mobile, referral, gender;
        String[] successMessageArray = {"0", "Unknown Reasons!"};

        SignUpAsyncTask(String pName, String pUserEmail, String pPassword, String mMobile, String mReferral) {
            this.name = pName;
            this.userEmail = pUserEmail;
            this.password = pPassword;
            this.mobile = mMobile;
            this.referral = mReferral;

            if (radioButtonMale.isChecked()) {
                gender = "1";
            } else {
                gender = "2";
            }

            progressDialog = new ProgressDialog(SignUpGoogleActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Sign Up");
            progressDialog.show();
        }

        @Override
        protected String[] doInBackground(Void... url) {
            try {
                successMessageArray = restWebservices.addNewUserGoogle(this.name, this.userEmail, this.mobile, this.referral, gender);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return successMessageArray;
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);

            try {

                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result[0].equalsIgnoreCase("1")) {
                    Intent intent = new Intent(SignUpGoogleActivity.this, OtpActivity.class);
                    intent.putExtra("google_id", googleId);
                    startActivityForResult(intent, RC_OTP);
                } else {
                    Toast.makeText(SignUpGoogleActivity.this, "SignUp Failed, Reason:" + successMessageArray[1], Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                String TAG = "SignUpGoogleActivity";
                Log.e(TAG, "<<Exception on SignUpFacebook>>" + e.getMessage());
            }
        }
    }

    private boolean phoneValidator(String pMobile) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        Pattern pattern = Pattern.compile(regexStr);
        Matcher matcher2 = pattern.matcher(pMobile);
        return matcher2.matches();
    }

    private void signUp() {

        String mMobileString = contactEditText.getText().toString().trim();
        String mReferralString = referralEditText.getText().toString().trim();

        if (!phoneValidator(mMobileString)) {
            Toast.makeText(SignUpGoogleActivity.this, "Please enter a valid mobile number.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!checked) {
            Toast.makeText(SignUpGoogleActivity.this, "Please Agree with Terms and conditions.", Toast.LENGTH_LONG).show();
            return;
        }

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new SignUpAsyncTask(personName, email, "", mMobileString, mReferralString).execute(null, null, null);
        } else {
            Toast.makeText(SignUpGoogleActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
        }

    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    private void gotoTerms(int contentType) {
        Intent intent = new Intent(SignUpGoogleActivity.this, TermsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("content_type", contentType);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_OTP) {
            if (resultCode == RESULT_OK) {
                goBack(true);
            }
        }
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.login_signup_textview:
                Intent loginActivity = new Intent(getApplicationContext(), SignInPageActivity.class);
                startActivity(loginActivity);
                break;

            case R.id.signup_button_signup:
                signUp();
                break;

            case R.id.check_terms_conditions_layout:
            case R.id.check_terms_conditions:
                checked = !checked;
                if (checked) termsCheck.setBackgroundResource(R.drawable.check_box_with);
                else termsCheck.setBackgroundResource(R.drawable.check_box_without);
                break;

            case R.id.terms_link:
                gotoTerms(0);
                break;

            default:
                break;

        }
    }
}