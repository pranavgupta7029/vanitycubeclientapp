package com.vanitycube.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.CustomPagerAdapterDashboard;
import com.vanitycube.adapter.LeftDrawerAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.fonts.VCButton;
import com.vanitycube.fragments.GalleryFragment;
import com.vanitycube.fragments.MainSplashFragment;
import com.vanitycube.fragments.NotificationFragment;
import com.vanitycube.fragments.ProfileFragment;
import com.vanitycube.fragments.SettingsFragment;
import com.vanitycube.model.Banner;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.NavigationDrawerItem;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.model.ServiceModelNewAPI;
import com.vanitycube.model.ServiceModelResponse;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.utilities.RoundedImageView;
import com.vanitycube.utilities.Strings;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

public class MainSplashActivity extends FragmentActivity implements View.OnClickListener, ServerResponseListener {

    private GridView gridView;
    private ViewPager viewPager;
    CustomPagerAdapterDashboard customPagerAdapter;

    private int dotsCount;

    private ImageView[] dots;

    private Context context;
    private ArrayList<Banner> bannerItemsList;
    private RestWebServices restWebServices;
    private SharedPref pref;
    private FireBaseHelper firebaseHelper;
    private String TAG;
    private Gson gson;
    private ArrayList<ServiceModel> allServicesWithServiceTypesList;
    private TextView headerTitleTextView;
    private LinearLayout headerAreaLayout;
    private TextView headerAreaTextView;
    private View headerView, drawerHeaderView;
    private DrawerLayout drawerLayout;
    private ListView drawerListView;
    private LeftDrawerAdapter drawerAdapter;
    ProgressDialog progressDialog;

    private Calendar startTime;
    private NavigationDrawerItem drawerItems;
    private String userId, userName, userProfilePhotoPath;
    private LinearLayout mainLayout;

    private boolean isLoggedIn, isGuest;
    private boolean firstRunDashboardActivity, isInviteVisible, showActiveBooking, showNotification, otherFragmentsActive;
    private boolean[] drawerItemsLoggedOut = {true, true, true, false, false, true, true, true, false};
    private boolean[] drawerItemsLoggedIn = {true, true, true, true, true, true, true, true, true};

    public String areaName, areaID, hubID, configNumber, json, checkYourInternet, couponLabel, cityName;
    private static final int RC_CALL_PHONE = 200;
    private int RC_DASH_LOGIN = 201;
    private int RC_SELECT_AREA = 202;
    public static String scheme;
    boolean doubleBackToExitPressedOnce = false;
    public static boolean changeHeader = false;
    private ApiManager apiManager;
    private static final String GET_SERVICES = "getServices";
    private ArrayList<ServiceModelResponse> serviceModelResponse = new ArrayList<>();


    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.main_splash_screen);

        Intent intent = getIntent();

        if (intent != null && intent.getExtras() != null) {
            //get action
            scheme = intent.getExtras().getString("scheme");
            if (scheme != null)
                System.out.print(scheme);
        }

        context = MainSplashActivity.this;

        TAG = "MainSplashActivity";
        checkYourInternet = getResources().getString(R.string.check_your_internet_text);
        couponLabel = getResources().getString(R.string.coupon_code_text);

        pref = new SharedPref(VcApplicationContext.getInstance());
        restWebServices = new RestWebServices();
        gson = new Gson();
        bannerItemsList = new ArrayList<>();

        firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_CATEGORY);
        progressDialog = new ProgressDialog(MainSplashActivity.this);
        apiManager = new ApiManager(MainSplashActivity.this, MainSplashActivity.this);
        /*mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        mainLayout.setVisibility(View.VISIBLE);*/

        //Get all shared preferences data
        getSharedPrefData();

        isInviteVisible = false;
        showActiveBooking = false;
        showNotification = false;
        firstRunDashboardActivity = true;

        //Get user
        getUser();

        //Get configuration data
        getConfiguration();
        getBanners();

        //Common header section
        setHeader();

        drawerLayout = findViewById(R.id.dash_drawer_layout);
        if (drawerLayout != null)
            drawerLayout.setVisibility(View.VISIBLE);
        //Load drawer
        loadDrawer();

        //Check if we need to show active bookings
        showActiveBooking = getIntent().getBooleanExtra("showActiveBookings", false);


        allServicesWithServiceTypesList = new ArrayList<>();

        /*gridView = (GridView)findViewById(R.id.gridview);
        gridView.setBackgroundColor(Color.WHITE);
        gridView.setVerticalSpacing(1);
        gridView.setHorizontalSpacing(1);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent i = new Intent(MainSplashActivity.this, DashboardActivity.class);
                i.putParcelableArrayListExtra("serviceList",allServicesWithServiceTypesList);
                i.putExtra("serviceName",allServicesWithServiceTypesList.get(position).getServiceName());
                startActivity(i);
            }
        });

        viewPager = findViewById(R.id.DashPager);

        bannerItemsList = new ArrayList<>();*/


        /*if (showActiveBooking) {
            showActiveBooking = false;
            gotoBookingHistory(true);
        } else if (showNotification) {
            showNotification = false;
            loadNotificationsFragment();
        } else {
            loadDashFrag();
        }*/


        //loadDashFrag();

        /*Button crashButton = new Button(this);
        crashButton.setText("Crash!");
        crashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Crashlytics.getInstance().crash(); // Force a crash
            }
        });
        addContentView(crashButton,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));*/
        updateCart();
        //getServices();

    }

    private void setHeader() {
        headerView = View.inflate(getApplicationContext(), R.layout.common_header_new, null);

        LinearLayout headerMenuButtonLayout = headerView.findViewById(R.id.headerFirstButton);
        headerMenuButtonLayout.setOnClickListener(this);

        Button headerMenuButton = headerView.findViewById(R.id.headerMenu);
        headerMenuButton.setOnClickListener(this);

        headerTitleTextView = headerView.findViewById(R.id.headerText);
        if (headerTitleTextView != null)
            headerTitleTextView.setVisibility(View.GONE);

        headerAreaLayout = headerView.findViewById(R.id.areaLayout);
        if (headerAreaLayout != null)
            headerAreaLayout.setVisibility(View.VISIBLE);

        Button search = headerView.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainSplashActivity.this, DashboardActivity.class);
                i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
                i.putExtra("serviceName", "Packages");
                i.putExtra("search", "true");
                startActivity(i);
            }
        });

        headerAreaTextView = headerView.findViewById(R.id.areaTextView);
        Button titleDownArrow = headerView.findViewById(R.id.titleDownArrow);
        titleDownArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSearchAreaList();
            }
        });
        headerAreaTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSearchAreaList();
            }
        });


        //Set cart view and click action
        RelativeLayout cartLayout = headerView.findViewById(R.id.cartLayout);
        if (cartLayout != null) {
            cartLayout.setVisibility(View.VISIBLE);
            cartLayout.setOnClickListener(this);
            //cartLayout.setVisibility(View.GONE);
        }

        RelativeLayout whatsappLayout = headerView.findViewById(R.id.whatsAppLayout);
        if (whatsappLayout != null) {
            whatsappLayout.setVisibility(View.GONE);
            whatsappLayout.setOnClickListener(this);
            whatsappLayout.setVisibility(View.GONE);
        }

        VCButton whatappButton = headerView.findViewById(R.id.whatsAppButton);
        if (whatappButton != null)
            whatappButton.setVisibility(View.GONE);
        // whatappButton.setOnClickListener(this);


        Button cartButton = headerView.findViewById(R.id.cartButton);
        cartButton.setOnClickListener(this);

        TextView cartQuantityTextView = headerView.findViewById(R.id.cartQuantityTextView);
        cartQuantityTextView.setOnClickListener(this);
    }

    public void getSharedPrefData() {
        areaID = pref.getAreaId();
        hubID = pref.getHubId();
        otherFragmentsActive = pref.getOtherFragmentsActive();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();

            if (isValidString(userId)) {
                isGuest = userModel.getGuest();
                userName = Strings.nullSafeString(userModel.getName()).isEmpty() ? userModel.getNumber() : userModel.getName();
                userProfilePhotoPath = userModel.getProfileImagePath();
                isLoggedIn = !isGuest;
            } else {
                isLoggedIn = false;
            }
        }
    }

    private void getConfiguration() {
        ConfigurationModel configurationModel = gson.fromJson(pref.getConfiguration(), new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            configNumber = configurationModel.getPhoneNumber();
        } else {
            configNumber = "";
        }
    }

    public void loadDrawer() {
        ActionBar mActionBar = getActionBar();
        assert mActionBar != null;

        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(true);
        mActionBar.setCustomView(headerView);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);


        try {
            Toolbar toolbar = (Toolbar) headerView.getParent();
            if (toolbar != null) {
                toolbar.setContentInsetsRelative(0, 0);
                toolbar.setContentInsetsAbsolute(0, 0);
                //toolbar.setCon
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            android.support.v7.widget.Toolbar parent = (android.support.v7.widget.Toolbar) headerView.getParent();
            if (parent != null)
                parent.setContentInsetStartWithNavigation(0);
        } finally {
            Log.i(TAG, "in finally");
        }

        View v = getActionBar().getCustomView();
        LayoutParams lp = v.getLayoutParams();
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
        v.setLayoutParams(lp);

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if (drawerAdapter != null) {
                    refreshDrawerItemsList();
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }
        });

        drawerListView = findViewById(R.id.dash_left_drawer);
        drawerHeaderView = View.inflate(getApplicationContext(), R.layout.drawer_header, null);
        drawerItems = new NavigationDrawerItem();
        headerTitleTextView.setText(R.string.dashboard_text);
        setUserInDrawerHeader();

        //If logged out or is a guest user
        if (!isLoggedIn) {
            setDrawer(drawerItemsLoggedOut);
        } else {
            setDrawer(drawerItemsLoggedIn);
        }

        drawerListView.addHeaderView(drawerHeaderView);
        drawerListView.setAdapter(drawerAdapter);
        drawerListView.setOnItemClickListener(new DrawerItemClickListener());

        loadUserProfilePic();

        areaName = pref.getAreaName();
        cityName = pref.getCityName();

        //Set location in drawer
        if (isValidString(areaName)) {
            headerAreaTextView.setText(areaName + ", " + cityName);
            TextView lUserlocation = drawerHeaderView.findViewById(R.id.drawerUserLocation);
            lUserlocation.setText(areaName);
        }
    }

    public void setUserInDrawerHeader() {
        TextView userNameTextView = drawerHeaderView.findViewById(R.id.drawerUserName);

        if (isValidString(userId)) {
            userNameTextView.setText(userName);
        } else {
            userNameTextView.setText(R.string.login_text);
        }
    }

    private void loadUserProfilePic() {
        RoundedImageView roundedImageView = drawerHeaderView.findViewById(R.id.drawerUserImage);
        String imageDownloadURL = ApplicationSettings.PROFILE_IMAGE_PATH + userProfilePhotoPath;

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new DownloadImageAsyncTask(roundedImageView).execute(imageDownloadURL, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |   DOWNLOAD USER PROFILE PHOTO
        |
        -------------------------------------
    */

    private class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {
        RoundedImageView roundedImageView;

        DownloadImageAsyncTask(RoundedImageView roundedImageView) {
            this.roundedImageView = roundedImageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();

                if (in != null) {
                    bitmap = getScaledBitmap(in);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                roundedImageView.setImageBitmap(result);
            }
        }
    }

    private Bitmap getScaledBitmap(InputStream in) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        int inSampleSize = calculateInSampleSize(sizeOptions);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(in, null, sizeOptions);
    }

    private int calculateInSampleSize(BitmapFactory.Options options) {
        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > 800 || width > 800) {
            final int heightRatio = Math.round((float) height / (float) 800);
            final int widthRatio = Math.round((float) width / (float) 800);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private void setDrawer(boolean[] drawerItemsArray) {
        drawerItems.setAvailableItems(this, drawerItemsArray);
        ArrayList<NavigationDrawerItem> drawerItemsList = drawerItems.getAvailableItems(this);
        drawerAdapter = new LeftDrawerAdapter(this, drawerItemsList);
    }

    public void updateCart() {
        TextView cartQuantityTextView = headerView.findViewById(R.id.cartQuantityTextView);
        ArrayList<ServiceTypeModel> mFinalSelectedServiceTypesList = pref.getCartData();

        //Set the cart count
        if (mFinalSelectedServiceTypesList != null) {
            String cartSizeString = String.valueOf(mFinalSelectedServiceTypesList.size());
            cartQuantityTextView.setText(cartSizeString);
        } else {
            cartQuantityTextView.setText("0");
        }

        cartQuantityTextView.setOnClickListener(this);
    }

    public void setNameInDrawer(String name) {
        TextView lUserlocation = drawerHeaderView.findViewById(R.id.drawerUserName);
        lUserlocation.setText(name);
    }

    public void setImageInDrawer(Bitmap bitmap) {
        RoundedImageView lProfileHeaderImage = drawerHeaderView.findViewById(R.id.drawerUserImage);

        if (bitmap != null) {
            lProfileHeaderImage.setImageBitmap(bitmap);
        } else {
            lProfileHeaderImage.setImageResource(R.drawable.user);
        }
    }

    public void setTitle(String title) {
        headerTitleTextView.setText(title);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /*  -------------------------------------
        |
        |   DASHBOARD ITEM SELECT
        |
        -------------------------------------
    */

    private void selectItem(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        otherFragmentsActive = pref.getOtherFragmentsActive();

        if (!otherFragmentsActive && position != 9) {
            pref.putOtherFragmentsActive(true);
        }

        switch (position) {

            case 0:
                // Profile
                if (headerAreaLayout != null)
                    headerAreaLayout.setVisibility(View.GONE);
                if (headerTitleTextView != null)
                    headerTitleTextView.setVisibility(View.VISIBLE);

                if (!isLoggedIn) {
                    gotoSignInPage();
                } else {

                    //lareaLayout.setVisibility(View.GONE);
                    //headerText.setVisibility(View.VISIBLE);
                    //headerText.setText("Profile");
                    changeHeader = false;
                    headerTitleTextView.setText("Profile");
                    ProfileFragment profileFrag = new ProfileFragment();
                    transaction.replace(R.id.dash_content_frame, profileFrag);
                    transaction.commit();
                    break;
                    //gotoProfileActivity();
                }

                break;

            case 1:
                //Home
                if (headerAreaLayout != null)
                    headerAreaLayout.setVisibility(View.VISIBLE);
                if (headerTitleTextView != null)
                    headerTitleTextView.setVisibility(View.GONE);

                if (drawerLayout.isDrawerOpen(drawerListView)) {
                    drawerLayout.closeDrawer(drawerListView);
                }

                loadDashFrag();

                break;

            case 2:
                // Notifications
                if (headerAreaLayout != null)
                    headerAreaLayout.setVisibility(View.GONE);
                if (headerTitleTextView != null)
                    headerTitleTextView.setVisibility(View.VISIBLE);
                headerTitleTextView.setText(R.string.drawer_notification_text);
                changeHeader = false;
                NotificationFragment notifyFragment = new NotificationFragment();
                transaction.replace(R.id.dash_content_frame, notifyFragment);
                transaction.commit();
                break;

            case 3:
                // Gallery
                if (headerAreaLayout != null)
                    headerAreaLayout.setVisibility(View.GONE);
                if (headerTitleTextView != null)
                    headerTitleTextView.setVisibility(View.VISIBLE);
                headerTitleTextView.setText(R.string.drawer_gallery_text);
                changeHeader = false;
                GalleryFragment nFragment = new GalleryFragment();
                transaction.replace(R.id.dash_content_frame, nFragment);
                transaction.commit();
                break;

            case 4:
                // Active Booking or Invite friends
                if (!isLoggedIn) {
                    if (!isInviteVisible) {
                        drawerAdapter.setVisible(false);
                    } else {
                        drawerAdapter.setInVisible(false);
                    }

                    drawerAdapter.notifyDataSetChanged();
                    isInviteVisible = !isInviteVisible;
                } else {
                    changeHeader = false;
                    gotoBookingHistory(true);
                }

                break;

            case 5:
                // History or SettingsActivity
                /*if(headerAreaLayout!=null)
                    headerAreaLayout.setVisibility(View.GONE);*/
                /*if(headerTitleTextView!=null)
                    headerTitleTextView.setVisibility(View.VISIBLE);*/
                changeHeader = false;
                if (!isValidString(userId) || isGuest) {
                    loadSettingsFragment();
                } else {
                    gotoBookingHistory(false);
                }

                break;

            case 6:
                // Invite Friends or Call Us
                if (!isLoggedIn) {
                    initiateCallProcess();
                } else {
                    if (!isInviteVisible) {
                        drawerAdapter.setVisible(true);
                    } else {
                        drawerAdapter.setInVisible(true);
                    }

                    drawerAdapter.notifyDataSetChanged();
                    isInviteVisible = !isInviteVisible;
                }

                break;

            case 7:
                // SettingsActivity
                if (headerAreaLayout != null)
                    headerAreaLayout.setVisibility(View.GONE);
                if (headerTitleTextView != null)
                    headerTitleTextView.setVisibility(View.VISIBLE);
                changeHeader = false;
                loadSettingsFragment();
                break;

            case 8:
                // Call Us
                initiateCallProcess();
                break;

            case 9:
                // Logout
                pref.putUserModel("");
                pref.clearCartData();
                pref.putCouponModel("");
                pref.putUserId("");
                pref.putGuestAddresses("");
                Toast.makeText(this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                reloadActivity();
                break;

        }

        drawerListView.setItemChecked(position, true);

        if (!isLoggedIn) {
            if (position != 4) {
                drawerLayout.closeDrawer(drawerListView);
            }
        } else {
            if (position != 6) {
                drawerLayout.closeDrawer(drawerListView);
            }
        }
    }

    /*  -------------------------------------
        |
        |   LOAD FRAGMENTS
        |
        -------------------------------------
    */
    public void loadDashFrag() {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        //transaction.commitAllowingStateLoss();
        MainSplashFragment dashFrag = MainSplashFragment.newInstanceForMainSplashFragment(
                bannerItemsList,
                allServicesWithServiceTypesList,
                userId
        );
        transaction.replace(R.id.dash_content_frame, dashFrag);
        transaction.commit();
    }


    private void loadSettingsFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        headerTitleTextView.setText(R.string.drawer_settings_text);
        SettingsFragment settingFrag = new SettingsFragment();
        transaction.replace(R.id.dash_content_frame, settingFrag);
        transaction.commit();
    }

    public void loadNotificationsFragment() {
        LinearLayout areaLayout = headerView.findViewById(R.id.areaLayout);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (headerAreaLayout != null)
            headerAreaLayout.setVisibility(View.GONE);
        if (headerTitleTextView != null) {
            headerTitleTextView.setVisibility(View.VISIBLE);
            headerTitleTextView.setText(R.string.drawer_notification_text);
        }
        NotificationFragment notifyFrag = new NotificationFragment();
        transaction.replace(R.id.dash_content_frame, notifyFrag);
        transaction.commit();
    }

    private void fireCallIntent() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + configNumber));
        startActivity(intent);
    }

    private void initiateCallProcess() {
        if ((Build.VERSION.SDK_INT >= 23)) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainSplashActivity.this, new String[]{Manifest.permission.CALL_PHONE}, RC_CALL_PHONE);
            } else {
                fireCallIntent();
            }
        } else {
            fireCallIntent();
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    private void gotoProfileActivity() {
        Intent intent = new Intent(context, ProfileActivity.class);
        startActivity(intent);
    }

    public void reloadActivity() {
        Intent reloadIntent = getIntent();
        reloadIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        reloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(reloadIntent);
        //finish();
    }

    public void gotoBookingSummary() {
        Intent intent = new Intent(context, BookingSummaryActivity.class);
        startActivity(intent);
    }

    public void sendTowhatsapp() {
        Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=918010801091&text=Hey!%20I%27m%20interested%20in%20your%20at%20home%20beauty%20services.%20Can%20you%20please%20guide%20me"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }

    public void gotoBookingHistory(Boolean activeBookings) {
        Intent intent = new Intent(context, BookingHistoryActivity.class);
        intent.putExtra("active_bookings", activeBookings);
        startActivity(intent);
    }

    public void gotoSearchAreaList() {
        Intent intent = new Intent(context, SearchAreaListActivity.class);
        intent.putExtra("fromBooking", "true");
        //startActivityForResult(intent, RC_SELECT_AREA);
        startActivity(intent);
    }

    private void gotoSignInPage() {
        Intent intent = new Intent(this, SignInPageActivity.class);
        intent.putExtra("dashboard_login", true);
        startActivityForResult(intent, RC_DASH_LOGIN);
    }

    private void refreshDrawerItemsList() {
        drawerAdapter.notifyDataSetChanged();
    }

    /*private void reloadDrawerHeader() {
        getUser();
        setUserInDrawerHeader();
    }*/

    private void onResumeOperations() {
        boolean reloadRequired = pref.getReloadRequired();

        if (reloadRequired) {
            pref.putReloadRequired(false);
            reloadActivity();
        } else {
            /*if (firstRunDashboardActivity) {
                firstRunDashboardActivity = false;
            } else {*/

            //}

            loadDashFrag();

            //updateCart();
        }
    }

    public void logGoogleEvents(String eventName, Bundle bundle) {
        try {
            FireBaseHelper firebaseHelper = new FireBaseHelper(this);
            firebaseHelper.logEvents(eventName.toLowerCase(), bundle);
        } catch (Exception ex) {
            Log.e("PackageSelected", ex.getMessage());
        }
    }


    private void DeepLink() {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        if (MainSplashActivity.scheme.contains("_")) {
            MainSplashActivity.scheme = MainSplashActivity.scheme.replaceAll("_", " ");
        }

        boolean flag = false;
        for (ServiceModel sm : allServicesWithServiceTypesList) {
            if (sm.getServiceName().equalsIgnoreCase(MainSplashActivity.scheme)) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }
        if (!flag) {
            loadDashFrag();
        } else {
            Intent i = new Intent(this, DashboardActivity.class);
            i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
            i.putExtra("serviceName", MainSplashActivity.scheme);
            startActivity(i);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }


    public void bannerAction(String acionUrl) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        boolean flag = false;
        for (ServiceModel sm : allServicesWithServiceTypesList) {
            if (sm.getServiceName().equalsIgnoreCase(MainSplashActivity.scheme)) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }
        if (!flag) {
            loadDashFrag();
        } else {
            Intent i = new Intent(this, DashboardActivity.class);
            i.putParcelableArrayListExtra("serviceList", allServicesWithServiceTypesList);
            i.putExtra("serviceName", MainSplashActivity.scheme);
            startActivity(i);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }
    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fireCallIntent();
                } else {
                    Toast.makeText(context, "Operation cancelled", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_DASH_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                reloadActivity();
            }
        } else if (requestCode == RC_SELECT_AREA) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                reloadActivity();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerMenu:
                if (drawerLayout.isDrawerOpen(drawerListView)) {
                    drawerLayout.closeDrawer(drawerListView);
                } else {
                    drawerLayout.openDrawer(drawerListView);
                }
                break;

            case R.id.cartLayout:
            case R.id.cartButton:
            case R.id.cartQuantityTextView:
                gotoBookingSummary();
                break;

            case R.id.whatsAppLayout:
            case R.id.whatsAppButton:
                sendTowhatsapp();
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
        //showActiveBooking = false;
        boolean reloadRequired = pref.getReloadRequired();
        updateCart();
        /*if(changeHeader)
        setHeader();*/
        if (reloadRequired) {
            pref.putReloadRequired(false);
            //showActiveBooking = false;
            reloadActivity();
        } else {
            //setHeader();
            loadDashFrag();
        }

        //onResumeOperations();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        firebaseHelper.logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_CATEGORY);
    }

    @Override
    public void onBackPressed() {
        otherFragmentsActive = pref.getOtherFragmentsActive();

        if (otherFragmentsActive) {
            pref.putOtherFragmentsActive(false);
            reloadActivity();
            //loadDashFrag();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }
    }

    /*private void createViewpager() {
        customPagerAdapter = new CustomPagerAdapterDashboard(MainSplashActivity.this, bannerItemsList,MainSplashFragment);
        viewPager.setAdapter(customPagerAdapter);
        LinearLayout dotsContainer = findViewById(R.id.dotsContainer);

        dotsCount = customPagerAdapter.getCount();

        dots = new ImageView[dotsCount];

        context = this;

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(context);
            dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.dot_light));

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(8, 0, 8, 0);
            dotsContainer.addView(dots[i], layoutParams);
        }

        if (dots.length > 0) {
            dots[0].setImageDrawable((ContextCompat.getDrawable(context, R.drawable.dot)));
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.dot_light));
                }

                dots[position].setImageDrawable((ContextCompat.getDrawable(context, R.drawable.dot)));
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        new MainSplashActivity.GetAllServicesWithServiceTypesAsyncTask().execute(null, null, null);
    }*/

    private void getBanners() {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetBannersAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(context, "Check your internet", Toast.LENGTH_LONG).show();
        }
    }

    private class GetBannersAsyncTask extends AsyncTask<Void, Void, ArrayList<Banner>> {
        ArrayList<Banner> resultArrayList;
        ProgressDialog progressDialog;

        GetBannersAsyncTask() {
            progressDialog = new ProgressDialog(context);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading Services");
            progressDialog.show();

        }

        protected ArrayList<Banner> doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            return restWebServices.getBanners();
        }

        protected void onPostExecute(ArrayList<Banner> result) {
            super.onPostExecute(resultArrayList);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                if (result != null && result.size() > 0) {
                    bannerItemsList = result;
                    new GetAllServicesWithServiceTypesAsyncTask().execute(null, null, null);
                    //createViewpager();
                }

                //new DashboardActivity.GetOffersAsyncTask().execute(null, null, null);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }


    private class GetAllServicesWithServiceTypesAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private final WeakReference<MainSplashActivity> dashboardActivityWeakReference = new WeakReference<>(MainSplashActivity.this);
        ProgressDialog progressDialog;
        String mHubId = pref.getHubId();

        GetAllServicesWithServiceTypesAsyncTask() {
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!dashboardActivityWeakReference.get().isFinishing()) {
                progressDialog.setMessage("Loading Services");
                progressDialog.show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... url) {

            allServicesWithServiceTypesList = restWebServices.getAllServicesWithServiceTypes(mHubId);
            if (allServicesWithServiceTypesList != null)
                return allServicesWithServiceTypesList.size() > 0;
            else
                return null;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    //allServicesWithServiceTypesList = new ArrayList<>();
                    //gridView.setAdapter(new MainSplashAdapter(MainSplashActivity.this,allServicesWithServiceTypesList));
                    // mainLayout.setVisibility(View.GONE);
                    if (showActiveBooking) {
                        showActiveBooking = false;
                        gotoBookingHistory(true);
                    } else if (showNotification) {
                        showNotification = false;
                        loadNotificationsFragment();
                    } else {
                        if (MainSplashActivity.scheme != null && !MainSplashActivity.scheme.equals("")) {
                            DeepLink();
                        } else {
                            loadDashFrag();
                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if (Objects.equals(TAG, GET_SERVICES) && responseObj instanceof ServiceModelNewAPI) {
            ServiceModelNewAPI serviceModelNewAPI = (ServiceModelNewAPI) responseObj;

            if (serviceModelNewAPI.getResponse().equals("1")) {
                serviceModelResponse = serviceModelNewAPI.getData();
                //String checksum = paytmChecksumResponse.getResponsedata().getChecksum();
                //completePayTmTransaction(checksum);
            } else {
                Toast.makeText(MainSplashActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void positiveResponse(String TAG, String response) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    private void getServices() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(this, this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(ServiceModelNewAPI.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_GET_SERVICES);
        //String data = getLoyalityPointsJsonRequest(userId);
        apiManager.getStringPostResponse(GET_SERVICES, ApplicationSettings.VANITYCUBE_API_URL, params);
    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {

    }

}