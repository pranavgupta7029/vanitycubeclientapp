
package com.vanitycube.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.HorizontalScrollView;

import com.vanitycube.R;

public class GalleryActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        HorizontalScrollView h1 = (HorizontalScrollView) findViewById(R.id.imageScrollView);
//        HorizontalScrollView h2 = (HorizontalScrollView) findViewById(R.id.catagoriesScrollView);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        // Add the params for your parent. Weird I know. Find out why???
        HorizontalScrollView.LayoutParams params = new HorizontalScrollView.LayoutParams(metrics.widthPixels / 8, HorizontalScrollView.LayoutParams.MATCH_PARENT);

//        LinearLayout icon1 = (LinearLayout) findViewById(R.id.ll1);
//        icon1.setLayoutParams(params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
