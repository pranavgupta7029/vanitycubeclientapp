package com.vanitycube.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.LeftDrawerAdapter;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.fonts.VCButton;
import com.vanitycube.fragments.DashboardFragment;
import com.vanitycube.fragments.NotificationFragment;
import com.vanitycube.fragments.SettingsFragment;
import com.vanitycube.model.Banner;
import com.vanitycube.model.ConfigurationModel;
import com.vanitycube.model.NavigationDrawerItem;
import com.vanitycube.model.Offer;
import com.vanitycube.model.ServiceModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.utilities.RoundedImageView;
import com.vanitycube.utilities.Strings;
import com.vanitycube.webservices.RestWebServices;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DashboardActivity extends FragmentActivity implements OnClickListener {
    private Context context;
    private String TAG;
    private Gson gson;
    private RestWebServices restWebServices;
    private SharedPref pref;
    private FireBaseHelper firebaseHelper;

    private View headerView, drawerHeaderView;
    private DrawerLayout drawerLayout;
    private ListView drawerListView;
    private LeftDrawerAdapter drawerAdapter;
    private TextView headerTitleTextView;
    private LinearLayout headerAreaLayout;
    private TextView headerAreaTextView;

    private ArrayList<Offer> offerItemsList;
    private ArrayList<Banner> bannerItemsList;
    private ArrayList<ServiceModel> allServicesWithServiceTypesList;
    private ArrayList<String> serviceNamesList;
    private HashMap<String, ArrayList<ServiceTypeModel>> allServicesWithServiceTypesMap;

    private ArrayList<ServiceTypeModel> allServices;

    private Calendar startTime;
    private NavigationDrawerItem drawerItems;
    private String userId, userName, userProfilePhotoPath;

    private boolean isLoggedIn, isGuest;
    private boolean firstRunDashboardActivity, isInviteVisible, showActiveBooking, showNotification, otherFragmentsActive;
    private boolean[] drawerItemsLoggedOut = {true, true, true, false, false, true, true, true, false};
    private boolean[] drawerItemsLoggedIn = {true, true, true, true, true, true, true, true, true};

    public String areaName, areaID, hubID, configNumber, json, checkYourInternet, couponLabel, cityName;
    private String serviceName;

    public static final int RC_CALL_PHONE = 200;
    private int RC_DASH_LOGIN = 201;
    private int RC_SELECT_AREA = 202;
    public static Map<String, Integer> servicePositionMap = new HashMap<>();
    public static Boolean isMember = false;
    DashboardFragment dashFrag;
    String searchString;
    public static int RC_MEMBER_LOGIN = 205;
    private Boolean isToWebView = false;
    ProgressDialog progressDialog;

    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_dashboard);

        context = DashboardActivity.this;
        MainSplashActivity.scheme = null;

        TAG = getResources().getString(R.string.dashboard_activity_tag);
        checkYourInternet = getResources().getString(R.string.check_your_internet_text);
        couponLabel = getResources().getString(R.string.coupon_code_text);


        offerItemsList = new ArrayList<>();
        bannerItemsList = new ArrayList<>();


        allServicesWithServiceTypesList = getIntent().getParcelableArrayListExtra("serviceList");
        serviceName = getIntent().getStringExtra("serviceName");
        serviceNamesList = new ArrayList<>();
        allServicesWithServiceTypesMap = new HashMap<>();
        allServices = new ArrayList<>();

        pref = new SharedPref(VcApplicationContext.getInstance());
        restWebServices = new RestWebServices();
        gson = new Gson();

        firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_CATEGORY);

        //Get all shared preferences data
        getSharedPrefData();

        isInviteVisible = false;
        showActiveBooking = false;
        showNotification = false;
        firstRunDashboardActivity = true;

        //Get user
        //getUser();

        //Get configuration data
        getConfiguration();

        //Common header section
        headerView = View.inflate(getApplicationContext(), R.layout.common_header_new, null);

        /*LinearLayout headerMenuButtonLayout = headerView.findViewById(R.id.headerFirstButton);
        headerMenuButtonLayout.setOnClickListener(this);
        if(headerMenuButtonLayout!=null)
            headerMenuButtonLayout.setVisibility(View.GONE);*/

        Button headerMenuButton = headerView.findViewById(R.id.headerMenu);
        headerMenuButton.setOnClickListener(this);
        if (headerMenuButton != null)
            headerMenuButton.setVisibility(View.GONE);

        headerTitleTextView = headerView.findViewById(R.id.headerText);
        if (headerTitleTextView != null)
            headerTitleTextView.setVisibility(View.GONE);

        headerAreaLayout = headerView.findViewById(R.id.areaLayout);
        if (headerAreaLayout != null)
            headerAreaLayout.setVisibility(View.VISIBLE);

        headerAreaTextView = headerView.findViewById(R.id.areaTextView);
        headerAreaTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSearchAreaList();
            }
        });
        Button titleDownArrow = headerView.findViewById(R.id.titleDownArrow);
        titleDownArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSearchAreaList();
            }
        });

        VCButton backButton = headerView.findViewById(R.id.headerArrow);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(this);

        Button search = headerView.findViewById(R.id.search);
        search.setVisibility(View.VISIBLE);
        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dashFrag.openSearch();
            }
        });


        //Set cart view and click action
        RelativeLayout cartLayout = headerView.findViewById(R.id.cartLayout);
        if (cartLayout != null) {
            cartLayout.setVisibility(View.VISIBLE);
            cartLayout.setOnClickListener(this);
        }

        RelativeLayout whatsappLayout = headerView.findViewById(R.id.whatsAppLayout);
        if (whatsappLayout != null) {
            whatsappLayout.setVisibility(View.GONE);
            whatsappLayout.setOnClickListener(this);
        }

        VCButton whatappButton = headerView.findViewById(R.id.whatsAppButton);
        if (whatappButton != null) {
            whatappButton.setVisibility(View.VISIBLE);
            whatappButton.setOnClickListener(this);
        }


        Button cartButton = headerView.findViewById(R.id.cartButton);
        cartButton.setOnClickListener(this);

        TextView cartQuantityTextView = headerView.findViewById(R.id.cartQuantityTextView);
        cartQuantityTextView.setOnClickListener(this);

        drawerLayout = findViewById(R.id.dash_drawer_layout);
        //drawerLayout.setVisibility(View.GONE);

        //Load drawer
        loadDrawer();

        //Check if we need to show active bookings
        showActiveBooking = getIntent().getBooleanExtra("showActiveBookings", false);

        //Start the api call cycle
        //getBanners();

        createServiceHashMap();

        searchString = getIntent().getStringExtra("search");
        /*if (showActiveBooking) {
            showActiveBooking = false;
            gotoBookingHistory(true);
        } else if (showNotification) {
            showNotification = false;
            loadNotificationsFragment();
        } else {
            loadDashFrag();
        }*/


    }

    public void getSharedPrefData() {
        areaID = pref.getAreaId();
        hubID = pref.getHubId();
        otherFragmentsActive = pref.getOtherFragmentsActive();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();
            pref.putUserId(userId);

            if (isValidString(userId)) {
                isGuest = userModel.getGuest();
                userName = Strings.nullSafeString(userModel.getName()).isEmpty() ? userModel.getNumber() : userModel.getName();
                userProfilePhotoPath = userModel.getProfileImagePath();
                isLoggedIn = !isGuest;
            } else {
                isLoggedIn = false;
            }
        }
    }

    private void getConfiguration() {
        ConfigurationModel configurationModel = gson.fromJson(pref.getConfiguration(), new TypeToken<ConfigurationModel>() {
        }.getType());

        if (configurationModel != null) {
            configNumber = configurationModel.getPhoneNumber();
        } else {
            configNumber = "";
        }
    }

    private void createServiceHashMap() {
        for (ServiceModel sm : allServicesWithServiceTypesList) {
            String serviceName = sm.getServiceName();

            serviceNamesList.add(serviceName);
            for (ServiceTypeModel serviceModel : sm.getServiceTypes()) {
                serviceModel.setServiceName(serviceName);
                serviceModel.setServiceID(sm.getServiceID());
                serviceModel.setPercentageForMembers(sm.getMembershippercentage());
            }

            allServicesWithServiceTypesMap.put(serviceName, sm.getServiceTypes());
            allServices.addAll(sm.getServiceTypes());
            /*for(ServiceTypeModel serviceTypeModel:allServices){
                serviceTypeModel.setServiceID(sm.getServiceID());
                serviceTypeModel.setServiceName(sm.getServiceName());
            }*/
        }
    }

    /*  -------------------------------------
        |
        |   MENU DRAWER AND ACTION BAR
        |
        -------------------------------------
    */

    public void loadDrawer() {
        ActionBar mActionBar = getActionBar();
        assert mActionBar != null;

        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setCustomView(headerView);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);

        try {
            Toolbar toolbar = (Toolbar) headerView.getParent();
            if (toolbar != null) {
                toolbar.setContentInsetsRelative(0, 0);
                toolbar.setContentInsetsAbsolute(0, 0);
                //toolbar.setCon
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            android.support.v7.widget.Toolbar parent = (android.support.v7.widget.Toolbar) headerView.getParent();
            if (parent != null)
                parent.setContentInsetStartWithNavigation(0);
        } finally {
            Log.e(TAG, "in finally");
        }

        View v = getActionBar().getCustomView();
        LayoutParams lp = v.getLayoutParams();
        lp.width = LayoutParams.MATCH_PARENT;
        lp.height = LayoutParams.MATCH_PARENT;
        v.setLayoutParams(lp);

        /*drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if (drawerAdapter != null) {
                    refreshDrawerItemsList();
                }
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }
        });*/

        /*drawerListView = findViewById(R.id.dash_left_drawer);
        drawerListView.setVisibility(View.GONE);*/
        drawerHeaderView = View.inflate(getApplicationContext(), R.layout.drawer_header, null);
        //drawerItems = new NavigationDrawerItem();
        headerTitleTextView.setText(R.string.dashboard_text);
        setUserInDrawerHeader();

        //If logged out or is a guest user
        /*if (!isLoggedIn) {
            setDrawer(drawerItemsLoggedOut);
        } else {
            setDrawer(drawerItemsLoggedIn);
        }

        drawerListView.addHeaderView(drawerHeaderView);
        drawerListView.setAdapter(drawerAdapter);
        drawerListView.setOnItemClickListener(new DrawerItemClickListener());

        loadUserProfilePic();*/

        areaName = pref.getAreaName();
        cityName = pref.getCityName();


        //Set location in drawer
        if (isValidString(areaName)) {
            headerAreaTextView.setText(areaName + ", " + cityName);
            TextView lUserlocation = drawerHeaderView.findViewById(R.id.drawerUserLocation);
            lUserlocation.setText(areaName);
        }
    }

    /*  -------------------------------------
        |
        |  GET BANNERS ASYNC TASK
        |
        -------------------------------------
    */

    private class GetBannersAsyncTask extends AsyncTask<Void, Void, ArrayList<Banner>> {
        ArrayList<Banner> resultArrayList;

        GetBannersAsyncTask() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected ArrayList<Banner> doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            return restWebServices.getBanners();
        }

        protected void onPostExecute(ArrayList<Banner> result) {
            super.onPostExecute(resultArrayList);

            try {
                if (result.size() > 0) {
                    bannerItemsList = result;
                }
                new GetOffersAsyncTask().execute(null, null, null);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  --------------------------------------------------
        |
        |  GET OFFERS ASYNC TASK
        |
        --------------------------------------------------
    */

    private class GetOffersAsyncTask extends AsyncTask<Void, Void, ArrayList<Offer>> {
        GetOffersAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Offer> doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            return restWebServices.getOffers();
        }

        @Override
        protected void onPostExecute(ArrayList<Offer> result) {
            super.onPostExecute(result);

            if (result != null && result.size() > 0) {
                offerItemsList = result;
            }

            createServiceHashMap();


            if (showActiveBooking) {
                showActiveBooking = false;
                gotoBookingHistory(true);
            } else if (showNotification) {
                showNotification = false;
                loadNotificationsFragment();
            } else {
                loadDashFrag();
            }

            //new GetAllServicesWithServiceTypesAsyncTask().execute(null, null, null);
        }
    }

    /*  --------------------------------------------------
        |
        |  GET ALL SERVICES WITH SERVICE TYPES ASYNC TASK
        |
        --------------------------------------------------
    */

    private void getBanners() {

        /*if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new GetOffersAsyncTask().execute(null, null, null);
            //new GetBannersAsyncTask().execute(null, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }*/
    }

    private class GetAllServicesWithServiceTypesAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private final WeakReference<DashboardActivity> dashboardActivityWeakReference = new WeakReference<>(DashboardActivity.this);
        ProgressDialog progressDialog;
        String mHubId = pref.getHubId();

        GetAllServicesWithServiceTypesAsyncTask() {
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (!dashboardActivityWeakReference.get().isFinishing()) {
                progressDialog.setMessage("Loading Services");
                progressDialog.show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            allServicesWithServiceTypesList = restWebServices.getAllServicesWithServiceTypes(mHubId);
            return allServicesWithServiceTypesList.size() > 0;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (!result) {
                    allServicesWithServiceTypesList = new ArrayList<>();
                } else {
                    createServiceHashMap();
                }

                if (showActiveBooking) {
                    showActiveBooking = false;
                    gotoBookingHistory(true);
                } else if (showNotification) {
                    showNotification = false;
                    loadNotificationsFragment();
                } else {
                    loadDashFrag();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void setUserInDrawerHeader() {
        TextView userNameTextView = drawerHeaderView.findViewById(R.id.drawerUserName);

        if (isValidString(userId)) {
            userNameTextView.setText(userName);
        } else {
            userNameTextView.setText(R.string.login_text);
        }
    }

    private void loadUserProfilePic() {
        RoundedImageView roundedImageView = drawerHeaderView.findViewById(R.id.drawerUserImage);
        String imageDownloadURL = ApplicationSettings.PROFILE_IMAGE_PATH + userProfilePhotoPath;

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new DownloadImageAsyncTask(roundedImageView).execute(imageDownloadURL, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |   DOWNLOAD USER PROFILE PHOTO
        |
        -------------------------------------
    */

    private class DownloadImageAsyncTask extends AsyncTask<String, Void, Bitmap> {
        RoundedImageView roundedImageView;

        DownloadImageAsyncTask(RoundedImageView roundedImageView) {
            this.roundedImageView = roundedImageView;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap bitmap = null;

            try {
                InputStream in = new java.net.URL(urldisplay).openStream();

                if (in != null) {
                    bitmap = getScaledBitmap(in);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null) {
                roundedImageView.setImageBitmap(result);
            }
        }
    }

    private Bitmap getScaledBitmap(InputStream in) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        int inSampleSize = calculateInSampleSize(sizeOptions);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(in, null, sizeOptions);
    }

    private int calculateInSampleSize(BitmapFactory.Options options) {
        final int height = options.outHeight;
        final int width = options.outWidth;

        int inSampleSize = 1;

        if (height > 800 || width > 800) {
            final int heightRatio = Math.round((float) height / (float) 800);
            final int widthRatio = Math.round((float) width / (float) 800);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private void setDrawer(boolean[] drawerItemsArray) {
        drawerItems.setAvailableItems(this, drawerItemsArray);
        ArrayList<NavigationDrawerItem> drawerItemsList = drawerItems.getAvailableItems(this);
        drawerAdapter = new LeftDrawerAdapter(this, drawerItemsList);
    }

    public void updateCart() {
        TextView cartQuantityTextView = headerView.findViewById(R.id.cartQuantityTextView);
        ArrayList<ServiceTypeModel> mFinalSelectedServiceTypesList = pref.getCartData();

        //Set the cart count
        if (mFinalSelectedServiceTypesList != null) {
            String cartSizeString = String.valueOf(mFinalSelectedServiceTypesList.size());
            cartQuantityTextView.setText(cartSizeString);
        } else {
            cartQuantityTextView.setText("0");
        }

        cartQuantityTextView.setOnClickListener(this);
    }

    public void setNameInDrawer(String name) {
        TextView lUserlocation = drawerHeaderView.findViewById(R.id.drawerUserName);
        lUserlocation.setText(name);
    }

    public void setImageInDrawer(Bitmap bitmap) {
        RoundedImageView lProfileHeaderImage = drawerHeaderView.findViewById(R.id.drawerUserImage);

        if (bitmap != null) {
            lProfileHeaderImage.setImageBitmap(bitmap);
        } else {
            lProfileHeaderImage.setImageResource(R.drawable.user);
        }
    }

    public void setTitle(String title) {
        headerTitleTextView.setText(title);
    }

    /*private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }*/

    /*  -------------------------------------
        |
        |   DASHBOARD ITEM SELECT
        |
        -------------------------------------
    */

    /*private void selectItem(int position) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        otherFragmentsActive = pref.getOtherFragmentsActive();

        if (!otherFragmentsActive && position != 0 && position != 9) {
            pref.putOtherFragmentsActive(true);
        }

        switch (position) {

            case 0:
                // Profile
                headerAreaLayout.setVisibility(View.GONE);
                headerTitleTextView.setVisibility(View.VISIBLE);

                if (!isLoggedIn) {
                    gotoSignInPage();
                } else {

                    //lareaLayout.setVisibility(View.GONE);
                    //headerText.setVisibility(View.VISIBLE);
                    //headerText.setText("Profile");
                    ProfileFragment profileFrag = new ProfileFragment();
                    transaction.replace(R.id.dash_content_frame, profileFrag);
                    transaction.commit();
                    break;
                    //gotoProfileActivity();
                }

                break;

            case 1:
                //Home
                headerAreaLayout.setVisibility(View.VISIBLE);
                headerTitleTextView.setVisibility(View.GONE);

                if (drawerLayout.isDrawerOpen(drawerListView)) {
                    drawerLayout.closeDrawer(drawerListView);
                }

                loadDashFrag();

                break;

            case 2:
                // Notifications
                headerAreaLayout.setVisibility(View.GONE);
                headerTitleTextView.setVisibility(View.VISIBLE);
                headerTitleTextView.setText(R.string.drawer_notification_text);
                NotificationFragment notifyFragment = new NotificationFragment();
                transaction.replace(R.id.dash_content_frame, notifyFragment);
                transaction.commit();
                break;

            case 3:
                // Gallery
                headerAreaLayout.setVisibility(View.GONE);
                headerTitleTextView.setVisibility(View.VISIBLE);
                headerTitleTextView.setText(R.string.drawer_gallery_text);
                GalleryFragment nFragment = new GalleryFragment();
                transaction.replace(R.id.dash_content_frame, nFragment);
                transaction.commit();
                break;

            case 4:
                // Active Booking or Invite friends
                if (!isLoggedIn) {
                    if (!isInviteVisible) {
                        drawerAdapter.setVisible(false);
                    } else {
                        drawerAdapter.setInVisible(false);
                    }

                    drawerAdapter.notifyDataSetChanged();
                    isInviteVisible = !isInviteVisible;
                } else {
                    gotoBookingHistory(true);
                }

                break;

            case 5:
                // History or SettingsActivity
                headerAreaLayout.setVisibility(View.GONE);
                headerTitleTextView.setVisibility(View.VISIBLE);

                if (!isValidString(userId) || isGuest) {
                    loadSettingsFragment();
                } else {
                    gotoBookingHistory(false);
                }

                break;

            case 6:
                // Invite Friends or Call Us
                if (!isLoggedIn) {
                    initiateCallProcess();
                } else {
                    if (!isInviteVisible) {
                        drawerAdapter.setVisible(true);
                    } else {
                        drawerAdapter.setInVisible(true);
                    }

                    drawerAdapter.notifyDataSetChanged();
                    isInviteVisible = !isInviteVisible;
                }

                break;

            case 7:
                // SettingsActivity
                headerAreaLayout.setVisibility(View.GONE);
                headerTitleTextView.setVisibility(View.VISIBLE);
                loadSettingsFragment();
                break;

            case 8:
                // Call Us
                initiateCallProcess();
                break;

            case 9:
                // Logout
                pref.putUserModel("");
                pref.clearCartData();
                pref.putCouponModel("");
                pref.putGuestAddresses("");
                Toast.makeText(this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                reloadActivity();
                break;

        }

        drawerListView.setItemChecked(position, true);

        if (!isLoggedIn) {
            if (position != 4) {
                drawerLayout.closeDrawer(drawerListView);
            }
        } else {
            if (position != 6) {
                drawerLayout.closeDrawer(drawerListView);
            }
        }
    }*/

    /*  -------------------------------------
        |
        |   LOAD FRAGMENTS
        |
        -------------------------------------
    */

    public void loadDashFrag() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        dashFrag = DashboardFragment.newInstanceForDashboardFragment(
                bannerItemsList,
                offerItemsList,
                allServicesWithServiceTypesList,
                serviceNamesList,
                allServicesWithServiceTypesMap,
                userId,
                allServices,
                serviceName,
                searchString
        );
        transaction.replace(R.id.dash_content_frame, dashFrag);
        transaction.commit();
    }



    private void loadSettingsFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        headerTitleTextView.setText(R.string.drawer_settings_text);
        SettingsFragment settingFrag = new SettingsFragment();
        transaction.replace(R.id.dash_content_frame, settingFrag);
        transaction.commit();
    }

    public void loadNotificationsFragment() {
        LinearLayout areaLayout = headerView.findViewById(R.id.areaLayout);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (headerAreaLayout != null)
            headerAreaLayout.setVisibility(View.GONE);
        if (headerTitleTextView != null)
            headerTitleTextView.setVisibility(View.VISIBLE);

        headerTitleTextView.setText(R.string.drawer_notification_text);
        NotificationFragment notifyFrag = new NotificationFragment();
        transaction.replace(R.id.dash_content_frame, notifyFrag);
        transaction.commit();
    }

    private void fireCallIntent() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + configNumber));
        startActivity(intent);
    }

    private void initiateCallProcess() {
        if ((Build.VERSION.SDK_INT >= 23)) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CALL_PHONE}, RC_CALL_PHONE);
            } else {
                fireCallIntent();
            }
        } else {
            fireCallIntent();
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    private void gotoProfileActivity() {
        Intent intent = new Intent(context, ProfileActivity.class);
        startActivity(intent);
    }

    public void reloadActivity(boolean isWebView) {
        if (!isWebView) {
            Intent reloadIntent = getIntent();
            reloadIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            reloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(reloadIntent);
            finish();
        } else {
            isToWebView = true;
            checkIsMemberWebView();
        }
    }

    public void gotoBookingSummary() {
        Intent intent = new Intent(context, BookingSummaryActivity.class);
        startActivity(intent);
    }

    public void sendTowhatsapp() {
        Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=918010801091&text=Hey!%20I%27m%20interested%20in%20your%20at%20home%20beauty%20services.%20Can%20you%20please%20guide%20me"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);

    }

    public void gotoBookingHistory(Boolean activeBookings) {
        Intent intent = new Intent(context, BookingHistoryActivity.class);
        intent.putExtra("active_bookings", activeBookings);
        startActivity(intent);
    }

    public void gotoSearchAreaList() {
        Intent intent = new Intent(context, SearchAreaListActivity.class);
        intent.putExtra("fromBooking", "true");
        startActivityForResult(intent, RC_SELECT_AREA);
    }

    private void gotoSignInPage() {
        Intent intent = new Intent(this, SignInPageActivity.class);
        intent.putExtra("dashboard_login", true);
        startActivityForResult(intent, RC_DASH_LOGIN);
    }

    private void refreshDrawerItemsList() {
        drawerAdapter.notifyDataSetChanged();
    }

    /*private void reloadDrawerHeader() {
        getUser();
        setUserInDrawerHeader();
    }*/

    private void onResumeOperations() {
        boolean reloadRequired = pref.getReloadRequired();

        if (reloadRequired) {
            pref.putReloadRequired(false);
            reloadActivity(false);
        } else {
            if (firstRunDashboardActivity) {
                firstRunDashboardActivity = false;
            } else {
                loadDashFrag();
            }

            updateCart();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_CALL_PHONE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fireCallIntent();
                } else {
                    Toast.makeText(context, "Operation cancelled", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_DASH_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                reloadActivity(false);
            }
        } else if (requestCode == RC_SELECT_AREA) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                reloadActivity(false);
            }
        } else if (requestCode == RC_MEMBER_LOGIN) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                reloadActivity(true);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerArrow:
                onBackPressed();
                break;
            case R.id.headerMenu:
                if (drawerLayout.isDrawerOpen(drawerListView)) {
                    drawerLayout.closeDrawer(drawerListView);
                } else {
                    drawerLayout.openDrawer(drawerListView);
                }
                break;

            case R.id.cartLayout:
            case R.id.cartButton:
            case R.id.cartQuantityTextView:
                gotoBookingSummary();
                break;

            case R.id.whatsAppLayout:
            case R.id.whatsAppButton:
                sendTowhatsapp();
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
        //updateCart();
        //onResumeOperations();
        loadDashFrag();
        if (!isToWebView)
            checkIsMember();
    }

    /*@Override
    protected void onResumeFragments() {
        super.onResume();
        startTime = Calendar.getInstance();
        //updateCart();
        //onResumeOperations();
        loadDashFrag();
        if (!isToWebView)
            checkIsMember();
    }*/

    private void checkIsMember() {
        if (userId != null && !userId.equalsIgnoreCase("")) {
            /*isMember = true;
            pref.putIsUserMember(true);*/
            new GetMemberAsyncTask().execute(null, null, null);
        } else {
            getUser();
            if (userId != null) {
                /*isMember = true;
                pref.putIsUserMember(true);*/
                new GetMemberAsyncTask().execute(null, null, null);

            } else {
                isMember = false;
                pref.putIsUserMember(false);
                loadDashFrag();
            }
        }

    }

    private void checkIsMemberWebView() {
        if (userId != null && !userId.equalsIgnoreCase("")) {
            /*isMember = true;
            pref.putIsUserMember(true);*/
            new GetMemberAsyncTaskForWebView().execute(null, null, null);
        } else {
            getUser();
            if (userId != null) {
                new GetMemberAsyncTaskForWebView().execute(null, null, null);
            } else {
                isMember = false;
                pref.putIsUserMember(false);
                //gotowebview
               /* isToWebView=false;
                Intent i = new Intent(this, WebPageActivity.class);
                startActivity(i);*/
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        firebaseHelper.logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_CATEGORY);
    }

    @Override
    public void onBackPressed() {
        otherFragmentsActive = pref.getOtherFragmentsActive();

        if (otherFragmentsActive) {
            pref.putOtherFragmentsActive(false);
            loadDashFrag();
        } else {
            super.onBackPressed();
        }
    }

    private class GetMemberAsyncTask extends AsyncTask<Void, Void, Boolean> {
        boolean isSubmitted = false;
        ProgressDialog progressDialog;


        GetMemberAsyncTask() {
            progressDialog = new ProgressDialog(context);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Processing...");
                progressDialog.show();
            }

        }

        protected Boolean doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            try {
                isSubmitted = restWebServices.getIsUserMember(userId);
            } catch (Exception ex) {

            }
            return isSubmitted;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                pref.putIsUserMember(result);

                loadDashFrag();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    private class GetMemberAsyncTaskForWebView extends AsyncTask<Void, Void, Boolean> {
        boolean isSubmitted = false;
        ProgressDialog progressDialog;


        GetMemberAsyncTaskForWebView() {
            progressDialog = new ProgressDialog(context);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Processing...");
                progressDialog.show();
            }

        }

        protected Boolean doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            try {
                isSubmitted = restWebServices.getIsUserMember(userId);
            } catch (Exception ex) {

            }
            return isSubmitted;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                pref.putIsUserMember(result);
                //loadDashFrag();
                isToWebView = false;
                if (!result) {
                    Intent i = new Intent(DashboardActivity.this, WebPageActivity.class);
                    i.putExtra("userId", userId);
                    startActivity(i);
                } else {
                    reloadActivity(false);
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

}