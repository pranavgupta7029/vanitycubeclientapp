package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.webservices.RestWebServices;

public class ForgotPasswordActivity extends Activity implements OnClickListener {
    private EditText mMobileEditText;
    private RestWebServices mRestWebservices;
    private int RC_CHANGE_PASSWORD = 801;
    private boolean changePassword = false;
    private boolean dashboardLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot_password_new_design);

        mMobileEditText = findViewById(R.id.forgot_pass_email);
        Button mForgotPassword = findViewById(R.id.forgot_pass_button);

        mForgotPassword.setOnClickListener(this);
        mRestWebservices = new RestWebServices();
        changePassword = getIntent().getBooleanExtra("changePassword", false);
        dashboardLogin = getIntent().getBooleanExtra("dashboard_login", false);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.forgot_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    class ResetPasswordAsyncTask extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;
        String userMobile;
        String userID;

        ResetPasswordAsyncTask(String pUserMobile) {
            this.userMobile = pUserMobile;
            progressDialog = new ProgressDialog(ForgotPasswordActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Void... url) {
            userID = mRestWebservices.forgotPassword(userMobile);
            return userID;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (result.length() > 0) {
                Toast.makeText(ForgotPasswordActivity.this, "Please check your SMS, " + "a new password has been sent to you",
                        Toast.LENGTH_LONG).show();
                Intent int_next = new Intent(ForgotPasswordActivity.this, ChangePasswordActivity.class);
                int_next.putExtra("user_id", result);
                int_next.putExtra("changePassword", changePassword);
                int_next.putExtra("dashboard_login", dashboardLogin);
                startActivityForResult(int_next, RC_CHANGE_PASSWORD);
            } else {
                Toast.makeText(ForgotPasswordActivity.this, "Contact number does not exist!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private boolean resetPassword() {
        String mobileNumberString = mMobileEditText.getText().toString().trim();

        if (!isValidString(mobileNumberString)) {
            Toast.makeText(ForgotPasswordActivity.this, "Please enter a valid mobile number", Toast.LENGTH_LONG).show();
            return false;
        } else {
            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                new ResetPasswordAsyncTask(mobileNumberString).execute(null, null, null);
            } else {
                Toast.makeText(ForgotPasswordActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
            }
        }

        return false;
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode, boolean dashboard) {
        Intent intBack = new Intent();
        intBack.putExtra("dashboard_login", dashboardLogin);
        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_CHANGE_PASSWORD) {
            if (resultCode == RESULT_OK) {
                goBack(true, data.getBooleanExtra("dashboard_login", false));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack(false, dashboardLogin);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forgot_pass_button:
                resetPassword();
                break;

            default:
                break;
        }
    }


}
