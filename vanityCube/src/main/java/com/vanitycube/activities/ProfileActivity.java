package com.vanitycube.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.utilities.ImagePathUtil;
import com.vanitycube.utilities.RoundedImageView;
import com.vanitycube.webservices.RestWebServices;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProfileActivity extends Activity implements OnClickListener, DatePickerDialog.OnDateSetListener {
    private Context context;
    private String TAG, checkYourInternet;
    private EditText nameEditText, phoneEditText, emailEditText, DOBEditText;
    private TextView ageTextView, footerButtonTextView;
    private RoundedImageView roundedImageView;
    private RestWebServices restWebServices;
    private Gson gson;
    private UserModel userModel;
    private String editProfile, userId, userReferralName, userName, userEmail, userDOB, userAge, userProfilePhotoPath, userContact;
    private ImageView editFlagProfilePlus;
    private SharedPref pref;
    private static final int REQUEST_CAMERA = 0, REQUEST_READ_WRITE_PERMISSION = 1, TAKE_PHOTO = 2, SELECT_FILE = 3;
    private List<String> picOptionsList;

    public void showToast(String message) {
        Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);

        context = ProfileActivity.this;

        TAG = getResources().getString(R.string.profile_activity_tag);
        checkYourInternet = getResources().getString(R.string.check_your_internet_text);
        editProfile = getResources().getString(R.string.edit_profile_text);

        pref = new SharedPref(VcApplicationContext.getInstance());
        gson = new Gson();
        restWebServices = new RestWebServices();

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_CART);

        getUser();
        populateOptionsList();
        setHeader();
        setFooter();
        init();
        loadUserProfilePhoto();
        preFill();
        setupProfile();
    }

    private void setHeader() {
        RelativeLayout headerLayout = findViewById(R.id.header);
        Button headerMenuButton = headerLayout.findViewById(R.id.headerMenu);
        headerMenuButton.setBackgroundResource(R.drawable.arrow_left);
        headerLayout.findViewById(R.id.headerFirstButton).setOnClickListener(this);
        headerMenuButton.setOnClickListener(this);
        TextView mHeaderText = headerLayout.findViewById(R.id.headerText);
        mHeaderText.setText(R.string.profile_activity_tag);
    }

    public boolean hasCamera() {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void populateOptionsList() {
        picOptionsList = new ArrayList<>();
        picOptionsList.add("Choose from Library");
        if (hasCamera()) {
            picOptionsList.add("Take Photo");
        }
        picOptionsList.add("Cancel");
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    public void getUser() {
        String json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();

            if (isValidString(userId)) {
                userReferralName = userModel.getReferralName();
                userName = userModel.getName();
                userEmail = userModel.getEmail();
                userDOB = userModel.getDOB();
                userAge = getUserAge(userDOB);
                userProfilePhotoPath = userModel.getProfileImagePath();
                userContact = userModel.getNumber();
            } else {
                Toast.makeText(context, "Please login first", Toast.LENGTH_LONG).show();
            }
        }
    }

    /*  -------------------------------------
        |
        |   DOWNLOAD USER PROFILE PHOTO
        |
        -------------------------------------
    */

    private void loadUserProfilePhoto() {
        String imageDownloadURL = ApplicationSettings.PROFILE_IMAGE_PATH + userProfilePhotoPath;

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new DownloadImageAsyncTask(imageDownloadURL).execute(null, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private class DownloadImageAsyncTask extends AsyncTask<Void, Void, Bitmap> {
        ProgressDialog progressDialog;
        String imageURL;

        DownloadImageAsyncTask(String imageURL) {
            this.imageURL = imageURL;
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }

        protected Bitmap doInBackground(Void... url) {
            Bitmap bitmap = null;

            try {
                InputStream in = new java.net.URL(imageURL).openStream();
                if (in != null) {
                    bitmap = getScaledBitmapFromInputStream(in);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap result) {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (result != null) {
                roundedImageView.setImageBitmap(result);
            }
        }
    }

    private String getUserAge(String dateString) {
        int age = 0;

        if (dateString.equals("0000-00-00")) {
            return "";
        }

        try {
            if (isValidString(dateString)) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat givenFormat = new SimpleDateFormat("yyyy-MM-dd");

                Date date = givenFormat.parse(dateString);

                if (date != null) {
                    @SuppressLint("SimpleDateFormat") int day = Integer.parseInt(new SimpleDateFormat("dd").format(date));
                    @SuppressLint("SimpleDateFormat") int month = Integer.parseInt(new SimpleDateFormat("MM").format(date));
                    @SuppressLint("SimpleDateFormat") int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));

                    final Calendar calenderToday = Calendar.getInstance();
                    int currentYear = calenderToday.get(Calendar.YEAR);
                    int currentMonth = 1 + calenderToday.get(Calendar.MONTH);
                    int todayDay = calenderToday.get(Calendar.DAY_OF_MONTH);

                    age = currentYear - year;

                    if (month > currentMonth) {
                        --age;
                    } else if (month == currentMonth) {
                        if (day > todayDay) {
                            --age;
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return String.valueOf(age);
    }

    /*  --------------------------------------------------------------------------
        |
        |   SELECT PROFILE PHOTO & SET A ROUNDED IMAGEVIEW
        |
        --------------------------------------------------------------------------
    */

    private void init() {
        roundedImageView = findViewById(R.id.profile_image);
        roundedImageView.setOnClickListener(this);
        roundedImageView.setClickable(false);

        TextView mAddAddressImageView = findViewById(R.id.manageAddressButton);
        nameEditText = findViewById(R.id.userNameEditText);
        ageTextView = findViewById(R.id.userAgeTextView);

        TextView referralCodeEditText = findViewById(R.id.userReferralCodeEditText);
        phoneEditText = findViewById(R.id.userPhoneEditText);
        emailEditText = findViewById(R.id.userEmailEditText);
        DOBEditText = findViewById(R.id.userDOBEditText);

        LinearLayout submitButtonLayout = findViewById(R.id.submitButton);
        footerButtonTextView = submitButtonLayout.findViewById(R.id.footerButton);
        submitButtonLayout.setOnClickListener(this);
        referralCodeEditText.setText(userReferralName);
        editFlagProfilePlus = findViewById(R.id.edit_plus);

        mAddAddressImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                gotoMultipleAddressActivity();
            }
        });
    }

    /*  -------------------------------------
        |
        |   PREFILL EDIT TEXT
        |
        -------------------------------------
    */

    public void preFill() {
        nameEditText.setText(userName);
        phoneEditText.setText(userContact);

        if (userEmail.length() > 24) {
            userEmail = userEmail.substring(0, userEmail.length() - 3);
            userEmail = userEmail.concat("...");
        }

        emailEditText.setText(userEmail);

        if (userDOB.split("-")[0].length() == 4) {
            String[] text = userDOB.split("-");
            DOBEditText.setText(text[2].concat("-").concat(text[1]).concat("-").concat(text[0]));
        } else {
            DOBEditText.setText(userDOB);
        }

        if (isValidString(userAge)) {
            ageTextView.setText(userAge);
        }
    }

    public void setDOB(String dob) {
        DOBEditText.setText(dob);
    }

    private void selectImage() {
        try {
            final Activity activity = ProfileActivity.this;
            final CharSequence[] cs = picOptionsList.toArray(new CharSequence[picOptionsList.size()]);

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Add Photo!");

            builder.setItems(cs, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (cs[item].equals("Take Photo")) {

                        if ((Build.VERSION.SDK_INT >= 23)) {
                            if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.CAMERA,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                            } else {
                                fireTakePhotoIntent();
                            }
                        } else {
                            fireTakePhotoIntent();
                        }

                    } else if (cs[item].equals("Choose from Library")) {

                        if ((Build.VERSION.SDK_INT >= 23)) {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_READ_WRITE_PERMISSION);
                            } else {
                                fireSelectFileIntent();
                            }
                        } else {
                            fireSelectFileIntent();
                        }

                    } else if (cs[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });

            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireTakePhotoIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    private void fireSelectFileIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == TAKE_PHOTO) {
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        Bundle extras = data.getExtras();
                        Bitmap bitmap = (Bitmap) extras.get("data");
                        onTakePhotoResult(bitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(context, "User cancelled image capture", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == SELECT_FILE) {

                if (resultCode == Activity.RESULT_OK) {

                    try {
                        final Uri fileUri = data.getData();
                        //final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                        //final Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                        //final Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
                        uploadPhotoFromUri(fileUri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(context, "User cancelled image selection", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Sorry! Failed to select image", Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    fireTakePhotoIntent();
                } else {
                    showToast("Operation cancelled");
                }

                break;

            case REQUEST_READ_WRITE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    fireSelectFileIntent();
                } else {
                    showToast("Operation cancelled");
                }

                break;
        }
    }

    private Bitmap getScaledBitmapFromInputStream(InputStream in) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        int inSampleSize = calculateInSampleSize(sizeOptions);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(in, null, sizeOptions);
    }

    private int calculateInSampleSize(BitmapFactory.Options options) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > 800 || width > 800) {
            final int heightRatio = Math.round((float) height / (float) 800);
            final int widthRatio = Math.round((float) width / (float) 800);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    private void onTakePhotoResult(Bitmap bitmap) {
        try {
            ByteArrayOutputStream lBytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, lBytes);

            File lDestination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            String filePath = lDestination.getAbsolutePath();
            lDestination.createNewFile();

            FileOutputStream lFileOutputStream;
            lFileOutputStream = new FileOutputStream(lDestination);
            lFileOutputStream.write(lBytes.toByteArray());
            lFileOutputStream.close();

            roundedImageView.setImageBitmap(bitmap);
            uploadPhoto(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadPhotoFromUri(Uri fileURI) {
        String filePath = ImagePathUtil.getPath(ProfileActivity.this, fileURI);

        //Make a bitmap from this path and upload
        BitmapFactory.Options lObjOptions = new BitmapFactory.Options();
        lObjOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(filePath, lObjOptions);

        final int REQUIRED_SIZE = 200;
        int scale = 1;

        while (lObjOptions.outWidth / scale / 2 >= REQUIRED_SIZE && lObjOptions.outHeight / scale / 2 >= REQUIRED_SIZE) {
            scale *= 2;
        }

        lObjOptions.inSampleSize = scale;
        lObjOptions.inJustDecodeBounds = false;

        Bitmap bitmap = BitmapFactory.decodeFile(filePath, lObjOptions);

        roundedImageView.setImageBitmap(bitmap);
        uploadPhoto(filePath);
    }

    private void setupEditProfile() {
        roundedImageView.setClickable(true);
        footerButtonTextView.setText(getResources().getString(R.string.save_text));
        nameEditText.setEnabled(true);
        emailEditText.setEnabled(true);
        emailEditText.setBackgroundResource(R.drawable.line);
        editFlagProfilePlus.setBackgroundResource(R.drawable.change_profile_pic_vector);
        DOBEditText.setEnabled(true);
        DOBEditText.setBackgroundResource(R.drawable.line);
        DOBEditText.setOnClickListener(this);
    }

    @SuppressWarnings("deprecation")
    public void setupProfile() {
        roundedImageView.setClickable(false);
        footerButtonTextView.setText(editProfile);
        nameEditText.setEnabled(false);
        phoneEditText.setEnabled(false);
        phoneEditText.setBackgroundDrawable(null);
        emailEditText.setEnabled(false);
        emailEditText.setGravity(Gravity.START);
        emailEditText.setBackgroundDrawable(null);
        DOBEditText.setEnabled(false);
        editFlagProfilePlus.setBackgroundDrawable(null);
        DOBEditText.setBackgroundDrawable(null);
        DOBEditText.setFocusableInTouchMode(false);
        DOBEditText.setFocusable(false);
    }

    private boolean emailValidator(String pEmail) {
        String regex = "^(.+)@(.+)\\.(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pEmail);
        return matcher.matches();
    }

    private void setFooter() {
        TextView FooterButton = findViewById(R.id.footerButton);
        FooterButton.setVisibility(View.VISIBLE);
        FooterButton.setText(editProfile);
    }

    public void emailExists(String name, String DOB, String imageURL, String email) {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new EmailExistsAsyncTask(name, DOB, imageURL, email).execute(null, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private class EmailExistsAsyncTask extends AsyncTask<Void, Void, Boolean> {
        String name;
        String dob;
        String imageURL;
        String email;

        EmailExistsAsyncTask(String name, String dob, String imageURL, String email) {
            this.name = name;
            this.dob = dob;
            this.imageURL = imageURL;
            this.email = email;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            return restWebServices.checkEmailExistsBool(email);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (result && !email.equalsIgnoreCase(userEmail)) {
                    showToast("Email already belongs to an existing account! Please enter a different one");
                } else {
                    new UpdateUserAsyncTask(name, dob, imageURL, email).execute(null, null, null);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdateUserAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private String name, DOB, imageURL, email;
        private boolean isChanged = false;
        ProgressDialog progressDialog;

        UpdateUserAsyncTask(String name, String DOB, String imageURL, String email) {
            this.name = name;
            this.DOB = DOB;
            this.imageURL = imageURL;
            this.email = email;
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Updating Profile");
            progressDialog.show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            isChanged = restWebServices.updateUser(name, DOB, imageURL, email);
            return isChanged;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            try {
                if (result) {
                    userModel.setName(name);
                    userModel.setDOB(DOB);
                    userModel.setEmail(email);
                    userModel.setProfileImagePath(imageURL);
                    pref.putUserModel(gson.toJson(userModel));
                    showToast("Profile updated");
                    reloadActivity();
                } else {
                    showToast("Profile update failed, please try again");
                }
            } catch (Exception e) {
                Log.e(TAG, "<<ProfileActivity Exception>>" + e.getMessage());
            }
        }
    }

    public void uploadPhoto(String filePath) {
        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new SingleUploadMediaObjectTask(filePath).execute(null, null, null);
        } else {
            Toast.makeText(context, checkYourInternet, Toast.LENGTH_LONG).show();
        }
    }

    private class SingleUploadMediaObjectTask extends AsyncTask<Void, Void, String> {
        private String filePath, returnFilePath;
        ProgressDialog progressDialog;

        SingleUploadMediaObjectTask(String filePath) {
            this.filePath = filePath;
            progressDialog = new ProgressDialog(ProfileActivity.this);
            progressDialog.setMessage("Uploading");
            progressDialog.show();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @SuppressWarnings("deprecation")
        @Override
        protected String doInBackground(Void... url) {
            try {
                File file = new File(filePath);
                returnFilePath = restWebServices.updateProfilePicOld(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return returnFilePath;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            try {
                if (isValidString(returnFilePath)) {
                    userModel.setProfileImagePath(returnFilePath);
                    pref.putUserModel(gson.toJson(userModel));
                    new UpdateUserAsyncTask(userModel.getName(), userModel.getDOB(), userModel.getProfileImagePath(), userModel.getEmail()).execute(null, null, null);
                } else {
                    showToast("Upload failed, please try again");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*  -------------------------------------
        |
        |   GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void gotoMultipleAddressActivity() {
        Intent intent = new Intent(context, MultipleAddressActivity.class);
        intent.putExtra("user_id", userId);
        intent.putExtra("user_name", userName);
        intent.putExtra("came_from_settings", true);
        startActivity(intent);
    }

    private void reloadActivity() {
        Intent reloadIntent = getIntent();
        startActivity(reloadIntent);
        finish();
    }

    /*  -------------------------------------
        |
        |   DEFAULT METHODS
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        String strVal = footerButtonTextView.getText().toString();

        switch (v.getId()) {
            case R.id.headerFirstButton:
            case R.id.headerMenu:
                onBackPressed();
                break;

            case R.id.submitButton:
            case R.id.footerButton:


                if (strVal.equals("Edit Profile")) {
                    setupEditProfile();
                } else if (strVal.equals("Save")) {
                    String name = nameEditText.getText().toString();
                    String dob = DOBEditText.getText().toString();
                    String email = emailEditText.getText().toString();

                    if (!emailValidator(email)) {
                        Toast.makeText(context, "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    emailExists(name, dob, userProfilePhotoPath, email);
                }

                break;

            case R.id.profile_image:
                if (strVal.equals("Save")) {
                    selectImage();
                }
                break;

            case R.id.userDOBEditText:
                Calendar cal = Calendar.getInstance();

                DatePickerDialog dateDialog = new DatePickerDialog(context, new OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        setDOB(String.valueOf(year) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(dayOfMonth));
                        ageTextView.setText(getUserAge(DOBEditText.getText().toString()));
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

                dateDialog.getDatePicker().setMaxDate(new Date().getTime());
                dateDialog.show();

                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        pref.putReloadRequired(true);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
    }
}
