package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.fonts.VCTextView;
import com.vanitycube.model.CheckOTPResponseModel;
import com.vanitycube.model.signinup.OtpVerificationForNewUser;
import com.vanitycube.model.signinup.SendOtpToUserResponse;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OtpActivity extends Activity implements OnClickListener,ServerResponseListener {
    private String TAG;
    private EditText otpText;
    private RestWebServices mRestWebservices;
    private SharedPref pref;
    private UserModel userModel;
    private Gson gson;
    private String userId;
    private String googleId;
    private String facebookId;
    private boolean dashboardLogin,memberLogin;
    ProgressDialog progressDialog;
    private ApiManager apiManager;
    private static final String RESEND_OTP_TO_USER="resendOTPToUser";
    private static final String SUBMIT_OTP="submitOtp";
    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_otp_new_design);

        TAG = getResources().getString(R.string.otp_activity_tag);

        mRestWebservices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());
        gson = new Gson();

        googleId = getIntent().getStringExtra("google_id");
        facebookId = getIntent().getStringExtra("facebook_id");
        dashboardLogin = getIntent().getBooleanExtra("dashboard_login", false);
        memberLogin = getIntent().getBooleanExtra("membership_login", false);

        progressDialog = new ProgressDialog(OtpActivity.this);

        //Get user
        userModel = getIntent().getParcelableExtra("user_model");
        if (userModel == null && userModel.getID() == null) {
            getUser();
        } else {
            userId = userModel.getID();
        }

        otpText = findViewById(R.id.otpEditText);
        otpText.setEnabled(true);
        otpText.setClickable(true);

        final Button verifyButton = findViewById(R.id.verifyButton);
        Button resendButton = findViewById(R.id.resendButton);
        VCTextView goBackTextView = findViewById(R.id.goBackTextView);

        verifyButton.setOnClickListener(this);
        resendButton.setOnClickListener(this);
        goBackTextView.setOnClickListener(this);
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {

                // b=sender.endsWith("WNRCRP");  //Just to fetch otp sent from WNRCRP
                String otp=getOtp(message);

                //String code = parseCode(message);//Parse verification code
                //etCode.setText(code);//set code in edit text
                //then you can send verification code to server
                otpText.setText(otp);
                verifyButton.performClick();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private String getOtp(String msg){
        Pattern pattern = Pattern.compile("(\\d{4})");

//   \d is for a digit
//   {} is the number of digits here 4.

        Matcher matcher = pattern.matcher(msg);
        String val = "";
        if (matcher.find()) {
            val = matcher.group(1);  // 4 digit number
        }
        return val;
    }

    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (Objects.equals(TAG, RESEND_OTP_TO_USER) && responseObj instanceof SendOtpToUserResponse) {
            SendOtpToUserResponse sendOtpToUserResponse = (SendOtpToUserResponse) responseObj;
            //if (sendOtpToUserResponse.getResponsedata().getSuccess() == 1) {

            Toast.makeText(OtpActivity.this, sendOtpToUserResponse.getResponsedata().getMsg(), Toast.LENGTH_SHORT).show();
                    /*otpLen.setVisibility(View.VISIBLE);
                    passwordLen.setVisibility(View.VISIBLE);
                    sendOtpLen.setVisibility(View.GONE);
                    loginBtnLen.setVisibility(View.VISIBLE);
                    resendOtp.setVisibility(View.VISIBLE);
                    or.setVisibility(View.VISIBLE);
                    phoneNumLen.setVisibility(View.GONE);*/
            // }
        } else if (Objects.equals(TAG, SUBMIT_OTP) && responseObj instanceof OtpVerificationForNewUser) {
            OtpVerificationForNewUser otpVerificationForNewUser = (OtpVerificationForNewUser) responseObj;
            if(otpVerificationForNewUser.getResponsedata().getMessageCode()>=1){
                userModel.setNumberVerified(true);
                userModel.setGoogleId(googleId);
                userModel.setFacebookId(facebookId);

                pref.putUserModel(gson.toJson(userModel));
                pref.putGuestAddresses("");
                pref.putReloadRequired(true);

                Map<String, Object> afEventValue = new HashMap<>();
                afEventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
                AppsFlyerLib.trackEvent(VcApplicationContext.getInstance(), AFInAppEventType.LOGIN, afEventValue);
                new FireBaseHelper(OtpActivity.this).logLogin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

                Toast.makeText(OtpActivity.this, "OTP verified!, You may login now", Toast.LENGTH_LONG).show();

                goBack(true);
            } else {
                Toast.makeText(OtpActivity.this, otpVerificationForNewUser.getResponsedata().getMsg(), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(OtpActivity.this, "Something went wrong! Please try again...", Toast.LENGTH_SHORT).show();
        }


        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void positiveResponse(String TAG, String response) {
        String popints = "";

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {

        if (OtpActivity.this.isDestroyed() || OtpActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
            return;
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    private void resendOtpToUser(){
        progressDialog.setMessage("Sending new otp...");
        progressDialog.show();
        apiManager = new ApiManager(OtpActivity.this, OtpActivity.this);
        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SendOtpToUserResponse.class);
        HashMap<String, String> params = new HashMap<>();
        params.put("otp", userModel.getNumber());
        apiManager.getStringPostResponse(RESEND_OTP_TO_USER, ApplicationSettings.BASE_URL_PAY+RestWebServices.METHOD_RESENDOTP_TO_USER, params);
    }

    private void otpSubmit(){
        progressDialog.setMessage("Verfiying otp...");
        progressDialog.show();
        apiManager = new ApiManager(OtpActivity.this, OtpActivity.this);
        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(OtpVerificationForNewUser.class);
        HashMap<String, String> params = new HashMap<>();
        params.put("otp", otpText.getText().toString());
        params.put("contact", userModel.getNumber());
        apiManager.getStringPostResponse(SUBMIT_OTP, ApplicationSettings.BASE_URL_PAY+RestWebServices.METHOD_OTP_VERFICATION_NEW_USER, params);
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            userId = userModel.getID();
        }
    }

    class CheckOtpAsyncTask extends AsyncTask<Void, Void, CheckOTPResponseModel> {
        CheckOTPResponseModel checkOTPResponseModel = new CheckOTPResponseModel();
        ProgressDialog progressDialog;
        String otpText, responseMessage;
        Boolean isLogin = false;

        CheckOtpAsyncTask(String otpText) {
            this.otpText = otpText;
            progressDialog = new ProgressDialog(OtpActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Verifying OTP");
            progressDialog.show();
        }

        @Override
        protected CheckOTPResponseModel doInBackground(Void... url) {
            try {
                checkOTPResponseModel = mRestWebservices.checkOTP(otpText, userId);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

            return checkOTPResponseModel;
        }

        @Override
        protected void onPostExecute(CheckOTPResponseModel result) {
            super.onPostExecute(result);

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            int success = result.getSuccess();
            JSONObject data = result.getData();
            String errorMessage = result.getResult();

            if (success == 1 && data != null) {
                UserModel userModel = new UserModel(data);

                userModel.setNumberVerified(true);
                userModel.setGoogleId(googleId);
                userModel.setFacebookId(facebookId);

                pref.putUserModel(gson.toJson(userModel));
                pref.putGuestAddresses("");
                pref.putReloadRequired(true);

                Map<String, Object> afEventValue = new HashMap<>();
                afEventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
                AppsFlyerLib.trackEvent(VcApplicationContext.getInstance(), AFInAppEventType.LOGIN, afEventValue);
                new FireBaseHelper(OtpActivity.this).logLogin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

                Toast.makeText(OtpActivity.this, "OTP verified!, You may login now", Toast.LENGTH_LONG).show();

                goBack(true);
            } else {
                if (isValidString(errorMessage)) {
                    Toast.makeText(OtpActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(OtpActivity.this, "An error occurred, please try again", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private class ResendOtpAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog progressDialog;
        private String userId;

        ResendOtpAsyncTask() {
            userId = userModel.getID();
            progressDialog = new ProgressDialog(OtpActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Resending OTP");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            boolean isDone = false;

            try {
                isDone = mRestWebservices.resendOTP(userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return isDone;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (result) {
                Toast.makeText(OtpActivity.this, "A new OTP has been sent to your phone, please check your SMS", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(OtpActivity.this, "An error occurred, please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu, this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.otp, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.verifyButton:
                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    if (otpText.getText() != null && !otpText.getText().toString().equals(""))
                        otpSubmit();
                        //new CheckOtpAsyncTask(otpText.getText().toString()).execute(null, null, null);
                    else
                        Toast.makeText(OtpActivity.this, "Please provide OTP", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(OtpActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.resendButton:
                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    resendOtpToUser();
                    //new ResendOtpAsyncTask().execute(null, null, null);
                } else {
                    Toast.makeText(OtpActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.goBackTextView:
                goBack(false);
                break;

            default:
                break;

        }
    }

}
