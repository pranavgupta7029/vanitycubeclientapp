package com.vanitycube.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.model.GetUserStateResponseModel;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONObject;

// Created by prade on 11/15/2017.

public class GuestLoginActivity extends AppCompatActivity implements View.OnClickListener {
    private RestWebServices mRestWebservices;
    private EditText nameEditText, mobileNumberEditText;
    private String TAG;
    private SharedPref pref;
    private UserModel userModel;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_guest_login_new_design);

        TAG = "GuestLogin";
        pref = new SharedPref(VcApplicationContext.getInstance());
        mRestWebservices = new RestWebServices();
        gson = new Gson();

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.otf");
        nameEditText = findViewById(R.id.nameEditText);
        mobileNumberEditText = findViewById(R.id.mobileNumberEditText);

        //Continue button
        Button mContinueLoginButton = findViewById(R.id.continue_button);
        mContinueLoginButton.setTypeface(tf);
        mContinueLoginButton.setOnClickListener(this);

        userModel = new UserModel();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    /*  -------------------------------------
        |
        |  GET USER STATE ASYNC TASK
        |
        -------------------------------------
    */

    private class GetUserStateAsyncTask extends AsyncTask<Void, Void, GetUserStateResponseModel> {
        private String name, mobileNum;
        private boolean isGuest;
        private ProgressDialog progressDialog;

        GetUserStateAsyncTask(String name, String mobileNum) {
            progressDialog = new ProgressDialog(GuestLoginActivity.this);
            this.name = name;
            this.mobileNum = mobileNum;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Logging In");
            progressDialog.show();
        }

        protected GetUserStateResponseModel doInBackground(Void... url) {
            mRestWebservices = new RestWebServices();
            return mRestWebservices.getUserState(name, mobileNum);
        }

        protected void onPostExecute(GetUserStateResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result.getSuccess() == 1) {
                    JSONObject resultObject = result.getResult();
                    String isGuestUser = resultObject.getString("isGuestUser");

                    if (isGuestUser.equals("1")) {
                        String userId = resultObject.getString("userId");
                        userModel.setID(userId);
                        userModel.setName(name);
                        userModel.setNumber(mobileNum);
                        userModel.setGuest(true);
                        userModel.setNumberVerified(false);

                        // Update shared preferences
                        pref.putUserModel(gson.toJson(userModel));
                        pref.putReloadRequired(true);

                        goBack(true);

                    } else {
                        Toast.makeText(GuestLoginActivity.this, "User already exists, please login", Toast.LENGTH_SHORT).show();
                        pref.putGuestUserPrefillPhone(mobileNum);
                        goBack(false);
                    }

                } else {
                    String errorMessage = result.getMessage();

                    if (isValidString(errorMessage)) {
                        Toast.makeText(GuestLoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(GuestLoginActivity.this, "An error occurred while logging in, please try again", Toast.LENGTH_SHORT).show();
                    }

                    goBack(false);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        String name = nameEditText.getText().toString().trim();
        String mobileNum = mobileNumberEditText.getText().toString().trim();

        switch (v.getId()) {
            case R.id.continue_button:

                if (isValidString(name) && isValidString(mobileNum)) {
                    new GetUserStateAsyncTask(name, mobileNum).execute(null, null, null);
                } else {
                    Toast.makeText(GuestLoginActivity.this, "Fields cannot be blank", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }
}
