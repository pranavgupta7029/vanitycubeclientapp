package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.adapter.BookingSummaryAdapter;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.database.VcDatabaseQuery;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.fonts.VCButton;
import com.vanitycube.model.ConvienenceFeeResponse;
import com.vanitycube.model.ConvienenceFeeResponseMain;
import com.vanitycube.model.CouponModel;
import com.vanitycube.model.ServiceTypeModel;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.ConfirmDialogHelper;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.ResponseHandler;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class BookingSummaryActivity extends Activity implements OnClickListener, ServerResponseListener {
    private String TAG;
    private ListView serviceTypesListView;
    private Calendar startTime;

    private RelativeLayout emptyCartLayout;
    private LinearLayout checkout;
    private RelativeLayout finalAmountLayout, addMemberRel;
    private RestWebServices mRestWebServices;
    private ArrayList<ServiceTypeModel> mFinalSelectedServiceTypes;

    private SharedPref pref;
    private Gson gson;
    private boolean isLoggedIn, isGuestUser;

    private CouponModel couponModel;
    private String minAMount;

    private BookingSummaryAdapter bookingSummaryAdapter;
    private TextView finalAmountTextView, couponLabelTextView, couponDiscountTextView, cartQuantityTextView, percentage;
    private FlexboxLayout couponDiscountBarLayout;

    private int RC_BOOKING_LOGIN = 202;
    private String finalBillAmount = "";
    public static int RC_MEMBER_LOGIN = 205;
    String userId;
    private RestWebServices restWebServices;
    private Boolean isToWebView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_booking_summary);

        TAG = getResources().getString(R.string.booking_summary_activity_tag);

        couponModel = new CouponModel();
        mRestWebServices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());
        restWebServices = new RestWebServices();
        gson = new Gson();

        //Get user data
        getUser();

        FireBaseHelper firebaseHelper = new FireBaseHelper(this);
        firebaseHelper.logPage(ApplicationSettings.PAGE_CART);

        minAMount = "0";
        setHeader();
        //Get selected service types list
        getFinalSelectedServiceTypesList();

        emptyCartLayout = findViewById(R.id.emptyCartLayout);
        finalAmountLayout = findViewById(R.id.finalAmountLayout);
        finalAmountTextView = findViewById(R.id.finalAmountTextView);
        serviceTypesListView = findViewById(R.id.bookingSummaryListView);

        couponDiscountBarLayout = findViewById(R.id.couponDiscountBarLayout);
        couponLabelTextView = findViewById(R.id.couponLabelTextView);
        couponDiscountTextView = findViewById(R.id.couponDiscountTextView);
        addMemberRel = findViewById(R.id.addMemberRel);
        percentage = findViewById(R.id.percentage);
        if (!pref.getIsUserMember()) {
            addMemberRel.setVisibility(View.VISIBLE);
            addMemberRel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = pref.getUserId();
                    if (userId != null && !userId.equalsIgnoreCase("")) {

                        Intent i = new Intent(BookingSummaryActivity.this, WebPageActivity.class);
                        i.putExtra("userId", userId);
                        startActivity(i);

                    } else {
                        Intent intent = new Intent(BookingSummaryActivity.this, SignInPageActivity.class);
                        intent.putExtra("membership_login", true);
                        startActivityForResult(intent, RC_MEMBER_LOGIN);
                    }
                }
            });
        } else {
            addMemberRel.setVisibility(View.GONE);
        }

        getOfferCoupon();

        //populateBookingList();


        //getConvenienceFee();

        checkout = findViewById(R.id.checkout_layout);
        checkout.setOnClickListener(this);

        LinearLayout addMoreServices = findViewById(R.id.add_more_services_layout);
        addMoreServices.setOnClickListener(this);

        //fetchAreasForMinAmount();
    }

    private class GetMemberAsyncTaskForWebView extends AsyncTask<Void, Void, Boolean> {
        boolean isSubmitted = false;
        ProgressDialog progressDialog;


        GetMemberAsyncTaskForWebView() {
            progressDialog = new ProgressDialog(BookingSummaryActivity.this);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Processing...");
                progressDialog.show();
            }

        }

        protected Boolean doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            try {
                isSubmitted = restWebServices.getIsUserMember(userId);
            } catch (Exception ex) {

            }
            return isSubmitted;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                pref.putIsUserMember(result);
                //loadDashFrag();
                isToWebView = false;
                if (!result) {
                    Intent i = new Intent(BookingSummaryActivity.this, WebPageActivity.class);
                    i.putExtra("userId", userId);
                    startActivity(i);
                } else {
                    reloadActivity();
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    public void reloadActivity() {
        Intent reloadIntent = getIntent();
        reloadIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        reloadIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(reloadIntent);
        finish();

    }


    private void checkIsMemberWebView() {
        userId = pref.getUserId();
        if (userId != null && !userId.equalsIgnoreCase("")) {
            /*isMember = true;
            pref.putIsUserMember(true);*/
            new GetMemberAsyncTaskForWebView().execute(null, null, null);
        } else {
            getUser();
            if (userId != null) {
                new GetMemberAsyncTaskForWebView().execute(null, null, null);
            } else {
                pref.putIsUserMember(false);
                //gotowebview
               /* isToWebView=false;
                Intent i = new Intent(this, WebPageActivity.class);
                startActivity(i);*/
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();
            pref.putUserId(userId);

            if (isValidString(userId)) {
                isGuestUser = userModel.getGuest();
                isLoggedIn = !isGuestUser;
            } else {
                isLoggedIn = false;
            }
        }
    }

    public void getFinalSelectedServiceTypesList() {
        mFinalSelectedServiceTypes = pref.getCartData();

        if (mFinalSelectedServiceTypes == null) {
            mFinalSelectedServiceTypes = new ArrayList<>();
        }
        cartQuantityTextView.setText(mFinalSelectedServiceTypes.size() + "");
    }

    private void fetchAreasForMinAmount() {
        mRestWebServices = new RestWebServices();
        new GetAllAreasAsyncTask().execute(null, null, null);
    }

    private void getOfferCoupon() {
        try {
            String json = pref.getCouponModel();
            CouponModel tempCouponModel = gson.fromJson(json, new TypeToken<CouponModel>() {
            }.getType());

            if (tempCouponModel != null) {
                couponModel = tempCouponModel;

                String couponId = couponModel.getCouponId();
                String couponCode = couponModel.getCouponCode();
                String couponAmount = couponModel.getCouponAmount();

                if (!isValidString(couponId) && isValidString(couponCode) && !isValidString(couponAmount)) {
                    showCouponCode(couponCode);
                } else {
                    showCouponDiscount(couponAmount);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void showCouponDiscount(String couponAmount) {
        couponDiscountBarLayout.setVisibility(View.VISIBLE);
        couponLabelTextView.setText(getResources().getString(R.string.coupon_discount_text));
        couponDiscountTextView.setText(couponAmount);
    }

    private void showCouponCode(String couponCode) {
        couponDiscountBarLayout.setVisibility(View.VISIBLE);
        couponLabelTextView.setText(getResources().getString(R.string.coupon_code_text));
        couponDiscountTextView.setText(String.valueOf(couponCode));
    }

    private void calculateFinalBillAmount() {
        double total = 0;
        String couponAmount = couponModel.getCouponAmount();
        Integer discount = 0;

        for (ServiceTypeModel stm : mFinalSelectedServiceTypes) {
            try {
                int price = 0;
                if (pref.getIsUserMember()) {
                    price = Integer.parseInt(stm.getMembersPrice());
                    discount = discount + (Integer.parseInt(stm.getFinalAmount()) - Integer.parseInt(stm.getMembersPrice()));
                } else {
                    price = Integer.parseInt(stm.getFinalAmount());
                    discount = discount + (Integer.parseInt(stm.getFinalAmount()) - Integer.parseInt(stm.getMembersPrice()));
                }
                int numOfPeople = Integer.parseInt(stm.getNumOfPeople());
                //if(!pref.getIsUserMember()){
                discount = discount * numOfPeople;
                //}
                total += (price * numOfPeople);
            } catch (Exception ex) {

            }
        }

        if (!pref.getIsUserMember()) {
            percentage.setText(discount + "");

        } else {
            pref.putMemberDiscount(discount + "");
        }

        if (isValidString(couponAmount)) {
            total -= Double.parseDouble(couponAmount);
        }

        setFinalAmount(total);
    }

    private void setFinalAmount(double finalAmount) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");

        try {
            if (mFinalSelectedServiceTypes != null && !mFinalSelectedServiceTypes.isEmpty()) {
                finalAmountTextView.setText(String.valueOf(formatter.format(finalAmount)));
                finalBillAmount = String.valueOf(finalAmount);
            } else {
                finalAmountLayout.setVisibility(View.GONE);
                emptyCartLayout.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void populateBookingList() {
        calculateFinalBillAmount();

        bookingSummaryAdapter = new BookingSummaryAdapter(BookingSummaryActivity.this, mFinalSelectedServiceTypes);
        if (mFinalSelectedServiceTypes.size() > 0) {
            emptyCartLayout.setVisibility(View.GONE);

        } else {
            emptyCartLayout.setVisibility(View.VISIBLE);
        }

        //When our list is updated
        bookingSummaryAdapter.setOnListUpdatedListener(new BookingSummaryAdapter.OnListUpdatedListener() {
            @Override
            public void onListUpdated() {
                setSelectedServicesList();

                pref.putCouponModel("");

                couponDiscountBarLayout.setVisibility(View.GONE);

                calculateFinalBillAmount();

                refreshAdapter();
            }
        });

        serviceTypesListView.setAdapter(bookingSummaryAdapter);
    }

    private void refreshAdapter() {
        bookingSummaryAdapter.setServicesList(mFinalSelectedServiceTypes);
        bookingSummaryAdapter.notifyDataSetChanged();
    }

    //Set the selected services as final
    private void setSelectedServicesList() {
        getFinalSelectedServiceTypesList();

        if (mFinalSelectedServiceTypes == null || mFinalSelectedServiceTypes.size() == 0) {
            emptyCartLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setHeader() {
        //View headerView = View.inflate(getApplicationContext(), R.layout.common_header, null);

        //RelativeLayout headerLayout = (RelativeLayout) headerView.findViewById(R.id.header);

        VCButton headerMenuButton = findViewById(R.id.headerMenu);
        headerMenuButton.setVisibility(View.GONE);

        VCButton backButton = findViewById(R.id.headerArrow);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(this);

        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);

        cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);

        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText(R.string.cart_text);
    }

    private boolean checkMinAmountReached() {
        boolean isReached = true;

        VcDatabaseQuery lDBQuery = new VcDatabaseQuery();
        minAMount = lDBQuery.getMinAmountOfArea(pref.getAreaId());

        int amount = 0;

        if (minAMount.length() == 0) {
            minAMount = "0";
        }

        for (int i = 0; i < mFinalSelectedServiceTypes.size(); i++) {
            amount += Integer.valueOf(mFinalSelectedServiceTypes.get(i).getPrice()) * Integer.valueOf(mFinalSelectedServiceTypes.get(i).getNumOfPeople());
        }

        if (amount >= Integer.valueOf(minAMount)) {
            isReached = false;
        }

        return isReached;
    }

    private int[] calculateTotalTimeToServe() {
        int[] hourMinutes = {0, 0};

        int time = 0;

        for (int i = 0; i < mFinalSelectedServiceTypes.size(); i++) {
            time += Integer.valueOf(mFinalSelectedServiceTypes.get(i).getTime()) * Integer.valueOf(mFinalSelectedServiceTypes.get(i).getNumOfPeople());
        }

        int hour = time / 60;
        hourMinutes[0] = hour;

        int minutes = time % 60;
        hourMinutes[1] = minutes;

        return hourMinutes;
    }

    private boolean checkMaxServiceTime() {
        boolean isReached = false;
        int[] hourMinutes = calculateTotalTimeToServe();

        if (hourMinutes[0] > ApplicationSettings.MAX_SERVICE_TIME) {
            isReached = true;
        } else if (hourMinutes[0] == ApplicationSettings.MAX_SERVICE_TIME && hourMinutes[1] > 0) {
            isReached = true;
        }

        return isReached;
    }

    public void continueAction() {
        /*if(checkMinAmountReached()){
            pref.putIsConvenienceFee(true);
        }*/
        //getConvenienceFee();
        if (!pref.getIsUserMember()) {
            if (percentage.getText() != null && !percentage.getText().toString().equals("")) {
                pref.putMemberDiscount(percentage.getText().toString());
            }
        }
        if (isLoggedIn || isGuestUser) {
            gotoMultipleAddress();
        } else {
            gotoSignInPage();
        }
    }

    /*  -------------------------------------
        |
        |  GET CONVENIENCE FEE API CALL
        |
        -------------------------------------
    */

    private void getConvenienceFee() {
        ApiManager apiManager = new ApiManager(this, this);
        apiManager.doJsonParsing();
        apiManager.setClassTypeForJson(ConvienenceFeeResponse.class);

        HashMap<String, String> params = new HashMap<>();
        params.put(ApplicationSettings.PARAM_METHOD, ApplicationSettings.METHOD_GET_CONVIENENCE_FEES);
        params.put("apiData[cartAmount]", finalBillAmount);

        apiManager.getStringPostResponse(ApplicationSettings.KEY_CONVENIENCE_FEE, ApplicationSettings.APPLICATION_SERVICE_URL, params);
    }

    @Override
    public void positiveResponse(String TAG, String response) {
    }

    @Override
    public void positiveResponse(String TAG, final Object responseObj) {
        if (responseObj != null) {
            ResponseHandler.handleResponse(((ConvienenceFeeResponse) responseObj).getResponsedata(), new ResponseHandler.ResponseCode() {
                @Override
                public void preformCode() {
                    try {
                        ConvienenceFeeResponse response = (ConvienenceFeeResponse) responseObj;
                        ConvienenceFeeResponseMain convienenceFeeResponseMain = response.getResponsedata().getResult();
                        if (convienenceFeeResponseMain.getConvenienceFee() != null && !convienenceFeeResponseMain.getConvenienceFee().equals("")) {
                            if (Double.parseDouble(convienenceFeeResponseMain.getConvenienceFee()) > 0) {
                                pref.putIsConvenienceFee(true);
                                pref.putConvenienceFee(convienenceFeeResponseMain.getConvenienceFee());
                                minAMount = convienenceFeeResponseMain.getHubAreaDistanceAmount();
                                if (minAMount != null && !minAMount.equals("")) {
                                    Double min = Double.parseDouble(minAMount);
                                    if (min >= 0) {
                                        //min = min / 1.18;
                                        minAMount = String.valueOf(min.intValue());
                                    }
                                }

                            } else {
                                pref.putIsConvenienceFee(false);
                            }
                        }
                    } catch (Exception ex) {
                        pref.putIsConvenienceFee(false);
                    }
                    if (pref.getIsConvenienceFee()) {
                        ConfirmDialogHelper.with(BookingSummaryActivity.this).confirm("Amount below min amount", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                continueAction();
                            }
                        }, String.format("Your order total is less than the minimum required ₹%s. A convenience fee of ₹%s will be charged as extra.",
                                String.valueOf(minAMount), pref.getConvenienceFee()));

                    } else if (checkMaxServiceTime()) {
                        Toast.makeText(BookingSummaryActivity.this, "The Maximum service time for this area is " +
                                String.valueOf(ApplicationSettings.MAX_SERVICE_TIME) + " Hours", Toast.LENGTH_SHORT).show();
                    } else {
                        continueAction();
                    }

                }
            });
        } else {
            pref.putIsConvenienceFee(false);
            if (pref.getIsConvenienceFee()) {
                ConfirmDialogHelper.with(BookingSummaryActivity.this).confirm("Amount below min amount", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        continueAction();
                    }
                }, String.format("Your order total is less than the minimum required ₹%s. A convenience fee of ₹%s will be charged as extra.",
                        String.valueOf(minAMount), pref.getConvenienceFee()));

            } else if (checkMaxServiceTime()) {
                Toast.makeText(BookingSummaryActivity.this, "The Maximum service time for this area is " +
                        String.valueOf(ApplicationSettings.MAX_SERVICE_TIME) + " Hours", Toast.LENGTH_SHORT).show();
            } else {
                continueAction();
            }
        }
    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {
        if (TAG.equalsIgnoreCase(ApplicationSettings.KEY_CONVENIENCE_FEE)) {
            pref.putIsConvenienceFee(false);
        }
        if (pref.getIsConvenienceFee()) {
            ConfirmDialogHelper.with(BookingSummaryActivity.this).confirm("Amount below min amount", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    continueAction();
                }
            }, String.format("Your order total is less than the minimum required ₹%s. A convenience fee of ₹%s will be charged as extra.",
                    String.valueOf(minAMount), pref.getConvenienceFee()));

        } else if (checkMaxServiceTime()) {
            Toast.makeText(BookingSummaryActivity.this, "The Maximum service time for this area is " +
                    String.valueOf(ApplicationSettings.MAX_SERVICE_TIME) + " Hours", Toast.LENGTH_SHORT).show();
        } else {
            continueAction();
        }
    }

    /*  -------------------------------------
        |
        |  GET ALL AREAS ASYNC TASK
        |
        -------------------------------------
    */

    private class GetAllAreasAsyncTask extends AsyncTask<Void, Void, Bundle> {
        @Override
        protected Bundle doInBackground(Void... params) {
            mRestWebServices.getAreaDataFromServer();
            return null;
        }

        @Override
        protected void onPostExecute(Bundle result) {
            super.onPostExecute(result);
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    private void gotoMultipleAddress() {
        Intent intent = new Intent(BookingSummaryActivity.this, MultipleAddressActivity.class);
        MultipleAddressActivity.flagOfEmptyAddress = true;
        startActivity(intent);
    }

    private void gotoSignInPage() {
        Intent intent = new Intent(this, SignUpActivity.class);
        intent.putExtra("booking_login", true);
        startActivityForResult(intent, RC_BOOKING_LOGIN);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_BOOKING_LOGIN) {
            //If we've successfully logged in
            if (resultCode == Activity.RESULT_OK) {
                checkout.performClick();
            }
        } else if (requestCode == RC_MEMBER_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                //reloadActivity(false);
                getUser();
                isToWebView = true;
                checkIsMemberWebView();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.headerArrow:
                onBackPressed();
                break;

            case R.id.checkout_layout:
                getUser();
                boolean isConvFees = pref.getIsConvenienceFee();

                if (mFinalSelectedServiceTypes.size() == 0 || mFinalSelectedServiceTypes == null) {
                    Toast.makeText(BookingSummaryActivity.this, "Please select at least one service to checkout!", Toast.LENGTH_SHORT).show();
                } else {
                    getConvenienceFee();
                    /*if (isConvFees) {
                        ConfirmDialogHelper.with(BookingSummaryActivity.this).confirm("Amount below min amount", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                continueAction();
                            }
                        }, String.format("Your order total is less than the minimum required ₹%s. A convenience fee of ₹%s will be charged as extra.",
                                String.valueOf(minAMount), pref.getConvenienceFee()));

                    } else if (checkMaxServiceTime()) {
                        Toast.makeText(BookingSummaryActivity.this, "The Maximum service time for this area is " +
                                String.valueOf(ApplicationSettings.MAX_SERVICE_TIME) + " Hours", Toast.LENGTH_SHORT).show();
                    } else {
                        continueAction();
                    }*/
                }

                break;

            case R.id.add_more_services_layout:
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startTime = Calendar.getInstance();
        populateBookingList();
        if (!isToWebView && !pref.getIsUserMember()) {
            checkIsMember();
        }
    }

    private void checkIsMember() {
        if (userId != null && !userId.equalsIgnoreCase("")) {
            /*isMember = true;
            pref.putIsUserMember(true);*/
            new GetMemberAsyncTask().execute(null, null, null);
        } else {
            getUser();
            if (userId != null) {
                /*isMember = true;
                pref.putIsUserMember(true);*/
                new GetMemberAsyncTask().execute(null, null, null);

            } else {
                pref.putIsUserMember(false);
                populateBookingList();
            }
        }

    }

    private class GetMemberAsyncTask extends AsyncTask<Void, Void, Boolean> {
        boolean isSubmitted = false;
        ProgressDialog progressDialog;


        GetMemberAsyncTask() {
            progressDialog = new ProgressDialog(BookingSummaryActivity.this);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            if (progressDialog != null && !isFinishing()) {
                progressDialog.setMessage("Processing...");
                progressDialog.show();
            }

        }

        protected Boolean doInBackground(Void... url) {
            restWebServices = new RestWebServices();
            try {
                isSubmitted = restWebServices.getIsUserMember(userId);
            } catch (Exception ex) {

            }
            return isSubmitted;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
                pref.putIsUserMember(result);
                //loadDashFrag();

                populateBookingList();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar endTime = Calendar.getInstance();
        new FireBaseHelper(this).logPageRuntime(startTime, endTime, ApplicationSettings.PAGE_CART);
    }

}
