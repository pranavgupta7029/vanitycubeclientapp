package com.vanitycube.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.fonts.VCEditText;
import com.vanitycube.model.LoginResponseModel;
import com.vanitycube.model.signinup.DataLoginResponse;
import com.vanitycube.model.signinup.LoginUserResponse;
import com.vanitycube.model.signinup.SendOtpToUserResponse;
import com.vanitycube.settings.ApplicationSettings;
import com.vanitycube.utilities.FireBaseHelper;
import com.vanitycube.webservices.ApiManager;
import com.vanitycube.webservices.RestWebServices;
import com.vanitycube.webservices.ServerResponseListener;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignInPageActivity extends AppCompatActivity implements OnClickListener, ServerResponseListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    public String TAG;
    private VCEditText mUserPhoneEditText, mUserPasswordEditText;
    private GoogleApiClient mGoogleApiClient;

    private static final int RC_GOOGLE_LOG_IN = 301;
    private static final int RC_GUEST_LOG_IN = 302;
    private static final int RC_SIGN_UP = 303;
    private static final int RC_FACEBOOK_SIGN_UP = 304;
    private static final int RC_GOOGLE_SIGN_UP = 305;

    private boolean dashboardLogin, bookingLogin, memberLogin;
    private CallbackManager callbackManager;
    private RestWebServices mRestWebservices;

    private Gson gson;

    private UserModel userModel;
    private String userPhone;

    private SharedPref pref;
    private int RC_FORGOT_PASSWORD = 701;
    private LinearLayout phoneNumLen, otpLen, passwordLen, sendOtpLen, loginBtnLen;
    private TextView resendOtp, or;
    private EditText useOtp;
    private Button sendOTP;
    ProgressDialog progressDialog;
    private ApiManager apiManager;
    private static final String SEND_OTP_TO_USER = "sendOTPToUser";
    private static final String USER_LOGIN_OTP_PASSWORD = "userLogin";
    private static final String RESEND_OTP_TO_USER = "resendOTPToUser";
    private Boolean isUserNotExist;
    SmsVerifyCatcher smsVerifyCatcher;
    Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login_new_design);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.otf");

        TAG = getResources().getString(R.string.sign_in_page_activity_tag);

        userPhone = "";

        mRestWebservices = new RestWebServices();

        pref = new SharedPref(VcApplicationContext.getInstance());

        gson = new Gson();

        getUser();

        mUserPhoneEditText = findViewById(R.id.userPhoneEditText);
        mUserPasswordEditText = findViewById(R.id.userPasswordEditText);
        TextView mForgotPassImageView = findViewById(R.id.forgot_password_image);
        useOtp = findViewById(R.id.useOtp);
        sendOTP = findViewById(R.id.sendOTP);
        resendOtp = findViewById(R.id.resendOtp);
        or = findViewById(R.id.or);
        phoneNumLen = findViewById(R.id.phoneNumLen);
        otpLen = findViewById(R.id.otpLen);
        passwordLen = findViewById(R.id.passwordLen);
        sendOtpLen = findViewById(R.id.sendOtpLen);
        loginBtnLen = findViewById(R.id.loginBtnLen);
        progressDialog = new ProgressDialog(SignInPageActivity.this);

        isUserNotExist = getIntent().getBooleanExtra("sentOtp", false);
        userPhone = getIntent().getStringExtra("phone");

        if (isUserNotExist && userPhone != null && !userPhone.equals("")) {
            otpLen.setVisibility(View.VISIBLE);
            passwordLen.setVisibility(View.VISIBLE);
            sendOtpLen.setVisibility(View.GONE);
            loginBtnLen.setVisibility(View.VISIBLE);
            resendOtp.setVisibility(View.VISIBLE);
            or.setVisibility(View.VISIBLE);
            phoneNumLen.setVisibility(View.GONE);
            mUserPhoneEditText.setText(userPhone);
        }

        mLoginButton = findViewById(R.id.login_button);
        mLoginButton.setTypeface(tf);
        mLoginButton.setOnClickListener(this);

        sendOTP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUserPhoneEditText.getText() != null && !mUserPhoneEditText.getText().toString().equals("")) {
                    if (mUserPhoneEditText.getText().toString().length() < 10) {
                        Toast.makeText(SignInPageActivity.this, "Please enter valid phone number", Toast.LENGTH_SHORT);
                    } else {
                        sendOtpToUser(mUserPhoneEditText.getText().toString());
                    }
                } else {
                    Toast.makeText(SignInPageActivity.this, "Please enter phone number", Toast.LENGTH_SHORT);
                }
            }
        });

        resendOtp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mUserPhoneEditText.getText() != null && !mUserPhoneEditText.getText().toString().equals("")) {
                    if (mUserPhoneEditText.getText().toString().length() < 10) {
                        Toast.makeText(SignInPageActivity.this, "Please enter valid phone number", Toast.LENGTH_SHORT);
                    } else {
                        resendOtpToUser(mUserPhoneEditText.getText().toString());
                    }
                } else {
                    Toast.makeText(SignInPageActivity.this, "Please enter phone number", Toast.LENGTH_SHORT);
                }
            }
        });

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {

                // b=sender.endsWith("WNRCRP");  //Just to fetch otp sent from WNRCRP
                String otp = getOtp(message);

                //String code = parseCode(message);//Parse verification code
                //etCode.setText(code);//set code in edit text
                //then you can send verification code to server
                useOtp.setText(otp);
                mLoginButton.performClick();
            }
        });


        //Login button


        //Get intent params
        dashboardLogin = getIntent().getBooleanExtra("dashboard_login", false);
        bookingLogin = getIntent().getBooleanExtra("booking_login", false);
        memberLogin = getIntent().getBooleanExtra("membership_login", false);

        //Guest Login button
        LinearLayout mGuestLoginLayout = findViewById(R.id.guest_login_layout);
        TextView mGuestLoginButton = findViewById(R.id.guest_login_button);

        mGuestLoginButton.setTypeface(tf);
        mGuestLoginLayout.setOnClickListener(this);
        mGuestLoginButton.setOnClickListener(this);

        if (!bookingLogin) mGuestLoginLayout.setVisibility(View.GONE);

        //Signup button
        TextView mSignUpButton = findViewById(R.id.signup_button);
        mSignUpButton.setOnClickListener(this);

        mForgotPassImageView.setOnClickListener(this);

        //This is instantiated only once, keep it here only
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        //Facebook Sign In
        initializeFacebookSdk();
        TextView lFacebook = findViewById(R.id.SignInFacebook);
        lFacebook.setOnClickListener(this);

        //Google Sign In button
        SignInButton mGoogleSignInBtn = (SignInButton) findViewById(R.id.SignInGoogle);
        mGoogleSignInBtn.setOnClickListener(this);
        setGooglePlusButtonText(mGoogleSignInBtn);*/

    }

    private String getOtp(String msg) {
        Pattern pattern = Pattern.compile("(\\d{4})");

//   \d is for a digit
//   {} is the number of digits here 4.

        Matcher matcher = pattern.matcher(msg);
        String val = "";
        if (matcher.find()) {
            val = matcher.group(1);  // 4 digit number
        }
        return val;
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void positiveResponse(String TAG, Object responseObj) {
        if (Objects.equals(TAG, SEND_OTP_TO_USER) && responseObj instanceof SendOtpToUserResponse) {
            SendOtpToUserResponse sendOtpToUserResponse = (SendOtpToUserResponse) responseObj;

            if (sendOtpToUserResponse.getResponsedata().getSuccess() == 1) {
                otpLen.setVisibility(View.VISIBLE);
                passwordLen.setVisibility(View.VISIBLE);
                sendOtpLen.setVisibility(View.GONE);
                loginBtnLen.setVisibility(View.VISIBLE);
                resendOtp.setVisibility(View.VISIBLE);
                or.setVisibility(View.VISIBLE);
                phoneNumLen.setVisibility(View.GONE);
            } else {
                Toast.makeText(SignInPageActivity.this, "User does not exist! please signup.", Toast.LENGTH_SHORT).show();
                Intent signup_activity = new Intent(SignInPageActivity.this, SignUpActivity.class);
                if (dashboardLogin) signup_activity.putExtra("dashboard_login", true);
                else if (bookingLogin) signup_activity.putExtra("booking_login", true);
                else if (memberLogin) signup_activity.putExtra("membership_login", true);
                signup_activity.putExtra("sentOtp", true);
                signup_activity.putExtra("phone", mUserPhoneEditText.getText().toString());
                startActivity(signup_activity);
                finish();
            }

        } else if (Objects.equals(TAG, USER_LOGIN_OTP_PASSWORD) && responseObj instanceof LoginUserResponse) {
            LoginUserResponse loginUserResponse = (LoginUserResponse) responseObj;

            if (loginUserResponse.getResponsedata().getMessageCode() == 1 && loginUserResponse.getResponsedata().getMsg().contains("Login successful")) {
                UserModel userModel = new UserModel();
                DataLoginResponse dataLoginResponse = loginUserResponse.getResponsedata().getData();
                userModel.setID(dataLoginResponse.getUser_id() + "");
                userModel.setGender(dataLoginResponse.getGender());
                userModel.setNumber(dataLoginResponse.getContact());
                userModel.setReferralName(dataLoginResponse.getReferral_name());
                userModel.setName(dataLoginResponse.getName());
                userModel.setEmail(dataLoginResponse.getEmail());
                userModel.setDOB(dataLoginResponse.getDob());
                userModel.setProfileImagePath(dataLoginResponse.getProfile_pic());
                userModel.setGuest(dataLoginResponse.getIs_guest_user()==0?false:true);
                //userId = userModel.getID();
                userModel.setFacebookId("");
                userModel.setGoogleId("");
                userModel.setNumberVerified(true);
                userModel.setGuest(false);

                pref.putUserModel(gson.toJson(userModel));
                pref.putGuestAddresses("");
                pref.putReloadRequired(true);

                Map<String, Object> afEventValue = new HashMap<>();
                afEventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, dataLoginResponse.getUser_id() + "");
                AppsFlyerLib.trackEvent(VcApplicationContext.getInstance(), AFInAppEventType.LOGIN, afEventValue);
                new FireBaseHelper(SignInPageActivity.this).logLogin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

                goBack(true);
            }
        } else if (Objects.equals(TAG, RESEND_OTP_TO_USER) && responseObj instanceof SendOtpToUserResponse) {
            SendOtpToUserResponse sendOtpToUserResponse = (SendOtpToUserResponse) responseObj;
            //if (sendOtpToUserResponse.getResponsedata().getSuccess() == 1) {

            Toast.makeText(SignInPageActivity.this, sendOtpToUserResponse.getResponsedata().getMsg(), Toast.LENGTH_SHORT).show();
                    /*otpLen.setVisibility(View.VISIBLE);
                    passwordLen.setVisibility(View.VISIBLE);
                    sendOtpLen.setVisibility(View.GONE);
                    loginBtnLen.setVisibility(View.VISIBLE);
                    resendOtp.setVisibility(View.VISIBLE);
                    or.setVisibility(View.VISIBLE);
                    phoneNumLen.setVisibility(View.GONE);*/
            // }
        } else {
            Toast.makeText(SignInPageActivity.this, "Something went wrong! Please try again...", Toast.LENGTH_SHORT).show();
        }


        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void positiveResponse(String TAG, String response) {
        String popints = "";

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

    }

    @Override
    public void negativeResponse(String TAG, String errorResponse) {

        if (SignInPageActivity.this.isDestroyed() || SignInPageActivity.this.isFinishing()) { // or call isFinishing() if min sdk version < 17
            return;
        } else {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    private void sendOtpToUser(String pContact) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(SignInPageActivity.this, SignInPageActivity.this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SendOtpToUserResponse.class);

        HashMap<String, String> params = new HashMap<>();
        //params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SENDOTP_TO_USER);

        params.put("otp", pContact);

        apiManager.getStringPostResponse(SEND_OTP_TO_USER, ApplicationSettings.BASE_URL_PAY + RestWebServices.METHOD_SENDOTP_TO_USER, params);
    }

    private void resendOtpToUser(String pContact) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(SignInPageActivity.this, SignInPageActivity.this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(SendOtpToUserResponse.class);

        HashMap<String, String> params = new HashMap<>();
        //params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SENDOTP_TO_USER);

        params.put("otp", pContact);

        apiManager.getStringPostResponse(RESEND_OTP_TO_USER, ApplicationSettings.BASE_URL_PAY + RestWebServices.METHOD_RESENDOTP_TO_USER, params);
    }

    private void userLogin(String otp, String password, String contact) {
        progressDialog.setMessage("Processing...");
        progressDialog.show();

        apiManager = new ApiManager(SignInPageActivity.this, SignInPageActivity.this);

        apiManager.doJsonParsing(true);
        apiManager.setClassTypeForJson(LoginUserResponse.class);

        HashMap<String, String> params = new HashMap<>();
        //params.put(ApplicationSettings.PARAM_METHOD, RestWebServices.METHOD_SENDOTP_TO_USER);
        boolean flag = false;
        if (otp != null) {
            params.put("otp", otp);
            flag = true;
        } else {
            params.put("otp", "");
        }

        if (!flag && password != null) {
            params.put("password", password);
        } else {
            params.put("password", "");
        }
        params.put("contact", contact);


        apiManager.getStringPostResponse(USER_LOGIN_OTP_PASSWORD, ApplicationSettings.BASE_URL_PAY + RestWebServices.METHOD_USER_LOGIN, params);
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    public void getUser() {
        String json = pref.getUserModel();
        userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel == null) {
            userModel = new UserModel();
        } else {
            String userId = userModel.getID();

            if (isValidString(userId)) {
                userPhone = userModel.getNumber();
            }
        }
    }

    /*  -------------------------------------
        |
        |   FACEBOOK SIGN IN
        |
        -------------------------------------
    */

    public void initializeFacebookSdk() {
        FacebookSdk.sdkInitialize(VcApplicationContext.getInstance());
        callbackManager = CallbackManager.Factory.create();
    }


    public void doFacebookLogin() {
        try {
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));

            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                @Override
                public void onSuccess(LoginResult loginResult) {
                    Bundle b = new Bundle();
                    b.putString("fields", "id,first_name,email,gender,last_name,birthday");

                    new GraphRequest(
                            AccessToken.getCurrentAccessToken(), "/me", b, HttpMethod.GET,
                            new GraphRequest.Callback() {
                                public void onCompleted(GraphResponse response) {
                                    JSONObject responseJson = response.getJSONObject();

                                    if (responseJson != null) {
                                        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                                            String email = responseJson.optString("email");
                                            new LoginAsyncTask(email, "", true, responseJson).execute(null, null, null);
                                        } else {
                                            Toast.makeText(SignInPageActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                    ).executeAsync();
                }

                @Override
                public void onCancel() {
                    Log.e(TAG, "Login attempt canceled.");
                    Log.i(TAG, "onCancel: onCompleted");
                }

                @Override
                public void onError(FacebookException e) {
                    Log.i(TAG, "onError: onCompleted");
                    Log.e(TAG, "Login failed :: Reason :: " + e.getMessage());
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*  -------------------------------------
        |
        |   GOOGLE SIGN IN
        |
        -------------------------------------
    */

    public void doGoogleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        this.startActivityForResult(signInIntent, RC_GOOGLE_LOG_IN);
    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended:");
    }

    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName;

            String givenName;
            if (acct != null) {
                givenName = acct.getGivenName();
                personName = acct.getDisplayName();

                String email = acct.getEmail();
                String personID = acct.getId();

                if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                    new LoginAsyncTask(personName, givenName, email, "", personID, true).execute(null, null, null);
                } else {
                    Toast.makeText(SignInPageActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not be available.
        GoogleApiAvailability mGoogleApiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = mGoogleApiAvailability.getErrorDialog(this, connectionResult.getErrorCode(), 1);
        dialog.show();
    }

    private boolean emailValidator(String pEmail) {
        String regex = "^[+]?[0-9]{10,13}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pEmail);
        return matcher.matches();
    }

    private void loginClick() {
        String mobileNumberString = mUserPhoneEditText.getText().toString().trim();
        String passwordString = null;
        String otp = null;
        if (mUserPasswordEditText.getText() != null && !mUserPasswordEditText.getText().toString().equals(""))
            passwordString = mUserPasswordEditText.getText().toString().trim();
        if (useOtp.getText() != null && !useOtp.getText().toString().equals(""))
            otp = useOtp.getText().toString().trim();


        if (!isValidString(passwordString) && !isValidString(otp)) {
            Toast.makeText(SignInPageActivity.this, "Please enter valid data", Toast.LENGTH_SHORT).show();
        } else if (passwordString != null || otp != null) {
            userLogin(otp, passwordString, mobileNumberString);
        }

        /*if (!isValidString(mobileNumberString) || !isValidString(passwordString)) {
            Toast.makeText(SignInPageActivity.this, "Please enter valid data", Toast.LENGTH_SHORT).show();
        } else {
            if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                new LoginAsyncTask(mobileNumberString, passwordString, false, null).execute(null, null, null);
            } else {
                Toast.makeText(SignInPageActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }*/

    }

    private void setGooglePlusButtonText(SignInButton signInButton) {
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (Button) v;
                tv.setTextSize(15);
                tv.setTypeface(null, Typeface.NORMAL);
                tv.setText(R.string.login_with_google_text);
                return;
            }
        }
    }

    /*  -------------------------------------
        |
        |   LOGIN ASYNC TASK
        |
        -------------------------------------
    */

    private class LoginAsyncTask extends AsyncTask<Void, Void, LoginResponseModel> {
        private ProgressDialog progressDialog;
        private String userId, userName, password, userEmail, personName, personId, givenName, responseMessage, googleId, facebookId;
        private boolean isLogin, isGoogle, isFacebook;
        private JSONObject responseJson;

        LoginAsyncTask(String pUserName, String pPassword, boolean isFacebook, JSONObject responseJson) {
            this.userName = pUserName;
            this.password = pPassword;
            this.isFacebook = isFacebook;
            this.responseJson = responseJson;

            if (this.responseJson != null) {
                facebookId = this.responseJson.optString("id");
            } else {
                facebookId = "";
            }

            progressDialog = new ProgressDialog(SignInPageActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        LoginAsyncTask(String personName, String givenName, String email, String password, String personId, boolean isGoogle) {
            this.personName = personName;
            this.givenName = givenName;
            this.userEmail = email;
            this.password = password;
            this.isGoogle = isGoogle;
            this.personId = personId;

            googleId = this.personId;

            progressDialog = new ProgressDialog(SignInPageActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Signing In");
            progressDialog.show();
        }

        @Override
        protected LoginResponseModel doInBackground(Void... url) {
            if (isGoogle) {
                return mRestWebservices.loginGoogle(this.userEmail, password);
            } else {
                return mRestWebservices.login(userName, password, isFacebook);
            }
        }

        @Override
        protected void onPostExecute(LoginResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                String messageCode = result.getMessageCode();
                responseMessage = result.getMessage();
                JSONObject data = result.getData();

                if (messageCode.equals("1") && data != null && data.length() != 0) {
                    UserModel userModel = new UserModel(data);

                    userId = userModel.getID();
                    userModel.setFacebookId(facebookId);
                    userModel.setGoogleId(googleId);
                    userModel.setNumberVerified(true);

                    pref.putUserModel(gson.toJson(userModel));
                    pref.putGuestAddresses("");
                    pref.putReloadRequired(true);

                    Map<String, Object> afEventValue = new HashMap<>();
                    afEventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
                    AppsFlyerLib.trackEvent(VcApplicationContext.getInstance(), AFInAppEventType.LOGIN, afEventValue);
                    new FireBaseHelper(SignInPageActivity.this).logLogin(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

                    goBack(true);
                } else {
                    if (isFacebook) {
                        Intent signUpFacebook = new Intent(SignInPageActivity.this, SignUpFacebookActivity.class);
                        signUpFacebook.putExtra("facebook_id", facebookId);
                        signUpFacebook.putExtra("first_name", responseJson.optString("first_name"));
                        signUpFacebook.putExtra("email", responseJson.optString("email"));
                        signUpFacebook.putExtra("gender", responseJson.optString("gender"));
                        signUpFacebook.putExtra("last_name", responseJson.optString("last_name"));
                        signUpFacebook.putExtra("birthday", responseJson.optString("birthday"));
                        startActivityForResult(signUpFacebook, RC_FACEBOOK_SIGN_UP);

                    } else if (isGoogle) {
                        Intent signUpGoogle = new Intent(SignInPageActivity.this, SignUpGoogleActivity.class);
                        signUpGoogle.putExtra("google_id", googleId);
                        signUpGoogle.putExtra("personName", this.personName);
                        signUpGoogle.putExtra("email", this.userEmail);
                        startActivityForResult(signUpGoogle, RC_GOOGLE_SIGN_UP);
                    } else {
                        if (isValidString(responseMessage)) {
                            Toast.makeText(SignInPageActivity.this, responseMessage, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(SignInPageActivity.this, "Login failed, please try again", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void gotoGuestLogin() {
        Intent intNext = new Intent(this, GuestLoginActivity.class);
        startActivityForResult(intNext, RC_GUEST_LOG_IN);
    }

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            if (!dashboardLogin && !memberLogin) {
                Intent intent = new Intent(SignInPageActivity.this, MultipleAddressActivity.class);
                MultipleAddressActivity.flagOfEmptyAddress = true;
                startActivity(intent);
                finish();
            } else {
                setResult(RESULT_OK, intBack);
            }

            //
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                loginClick();
                break;

            case R.id.signup_button:
                Intent signup_activity = new Intent(SignInPageActivity.this, SignUpActivity.class);
                if (dashboardLogin) signup_activity.putExtra("dashboard_login", true);
                else if (bookingLogin) signup_activity.putExtra("booking_login", true);
                else if (memberLogin) signup_activity.putExtra("membership_login", true);
                startActivity(signup_activity);
                break;

            case R.id.SignInFacebook:
                doFacebookLogin();
                break;

            case R.id.SignInGoogle:
                doGoogleSignIn();
                break;

            case R.id.forgot_password_image:
                Intent forgotPassIntent = new Intent(SignInPageActivity.this, ForgotPasswordActivity.class);
                forgotPassIntent.putExtra("dashboard_login", dashboardLogin);
                startActivityForResult(forgotPassIntent, RC_FORGOT_PASSWORD);
                break;

            case R.id.guest_login_button:
            case R.id.guest_login_layout:
                gotoGuestLogin();
                break;

            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_GOOGLE_LOG_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        if (!dashboardLogin && data != null) {
            dashboardLogin = data.getBooleanExtra("dashboard_login", false);
        }

        if (!memberLogin && data != null) {
            memberLogin = data.getBooleanExtra("membership_login", false);
        }

        if (requestCode == RC_GUEST_LOG_IN || requestCode == RC_SIGN_UP || requestCode == RC_FACEBOOK_SIGN_UP || requestCode == RC_GOOGLE_SIGN_UP
                || requestCode == RC_FORGOT_PASSWORD) {

            if (resultCode == RESULT_OK) {
                if (pref.getIsLogin())
                    goBack(true);
            } else if (resultCode == RESULT_CANCELED) {
                userPhone = pref.getGuestUserPrefillPhone();
                mUserPhoneEditText.setText(userPhone);
            }

        }

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null) mGoogleApiClient.disconnect();
    }

}