package com.vanitycube.activities;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.vanitycube.R;

public class ReferralCustomDialogClass extends Dialog implements android.view.View.OnClickListener {
    public Activity c;
    public Dialog d;
    public TextView mSubmit;
    public TextView mSkip;

    public ReferralCustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.referral_dialog_view);
        mSubmit = (TextView) findViewById(R.id.submit);
        mSubmit.setOnClickListener(this);
        mSkip = (TextView) findViewById(R.id.skip);
        mSkip.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.submit:
                // signUp();
                break;
            case R.id.skip:
                c.finish();
                break;
            default:
                break;
        }
        dismiss();
    }
}
