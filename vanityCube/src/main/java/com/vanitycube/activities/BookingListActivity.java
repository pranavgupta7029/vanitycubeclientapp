
package com.vanitycube.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.adapter.BookingListAdapter;
import com.vanitycube.model.BookingListModel;

import java.util.ArrayList;

public class BookingListActivity extends Activity implements OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);
        setHeader();
        ListView activitiesList = findViewById(R.id.bookingHistoryListView);
        ArrayList<BookingListModel> activities = new ArrayList<>();
        BookingListAdapter notifyAdapter = new BookingListAdapter(BookingListActivity.this, activities, false);
        activitiesList.setAdapter(notifyAdapter);
    }

    private void setHeader() {
        ImageView mImageView = findViewById(R.id.headerImage);
        mImageView.setVisibility(View.GONE);
        TextView mHeaderText = findViewById(R.id.headerText);
        mHeaderText.setText("History Booking");
    }

    @Override
    public void onClick(DialogInterface arg0, int arg1) {
    }
}