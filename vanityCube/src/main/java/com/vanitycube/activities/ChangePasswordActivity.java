package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.dbmodel.UserModel;
import com.vanitycube.webservices.RestWebServices;

public class ChangePasswordActivity extends Activity implements OnClickListener {
    private String TAG;
    private EditText oldPasswordEditText, newPasswordEditText, mConfirmEditText;
    private RestWebServices mRestWebServices;
    private SharedPref pref;
    private String userId;
    private boolean changePassword = false;
    private boolean dashboardLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_change_password_new_design);

        TAG = getResources().getString(R.string.change_password_activity_tag);
        mRestWebServices = new RestWebServices();
        pref = new SharedPref(VcApplicationContext.getInstance());

        //Get user
        userId = getIntent().getStringExtra("user_id");
        changePassword = getIntent().getBooleanExtra("changePassword", false);
        dashboardLogin = getIntent().getBooleanExtra("dashboard_login", false);
        if (!isValidString(userId)) {
            getUser();
        }

        //Initialize
        init();
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private void getUser() {
        String json = pref.getUserModel();
        Gson gson = new Gson();
        UserModel userModel = gson.fromJson(json, new TypeToken<UserModel>() {
        }.getType());

        if (userModel != null) {
            userId = userModel.getID();
        }
    }

    private void init() {
        oldPasswordEditText = findViewById(R.id.change_pass_password);
        if (changePassword) {
            oldPasswordEditText.setHint("Current Password");
        }
        newPasswordEditText = findViewById(R.id.change_pass_new);
        mConfirmEditText = findViewById(R.id.change_pass_confirm);
        Button changeButton = findViewById(R.id.change_pass_button);
        changeButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.footerButton:
                break;

            case R.id.change_pass_button:
                String oldPass = oldPasswordEditText.getText().toString();
                String newPass = newPasswordEditText.getText().toString();
                String confirmPass = mConfirmEditText.getText().toString();

                if (!isValidString(oldPass) || !isValidString(newPass) || !isValidString(confirmPass)) {
                    Toast.makeText(ChangePasswordActivity.this, "Please enter valid passwords", Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    if (newPass.equalsIgnoreCase(confirmPass)) {
                        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
                            new ChangePasswordAsyncTask(oldPass, newPass).execute(null, null, null);
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
                    }
                }

                break;

            default:
                break;
        }
    }

    private class ChangePasswordAsyncTask extends AsyncTask<Void, Void, Boolean> {
        private ProgressDialog progressDialog;
        private boolean isChanged = false;
        private String oldPassword, newPassword;

        ChangePasswordAsyncTask(String pOld, String pNew) {
            progressDialog = new ProgressDialog(ChangePasswordActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            this.oldPassword = pOld;
            this.newPassword = pNew;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            isChanged = mRestWebServices.changePassword(userId, oldPassword, newPassword);
            return isChanged;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                if (result) {
                    Toast.makeText(VcApplicationContext.getInstance(), "Your password has been changed. Please login with the new Password", Toast.LENGTH_SHORT).show();
                    goBack(true);
                } else {
                    Toast.makeText(VcApplicationContext.getInstance(), "Failed to change password. Please check the entered password.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            intBack.putExtra("dashboard_login", dashboardLogin);
            setResult(RESULT_OK, intBack);

            finish();
        } else {
            intBack.putExtra("dashboard_login", dashboardLogin);
            setResult(RESULT_CANCELED, intBack);

            finish();
        }
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack(false);
    }
}
