package com.vanitycube.activities;

// Created by heavenly spirits, corrected by prade on 04/11/2017

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;

import com.vanitycube.R;

@SuppressWarnings("deprecation")
public class HelpActiviity extends TabActivity implements OnClickListener {
    private TabHost tabHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_help);
        setHeader();

        tabHost = findViewById(android.R.id.tabhost);
        TabSpec tab1 = tabHost.newTabSpec("First Tab");

        tab1.setIndicator("Help");
        tab1.setContent(new Intent(this, HelpTab.class));

        tabHost.addTab(tab1);

        TextView tv1 = tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        tv1.setTextColor(Color.parseColor("#FF5A60"));

        tabHost.setOnTabChangedListener(new OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
                    TextView tv = tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                    tv.setTextColor(Color.parseColor("#FF5A60"));
                }

                TextView tv = tabHost.getCurrentTabView().findViewById(android.R.id.title);
                tv.setTextColor(Color.parseColor("#ffffff"));
            }
        });

        TabWidget widget = tabHost.getTabWidget();

        for (int i = 0; i < widget.getChildCount(); i++) {
            View v = widget.getChildAt(i);
            TextView tv = v.findViewById(android.R.id.title);
            if (tv == null) {
                continue;
            }
            v.setBackgroundResource(R.drawable.tabhost_style);
        }

        Intent goToNextActivity = getIntent();
        String txtData = goToNextActivity.getExtras().getString("terms");

        if (txtData != null && txtData.equals("t")) {
            tabHost.setCurrentTab(1);
        }

    }

    private void setHeader() {
        Button mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);
        TextView headerTextView = findViewById(R.id.headerText);
        headerTextView.setText(getResources().getString(R.string.help_activity_tag));
        ImageView imageView = findViewById(R.id.headerImage);
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.headerMenu:
                finish();
                break;

            default:
                break;
        }
    }

}