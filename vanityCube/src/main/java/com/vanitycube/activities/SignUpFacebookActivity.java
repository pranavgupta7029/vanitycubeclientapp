package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.vanitycube.R;
import com.vanitycube.constants.AppStatus;
import com.vanitycube.constants.VcApplicationContext;
import com.vanitycube.model.AddNewUserResponseModel;
import com.vanitycube.webservices.RestWebServices;

import org.json.JSONException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUpFacebookActivity extends Activity implements OnClickListener {
    private String TAG = "SignUpFacebook";
    private EditText mContactEditText, mReferralEditText;
    private RestWebServices restWebServices;
    private boolean checked;
    private ImageView termsCheck;
    private String firstName, lastName, email, facebookId;
    private RadioButton radioButtonMale;
    private int RC_OTP = 606;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_sign_up_facebook);
        checked = false;
        init();
    }

    private void init() {
        restWebServices = new RestWebServices();

        TextView mSignUpText = findViewById(R.id.facebookSignUpText);
        mContactEditText = findViewById(R.id.contact_signup_facebook);
        mReferralEditText = findViewById(R.id.referral_facebook);
        LinearLayout genderLayout = findViewById(R.id.genderLinearLayout);

        facebookId = getIntent().getStringExtra("facebook_id");
        firstName = getIntent().getStringExtra("first_name");
        lastName = getIntent().getStringExtra("last_name");
        email = getIntent().getStringExtra("email");

        String gender = getIntent().getStringExtra("gender");

        if (gender != null && gender.length() == 0) {
            genderLayout.setVisibility(View.GONE);
        }

        String hiText = "Hi ";
        mSignUpText.setText(hiText.concat(firstName).concat(", Please provide some additional details, in order for us to serve you better"));

        LinearLayout termsCheckLayout = findViewById(R.id.check_terms_conditions_layout);
        termsCheck = findViewById(R.id.check_terms_conditions);
        TextView mTerms = findViewById(R.id.terms_link);
        Button mSignUpButton = findViewById(R.id.signup_button_signup);
        TextView mLoginButton = findViewById(R.id.login_signup_textview);
        radioButtonMale = findViewById(R.id.radioMale);

        termsCheckLayout.setOnClickListener(this);
        termsCheck.setOnClickListener(this);
        mTerms.setOnClickListener(this);
        mSignUpButton.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
    }

    /*class imageDLAsyncTask extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progressDialog;
        URL mURL;
        InputStream in = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Getting Image from FB");
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(Void... url) {
            boolean isRegistered = false;
            try {
                in = (InputStream) mURL.getContent();
            } catch (IOException e) {
                Log.e(TAG, "<<Exception on SignUpFacebook>>" + e.getMessage());
            }
            if (in != null) {
                bitmap = BitmapFactory.decodeStream(in);
                isRegistered = true;
            }
            return isRegistered;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            {
                super.onPostExecute(result);
                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }

                    if (result) {
                        if (bitmap != null)
                            test.setImageBitmap(bitmap);
                    } else {
                        Toast.makeText(SignUpFacebookActivity.this, "Image download failed!", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "<<Exception on SignUpFacebook>>" + e.getMessage());
                }

            }
        }
    }*/

    class SignUpAsyncTask extends AsyncTask<Void, Void, AddNewUserResponseModel> {
        ProgressDialog progressDialog;
        String name, userEmail, password, mobile, referral, gender;
        String[] successMessageArray = {"0", "Unknown Reasons!"};

        SignUpAsyncTask(String pName, String pUserEmail, String pPassword, String mMobile, String mReferral) {
            this.name = pName;
            this.userEmail = pUserEmail;
            this.password = pPassword;
            this.mobile = mMobile;
            this.referral = mReferral;

            if (radioButtonMale.isChecked()) {
                gender = "1";
            } else {
                gender = "2";
            }

            progressDialog = new ProgressDialog(SignUpFacebookActivity.this);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Sign Up");
            progressDialog.show();
        }

        @Override
        protected AddNewUserResponseModel doInBackground(Void... url) {
            AddNewUserResponseModel addNewUserResponseModel = new AddNewUserResponseModel();

            try {
                addNewUserResponseModel = restWebServices.addNewUser(name, userEmail, "", mobile, referral, true, gender);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }

            return addNewUserResponseModel;
        }

        @Override
        protected void onPostExecute(AddNewUserResponseModel result) {
            super.onPostExecute(result);

            try {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }

                int success = result.getSuccess();
                String userId = result.getUserId();
                String responseMessage = result.getMessage();

                if (success == 1) {
                    Intent intent = new Intent(SignUpFacebookActivity.this, OtpActivity.class);
                    intent.putExtra("facebook_id", facebookId);
                    startActivityForResult(intent, RC_OTP);
                } else {
                    if (isValidString(responseMessage)) {
                        Toast.makeText(SignUpFacebookActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(SignUpFacebookActivity.this, "An error occurred, please try again", Toast.LENGTH_LONG).show();
                    }

                    goBack(false);
                }
            } catch (Exception e) {
                String TAG = "SignUpFacebookActivity";
                Log.e(TAG, "<<Exception on SignUpFacebook>>" + e.getMessage());
            }
        }
    }

    private boolean isValidString(String s) {
        return (s != null && !s.equals("") && !s.equals("null"));
    }

    private boolean phoneValidator(String pMobile) {
        String regexStr = "^[+]?[0-9]{10,13}$";
        Pattern pattern = Pattern.compile(regexStr);
        Matcher matcher2 = pattern.matcher(pMobile);
        return matcher2.matches();
    }

    private void signUp() {
        String mMobileString = mContactEditText.getText().toString().trim();
        String mReferralString = mReferralEditText.getText().toString().trim();

        if (!phoneValidator(mMobileString)) {
            Toast.makeText(SignUpFacebookActivity.this, "Please enter a valid mobile number.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!checked) {
            Toast.makeText(SignUpFacebookActivity.this, "Please agree to continue", Toast.LENGTH_LONG).show();
            return;
        }

        if (AppStatus.isOnline(VcApplicationContext.getInstance())) {
            new SignUpAsyncTask(firstName + " " + lastName, email, "", mMobileString, mReferralString).execute(null, null, null);
        } else {
            Toast.makeText(SignUpFacebookActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    /*  -------------------------------------
        |
        |  GO TO OTHER ACTIVITIES
        |
        -------------------------------------
    */

    public void goBack(boolean mode) {
        Intent intBack = new Intent();

        if (mode) {
            setResult(RESULT_OK, intBack);
            finish();
        } else {
            setResult(RESULT_CANCELED, intBack);
            finish();
        }
    }

    private void gotoTerms(int contentType) {
        Intent intent = new Intent(SignUpFacebookActivity.this, TermsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("content_type", contentType);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /*  -------------------------------------
        |
        |  DEFAULT BEHAVIOUR
        |
        -------------------------------------
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sign_up_facebook, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_OTP) {
            if (resultCode == RESULT_OK) {
                goBack(true);
            }
        }
    }

    @Override
    public void onBackPressed() {
        goBack(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_signup_textview:
                Intent loginActivity = new Intent(getApplicationContext(), SignInPageActivity.class);
                startActivity(loginActivity);
                break;

            case R.id.signup_button_signup:
                signUp();
                break;

            case R.id.check_terms_conditions_layout:
            case R.id.check_terms_conditions:
                checked = !checked;

                if (checked) {
                    termsCheck.setBackgroundResource(R.drawable.check_box_with);
                } else {
                    termsCheck.setBackgroundResource(R.drawable.check_box_without);
                }

                break;

            case R.id.terms_link:
                gotoTerms(0);
                break;

            default:
                break;
        }
    }

}
