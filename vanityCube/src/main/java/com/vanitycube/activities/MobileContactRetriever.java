
package com.vanitycube.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Organization;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.Photo;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Data;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vanitycube.R;
import com.vanitycube.adapter.MyFriendListAdapter;
import com.vanitycube.model.ContactsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class MobileContactRetriever extends Activity implements LoaderManager.LoaderCallbacks<Cursor>, OnClickListener {
    public final int GET_CONTACT_NAMES = 0;
    public ArrayList<ContactsModel> allContacts = new ArrayList<>();
    private ListView myfriendlist;
    private Button mMenuButton, mSettingsButton, mSearchButton, mBackButton;
    private TextView mHeaderText;
    private ProgressDialog progressDialog;
    private MyFriendListAdapter adapter;

    public static final String[] CONTACTS_SUMMARY_PROJECTION = new String[]{
            Contacts._ID, Contacts.DISPLAY_NAME, Contacts.CONTACT_STATUS,
            Contacts.CONTACT_PRESENCE, Contacts.LOOKUP_KEY};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mobilecontactretriever);
        myfriendlist = findViewById(R.id.myfriendlist);
        getLoaderManager().initLoader(GET_CONTACT_NAMES, null, this);
        progressDialog = new ProgressDialog(MobileContactRetriever.this);
        progressDialog.setMessage("Fetching contacts");
        progressDialog.show();
        setHeader();
        setFooter();
    }

    private void setHeader() {
        mMenuButton = findViewById(R.id.headerMenu);
        mMenuButton.setBackgroundResource(R.drawable.arrow_left);
        mMenuButton.setOnClickListener(this);
        ImageView mHeaderImage = findViewById(R.id.headerImage);
        mHeaderImage.setVisibility(View.GONE);
        mHeaderText = findViewById(R.id.headerText);
        mHeaderText.setText("Invite Contacts");
    }

    private void setFooter() {
        TextView lFooterButton = findViewById(R.id.footerButton);
        lFooterButton.setText(getResources().getString(R.string.send_invite_text));
        lFooterButton.setVisibility(View.GONE);
    }

    private void initTextWatcher() {
        EditText searchEdittext = findViewById(R.id.searchContactEditText);
        searchEdittext.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null) {
                    adapter.getFilter().filter(s.toString());
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> cursorLoader = null;
        switch (id) {
            case GET_CONTACT_NAMES:
                cursorLoader = getContactList();
                break;

            default:
                break;
        }

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {

            case GET_CONTACT_NAMES:
                try {
                    String displayName = null;

                    Long starttime, endTime;

                    starttime = (new Date()).getTime();

                    Log.i("TimeCalc", "Time started to read contacts - " + starttime);

                    while (data.moveToNext()) {
                        Long contactId = data.getLong(data.getColumnIndex(Contacts._ID));
                        String contactKey = data.getString(data.getColumnIndex(Contacts.LOOKUP_KEY));

                        Uri contactUri = Contacts.getLookupUri(contactId,
                                contactKey);

                        displayName = data.getString(data.getColumnIndex(Contacts.DISPLAY_NAME));

                        updateContactJSON(getApplicationContext(), contactUri, contactId);
                    }

                    int size = allContacts.size();

                    for (int i = 0; i < allContacts.size(); i++) {
                        String name = allContacts.get(i).getFname();
                        String profilePath = allContacts.get(i).getPhotoPath();
                        Log.i("name=", "" + name + profilePath);
                    }

                    adapter = new MyFriendListAdapter(MobileContactRetriever.this, allContacts);
                    myfriendlist.setAdapter(adapter);
                    initTextWatcher();

                    if (progressDialog != null) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }

                    endTime = (new Date()).getTime();
                    Log.i("TimeCalc", "Time after reading contacts - " + endTime);
                    Log.i("TimeCalc", "Time elapsed reading contacts - " + (endTime - starttime));

                } catch (Exception e) {
                    Log.i("contactJSON", "exception while creating JSON - " + e.getMessage() + "class - " + e.getClass());
                    e.printStackTrace();
                }

                break;

            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
    }

    private void updateContactJSON(Context context, Uri contactUri, Long contactId) {
        ContactsModel contacts = new ContactsModel();
        contactUri = Uri.withAppendedPath(contactUri,
                ContactsContract.Contacts.Entity.CONTENT_DIRECTORY);

        /*
         * Sets the columns to retrieve. RAW_CONTACT_ID is included to identify the raw contact associated with the data row.
         * DATA1 contains the first column in the data row (usually the most important one). DATA15 contains the photo blob
         * column in the data row. MIMETYPE indicates the type of data in the data row. Add more column per need basis
         */
        String[] projection = {
                ContactsContract.Contacts.Entity.RAW_CONTACT_ID,
                ContactsContract.Contacts.Entity.DATA1,
                ContactsContract.Contacts.Entity.VERSION,
                ContactsContract.Contacts.Entity.DATA2,
                ContactsContract.Contacts.Entity.DATA3,
                ContactsContract.Contacts.Entity.MIMETYPE,
                ContactsContract.Contacts.Entity.DATA15, // for photo blob
        };
        // where clause for Phone and Photo (if data15 is NOTNULL) MIME type
        String select = "(" + Data.MIMETYPE + " =?) OR" + "(" + Data.MIMETYPE
                + " =?) OR" + "((" + Data.MIMETYPE + " =?) AND (" + Data.DATA1
                + " NOTNULL)) OR" + "((" + Data.MIMETYPE + "=?) AND ("
                + Data.DATA15 + " NOTNULL)) OR" + "((" + Data.MIMETYPE
                + "=?) AND (" + Data.DATA1 + " NOTNULL))";

        /*
         * Sorts the retrieved cursor by raw contact id, to keep all data rows for a single raw contact collated together.
         */
        String sortOrder = ContactsContract.Contacts.Entity.RAW_CONTACT_ID + " ASC";

        /*
         * Returns a new CursorLoader. The arguments are similar to ContentResolver.query(), except for the Context argument,
         * which supplies the location of the ContentResolver to use.
         */
        Cursor allDetailsForContact = context.getContentResolver()
                .query(contactUri, projection, select,
                        new String[]{StructuredName.CONTENT_ITEM_TYPE,
                                Phone.CONTENT_ITEM_TYPE,
                                Email.CONTENT_ITEM_TYPE,
                                Photo.CONTENT_ITEM_TYPE,
                                Organization.CONTENT_ITEM_TYPE},
                        sortOrder);

        JSONObject aContactJSON = new JSONObject();
        JSONObject phoneNumbersJSON = new JSONObject();
        JSONObject mailIdsJSON = new JSONObject();

        String number, numberLabel, mailID, mailIdLabel;

        int numberType, mailIdType;

        try {
            aContactJSON.put("contactId", contactId);
            Uri uri = ContactsContract.RawContacts.CONTENT_URI;

            String[] projection2 = new String[]{
                    ContactsContract.RawContacts._ID,
                    ContactsContract.RawContacts.VERSION};

            String selection = ContactsContract.RawContacts._ID + " = '" + contactId + "'";

            Cursor cursor = getApplicationContext().getContentResolver().query(uri, projection2, selection, null, null);

            assert cursor != null;
            cursor.moveToFirst();

            cursor.close();

            assert allDetailsForContact != null;
            while (allDetailsForContact.moveToNext()) {

                if (allDetailsForContact
                        .getString(
                                allDetailsForContact
                                        .getColumnIndex(ContactsContract.Contacts.Entity.MIMETYPE))
                        .equals(ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)) {
                    String fname = allDetailsForContact
                            .getString(allDetailsForContact
                                    .getColumnIndex(ContactsContract.Contacts.Entity.DATA2));
                    Log.i("firstname==", " " + fname);
                    String lname = allDetailsForContact
                            .getString(allDetailsForContact
                                    .getColumnIndex(ContactsContract.Contacts.Entity.DATA3));

                    contacts.setFname(fname);
                    contacts.setlName(lname);

                } else if (allDetailsForContact
                        .getString(allDetailsForContact.getColumnIndex(ContactsContract.Contacts.Entity.MIMETYPE))
                        .equals(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)) {

                    number = allDetailsForContact.getString(allDetailsForContact.getColumnIndex(ContactsContract.Contacts.Entity.DATA1));
                    numberType = allDetailsForContact.getInt(allDetailsForContact.getColumnIndex(ContactsContract.Contacts.Entity.DATA2));
                    numberLabel = allDetailsForContact.getString(allDetailsForContact.getColumnIndex(ContactsContract.Contacts.Entity.DATA3));

                    contacts.setPhoneNumber(number);

                    if (Phone.TYPE_MAIN == numberType) {
                        aContactJSON.put("defaultNumber", number);
                    }
                } else if (allDetailsForContact
                        .getString(allDetailsForContact.getColumnIndex(ContactsContract.Contacts.Entity.MIMETYPE))
                        .equals(ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE)) {

                    byte[] photoByte = null;

                    String photoPath = null;

                    photoByte = allDetailsForContact.getBlob(allDetailsForContact.getColumnIndex("data15"));

                    if (photoByte != null) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(photoByte, 0, photoByte.length);

                        // Getting Caching directory
                        File cacheDirectory = getApplicationContext().getCacheDir();

                        // Temporary file to store the contact image
                        File tmpFile = new File(cacheDirectory.getPath() + "/wpta_" + System.currentTimeMillis() + ".png");

                        // The FileOutputStream to the temporary file
                        try {
                            FileOutputStream fOutStream = new FileOutputStream(
                                    tmpFile);

                            // Writing the bitmap to the temporary file as png
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                                    fOutStream);

                            // Flush the FileOutputStream
                            fOutStream.flush();

                            // Close the FileOutputStream
                            fOutStream.close();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        photoPath = tmpFile.getPath();
                        contacts.setPhotoPath(photoPath);
                    }

                }

            }

            allContacts.add(contacts);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Loader<Cursor> getContactList() {
        Uri baseUri = Contacts.CONTENT_URI;

        String select = "((" + Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + Contacts.DISPLAY_NAME + " != '' ))";
        return new CursorLoader(getApplicationContext(), baseUri,
                CONTACTS_SUMMARY_PROJECTION, select, null,
                Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.headerMenu:
                finish();
                break;

            case R.id.footerButton:

                for (int i = 0; i < MyFriendListAdapter.itemChecked.length; i++) {
                    boolean value = MyFriendListAdapter.itemChecked[i];

                    if (value) {
                        String phNumber = allContacts.get(i).getPhoneNumber();
                        phNumber = phNumber.replaceAll("[\\s\\-()]", "");

                        if (phNumber != null && phNumber.length() > 0) {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phNumber, null, "Hi Install Vanitycube, All the beauty services at home",
                                    null, null);

                            Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                            smsIntent.setType("vnd.android-dir/mms-sms");
                            smsIntent.putExtra("address", phNumber);
                            smsIntent.putExtra("sms_body", "Hi Install Vanitycube, All the beauty services at home");
                            startActivity(smsIntent);
                        }

                    }
                }
                break;

            default:
                break;
        }

        finish();
    }
}