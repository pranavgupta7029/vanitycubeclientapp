package com.vanitycube.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.vanitycube.R;
import com.vanitycube.constants.SharedPref;
import com.vanitycube.fonts.VCButton;


public class WebPageActivityBanner extends Activity {

    private WebView webView;
    private ProgressBar pocket;


    public ProgressDialog progressDialog;
    public FirebaseAnalytics mFirebaseAnalytics;
    private SharedPref pref;
    public View headerView;
    public String url = "https://www.vanitycube.in/v-club";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.webview_activity);
        setHeader();
        url = getIntent().getStringExtra("url");

       /* ActionBar mActionBar = getActionBar();

        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setDisplayShowTitleEnabled(true);
        mActionBar.setCustomView(headerView);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);

        View v = getActionBar().getCustomView();

        if(v!=null) {
            ViewGroup.LayoutParams lp = v.getLayoutParams();
            lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            v.setLayoutParams(lp);
        }*/


        //mActionBar.show();
        //actionBar.hide();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        /*if(!progressDialog.isShowing()){
            progressDialog.show();
        }*/
        //actionBar.setTitle("VLCC");
        //actionBar.set
        webView = (WebView) findViewById(R.id.webView);
        pocket = (ProgressBar) findViewById(R.id.pocket);
        webView.setWebChromeClient(new WebChromeClient() {
        });
        //webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.addJavascriptInterface(new JavaScriptInterface(this), "Android");


        //CookieManager.getInstance().setAcceptCookie(true);


        webView.getSettings().setAppCacheEnabled(false);
        // clearCookies();
        webView.clearCache(true);

        //pocket.setVisibility(View.VISIBLE);
        webView.setWebViewClient(new WebViewClient() {

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e("myError", description);
            }


            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }

                /*if (url != null && url.contains("membershipcomplete")) {
                    Toast.makeText(WebPageActivityBanner.this,"Thankyou for choosing exclusive VClub Membership! Please order and enjoy exciting benefits only for you. Incase you need any Assistance please call us on +91-80108 01091",Toast.LENGTH_SHORT).show();

                    finish();
                    //view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                } else if (url != null && url.contains("membershipfailed")) {
                    Toast.makeText(WebPageActivityBanner.this,"Thankyou for considering exclusive VClub Membership! If you are having trouble to complete Membership registration process, Please call us at +91-80108 01091 for further assistance.",Toast.LENGTH_SHORT).show();

                    finish();
                    //view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                } else if (url != null && url.equalsIgnoreCase("https://www.vanitycube.in/") || url.equalsIgnoreCase("http://staging.vanitycube.in/")) {
                    //Toast.makeText(WebPageActivity.this,"Thankyou for considering exclusive VClub Membership! If you are having trouble to complete Membership registration process, Please call us at +91-80108 01091 for further assistance.",Toast.LENGTH_SHORT).show();

                    finish();
                    //view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    return false;
                }else {*/
                //view.clearCache(true);
                view.loadUrl(url);
                return false;
                //}
            }
            //https://www.vanitycube.in/

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.e("onPageFinished", "url:" + url);
                super.onPageFinished(view, url);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        });

        String url = this.url;
        //String url = "http://13.233.10.216/";
        webView.loadUrl(url);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webView != null) {
            webView.onPause();
            //webView.pauseTimers();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webView != null) {
            webView.onResume();
            //webView.resumeTimers();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.destroy();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:

                this.finish();
                return true;
            default:
                break;
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webView.canGoBack()) {
                        webView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        webView.saveState(bundle);
    }


    private void clearCookies() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookie();
            CookieManager.getInstance().flush();
            return;
        }

        CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
        cookieSyncManager.startSync();
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        cookieManager.removeSessionCookie();
        cookieSyncManager.stopSync();
        cookieSyncManager.sync();
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public String getFromAndroid() {
            String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            return android_id;
        }
    }

    private void setHeader() {
        View.inflate(getApplicationContext(), R.layout.common_header, null);

        RelativeLayout headerLayout = findViewById(R.id.header);

        VCButton headerMenuButton = headerLayout.findViewById(R.id.headerMenu);
        headerMenuButton.setVisibility(View.GONE);

        VCButton backButton = headerLayout.findViewById(R.id.headerArrow);
        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView mHeaderText = findViewById(R.id.headerText);
        ImageView mHeaderImage = findViewById(R.id.headerImage);

        TextView cartQuantityTextView = (TextView) findViewById(R.id.cartQuantityTextView);
        cartQuantityTextView.setVisibility(View.GONE);
        Button cartButton = (Button) findViewById(R.id.cartButton);
        cartButton.setVisibility(View.GONE);

        mHeaderImage.setVisibility(View.GONE);
        mHeaderText.setText("VClub");
    }


}
