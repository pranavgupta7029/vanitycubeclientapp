package com.sanjeev.customcalendar;

import android.app.Application;

public class ApplicationContext extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		FontsOverride.setDefaultFont(this, "DEFAULT",
				"fonts/Montserrat-Regular.ttf");
		FontsOverride.setDefaultFont(this, "MONOSPACE",
				"fonts/Montserrat-Regular.ttf");
		FontsOverride
				.setDefaultFont(this, "SERIF", "fonts/Montserrat-Bold.ttf");
	}
}
